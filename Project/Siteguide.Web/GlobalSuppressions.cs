﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "3rd party code", Scope = "namespaceanddescendants", Target = "DiffMatchPatch")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "3rd party code", Scope = "namespaceanddescendants", Target = "DiffMatchPatch")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "3rd party code", Scope = "namespaceanddescendants", Target = "DiffMatchPatch")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "3rd party code", Scope = "namespaceanddescendants", Target = "DiffMatchPatch")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "3rd party code", Scope = "namespaceanddescendants", Target = "DiffMatchPatch")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "3rd party code", Scope = "namespaceanddescendants", Target = "DiffMatchPatch")]
