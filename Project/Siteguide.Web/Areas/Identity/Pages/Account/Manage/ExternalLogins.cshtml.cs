﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SiteGuide.Service;
using SiteGuide.Service.Helpers;
using SiteGuide.Web.Extensions;

namespace SiteGuide.Web.Areas.Identity.Pages.Account.Manage
{
    public class ExternalLoginsModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly ISafaExternalLogin _safaExternalLogin;
        private readonly ClaimsService _claimsService;

        public ExternalLoginsModel(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            ISafaExternalLogin safaExternalLogin,
            ClaimsService claimsService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _safaExternalLogin = safaExternalLogin;
            _claimsService = claimsService;
        }

        public IList<UserLoginInfo> CurrentLogins { get; set; }

        public IList<AuthenticationScheme> OtherLogins { get; set; }

        public bool ShowRemoveButton { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        public async Task<IActionResult> OnGetAsync(string returnUrl = null)
        {
            // Can't pass more than one url parameter to callback address for SAFA due to idiosyncratic SAFA code. Check if callback and redirect to handler if SAFA callback.
            if (returnUrl != null)
                return await OnGetLinkLoginCallbackAsync(returnUrl);

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            CurrentLogins = await _userManager.GetLoginsAsync(user);
            OtherLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync())
                .Where(auth => CurrentLogins.All(ul => auth.Name != ul.LoginProvider))
                .ToList();
            ShowRemoveButton = user.PasswordHash != null || CurrentLogins.Count > 1;
            return Page();
        }

        public async Task<IActionResult> OnPostRemoveLoginAsync(string loginProvider, string providerKey)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var result = await _userManager.RemoveLoginAsync(user, loginProvider, providerKey);
            if (!result.Succeeded)
            {
                var userId = await _userManager.GetUserIdAsync(user);
                throw new InvalidOperationException($"Unexpected error occurred removing external login for user with ID '{userId}'.");
            }

            await _claimsService.RemoveProviderClaimsAsync(user, loginProvider);

            await _signInManager.RefreshSignInAsync(user);
            StatusMessage = "The external login was removed.";
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostLinkLoginAsync(string provider)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            if (provider == "SAFA")
            {
                var redirectUrl = Url.Page("./ExternalLogins");
                return _safaExternalLogin.Challenge(Request, redirectUrl);
            }
            else
            {
                // Request a redirect to the external login provider to link a login for the current user
                var redirectUrl = Url.Page("./ExternalLogins", pageHandler: "LinkLoginCallback");
                var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl, _userManager.GetUserId(User));
                return new ChallengeResult(provider, properties);
            }
        }

        public async Task<IActionResult> OnGetLinkLoginCallbackAsync(string returnUrl = null)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            ExternalLoginInfo info;
            if (returnUrl != null)
            {
                // We're dealing with an SAFA login. Separate the jwt parameter that the SAFA appends:
                string jwtEncoded = _safaExternalLogin.UrlParamsExtractJwt(ref returnUrl);

                // Validate token
                info = _safaExternalLogin.GetLoginInfo(jwtEncoded);
            }
            else
            {
                info = await _signInManager.GetExternalLoginInfoAsync(await _userManager.GetUserIdAsync(user));
            }

            if (info == null)
            {
                throw new InvalidOperationException($"Unexpected error occurred loading external login info for user with ID '{user.Id}'.");
            }

            var result = await _userManager.AddLoginAsync(user, info);
            if (!result.Succeeded)
            {
                this.AddMessage(Models.MessageType.Warning, "Could not associate external login", string.Join("\r\n", result.Errors.Select(e => e.Description)));
                return RedirectToPage();
                //throw new InvalidOperationException($"Unexpected error occurred adding external login for user with ID '{user.Id}'. Result was: {result}");
            }

            await _claimsService.AddProviderClaimsAsync(user, info);

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            StatusMessage = "The external login was added.";
            return RedirectToPage();
        }
    }
}
