﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace SiteGuide.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegistrationEmailSent : PageModel
    {
        public string EmailAddress;

        public void OnGet(string emailAddress)
        {
            EmailAddress = emailAddress;
        }
    }
}
