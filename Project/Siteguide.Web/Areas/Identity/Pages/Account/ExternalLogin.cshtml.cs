﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SiteGuide.Service;
using SiteGuide.Web.Models;
using SiteGuide.Web.Extensions;
using Newtonsoft.Json;

namespace SiteGuide.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ExternalLoginModel : PageModel
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<ExternalLoginModel> _logger;
        private readonly ISafaExternalLogin _safaExternalLogin;
        private readonly NotificationService _notificationService;
        private readonly ClaimsService _claimsService;

        public ExternalLoginModel(
            SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            ILogger<ExternalLoginModel> logger,
            ISafaExternalLogin safaExternalLogin,
            NotificationService notificationService,
            ClaimsService claimsService)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
            _safaExternalLogin = safaExternalLogin;
            _notificationService = notificationService;
            _claimsService = claimsService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        [BindProperty]
        public string SafaJwt { get; set; }

        public string LoginProvider { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }
        }

        public async Task<IActionResult> OnGetAsync(string returnUrl = null)
        {
            if (returnUrl == null)
                return RedirectToPage("./Login");

            // We're dealing with a SAFA login. Separate the jwt parameter that SAFA appends:
            string jwtEncoded = _safaExternalLogin.UrlParamsExtractJwt(ref returnUrl);
            returnUrl = returnUrl ?? Url.Content("~/");

            // Validate token
            ExternalLoginInfo info = _safaExternalLogin.GetLoginInfo(jwtEncoded);
            return await ExternalSignIn(info, returnUrl, jwtEncoded);
        }

        public IActionResult OnPost(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            if (provider == "SAFA")
            {
                string callbackUrl = Url.Page("./ExternalLogin");
                return _safaExternalLogin.Challenge(Request, callbackUrl, returnUrl);
            }
            else
            {
                var redirectUrl = Url.Page("./ExternalLogin", pageHandler: "Callback", values: new { returnUrl });
                var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
                return new ChallengeResult(provider, properties);
            }
        }

        public async Task<IActionResult> OnGetCallbackAsync(string returnUrl = null, string remoteError = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (remoteError != null)
            {
                ErrorMessage = $"Error from external provider: {remoteError}";
                return RedirectToPage("./Login", new { ReturnUrl = returnUrl });
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            return await ExternalSignIn(info, returnUrl);
        }

        private async Task<IActionResult> ExternalSignIn(ExternalLoginInfo info, string returnUrl, string safaJwt = null)
        {
            if (info == null)
            {
                ErrorMessage = "Error loading external login information.";
                return RedirectToPage("./Login", new { ReturnUrl = returnUrl });
            }

            // Sign in the user with this external login provider if the user already has a login.
            var signInResult = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
            if (signInResult.Succeeded)
            {
                _logger.LogInformation("{Name} logged in with {LoginProvider} provider.", info.Principal.Identity.Name, info.LoginProvider);
                return LocalRedirect(returnUrl);
            }
            if (signInResult.IsLockedOut)
            {
                return RedirectToPage("./Lockout");
            }
            if (signInResult.IsNotAllowed)
            {
                ErrorMessage = "You need to confirm your email address before logging in.";
                return RedirectToPage("./Login", new { ReturnUrl = returnUrl });
            }
            if (signInResult.RequiresTwoFactor)
            {
                throw new NotImplementedException($"Unhandled result {nameof(signInResult.RequiresTwoFactor)}.");
            }
            else
            {
                // If the user does not have an account, create one.
                ReturnUrl = returnUrl;
                SafaJwt = safaJwt;
                LoginProvider = info.LoginProvider;
                if (info.Principal.HasClaim(c => c.Type == ClaimTypes.Email)) // Use the provided email.
                {
                    Input = new InputModel
                    {
                        Email = info.Principal.FindFirstValue(ClaimTypes.Email)
                    };
                    return await OnPostConfirmationAsync(returnUrl);
                }
                // Ask the user to provide an email address.
                return Page();
            }
        }

        public async Task<IActionResult> OnPostConfirmationAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            // Get the information about the user from the external login provider
            ExternalLoginInfo info;
            if (SafaJwt != null)
                info = _safaExternalLogin.GetLoginInfo(SafaJwt);
            else
                info = await _signInManager.GetExternalLoginInfoAsync();

            if (info == null)
            {
                ErrorMessage = "Error loading external login information during confirmation.";
                return RedirectToPage("./Login", new { ReturnUrl = returnUrl });
            }

            if (ModelState.IsValid)
            {
                var user = new IdentityUser { UserName = Input.Email, Email = Input.Email };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await _claimsService.AddProviderClaimsAsync(user, info);

                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);

                        // If we are using the externally provided email, mark it as confirmed, otherwise send confirmation email.
                        var emailConfirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        if (info.Principal.FindFirstValue(ClaimTypes.Email) == Input.Email)
                        {
                            this.AddMessage(MessageType.Success, "Account created", $"An account was created for your {info.LoginProvider} account.");

                            var confirmEmailResult = await _userManager.ConfirmEmailAsync(user, emailConfirmationToken);
                            if (confirmEmailResult != IdentityResult.Success)
                                throw new Exception($"Could not confirm email address: {confirmEmailResult}");
                            await _signInManager.SignInAsync(user, isPersistent: false);
                            return LocalRedirect(returnUrl);
                        }
                        else
                        {
                            // Don't sign them in as they haven't confirmed their email yet.
                            var callbackUrl = Url.Page(
                                "/Account/ConfirmEmail",
                                pageHandler: null,
                                values: new { userId = user.Id, code = emailConfirmationToken },
                                protocol: Request.Scheme);

                            await _notificationService.ConfirmAccountAsync(Input.Email, HtmlEncoder.Default.Encode(callbackUrl));

                            return RedirectToPage("./RegistrationEmailSent", new { emailAddress = Input.Email });
                        }
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
                if (result.Errors.Any(e => e.Code == "DuplicateUserName"))
                    this.AddMessage(MessageType.Warning, "An account using this email already exists.", "To associate an existing account with an external login, please log into the account, then go to Account Settings > External Logins.");
            }

            LoginProvider = info.LoginProvider;
            ReturnUrl = returnUrl;
            return Page();
        }
    }
}
