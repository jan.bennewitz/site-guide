﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using SiteGuide.Service;
using SiteGuide.Service.Extensions;
using SiteGuide.Service.Helpers;

namespace SiteGuide.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly NotificationService _notificationService;
        private readonly IConfiguration _configuration;
        private readonly RoleManager<IdentityRole> _roleManager;

        public RegisterModel(
            UserManager<IdentityUser> userManager,
            ILogger<RegisterModel> logger,
            NotificationService notificationService,
            IConfiguration configuration,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _logger = logger;
            _notificationService = notificationService;
            _configuration = configuration;
            _roleManager = roleManager;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public string PasswordRulesDescription => _userManager.Options.Password.ToDescription();

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (ModelState.IsValid)
            {
                var user = new IdentityUser { UserName = Input.Email, Email = Input.Email };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");

                    // If there is no admin user yet, and the new user matches the configured initial admin id, set them up with the admin role.
                    if ((await _userManager.GetUsersInRoleAsync(Permissions.AdminRoleName)).Count == 0 && user.Email == _configuration["InitialAdminId"])
                    {
                        _logger.LogInformation("No admin has been assigned and user id matches configured initial admin id. Assigning to admin role.");
                        if ((await _roleManager.FindByNameAsync(Permissions.AdminRoleName)) == null) //If there is no admin role yet, create it.
                        {
                            await _roleManager.CreateAsync(new IdentityRole(Permissions.AdminRoleName));
                            _logger.LogInformation("Admin role created.");
                        }
                        await _userManager.AddToRoleAsync(user, Permissions.AdminRoleName);
                        user.EmailConfirmed = true;
                        await _userManager.UpdateAsync(user);
                    }

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { userId = user.Id, code = code },
                        protocol: Request.Scheme);

                    await _notificationService.ConfirmAccountAsync(Input.Email, HtmlEncoder.Default.Encode(callbackUrl));

                    return RedirectToPage("./RegistrationEmailSent", new { emailAddress = Input.Email });
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
