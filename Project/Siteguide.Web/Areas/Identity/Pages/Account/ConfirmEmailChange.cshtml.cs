﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SiteGuide.Web.Extensions;
using SiteGuide.Web.Models;

namespace SiteGuide.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ConfirmEmailChangeModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;

        public ConfirmEmailChangeModel(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<IActionResult> OnGetAsync([FromQuery] string token, [FromQuery] string oldEmail, [FromQuery] string newEmail)
        {
            if (token == null || oldEmail == null || newEmail == null)
            {
                return RedirectToPage("/Index");
            }

            var user = await _userManager.FindByEmailAsync(oldEmail);
            if (user == null)
            {
                return NotFound($"Unable to load user with email '{oldEmail}'.");
            }

            var existingUser = await _userManager.FindByEmailAsync(newEmail);
            if (existingUser != null)
            {
                this.AddMessage(MessageType.Warning, "Could not change email address", "A user with this email address already exists.");
                return Page();
            }

            var resultEmailChange = await _userManager.ChangeEmailAsync(user, newEmail, token);
            if (!resultEmailChange.Succeeded)
            {
                if (resultEmailChange.Errors.FirstOrDefault()?.Code == "InvalidToken")
                {
                    this.AddMessage(MessageType.Warning, "Invalid confirmation code", "Your email change confirmation code is invalid.");
                    return Page();
                }
                else
                    throw new InvalidOperationException($"Error changing email for user '{user.UserName}':");
            }
            user.UserName = newEmail;
            var resultUserNameChange = await _userManager.UpdateAsync(user);
            if (!resultUserNameChange.Succeeded)
            {
                throw new InvalidOperationException($"Error changing user name for user '{user.UserName}':");
            }

            this.AddMessage(MessageType.Success, "Change of Email Address Confirmed", "Thank you for confirming your new email address. Your email address has been updated.");
            return Page();
        }
    }
}
