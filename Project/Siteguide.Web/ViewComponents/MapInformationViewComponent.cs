﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;

namespace SiteGuide.Web.ViewComponents
{
    public class MapInformationViewComponent : ViewComponent
    {
        private readonly SiteGuideContext _context;

        public MapInformationViewComponent(SiteGuideContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            //var items = await GetItemsAsync(maxPriority, isDone);
            //return View(items);
            return View();
        }
        //private Task<List<TodoItem>> GetItemsAsync(int maxPriority, bool isDone)
        //{
        //    return _context.ToDo.Where(x => x.IsDone == isDone &&
        //                         x.Priority <= maxPriority).ToListAsync();
        //}
    }
}
