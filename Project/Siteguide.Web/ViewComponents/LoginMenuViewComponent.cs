﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiteGuide.Service;
using SiteGuide.Web.Services;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;

namespace SiteGuide.Web.ViewComponents
{
    public class LoginMenuViewComponent : ViewComponent
    {
        private readonly SiteGuideContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public LoginMenuViewComponent(SiteGuideContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            LoginMenuModel model;

            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (user == null)
                model = null;
            else
            {
                // Get all of this user's draft edits
                var drafts = _context.EditVersion
                    .WhereIsDraftBy(user);

                var cacheUserClaimableEdits = _context.CacheUserClaimableEdits.SingleOrDefault(i => i.UserId == user.Id);

                model = new LoginMenuModel
                {
                    DraftEdits = drafts.Count(e => e.EditVersionNumber == 1),
                    SubmittedEdits = _context.EditVersion.Count(e =>
                        e.AuthorId == user.Id
                        && e.EditVersionNumber == 1
                        && e.StatusId != "D"),
                    DraftReviews = drafts.Count(e => e.EditVersionNumber > 1),
                    ClaimableReviews = cacheUserClaimableEdits?.ClaimableEditsCount ?? 0,
                };
            }
            return View(model);
        }
    }

    public class LoginMenuModel
    {
        public int DraftEdits { get; set; }
        public int SubmittedEdits { get; set; }
        public int DraftReviews { get; set; }
        public int ClaimableReviews { get; set; }
    }
}
