﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiteGuide.Service.Helpers;
using SiteGuide.Web.Models;
using SiteGuide.DAL.Models;

namespace SiteGuide.Web.ViewComponents
{
    public class ContactsViewComponentModel
    {
        public IEnumerable<SiteContactInfo> Contacts { get; set; }
        public string Title { get; set; }
    }

    public class ContactsViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(ContactsViewComponentModel model)
        {
            //var items = await GetItemsAsync(maxPriority, isDone);
            //return View(items);
            return View(model);
        }
        //private Task<List<TodoItem>> GetItemsAsync(int maxPriority, bool isDone)
        //{
        //    return _context.ToDo.Where(x => x.IsDone == isDone &&
        //                         x.Priority <= maxPriority).ToListAsync();
        //}
    }
}
