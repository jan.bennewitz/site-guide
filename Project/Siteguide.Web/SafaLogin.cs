﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace SiteGuide.Web
{
    public interface ISafaExternalLogin
    {
        IActionResult Challenge(HttpRequest request, string callbackUrl, string returnUrl = null);
        ExternalLoginInfo GetLoginInfo(string jwt);
        string UrlParamsExtractJwt(ref string returnUrl);
    }

    public class SafaLoginOptions
    {
        public string SigningSecret { get; set; }
    }

    public class SafaExternalLogin : ISafaExternalLogin
    {
        private readonly string _safaSigningSecret;
        private readonly ILogger<SafaExternalLogin> _logger;

        public SafaExternalLogin(
            IOptionsMonitor<SafaLoginOptions> safaOptionsAccessor,
            ILogger<SafaExternalLogin> logger
        )
        {
            _safaSigningSecret = safaOptionsAccessor.CurrentValue.SigningSecret;
            _logger = logger;
        }
        public IActionResult Challenge(HttpRequest request, string callbackUrl, string returnUrl = null)
        {
            var uriBuilder = new UriBuilder(request.Scheme, request.Host.Host)
            {
                Path = callbackUrl,
                Query = $"returnUrl={returnUrl}",
            };
            if (request.Host.Port != null)
                uriBuilder.Port = (int)request.Host.Port;

            return new RedirectResult($"https://members.safa.asn.au/jwt.php?url={uriBuilder.Uri}", false);
        }

        public string UrlParamsExtractJwt(ref string returnUrl)
        {
            var urlParameters = returnUrl.Split("?", 2); // returnUrl and jwt
            returnUrl = urlParameters[0];
            if (string.IsNullOrEmpty(returnUrl))
                returnUrl = null;
            if (urlParameters.Length < 2)
                return null;
            var jwtParameter = urlParameters[1].Split("=");
            if (!(jwtParameter[0].ToUpperInvariant() == "JWT"))
                //throw new ArgumentException("Expected jwt url parameter not found.");
                return null;
            string jwtEncoded = jwtParameter[1];
            return jwtEncoded;
        }

        public ExternalLoginInfo GetLoginInfo(string jwt)
        {
            if (string.IsNullOrEmpty(jwt))
                return null;
            ClaimsPrincipal principal;
            try
            {
                var validator = new JwtSecurityTokenHandler();
                principal = validator.ValidateToken(jwt, new TokenValidationParameters()
                {
                    ValidateLifetime = false,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_safaSigningSecret)),
                }, out var validatedToken);
            }
            catch (SecurityTokenInvalidSignatureException ex)
            {
                _logger.LogError(ex, "Error validating SAFA token.");
                return null;
            }

            // TODO: Validate token issue date
            // TODO: Validate token expiry date
            // TODO: Check request ip matches original request's?

            // Currently not using this, we'll get a list of editors for a site  from SAFA api instead.
            //// Update edit claims
            //var clubClaims = principal.Claims
            //    .Where(c => c.Type == "clubs")
            //    .Select(c => JObject.Parse(c.Value));

            //var siteGuideAdminClaims = clubClaims.Where(c =>
            //    (string)c["postition" /*sic*/ ] == "Site Guide Administrator"
            //    || (string)c["position"] == "Site Guide Administrator");

            //var otherClaims = clubClaims.Except(siteGuideAdminClaims);
            //Debug.Assert(!otherClaims.Any(), "User has non-site guide admin club claims.");

            ////var user = await _userManager.GetUserAsync(principal);

            //var editClaims = clubClaims.Select(c => (string)c["club"]);
            //foreach (var editClaim in editClaims) {}

            var claimSafa = principal.Claims.Single(c => c.Type == "safa" || c.Type == "hgfa").Value;
            return new ExternalLoginInfo(principal, "SAFA", claimSafa, "SAFA");
        }
    }
}
