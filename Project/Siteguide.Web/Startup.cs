﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
//using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SiteGuide.DAL.Models;
using Microsoft.AspNetCore.Identity.UI.Services;
using SiteGuide.Web.Areas.Identity;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using AutoMapper;
using Microsoft.Extensions.Hosting;
using SiteGuide.Service;
using Microsoft.AspNetCore.HttpOverrides;
using SiteGuide.DAL;
using SiteGuide.Web.Services;
using SiteGuide.Service.Services;
using System.Net;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.DataProtection;
using System.IO;

namespace SiteGuide.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Env { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<SiteGuideContext>(options => options
                .UseNpgsql(Configuration.GetConnectionString("SiteGuide"))
                .EnableSensitiveDataLogging()
            );

            services.AddIdentity<IdentityUser, IdentityRole>()
                //.AddRoles<IdentityRole>() // TODO: Do we need this?
                .AddEntityFrameworkStores<SiteGuideContext>()
                .AddDefaultTokenProviders();

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.Configure<IdentityOptions>(options =>
            {
                options.SignIn.RequireConfirmedEmail = true;
            });

            services.AddMvc().AddRazorPagesOptions(options =>
            {
                options.Conventions.AuthorizeAreaFolder("Identity", "/Account/Manage");
                options.Conventions.AuthorizeAreaPage("Identity", "/Account/Logout");
            });
            services.AddMvc().AddControllersAsServices();

            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    //options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
                    //options.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
                });

            services.AddSwaggerGen();

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = $"/Identity/Account/Login";
                options.LogoutPath = $"/Identity/Account/Logout";
                options.AccessDeniedPath = $"/Identity/Account/AccessDenied";
            });

            services.AddSingleton<IEmailService, MailJetSender>()
                .Configure<MailJetOptions>(options =>
                {
                    options.ApiKeyPrivate = Configuration["MailJet:ApiKey_Private"];
                    options.ApiKeyPublic = Configuration["MailJet:ApiKey_Public"];
                });

            string dataProtectionKeysDirectory = Configuration["DataProtectionKeysDirectory"];
            if (!string.IsNullOrEmpty(dataProtectionKeysDirectory))
                services.AddDataProtection()
                    .PersistKeysToFileSystem(new DirectoryInfo(dataProtectionKeysDirectory));

            services.AddAuthentication().AddGoogle(googleOptions =>
            {
                googleOptions.ClientId = Configuration["Authentication:Google:ClientId"];
                googleOptions.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
            });

            services.AddSingleton<ISafaExternalLogin, SafaExternalLogin>();
            services.Configure<SafaLoginOptions>(options =>
            {
                options.SigningSecret = Configuration["SafaSigningSecret"];
            });

            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSingleton(new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            }).CreateMapper());
            services
                .AddScoped<VersionService>()
                .AddScoped<VersionChangesDescriptorService>()
                .AddScoped<EditService>()
                .AddScoped<EditPermissionsService>()
                .AddScoped<EditorResolver>()
                .AddScoped<SiteService>()
                .AddScoped<NotificationService>()
                    .Configure<NotificationOptions>(options =>
                    {
                        options.SendToFolder = Configuration["Emails:SendToFolder"];
                        options.BccTo = Configuration["Emails:BccTo"];
                        options.NotificationsToRegisteredUsersOnly = bool.Parse(Configuration["Emails:NotificationsToRegisteredUsersOnly"] ?? "False");
                        options.FromAddress = Configuration["Emails:FromAddress"];
                        options.FromName = Configuration["Emails:FromName"];
                        options.Environment = Env.EnvironmentName;
                    })
                .AddScoped<ExternalCache>()
                .AddScoped<IBackupService, PostgresqlBackupService>()
                .AddScoped<IOnlineFileStorageService, DropBoxService>()
                .AddScoped<RazorViewToStringRenderer>()
                .AddScoped<ClaimsService>()

                //https://stackoverflow.com/a/58402965/3479465 :
                .AddSingleton<BackgroundTasksService>()
                .AddHostedService(provider => provider.GetService<BackgroundTasksService>());

            services.AddMvc();

            IMvcBuilder builder = services.AddRazorPages();
#if DEBUG
            if (Env.IsDevelopment())
            {
                builder.AddRazorRuntimeCompilation();
            }
#endif
            services.AddHttpsRedirection(options =>
            {
                if (Env.EnvironmentName == Environments.Development)
                    options.HttpsPort = 5001;
                //options.HttpsPort = 5001; // For non-dev, set this elsewhere instead (e.g. HTTPS_PORT environment variable in kestrel-siteguide.service file) so we can run multiple instance on the same server without conflict.
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/linux-nginx?view=aspnetcore-3.1
            // Because requests are forwarded by reverse proxy when deployed on Linux, use the Forwarded Headers Middleware:
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
                Microsoft.IdentityModel.Logging.IdentityModelEventSource.ShowPII = true;
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts(); // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            }

            app.UseHttpsRedirection();

            app.UseStatusCodePagesWithReExecute("/StatusCode", "?statusCode={0}");

            // Set up custom content types - associating file extension to MIME type
            var provider = new FileExtensionContentTypeProvider();
            provider.Mappings[".gpx"] = "application/gpx+xml";
            app.UseStaticFiles(new StaticFileOptions { ContentTypeProvider = provider });
            app.UseCookiePolicy();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });

            // https://stackoverflow.com/a/48357218/3479465
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(env.ContentRootPath, "App_Data"));
        }
    }
}
