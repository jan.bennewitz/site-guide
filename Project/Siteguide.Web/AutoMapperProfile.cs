﻿using AutoMapper;
using SiteGuide.DAL.Models;

namespace SiteGuide.Web
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Contact, Contact>().IgnoreRecordFields();
            CreateMap<Launch, Launch>().IgnoreRecordFields();
            CreateMap<MapShape, MapShape>().IgnoreRecordFields();
            CreateMap<SeeAlso, SeeAlso>().IgnoreRecordFields();
            CreateMap<Site, Site>().IgnoreRecordFields();
            CreateMap<SiteContact, SiteContact>().IgnoreRecordFields();
            CreateMap<SiteMapshape, SiteMapshape>().IgnoreRecordFields();
            CreateMap<Video, Video>().IgnoreRecordFields();
        }
    }

    public static class AutoMapperExtensions
    {
        public static IMappingExpression<T, T> IgnoreRecordFields<T>(this IMappingExpression<T, T> mappingExpression) where T : IRecord
            => mappingExpression
                .ForMember(r => r.RecordId, opt => opt.Ignore())
                .ForMember(r => r.EditVersionId, opt => opt.Ignore())
                .ForMember(r => r.IsDelete, opt => opt.Ignore())
                .ForMember(r => r.ValidFromVersion, opt => opt.Ignore())
                .ForMember(r => r.ValidToVersion, opt => opt.Ignore());
    }
}
