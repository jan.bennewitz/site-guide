﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Geo;
using MarkdownSharp;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using SiteGuide.Service.Helpers;
using SiteGuide.Web.Helpers;
using SiteGuide.Web.Models;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using SiteGuide.Service.Identifiers;
using System.Net;

namespace SiteGuide.Web.Controllers
{
    public class MapController : Controller
    {
        private readonly SiteGuideContext _context;
        private readonly IConfiguration _configuration;

        public MapController(SiteGuideContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        // GET: Map
        public async Task<IActionResult> Index()
        {
            ViewBag.GmapsApiKey = _configuration.GetValue<string>("GMaps:ApiKey");

            return View();
        }

        // GET: data
        public async Task<IActionResult> Data([FromQuery] string versionTreeNodeIdentifier = null)
        {
            VersionTreeNodeIdentifier versionTreeNode = string.IsNullOrWhiteSpace(versionTreeNodeIdentifier)
                ? null
                : VersionTreeNodeIdentifier.FromIdentifier(versionTreeNodeIdentifier, _context);

            Markdown markdownTransformer = new();

            string versionInfo = versionTreeNode switch
            {
                null => null,
                EditVersionIdentifier ev => ev.IdAndVersionNumber,
                VersionIdentifier v => v.Id.ToString(),
                //VersionIdentifier v => new Service.VersionInfo
                //{
                //    Id = v.Id,
                //    AppliedEditVersion = await _context.EditVersion
                //        .AsNoTracking()
                //        .SingleAsync(ev =>
                //            ev.EditId == v.EditId
                //            && ev.StatusId == "A")
                //},

                _ => throw new NotImplementedException(),
            };

            var sites = _context.Site
                .AsNoTracking()
                .ValidAtVersionTreeNode(versionTreeNode)
                .Select(s => new
                {
                    s.Id,
                    s.Name,
                    s.IsClosed,
                })
                .ToList()
                .GroupJoin(
                    _context.Launch
                    .AsNoTracking()
                    .ValidAtVersionTreeNode(versionTreeNode)
                    .Select(l => new
                    {
                        l.Id,
                        l.SiteId,
                        l.Name,
                        l.Lat,
                        l.Lon,
                        l.IsClosed,
                    })
                    .ToList(),
                s => s.Id, l => l.SiteId, (s, ll) => new
                {
                    s.Id,
                    s.Name,
                    s.IsClosed,
                    Launches = ll
                });

            var polyEncoderDetail = new PolylineEncoder(18, 2, 1e-5, true);

            var sitesByShapeId = _context.SiteMapshape
                .AsNoTracking()
                .ValidAtVersionTreeNode(versionTreeNode)
                .ToLookup(i => i.MapshapeId, i => i.SiteId);

            var shapes = _context.MapShape
                .AsNoTracking()
                .Include(m => m.Category)
                .ValidAtVersionTreeNode(versionTreeNode)
                .Select(m => new
                {
                    m.Id,
                    Category = m.Category.Name,
                    Coordinates = m.CoordinatesList,
                    m.Name,
                    m.Description,
                    m.EstaEmergencyMarker,
                })
                .ToList()
                .Select(m => new
                {
                    m.Category,
                    Coordinates = m.Coordinates.Select(p => polyEncoderDetail.DPEncode(Track.CreateFromPointsData(p, false))["encodedPoints"]),
                    m.Name,
                    Description = markdownTransformer.Transform(m.Description),
                    m.EstaEmergencyMarker,
                    SiteIds = sitesByShapeId[m.Id],
                });

            var clubs = await _context.ExtCacheClub
                .AsNoTracking()
                .Select(c => new
                {
                    c.Name,
                    c.Category,
                    c.Phone,
                    c.Email,
                    c.SafaCode,
                    Url = string.IsNullOrEmpty(c.Url) || c.Url.StartsWith("http") ? c.Url : "https://" + c.Url,
                    c.Lat,
                    c.Lng,
                })
                .ToListAsync();

            var mapShapeCategories = await _context.MapShapeCategory.ToListAsync();

            return Json(new
            {
                sites,
                shapes,
                clubs,
                mapShapeCategories,
                versionInfo,
            });
        }

        public async Task<IActionResult> CtafData()
        {
            var aerodromes = await _context.ExtCacheCtaf
                .AsNoTracking()
                .ToListAsync();

            var ersaEffectiveDate = _context.ExtCacheErsaEffectiveDate
                .Select(d => d.EffectiveDate)
                .Where(d => d <= DateOnly.FromDateTime(DateTime.Now))
                .OrderByDescending(d => d)
                .First()
                .ToString("ddMMMyyyy"); // Only used in link to ERSA FAC. Easiest to format here.

            return Json(new { aerodromes, ersaEffectiveDate });
        }

        public async Task<ActionResult> LaunchSummary(int? id)
        {
            if (id is null)
                return new BadRequestObjectResult($"Parameter missing: {nameof(id)}");

            var model = await GetLaunchSummaryModelAsync(_context, (int)id);

            if (model is null)
                return new BadRequestObjectResult($"Specified launch does not exist. {nameof(id)}: {id}");

            return PartialView("_LaunchSummary", model);
        }

        private async Task<LaunchInfo> GetLaunchSummaryModelAsync(SiteGuideContext context, int launchId)
        {
            var launch = context.Launch
                .ValidAtCurrentVersion()
                .SingleOrDefault(l => l.Id == launchId);
            if (launch == null)
                return null;
            var site = context.Site
                .ValidAtCurrentVersion()
                .Single(s => s.Id == launch.SiteId);

            var model = new LaunchInfo()
            {
                Site = site,
                Launch = launch,
            };
            model.Contacts = context.SiteContact
                .ValidAtCurrentVersion()
                .Where(sc => sc.SiteId == model.Site.Id)
                .ToList()
                .Join(
                    context.Contact
                        .ValidAtCurrentVersion(),
                    sc => sc.ContactId,
                    c => c.Id,
                    (sc, c) => new SiteContactInfo()
                    {
                        Contact = c,
                        IsContact = sc.IsContact,
                        IsResponsible = sc.IsResponsible,
                    }
                )
                .ToList();

            return model;
        }
    }
}
