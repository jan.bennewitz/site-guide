﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using SiteGuide.DAL;
using Humanizer;
using System.IO.Compression;
using System.IO;
using SiteGuide.Service;
using Version = SiteGuide.DAL.Models.Version;
using SiteGuide.Service.Services;
using SiteGuide.Service.Views.Kml.SitesKml;
using SiteGuide.Service.Views.Kml.Shared;
using SiteGuide.Service.Views.Kml.Car166Kml;
using SiteGuide.Web.Models;
using static System.Net.WebRequestMethods;

namespace SiteGuide.Web.Controllers
{
    public class KmlController : Controller
    {
        private readonly SiteGuideContext _context;
        private readonly RazorViewToStringRenderer _razorViewToStringRenderer;

        public KmlController(SiteGuideContext context, RazorViewToStringRenderer razorViewToStringRenderer)
        {
            _context = context;
            _razorViewToStringRenderer = razorViewToStringRenderer;
        }

        public async Task<FileResult> Index()
        {
            var model = new KmlIndexModel
            {
                ControllerPath = $"{Request.Scheme}://{Request.Host}{Request.PathBase}/{ControllerContext.ActionDescriptor.ControllerName}",
                Host = Request.Host.Value,
            };
            return await KmzFileAsync("KmlIndex", model, "siteguide.kmz");
        }

        public async Task<FileResult> Open()
        {
            var model = GetSitesKmlViewModel();
            return await KmzFileAsync("SitesKml", model, "open.kmz");
        }

        public async Task<FileResult> Closed()
        {
            var model = GetSitesKmlViewModel(true);
            return await KmzFileAsync("SitesKml", model, "closed.kmz");
        }

        public async Task<FileResult> CAR166()
        {
            //var ersaDate = await _context.ExtCacheErsaEffectiveDate
            //    .Select(d => d.EffectiveDate)
            //    .Where(d => d < DateTime.UtcNow.Date)
            //    .MaxAsync();
            //    The above is throwing an error against Postgresql. Just grab the whole list, it's short:
            var ersaDate = (await _context.ExtCacheErsaEffectiveDate
                .Select(d => d.EffectiveDate)
                .ToListAsync())
                .Where(d => d < DateOnly.FromDateTime(DateTime.UtcNow))
                .Max();

            var model = new Car166KmlViewModel
            {
                ErsaDate = ersaDate,
                Ctafs = _context.ExtCacheCtaf,
            };

            return await KmzFileAsync("Car166Kml", model, "car166.kmz");
        }

        private SitesKmlViewModel GetSitesKmlViewModel(bool isClosedSites = false)
        {
            var version = _context.Version
                .AsNoTracking()
                .Current();
            var editVersion = _context.EditVersion
                .AsNoTracking()
                .Single(ev =>
                    ev.EditId == version.EditId
                    && ev.StatusId == "A");

            var sites = _context.Site
                .AsNoTracking()
                .ValidAtCurrentVersion()
                .OrderBy(s => s.Name)
                .Where(s => string.IsNullOrWhiteSpace(s.Closed) == !isClosedSites)
                .ToList();
            var launches = _context.Launch
                .AsNoTracking()
                .ValidAtCurrentVersion()
                .OrderBy(s => s.Name)
                .ToLookup(l => l.SiteId);
            var seeAlsosBySiteId = _context.SeeAlso
                .AsNoTracking()
                .ValidAtCurrentVersion()
                //.OrderBy(s => s.Name)
                .ToLookup(l => l.SiteId);
            var singleSiteMapshapes = _context.SiteMapshape
                .AsNoTracking()
                .ValidAtCurrentVersion()
                .Select(i => new { SiteId = i.SiteId, MapshapeId = i.MapshapeId })
                .AsEnumerable()
                .GroupBy(i => i.MapshapeId, i => i.SiteId)
                .Where(g => g.Count() == 1)
                .Select(g => new { MapshapeId = g.Key, SiteId = g.Single() });
            var singleSiteMapshapeIds = singleSiteMapshapes
                .Select(i => i.MapshapeId);
            var mapshapesQuery = _context.MapShape
                            .AsNoTracking()
                            .ValidAtCurrentVersion()
                            .Include(m => m.Category);
            var sharedMapshapes = mapshapesQuery
                .Where(m => !singleSiteMapshapeIds.Contains(m.Id) && !isClosedSites); // Throw the shared mapshapes in with the open sites
            var mapshapesBySiteId = mapshapesQuery
                .ToList()
                .Join(singleSiteMapshapes, m => m.Id, ssm => ssm.MapshapeId, (m, ssm) => new { ssm.SiteId, Mapshape = m })
                .ToLookup(i => i.SiteId, i => i.Mapshape);
            var contactById = _context.Contact
                .AsNoTracking()
                .ValidAtCurrentVersion()
                .ToDictionary(c => c.Id);
            var contactsBySiteId = _context.SiteContact
                .AsNoTracking()
                .ValidAtCurrentVersion()
                .ToLookup(sc => sc.SiteId, sc => new ContactInfo
                {
                    IsContact = sc.IsContact,
                    IsResponsible = sc.IsResponsible,
                    Contact = contactById[sc.ContactId]
                });
            var model = new SitesKmlViewModel
            {
                Version = version,
                EditVersion = editVersion,
                IsClosedSites = isClosedSites,
                Styles = _context.MapShapeCategory.Select(GetMapshapeCategoryKmlStyle),
                Sites = sites,
                Launches = launches,
                SeeAlsos = seeAlsosBySiteId,
                SiteMapshapes = mapshapesBySiteId,
                Contacts = contactsBySiteId,
                SharedMapshapes = sharedMapshapes,
                SiteUrl = $"{Request.Scheme}://{Request.Host}",
                ImagesFolderPath = "/css/images/",
            };
            return model;
        }

        private async Task<FileResult> KmzFileAsync<TModel>(string viewName, TModel model, string fileName)
        {
            string kml = await _razorViewToStringRenderer.RenderViewToStringAsync($"/Views/Kml/{viewName}/{viewName}.cshtml", model);
            return KmzFile(kml, fileName);
        }

        private FileResult KmzFile(string kml, string fileName)
        {
            var memoryStream = new MemoryStream();
            using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
            {
                var doc = archive.CreateEntry("doc.kml");

                using var entryStream = doc.Open();
                using var streamWriter = new StreamWriter(entryStream);
                streamWriter.Write(kml);
            }

            memoryStream.Seek(0, SeekOrigin.Begin);
            byte[] kmzBytes = memoryStream.ToArray();
            return base.File(kmzBytes, "application/vnd.google-earth.kml+xml", fileName);
        }

        private KmlStyle GetMapshapeCategoryKmlStyle(MapShapeCategory category)
        {
            var color = ColorTranslator.FromHtml(category.Color);
            return new KmlStyle
            {
                Id = $"style{category.Name.Dehumanize()}",
                LineColor = $"ff{color.B:x2}{color.G:x2}{color.R:x2}",
                LineWidth = 1,
                PolygonColor = category.Dimensions == 1
                    ? null
                    : $"33{color.B:x2}{color.G:x2}{color.R:x2}",
            };
        }
    }
}