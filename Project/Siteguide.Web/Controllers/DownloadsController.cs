﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Geo;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiteGuide.Web.Helpers;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using SiteGuide.DAL;
using Humanizer;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using System.Security.Permissions;
using System.IO.Compression;
using System.IO;
using SiteGuide.Service;
using SiteGuide.Service.Helpers;
using Microsoft.Extensions.Logging;
using SiteGuide.Web.Services;
using NLog.Filters;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace SiteGuide.Web.Controllers
{
    public class DownloadsController : Controller
    {
        private readonly SiteGuideContext _context;
        private readonly VersionService _versionService;
        private readonly ILogger<DownloadsController> _logger;
        private readonly IWebHostEnvironment _environment;

        public DownloadsController(SiteGuideContext context, VersionService versionService, ILogger<DownloadsController> logger, IWebHostEnvironment environment)
        {
            _context = context;
            _versionService = versionService;
            _logger = logger;
            _environment = environment;
        }

        public async Task<IActionResult> IndexAsync()
        {
            var versionInfo = await _versionService.GetVersionInfoAsync();
            ViewData["Version"] = versionInfo.Id;
            ViewData["VersionDateTime"] = versionInfo.PublishedTime;
            return View();
        }

        // TODO: Generate these files when data version changes and cache them as they take a few seconds to generate on demand.
        public async Task<ActionResult> OpenAirAsync(string type = "default", int? includeAirspaceBelowFl = null)
        {
            //const int MinIncludeAirspaceBelowFl = 100;
            //if (includeAirspaceBelowFl != null && includeAirspaceBelowFl < MinIncludeAirspaceBelowFl)
            //    return ValidationProblem(new ValidationProblemDetails { Detail = $"{nameof(includeAirspaceBelowFl)} must be at least {MinIncludeAirspaceBelowFl}.", Title = "Unsupported parameter value." });

            string fileNameSuffix;
            Dictionary<string, string> openAirEquivalentAirspaceClasses;
            bool linesAsPolys;
            switch (type?.ToUpperInvariant())
            {
                case "DEFAULT":
                case "FLYSKYHY":
                    bool isDefault = type.ToUpperInvariant() == "DEFAULT";
                    openAirEquivalentAirspaceClasses = new Dictionary<string, string> {
                        { "Landing", "LZ"},
                        { "Access", "G"},
                        { "Feature", "G"},
                        { "No Landing", "GP"}, // Glider prohibited
                        { "Powerline", isDefault ? "Q" /*Danger*/ : "OBS"},
                        { "Hazard", "Q"},
                        { "Hazard Area", "Q"},
                        { "No Launching", "R"}, // Restricted
                        { "Emergency Landing", "R"},
                        { "No Fly Zone", "P"}, //Prohibited
                    };
                    fileNameSuffix = isDefault ? "" : ".FlySkyHy";
                    linesAsPolys = isDefault;
                    break;
                case "XCTRACK":
                    openAirEquivalentAirspaceClasses = new Dictionary<string, string> {
                            { "Landing", "W" },
                            { "Access", "W"},
                            { "Feature", "W"},
                            { "No Landing", "GP"},
                            { "Powerline", "Q"},
                            { "Hazard", "Q"},
                            { "Hazard Area", "Q"},
                            { "No Launching", "Q"},
                            { "Emergency Landing", "Q"},
                            { "No Fly Zone", "P"},
                        };
                    fileNameSuffix = ".XCTrack";
                    linesAsPolys = true;
                    break;
                default:
                    return ValidationProblem(new ValidationProblemDetails { Detail = $"Unsupported value of {nameof(type)} parameter: \"{type}\".", Title = "Unsupported parameter value." });
            }

            string airspace;
            if (includeAirspaceBelowFl != null)
            {
                var fileName = Path.Combine((string)AppDomain.CurrentDomain.GetData("DataDirectory"), "ExternalCache", "airspace.OpenAir.txt");

                string airspaceAll = await GetAirspaceAsync(fileName, "https://xcaustralia.org/download/class_all_NO_fia_&_e%20class%20freq.bnd.php", TimeSpan.FromDays(1));

                airspace = FilterOAAirspaceBelowHeight(airspaceAll, (int)includeAirspaceBelowFl);
            }
            else
                airspace = "";

            var versionInfo = await _versionService.GetVersionInfoAsync();

            var homeSite = Request.Host.Value;
            string header = $"""
                * {homeSite}
                * OpenAir {type} export
                {GetNonProductionWarningText()}
                * Version {_context.Version.Current().Id} published {versionInfo.PublishedTime.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture)} UTC
                {(includeAirspaceBelowFl == null ? "" : $"* Includes airspace data from XCAustralia.org, filtered to airspaces below FL{includeAirspaceBelowFl}, at end of file.\r\n")}
                """;

            string fileContent = GenerateOpenAir(openAirEquivalentAirspaceClasses, _context.Version.Current().Id, linesAsPolys);

            string versionDesc = $"v{_context.Version.Current().Id}.{((DateTime)versionInfo.PublishedTime).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}";

            return base.File(Encoding.UTF8.GetBytes(header + fileContent + "\r\n\r\n" + airspace), "application/octet-stream", $"siteGuide{fileNameSuffix}.OpenAir.{versionDesc}.txt");
        }

        private string GetNonProductionWarningText() => _environment.IsProduction()
            ? ""
            : $"* This file was generated from the {_environment.EnvironmentName} environment - the data may be fictitious and differ from the production (=real) data.";

        /// <summary>
        /// Filter out airspaces too high to be relevant. Airspace above 3000m uses FL for height limits.
        /// </summary>
        private string FilterOAAirspaceBelowHeight(string airspaceData, int highestFL)
        {
            var resultBuilder = new StringBuilder();
            var airspaces = SplitOAIntoAirspaces(airspaceData);
            foreach (var airspace in airspaces)
            {
                var lines = airspace.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                var al = lines.FirstOrDefault(l => l.TrimStart().StartsWith("AL FL"));
                if (al != null)
                {
                    var alFl = int.Parse(al.TrimStart().Substring("AL FL".Length));
                    if (alFl >= highestFL) // FL125 = 3800m
                        continue;
                }
                resultBuilder.Append(airspace);
            }

            return resultBuilder.ToString();
        }

        private static List<string> SplitOAIntoAirspaces(string source)
        {
            var lines = source.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            var airspaces = new List<string>();
            var airspaceBuilder = new StringBuilder();
            foreach (var line in lines)
            {
                if (line.TrimStart().StartsWith("AC "))
                {
                    var airspace = airspaceBuilder.ToString();
                    airspaces.Add(airspace);
                    airspaceBuilder.Clear();
                }
                airspaceBuilder.AppendLine(line);
            }
            airspaces.Add(airspaceBuilder.ToString());
            return airspaces;
        }

        private async Task<string> GetAirspaceAsync(string fileName, string url, TimeSpan maxAge)
        {
            if (System.IO.File.Exists(fileName) && DateTime.UtcNow - System.IO.File.GetLastWriteTimeUtc(fileName) <= maxAge)
                return System.IO.File.ReadAllText(fileName); // We have recent cached data. Use that.

            // Retrieve airspace data from url.
            var response = await new System.Net.Http.HttpClient()
                .GetAsync(url);
            if (!response.IsSuccessStatusCode)
            {
                var ex = new ApplicationException($"Can not retrieve web airspace file: {response.StatusCode} {response.ReasonPhrase}");
                if (System.IO.File.Exists(fileName))
                {
                    _logger.LogWarning(ex, "Falling back to cached file.");
                    return System.IO.File.ReadAllText(fileName);
                }
                throw ex;
            }
            var content = response.Content.ReadAsStringAsync().Result;

            Directory.CreateDirectory(Path.GetDirectoryName(fileName));
            System.IO.File.WriteAllText(fileName, content); // Cache the data.
            return content;
        }

        public async Task<FileResult> XCTrackJsonAsync()
        {
            var version = _context.Version.Current();
            string fileContent = await GenerateXCTrackJsonAsync(version.Id);
            return base.File(Encoding.UTF8.GetBytes(fileContent), "application/json", $"siteGuide.airspace_xcontest_org.v{version.Id}.json");
        }

        public FileResult OziExplorer()
        {
            var version = _context.Version.Current();

            //http://www.oziexplorer3.com/eng/help/fileformats.html
            var result = new StringBuilder();
            result.AppendLine("OziExplorer Waypoint File Version 1.1");
            result.AppendLine("WGS 84");
            result.AppendLine("Reserved");
            result.AppendLine("Reserved");

            var mapshapes = GetMapshapesForInstruments(version.Id);
            foreach (var mapshape in mapshapes)
            {
                string name = mapshape.Name;
                if (name != null)
                    name = OziExplorerWptFileTextClean(name);

                var drawSettings = new ShapeDrawSetting(mapshape.Category);
                if (drawSettings.Dimension != 1)
                {
                    int colour = OziExplorerColour(drawSettings.FillColour);

                    var polys = mapshape.CoordinatesList;
                    foreach (var poly in polys)
                    {
                        Track track = Track.CreateFromPointsData(poly.Trim(), false);
                        if (track.Points.Count > 2)
                        {
                            LatLng centroid = track.GetCentroid();
                            //result.AppendLine("AC " + openAirEquivAirspaceClass);
                            //result.AppendLine("AN " + category + (!String.IsNullOrWhiteSpace(name) ? ": " + name : ""));
                            //result.AppendLine("AL SFC");
                            //result.AppendLine("AH 100 ft");
                            //var coords = poly.InnerText.Split(new string[] { " ", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                            //foreach (string coord in coords)
                            //    AddOpenAirCoord(result, coord);
                            //if (drawSettings.Dimension == 1) // OpenAir has no line element in the airspace format, so close the polygon by reversing the path
                            //    foreach (string coordString in coords.Skip(1).Reverse().Skip(1))
                            //        AddOpenAirCoord(result, coordString);
                            result.AppendLine($"-1,{mapshape.Category.Name},{centroid.lat},{centroid.lng},,0,1,3,0,{colour},{name},0,0,0,-777,6,0,17");
                        }
                    }
                }
            }

            //XmlNodeList launches = xmlSites.SelectNodes("siteGuideData//launch");

            return base.File(Encoding.UTF8.GetBytes(result.ToString()), "application/octet-stream", $"siteGuide.OziExplorer.v{version.Id}.wpt");
        }
        private static string OziExplorerWptFileTextClean(string text)
        {
            return text.Replace("\r", "").Replace("\n", "").Replace(",", ((char)209).ToString()).Trim();
        }
        private static int OziExplorerColour(Color colour)
        {
            return colour.B * 0x10000 + colour.G * 0x100 + colour.R;
        }
        private string GenerateOpenAir(Dictionary<string, string> openAirEquivalentAirspaceClasses, int version, bool linesAsPolys)
        {
            //Closest to an OpenAir spec I could find: http://www.winpilot.com/UsersGuide/UserAirspace.asp
            var result = new StringBuilder();

            foreach (var mapshape in GetMapshapesForInstruments(version))
            {
                string name = mapshape.Name;
                string category = mapshape.Category.Name;

                var drawSettings = new ShapeDrawSetting(mapshape.Category);

                //result.AppendLine("T" + (drawSettings.Dimension == 1 ? "O" : "C") + " "
                //    + category
                //    + (!String.IsNullOrWhiteSpace(name) ? ": " + name : ""));
                //var color = System.Drawing.ColorTranslator.FromHtml("#" + drawSettings.Colour);
                //result.AppendLine("SP 0 1 " + color.R + " " + color.G + " " + color.B); // 0 =Solid line, 1 pixel wide
                //if (drawSettings.Dimension == 2)
                //    result.AppendLine("SB -1,-1,-1"); // Make polygon interior transparent
                //result.AppendLine("V Z=100"); // Show these shapes at zoom levels up to 100km. TODO: Make this smarter and dependent on mapShape size

                if (!openAirEquivalentAirspaceClasses.TryGetValue(category, out string openAirEquivAirspaceClass))
                    throw new ApplicationException("Unrecognised category: " + category);

                string airspaceHeader = $@"AC {openAirEquivAirspaceClass}
AN {category}{(!string.IsNullOrWhiteSpace(name) ? ": " + name.Trim().Replace("\r", " ").Replace("\n", " ") : "")}
AL SFC
AH 100 ft AGL";

                foreach (var poly in mapshape.CoordinatesList)
                {
                    result.AppendLine(airspaceHeader);
                    var coords = poly.Split(new string[] { " ", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);

                    if (linesAsPolys)
                    {
                        if (coords.Length == 2)
                        {
                            // HACK: add an extra dummy point in the centre to keep xcsoar happy - it complains if a poly has < 3 points. https://github.com/XCSoar/XCSoar/issues/1011#issuecomment-1245473637
                            var latLngStart = coords[0].Split(',');
                            var latLngEnd = coords[1].Split(',');
                            float lngDeg = (float.Parse(latLngStart[0]) + float.Parse(latLngEnd[0])) / 2;
                            float latDeg = (float.Parse(latLngStart[1]) + float.Parse(latLngEnd[1])) / 2; // This is a good quick and dirty approximation if the line is small relative to the earth and does not cross the 180E/W meridian - seems a pretty safe assumption.

                            var coordsList = coords.ToList();
                            coordsList.Insert(1, lngDeg + "," + latDeg);
                            coords = coordsList.ToArray();
                        }

                        foreach (string coord in coords)
                            AddOpenAirCoord(result, coord, "DP");
                        if (drawSettings.Dimension == 1) // Close the polygon by reversing the path
                            foreach (string coordString in coords.Skip(1).Reverse().Skip(1))
                                AddOpenAirCoord(result, coordString, "DP");
                        result.AppendLine();
                    }
                    else
                    {
                        string type;
                        if (drawSettings.Dimension == 1)
                        {
                            result.AppendLine("V W=0.054"); // Corridor width of 100m or 0.054 nautical miles
                            type = "DY";
                        }
                        else
                            type = "DP";

                        foreach (string coord in coords)
                            AddOpenAirCoord(result, coord, type);
                    }
                }
            }

            return result.ToString();
        }

        private static void AddOpenAirCoord(StringBuilder result, string coord, string type)
        {
            var latLng = coord.Split(',');
            float lngDeg = float.Parse(latLng[0]);
            float latDeg = float.Parse(latLng[1]);
            result.AppendLine($"{type} {CoordToDMMSSH(latDeg, "N", "S")} {CoordToDMMSSH(lngDeg, "E", "W")}"); //e.g. 38:56:00.00 N 120:02:00.00 W
        }

        private static string CoordToDMMSSH(float coord, string posHemisphere, string negHemisphere)
        {
            //http://stackoverflow.com/questions/3249700/convert-degrees-minutes-seconds-to-decimal-coordinates
            double sec = Math.Round(coord * 3600, 2);
            int deg = (int)sec / 3600;
            sec = Math.Abs(sec % 3600);
            int min = (int)sec / 60;
            sec %= 60;
            return Math.Abs(deg) + ":" + min.ToString("D2") + ":" + sec.ToString("00.00") + " " + (coord < 0 ? negHemisphere : posHemisphere);
        }

        private async Task<string> GenerateXCTrackJsonAsync(int version)
        {
            var versionInfo = await _versionService.GetVersionInfoAsync();
            var homeUrl = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
            var homeSite = Request.Host.Value;

            var result = new
            {
                airspaces = new List<XCTrackAirspace>(),
                oaname = $"{homeSite} version {version} as at {(DateTime)versionInfo.PublishedTime:yyyy-MM-dd H:mm zzz}",
                oadescription = $"Australian national site guide data: Landing, no-landing and emergency landing areas, significant powerlines and other data. See {homeUrl} . {GetNonProductionWarningText()}".Trim(),
            };

            foreach (var mapshape in GetMapshapesForInstruments(version))
            {
                string name = mapshape.Name;
                string category = mapshape.Category.Name;
                string description = mapshape.Description;

                var drawSettings = new ShapeDrawSetting(mapshape.Category);

                var airchecktypes = new Dictionary<string, string>{
                    { "Landing", "inverse" },
                    { "Access", "inverse" },
                    { "Feature", "inverse" },
                    { "No Landing", "restrict"},
                    { "Powerline", "obstacle"},
                    { "Hazard", "ignore"},
                    { "Hazard Area", "ignore"},
                    { "No Launching", "ignore"},
                    { "Emergency Landing", "ignore"},
                    { "No Fly Zone", "restrict"},
                };

                var shortCategories = new Dictionary<string, string>{
                    { "Landing", "LZ" },
                    { "Access", "Access" },
                    { "Feature", "Feature" },
                    { "No Landing", "NoLZ"},
                    { "Powerline", "Powerline"},
                    { "Hazard", "Haz"},
                    { "Hazard Area", "Haz"},
                    { "No Launching", "NoLaunch"},
                    { "Emergency Landing", "EmgyLZ"},
                    { "No Fly Zone", "NoFly"},
                };

                foreach (var poly in mapshape.CoordinatesList)
                {
                    var airspace = new XCTrackAirspace
                    {
                        airlower = new XCTrackAispaceVerticalBound { height = 0, type = "AGL" },
                        airupper = new XCTrackAispaceVerticalBound { height = 100, type = "AGL" },
                        airchecktype = airchecktypes[category],
                        aircatpg = true,
                        airname = shortCategories[category] + (!string.IsNullOrWhiteSpace(name) ? ": " + CollapseWhitespace(name) : ""),
                        airclass = shortCategories[category],
                        components = new List<double[]>(),
                        airparams = new object(),
                        descriptions = description == null ? new object() : new { en = CollapseWhitespace(description) },
                        airpen = new List<int> { 0, 2, drawSettings.FillColour.R, drawSettings.FillColour.G, drawSettings.FillColour.B },
                        airbrush = new List<int> { drawSettings.FillColour.R, drawSettings.FillColour.G, drawSettings.FillColour.B },
                    };

                    var coords = poly.Split(new string[] { " ", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string coord in coords)
                        AddJsonCoordinate(airspace, coord);
                    if (drawSettings.Dimension == 1 && airspace.airchecktype != "obstacle") // close the polygon by reversing the path
                        foreach (string coord in coords.Skip(1).Reverse().Skip(1))
                            AddJsonCoordinate(airspace, coord);

                    result.airspaces.Add(airspace);
                }
            }

            return Newtonsoft.Json.JsonConvert.SerializeObject(result);
        }

        private IQueryable<MapShape> GetMapshapesForInstruments(int version)
        {
            return _context
                    .MapShape
                    .ValidAtVersion(version)
                    .Include(s => s.Category)
                    .Where(s =>
                        !string.IsNullOrWhiteSpace(s.Coordinates)
                        && s.Category.Name != "Access" // Exclude categories which shouldn't be displayed on instruments
                    );
        }

        private static string CollapseWhitespace(string text) => text == null ? null : Regex.Replace(text, @"\s+", " ").Trim();

        private static void AddJsonCoordinate(XCTrackAirspace airspace, string coord)
        {
            var latLng = coord.Split(',');
            airspace.components.Add(new double[2] { double.Parse(latLng[1]), double.Parse(latLng[0]) });
        }

        private struct XCTrackAirspace
        {
            public string airchecktype;
            public ICollection<int> airpen;
            public ICollection<int> airbrush;
            public XCTrackAispaceVerticalBound airupper;
            public XCTrackAispaceVerticalBound airlower;
            public const object airacttime = null;
            public const object airendtime = null;
            public const object airstarttime = null;
            public object airparams;
            public string airname;
            public const object airautoid = null;
            public const object airemail = null;
            public ICollection<double[]> components;
            public bool aircatpg;
            public string airclass;
            public object descriptions;
        }

        private struct XCTrackAispaceVerticalBound
        {
            public string type;
            public int height;
        }
    }
}