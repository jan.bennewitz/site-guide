﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
//using System.Web.Http;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using SiteGuide.Web.Models;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using Humanizer;
using SiteGuide.DAL;
using SiteGuide.Service.Identifiers;
using Microsoft.Extensions.Logging;
using SiteGuide.Service;
using SiteGuide.Web.Extensions;
using SiteGuide.Service.Helpers;

namespace SiteGuide.Web.Controllers
{
    [Authorize]
    public class EditController : Controller
    {
        public const string CreateNewEditVersion = "create_new";

        private readonly SiteGuideContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly EditService _editService;
        private readonly VersionChangesDescriptorService _versionChangesDescriptorService;

        public EditController(SiteGuideContext context, UserManager<IdentityUser> userManager, EditService editService, VersionChangesDescriptorService versionChangesDescriptorService)
        {
            _context = context;
            _userManager = userManager;
            _editService = editService;
            _versionChangesDescriptorService = versionChangesDescriptorService;
        }

        //private async Task ApplyChangesToModelAsync(int editId, SiteInfo model)
        //{
        //    int siteId = model.Site.Id;
        //    var editVersions = _context.EditVersion.Where(v => v.EditId == editId);
        //    var maxEditVersionNumber = editVersions.Max(v => v.EditVersionNumber);
        //    int editVersionNumber = editVersions.Single(v => v.EditVersionNumber == maxEditVersionNumber).Id; // Latest
        //    var changeToSite = await _context.Site
        //        .Include(c => c.EditVersion)
        //        .WhereEditVersion(editVersionNumber)
        //        .FirstOrDefaultAsync(c => c.Id == siteId);

        //    if (changeToSite != null)
        //        CopyRecordContent(changeToSite, model.Site);

        //    ApplyChangesToModelData(
        //        _context.Launch,
        //        model.Launches,
        //        editVersionNumber,
        //        c => c.SiteId == siteId);
        //    ApplyChangesToModelData(
        //        _context.SeeAlso,
        //        model.SeeAlsos,
        //        editVersionNumber,
        //        c => c.SiteId == siteId);
        //    ApplyChangesToModelData(
        //        _context.SiteContact,
        //        _context.SiteContact.WhereVersion().Where(c => c.SiteId == siteId).ToList(),
        //        editVersionNumber,
        //        c => c.SiteId == siteId);
        //    var siteMapshapes = _context.SiteMapshape.WhereVersion().Where(sm => sm.SiteId == siteId).ToList();
        //    ApplyChangesToModelData(
        //        _context.SiteMapshape,
        //        siteMapshapes,
        //        editVersionNumber,
        //        c => c.SiteId == siteId);
        //    ApplyChangesToModelData(
        //        _context.MapShape,
        //        _context.MapShape.Where(m => siteMapshapes.Select(sm => sm.MapshapeId).Contains(m.Id)).ToList(),
        //        editVersionNumber,
        //        c => true // Do we even need this at all? 
        //    );
        //    // TODO: etc.
        //    // Contact handled separately?

        //    model.EditId = editId;
        //    model.EditVersionNumber = editVersionNumber;
        //}

        //private void ApplyChangesToModelData<T>(
        //    DbSet<T> changes,
        //    IEnumerable<T> modelData,
        //    int editVersionNumber,
        //    Func<T, bool> changeSelector
        //    )
        //    where T : class, IRecord
        //{
        //    foreach (var change in changes
        //        .Include(c => c.EditVersion)
        //        .WhereEditVersion(editVersionNumber)
        //        .Where(c => changeSelector(c))
        //        .GroupBy(c => c.Key, (key, g) => g.OrderByDescending(c => c.EditVersion.EditVersionNumber).First()))
        //    {
        //        // Find matching data row
        //        var dataRow = modelData.SingleOrDefault(d => change.HasSameKeyAs(d));
        //        if (dataRow == null)
        //        {
        //            // TODO: If the data has been deleted, we need to handle this somehow... delete the change? Let the user and editor know?
        //            // Site has been deleted: Remove change
        //            // Contact has been deleted: Remove change
        //            // Video: Immutable?
        //            // SiteMapshape: Immutable?

        //            // TODO: If the change is an addition, add it
        //        }
        //        else
        //            CopyRecordContent(change, dataRow);
        //    }
        //}

        //private static void SetReferenceObjectsToNull<T>(T newItem) where T : class, IRecord, IId, new()
        //{
        //    // Manually set the referenced objects to null to avoid EF complaining. Keep the actual foreign key instead: E.g. AreaId instead of Area.
        //    // Should probably use viewmodels for the data passed back and forth instead.
        //    var fkProps = newItem.GetType().GetProperties().Where(
        //        prop => Attribute.IsDefined(prop, typeof(System.ComponentModel.DataAnnotations.Schema.ForeignKeyAttribute)));
        //    foreach (var fkProp in fkProps)
        //        fkProp.SetValue(newItem, null);
        //}

        //private void UpdateChanges(int editVersionId, IQueryable<Site> currentItems, List<Site> changes)
        //{
        //    // TODO

        //    //// Current item that doesn't match a change: Deletion: TODO
        //    //// Either an addition has been removed again, or an existing item has been removed:
        //    //if (currentItem == null)
        //    //{
        //    //}
        //    //else
        //    //{ }

        //    //foreach (var change in changes)
        //    //{
        //    //    var currentItem = currentItems.SingleOrDefault(s => s.Id == currentChange.Id);

        //    //    if (IsRecordContentIdentical(current.Site, change))
        //    //    {
        //    //        // Unchanged, remove change record
        //    //        if (currentChange != null)
        //    //            _context.ChangeToSite.Remove(currentChange);
        //    //    }
        //    //    else
        //    //    {
        //    //        // Site data changed, update or create site change record
        //    //        if (currentChange != null)
        //    //            UpdateChangeFromData(currentChange, change);
        //    //        else
        //    //            _context.ChangeToSite.Add(CreateChangeFromData<ChangeToSite, Site>(change, editVersionId, false));
        //    //    }
        //    //}
        //}

        //private TChange CreateChangeFromData<TChange, TData>(TData data, int editVersionId, bool isDelete) where TChange : IRecord<TChange>, new()
        //{
        //    var result = new TChange()
        //    {
        //        EditVersionId = editVersionId,
        //        IsDelete = isDelete,
        //    };
        //    UpdateChangeFromData(result, data);
        //    return result;
        //}

        //private void UpdateChangeFromData<TChange, TData>(TChange change, TData data) where TChange : IRecord<TChange>
        //{
        //    CopyStringOrValueProperties(data, change);
        //}

        //private static void CopyRecordContent<T>(T source, T target) where T : class, IRecord
        //{
        //    var ignore = typeof(IRecord).GetProperties()
        //   .Select(p => p.Name)
        //   .ToList();

        //    foreach (var propInfo in typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
        //        if (!ignore.Contains(propInfo.Name) && propInfo.PropertyType.IsValueType || propInfo.PropertyType == "".GetType()) // Simple types only - is this what we want?
        //        {
        //            object sourceValue = propInfo.GetValue(source, null);
        //            if (propInfo != null && propInfo.CanWrite)
        //                propInfo.SetValue(target, sourceValue);
        //        }
        //}

        // TODO: Split api above into api controller

        /// <summary>
        /// List all draft edits for this user.
        /// </summary>
        public async Task<IActionResult> Drafts([FromQuery] string filter)
        {
            bool isReviews = filter?.ToUpperInvariant() == "REVIEWS"; // TODO: Split this into its own DraftReviews endpoint.

            // Get all of this user's draft edits
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var editIds = _context.EditVersion
                .WhereIsDraftBy(user)
                .Where(e => isReviews
                    ? e.EditVersionNumber > 1
                    : e.EditVersionNumber == 1)
                .Select(e => e.EditId);

            var model = new DraftsListViewModel
            {
                Edits = await GetEditSummaryListItemsAsync(editIds),
                IsReviews = isReviews,
            };

            return View(model);
        }

        private async Task<IEnumerable<EditSummaryListItem>> GetEditSummaryListItemsAsync(IEnumerable<int> editIds)
        {
            var editGroups = await _context.EditVersion
                .Where(e =>
                    editIds.Contains(e.EditId))
                .GroupBy(ev => ev.EditId)
                .ToListAsync();

            return editGroups.Select(g => new EditSummaryListItem
            {
                Id = g.Key,
                EditVersions = g.OrderBy(e => e.EditVersionNumber).ToList(),
            });
        }

        /// <summary>
        /// List all submitted edits for this user.
        /// </summary>
        public async Task<IActionResult> Submissions()
        {
            // Get all of this user's submissions
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var editIds = _context.EditVersion
                .Where(e =>
                    e.AuthorId == user.Id
                    && e.EditVersionNumber == 1
                    && e.StatusId != "D")
                .Select(e => e.EditId);

            var model = new EditsListViewModel
            {
                Edits = await GetEditSummaryListItemsAsync(editIds),
            };
            return View(model);
        }

        // TODO: Some repeated code between this and the method above - refactor
        /// <summary>
        /// Choose from all draft edits for this user.
        /// </summary>
        public async Task<IActionResult> ChooseDraft([FromQuery] string chooseForUrl)
        {
            if (string.IsNullOrEmpty(chooseForUrl))
                throw new ArgumentNullException(nameof(chooseForUrl));

            if (!Url.IsLocalUrl(chooseForUrl))
                return ValidationProblem($"{nameof(chooseForUrl)} must be local.");

            // Get all of this user's draft edits
            var user = await _userManager.GetUserAsync(HttpContext.User);
            var editIds = _context.EditVersion
                .WhereIsDraftBy(user)
                .Select(e => e.EditId);

            var model = new ChooseDraftViewModel
            {
                Edits = await GetEditSummaryListItemsAsync(editIds),
                ChooseForUrl = chooseForUrl,
                CustomButtons = new List<CustomButtonViewModel<EditSummaryListItem>>
                {
                    new CustomButtonViewModel<EditSummaryListItem>
                    {
                        Text = "Continue this edit",
                        Href = (edit) => ($"{chooseForUrl}?editVersion={edit.LatestEditVersion.EditIdAndVersionNumber}"),
                    },
                },
            };

            return View(model);
        }

        /// <summary>
        /// Get all unclaimed reviews (== submitted edits) which the current user could claim.
        /// </summary>
        public async Task<IActionResult> ClaimableReviews()
        {
            var claimableReviewIds = (await _editService.GetLatestEditVersionsAsync(e => e.StatusId == "S"))
                .Select(r => r.EditId)
                .ToList();
            var editIds = new List<int>();
            var user = await _userManager.GetUserAsync(HttpContext.User);
            foreach (var claimableReviewId in claimableReviewIds)
            {
                var info = await _editService.GetEditSummaryModel(claimableReviewId, user);
                if (info.UserPrivileges.EditorsWhoAreCurrentUser.Any())
                    editIds.Add(info.EditVersion.EditId);
            }

            var model = new DraftsListViewModel
            {
                Edits = await GetEditSummaryListItemsAsync(editIds),
                IsReviews = false,
            };

            return View(model);
        }

        [Authorize(Roles = Permissions.AdminRoleName)]
        [HttpGet("[controller]/All/{statusName}")]
        public async Task<IActionResult> All([FromRoute] string statusName)
        {
            var status = EditVersionStatus.All.SingleOrDefault(s => s.Name == statusName);
            if (status == null)
                return NotFound();
            var editIds = _context.EditVersion
                .GroupBy(ev => ev.EditId)
                .Select(e => e.Single(ev1 => ev1.EditVersionNumber == e.Max(ev2 => ev2.EditVersionNumber)))
                .ToList()
                .Where(ev => ev.StatusId == status.Id)
                .Select(ev => ev.EditId);

            var model = new EditsAdminViewModel
            {
                Edits = await GetEditSummaryListItemsAsync(editIds),
                Status = status,
            };

            return View("All", model);
        }

        [HttpGet("[controller]/{editId:int}")]
        public async Task<IActionResult> Get([FromRoute] int editId)
        {
            var currentUser = await _userManager.GetUserAsync(HttpContext.User);

            EditSummaryModel workflowInfo;
            try
            {
                workflowInfo = await _editService.GetEditSummaryModel(editId, currentUser);
            }
            catch (ValidationException valEx)
            {
                return ValidationProblem(valEx.Message);
            }

            if (workflowInfo == null)
                return View("Summary", null); // Edit does not exist.

            if (!workflowInfo.UserPrivileges.CanView)
                return Forbid();

            return View("Summary", workflowInfo);
        }

        [HttpPost("[controller]/{editId:int}")]
        public async Task<IActionResult> Post([FromRoute] int editId, [FromForm] string action, [FromForm] string role, [FromForm] string editVersionComment, [FromForm] string versionComment)
        {
            var currentUser = await _userManager.GetUserAsync(HttpContext.User);

            EditSummaryModel workflowInfo;
            try
            {
                workflowInfo = await _editService.GetEditSummaryModel(editId, currentUser);
            }
            catch (ValidationException valEx)
            {
                return ValidationProblem(valEx.Message);
            }

            if (workflowInfo == null)
                return View("Summary", workflowInfo);

            if (!workflowInfo.UserPrivileges.CanView)
                return Forbid();

            if (action == null)
                return View("Summary", workflowInfo);

            // TODO: This should be split into two procedures here -----8<----------------------

            var editAction = workflowInfo.PossibleActions.SingleOrDefault(a => a.Name == action);
            if (editAction == null)
                return BadRequest($"Action \"{action}\" is not available.");

            string authorRoleDesc; // "User", "SAFA member 80123", "Editor for Dynasoarers", "Editor for Victoria" etc.
            if (!editAction.RequiresEditRole.Any())
                authorRoleDesc = "Unprivileged";
            else
            {
                if (!Enum.TryParse(role, out EditActionRole asRoleEnum) || !editAction.RequiresEditRole.Any(r => r == asRoleEnum))
                {
                    return new ForbidResult($"This action is not available to the requested role \"{role}\".");
                    //this.AddMessage(MessageType.Error, "Invalid action", $"The action \"{actionEnum.Humanize(LetterCasing.LowerCase)}\" is not available.");
                    //return View("Summary", workflowInfo);
                }

                authorRoleDesc = asRoleEnum switch
                {
                    EditActionRole.DirectAdmin => asRoleEnum.Humanize() + ": " + workflowInfo.UserPrivileges.EditorsWhoAreCurrentUser
                        .Where(e => e.IsClosest)
                        .SelectMany(e => e.Reasons)
                        .Where(r => r.IsClosest)
                        .Select(r => r.ShortDescription)
                        .Distinct()
                        .Humanize(),
                    EditActionRole.IndirectAdmin => asRoleEnum.Humanize() + ": " + workflowInfo.UserPrivileges.EditorsWhoAreCurrentUser
                        .SelectMany(e => e.Reasons)
                        .Where(r => !r.IsClosest)
                        .Select(r => r.ShortDescription)
                        .Distinct()
                        .Humanize(),
                    EditActionRole.SystemAdmin => asRoleEnum.Humanize(),

                    _ => throw new NotImplementedException($"Unhandled {nameof(EditActionRole)} {asRoleEnum}."),
                };
            }

            // Perform requested action:
            var directAdmins = workflowInfo.Editors.Where(e => e.IsClosest);
            var allEditors = workflowInfo.Editors.Concat(workflowInfo.PartialEditors);

            switch (editAction.Name)
            {
                case string n when n == EditActions.SubmitForReview.Name:
                    // Validate
                    if (workflowInfo.EditVersion.EditVersionNumber == 1 && !_versionChangesDescriptorService.HasChanges(workflowInfo.EditVersion))
                    {
                        this.AddMessage(MessageType.Warning, "No Changes", "Your edit cannot be submitted for review because it contains no changes.");
                        break;
                    }
                    var validationResults = _editService.Validate(workflowInfo.EditVersion.Id).ToList();
                    if (validationResults.Any())
                    {
                        AddValidationMessage(validationResults);
                        break;
                    }

                    await _editService.SubmitDraftAsync(workflowInfo.EditVersion, editVersionComment, authorRoleDesc, currentUser.Email, workflowInfo.Editors);

                    this.AddMessage(MessageType.Success, "Edit Submitted", "Your edit has been submitted for review. Thank you!");

                    break;

                case string n when n == EditActions.Abandon.Name:
                    await _editService.AbandonDraftAsync(workflowInfo.EditVersion.Id, directAdmins);

                    this.AddMessage(MessageType.Success, "Edit Abandoned", "Your edit has been abandoned.");
                    if (workflowInfo.EditVersion.EditVersionNumber == 1)
                        return RedirectToAction("Index", "Home"); // We have abandoned the only edit version, so there's nothing to see anymore on this page. Redirect to the home page.
                    break;

                case string n when n == EditActions.ClaimForReview.Name:
                    await _editService.CreateReviewEditVersionAsync(workflowInfo.EditVersion.EditId, currentUser, authorRoleDesc);

                    this.AddMessage(MessageType.Success, "Edit Claimed for Review", "You have claimed this edit for review. You have exclusive access until you complete your review, so please don't take more time than necessary. Thanks!");
                    break;

                case string n when n == EditActions.Accept.Name:
                    validationResults = _editService.Validate(workflowInfo.EditVersion.Id).ToList();
                    if (validationResults.Any())
                    {
                        AddValidationMessage(validationResults);
                        break;
                    }
                    var draftEditVersionForAccept = await GetOrCreateDraftVersionAsync(currentUser, workflowInfo.EditVersion, authorRoleDesc);
                    await _editService.AcceptDraftAsync(draftEditVersionForAccept, editVersionComment, versionComment, authorRoleDesc, currentUser.Email, allEditors);

                    this.AddMessage(MessageType.Success, "Edit Applied", "The edit has been accepted and applied. Thank you.");
                    break;

                case string n when n == EditActions.Reject.Name:
                    var draftEditVersionForReject = await GetOrCreateDraftVersionAsync(currentUser, workflowInfo.EditVersion, authorRoleDesc);
                    await _editService.RejectDraftAsync(draftEditVersionForReject, editVersionComment, authorRoleDesc, currentUser.Email, allEditors);

                    this.AddMessage(MessageType.Success, "Edit Rejected", "The edit has been rejected.");
                    break;

                case string n when n == EditActions.ForwardToSubmitter.Name: // We may want to let the user choose whom to forward to?
                    var forwardToUser = await _userManager.FindByIdAsync(workflowInfo.EditVersions.First().AuthorId);
                    if (forwardToUser == null)
                    {
                        this.AddMessage(MessageType.Warning, "Could not forward", "There is no user associated with the original submission. The user account may have been deleted.");
                        break;
                    }
                    var draftEditVersionForForward = await GetOrCreateDraftVersionAsync(currentUser, workflowInfo.EditVersion, authorRoleDesc);
                    await _editService.SubmitDraftAsync(draftEditVersionForForward, editVersionComment, authorRoleDesc, currentUser.Email, workflowInfo.Editors);
                    await _editService.CreateReviewEditVersionAsync(workflowInfo.EditVersion.EditId, forwardToUser);
                    // TODO
                    //await NotifySubscribersAsync(workflowInfo.Editors.Concat(workflowInfo.PartialEditors), newEditVersion, editAction, currentUser);

                    this.AddMessage(MessageType.Success, "Edit Forwarded to Submitter", "The edit has been forwarded to the submitter.");
                    break;

                default:
                    throw new NotImplementedException();
            }

            return RedirectToRoute(new { });
        }

        private void AddValidationMessage(List<EntityValidationResult> validationResults)
        {
            string issuesDescription = string.Join("\r\n",
                validationResults.Select(e =>
                {
                    var validationResultTexts = new List<string>();
                    foreach (var validationResult in e.ValidationResults)
                        validationResultTexts.Add($"\t{validationResult.ErrorMessage}");
                    string issues = $"{string.Join("\r\n", validationResultTexts)}";
                    return $"{e.EntityName}:\r\n{issues}";
                }));
            this.AddMessage(MessageType.Warning, "Validation errors", $"Please correct the following issues:\r\n{issuesDescription}");
        }

        private async Task<EditVersion> GetOrCreateDraftVersionAsync(IdentityUser currentUser, EditVersion editVersion, string authorRoleDesc)
        {
            switch (editVersion.StatusId)
            {
                case "S":
                    var draftEditVersion = await _editService.CreateReviewEditVersionAsync(editVersion.EditId, currentUser, authorRoleDesc);
                    return draftEditVersion;
                case "D":
                    return editVersion;

                default: throw new InvalidOperationException($"Invalid status id \"{editVersion.StatusId}\" for this operation.");
            }
        }

        //// POST: Sites/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,AreaId,Name,Closed,Rating,Height,Type,Restrictions,ShortLocation,ExtendedLocation,Conditions,Description,Takeoff,Landing,Landowners,FlightComments,HazardsComments,InsetMapPositionId,SmallPicture,SmallPictureHref,SmallPictureTitle,LastReviewDate")] Site site)
        //{
        //    if (id != site.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(site);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!SiteExists(site.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["AreaId"] = new SelectList(_context.Area, "Id", "Name", site.AreaId);
        //    ViewData["InsetMapPositionId"] = new SelectList(_context.InsetMapPosition, "Id", "Name", site.InsetMapPositionId);
        //    return View(site);
        //}

        //// GET: Sites/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var site = await _context.Site
        //        .Include(s => s.Area)
        //        .Include(s => s.InsetMapPosition)
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (site == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(site);
        //}

        //// POST: Sites/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var site = await _context.Site.FindAsync(id);
        //    _context.Site.Remove(site);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}
    }

    public class AddVideoInfo
    {
        public int EditId { get; set; }
        public int SiteId { get; set; }
        public int TypeId { get; set; }
        public string Identifier { get; set; }
    }

    public class AddEntityInfo
    {
        public int EditId { get; set; }
        public string Entity { get; set; }
        public int SiteId { get; set; }
    }

    public class AddSiteManyToManyInfo : AddEntityInfo
    {
        public int ForeignKeyId { get; set; }
    }

    public interface IRecordIdInfo
    {
        /// <summary>
        /// Id of a primary record or Site id for many to many record.
        /// </summary>
        public int PrimaryId { get; set; }
        /// <summary>
        /// Empty for a primary record or foreign key id for many to many record.
        /// </summary>
        public int? SecondaryId { get; set; }
    }
    public class DeleteEntityInfo : IRecordIdInfo
    {
        public int EditId { get; set; }
        public string Entity { get; set; }
        /// <summary>
        /// Id of a primary record or Site id for many to many record.
        /// </summary>
        public int PrimaryId { get; set; }
        /// <summary>
        /// Empty for a primary record or foreign key id for many to many record.
        /// </summary>
        public int? SecondaryId { get; set; }
    }

    public class EditFieldInfo : IRecordIdInfo
    {
        public int EditId { get; set; }
        public string Entity { get; set; }
        /// <summary>
        /// Id of a primary record or Site id for many to many record.
        /// </summary>
        public int PrimaryId { get; set; }
        /// <summary>
        /// Empty for a primary record or foreign key id for many to many record.
        /// </summary>
        public int? SecondaryId { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
    }

    public class EditLaunchCoordsInfo
    {
        public int EditId { get; set; }
        public int LaunchId { get; set; }
        public decimal Lat { get; set; }
        public decimal Lon { get; set; }
    }
}
