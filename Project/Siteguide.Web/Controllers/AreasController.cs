﻿using Microsoft.AspNetCore.Mvc;

namespace SiteGuide.Web.Controllers
{
    public class AreasController : Controller
    {
        //public IActionResult Index() => View();
        public IActionResult Details(string id)
        {
            if (id == "Tasmania") // Hard coded for now - TODO: Expand
                return View(id);
            return NotFound();
        }
    }
}
