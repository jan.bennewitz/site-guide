﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Threading.Tasks;
using DiffMatchPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Diagnostics;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.Extensions.Configuration;
using SiteGuide.Web.Helpers;
using SiteGuide.Web.Models;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using Version = SiteGuide.DAL.Models.Version;
using SiteGuide.DAL;
using Microsoft.AspNetCore.Identity;
using SiteGuide.Service.Identifiers;
using SiteGuide.Service;
using SiteGuide.Web.Extensions;
using Siteguide.Web.Authorisation;
using Microsoft.AspNetCore.Authorization;
using SiteGuide.Service.Helpers;

namespace SiteGuide.Web.Controllers
{
    public class VersionsController : Controller
    {
        private readonly SiteGuideContext _context;
        private readonly SiteService _siteService;
        private readonly VersionChangesDescriptorService _versionChangesService;
        private readonly VersionService _versionService;
        private readonly UserManager<IdentityUser> _userManager;

        public VersionsController(SiteGuideContext context, VersionChangesDescriptorService versionChangesService, VersionService versionService, UserManager<IdentityUser> userManager, SiteService siteService)
        {
            _context = context;
            _versionChangesService = versionChangesService;
            _versionService = versionService;
            _userManager = userManager;
            _siteService = siteService;

        }

        [AuthoriseSafaMember]
        public async Task<IActionResult> Index([FromQuery] string filterForSite)
        {
            if (string.IsNullOrEmpty(filterForSite))
                return View();

            // Ensure that there is a single match for this site
            var matches = await _siteService.FindSiteByNameAsync(filterForSite);
            if (matches.Count() != 1)
                return RedirectToAction("Search", "Sites", new { id = filterForSite, version = (string)null });
            var siteName = matches.Single().Name;

            return View((object)siteName);
        }

        [AuthoriseSafaMember]
        public async Task<IActionResult> Details(int id, int? compareToVersionId = null)
        {
            compareToVersionId ??= id - 1; // By default, compare to immediately preceding version

            var thisVersionInfo = await _versionService.GetVersionInfoAsync(id);
            var baseVersionInfo = await _versionService.GetVersionInfoAsync(compareToVersionId);

            if (thisVersionInfo is null)
                throw new KeyNotFoundException($"Version {id} does not seem to exist."); // TODO: Show useful message to user.

            // If baseVersionInfo is null, assume we're comparing to initial empty state.
            //if (baseVersionInfo is null)
            //    throw new KeyNotFoundException($"Version {compareToVersionId} does not seem to exist."); // TODO: Show useful message to user.

            //var thisVersionOrEditVersionId = new VersionIdentifier(id);

            var model = new VersionDetailsModel
            {
                ThisVersion = thisVersionInfo,
                CompareToVersion = baseVersionInfo,
                PreviousVersionExists = baseVersionInfo != null,
                NextVersionExists = _context.Version.Any(v => v.Id == thisVersionInfo.Id + 1),
            };

            return View(model);
        }

        /// <summary>
        /// Get list of all versions. If site is specified, only return those versions that changed the specified site.
        /// </summary>
        [AuthoriseSafaMember]
        [HttpGet("VersionsListPartial")]
        [HttpGet("VersionsListPartial/{siteName}")]
        public async Task<IActionResult> VersionsListPartial([FromRoute] string siteName) // TODO: We should probably use the id here rather than the name. Name could be duplicate over version history.
        {
            IEnumerable<VersionInfo> versionInfos;
            if (siteName is null)
                versionInfos = (await _versionService.GetVersionInfosAsync());
            else
            {
                var siteMatches = await _siteService.FindSiteByNameAsync(siteName);

                if (siteMatches.Count() != 1)
                    //return RedirectToAction("Search", new { id = matches[0].Name, version = (string)null });
                    throw new ArgumentException($"{nameof(siteName)} must exactly match the name of a specific site.");

                var site = siteMatches.Single();
                versionInfos = (await _versionService.GetVersionsForSiteAsync(site.Id));
            }

            return PartialView("_Versions", versionInfos.OrderByDescending(i => i.Id));
        }

        [AuthoriseSafaMember]
        [HttpGet("[controller]/Compare/{editId}.{editVersionNumber}")]
        public async Task<IActionResult> Compare([FromRoute] int editId, [FromRoute] int? editVersionNumber, [FromQuery] int? compareToVersion = null)
        {
            var currentUser = await _userManager.GetUserAsync(HttpContext.User);
            var model = await _versionChangesService.GetEditVersionChangesDetailsAsync(editId, editVersionNumber, compareToVersion, currentUser);
            if (model is null)
                return NotFound();
            if (model.ThisEdit.StatusId == "D" && !model.IsEditableByCurrentUser)
                return Forbid();

            return View(model);
        }

        //[AuthoriseSafaMember] // Allow for SAFA members or author
        [HttpGet("DetailsOfEditPartial/{treeNodeId}")]
        public async Task<IActionResult> DetailsOfEditPartial([FromRoute] string treeNodeId, [FromQuery] int? compareToVersion = null, [FromQuery] bool showViewButtons = false, [FromQuery] bool showEditButton = false)
        {
            var nodeId = VersionTreeNodeIdentifier.FromIdentifier(treeNodeId, _context);
            if (nodeId is null)
            {
                this.AddMessage(MessageType.Error, $"Edit {treeNodeId} does not exist.", "It may have been abandoned."); // TODO - wire up. Currently not displayed.
                return NotFound();
            }
            if (!(Permissions.IsSafaMember(User) || Permissions.IsAdmin(User)))
            {
                // If not a safa member of admin, then only show user's own edit versions and disallow compares
                if (compareToVersion != null)
                    return Unauthorized();
                switch (nodeId)
                {
                    case EditVersionIdentifier editVersionIdentifier:
                        var authorId = _context.EditVersion.Single(ev => ev.Id == editVersionIdentifier.Id).AuthorId;
                        if (authorId != _userManager.GetUserId(User))
                            return Unauthorized();
                        break;
                    case VersionIdentifier:
                        return Unauthorized();
                    default: throw new NotImplementedException();
                }
            }

            compareToVersion ??= nodeId switch // If compare to version isn't specified, set default:
            {
                EditVersionIdentifier editVersionId => editVersionId.ReferenceVersion,
                VersionIdentifier versionId => (await _context.Version.SingleOrDefaultAsync(v => v.Id == versionId.Id - 1))?.Id, // Immediately preceding version, if any
                _ => throw new NotImplementedException(),
            };

            var model = new ChangesViewModel
            {
                GroupedChanges = _versionChangesService.GetGroupedChanges(nodeId, compareToVersion),
                ThisVersionId = nodeId,
                CompareToVersionId = compareToVersion,
                ShowButtons = showViewButtons,
                ShowEditButton = showEditButton,
            };

            return PartialView("_Changes", model);
        }
    }
}
