﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SiteGuide.Web.Models;
using SiteGuide.DAL;

namespace SiteGuide.Web.Controllers
{
    public class ArticleController : Controller
    {
        //public IActionResult Index() => View();
        public IActionResult NotesForSiteManagers() => View();
        public IActionResult SiteDevelopment() => View();
        public IActionResult Hoodies() => View();
    }
}
