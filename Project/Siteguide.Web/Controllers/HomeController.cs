﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SiteGuide.Web.Models;
using SiteGuide.DAL;

namespace SiteGuide.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly SiteGuideContext _context;
        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}
        public HomeController(SiteGuideContext context)
        {
            _context = context;
        }
        public IActionResult Index() => View();

        public IActionResult Disclaimer() => View();

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() => View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
