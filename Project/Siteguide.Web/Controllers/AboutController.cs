﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SiteGuide.Service;
using SiteGuide.Web.Models;
using SiteGuide.DAL;

namespace SiteGuide.Web.Controllers
{
    public class AboutController : Controller
    {
        private readonly SiteGuideContext _context;
        private readonly VersionService _versionService;

        public AboutController(SiteGuideContext context, VersionService versionService)
        {
            _context = context;
            _versionService = versionService;
        }

        public async Task<IActionResult> IndexAsync()
        {
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            DateTime buildDate = new DateTime(2000, 1, 1)
                .AddDays(version.Build)
                .AddSeconds(version.Revision * 2);

            var versionInfo = await _versionService.GetVersionInfoAsync();

            var model = new AboutViewModel
            {
                StateAssociations = _context.ExtCacheClub
                    .Where(i => i.Category == "a")
                    .ToList(),

                SoftwareVersion = version,
                SoftwareBuildDate = buildDate,
                DataVersion = versionInfo.Id,
                DataVersionDateTime = versionInfo.PublishedTime,
            };

            return View(model);
        }
    }
}
