﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SiteGuide.Service;
using SiteGuide.Web.Helpers;
using SiteGuide.Web.Models;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using SiteGuide.Service.Identifiers;
using Version = SiteGuide.DAL.Models.Version;
using SiteGuide.Service.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using Siteguide.Web.Authorisation;
using Siteguide.Web.Models;

namespace SiteGuide.Web.Controllers
{
    public class SitesController : Controller
    {
        private readonly SiteGuideContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SiteService _siteService;
        private readonly VersionService _versionService;
        private readonly IConfiguration _configuration;
        private readonly EditService _editService;

        public SitesController(SiteGuideContext context, UserManager<IdentityUser> userManager, VersionService versionService, SiteService siteService, EditService editService, IConfiguration configuration)
        {
            _context = context;
            _userManager = userManager;
            _versionService = versionService;
            _siteService = siteService;
            _editService = editService;
            _configuration = configuration;
        }

        // GET: Sites
        public async Task<IActionResult> Index()
        {
            var areas = await _context.Area
                .AsNoTracking()
                .ValidAtCurrentVersion()
                .OrderBy(a => a.ParentAreaId)
                .ThenBy(a => a.FullName ?? a.Name)
                .Select(a => new SitesIndexAreaViewModel
                {
                    Id = a.Id,
                    ParentAreaId = a.ParentAreaId,
                    Name = a.Name,
                    FullName = a.FullName ?? a.Name,
                    Areas = new List<SitesIndexAreaViewModel>(),
                    Sites = new List<SitesIndexSiteViewModel>(),
                })
                .ToListAsync();

            var areaDict = areas
                .ToDictionary(a => a.Id, a => a);

            var sites = await _context.Site
                .AsNoTracking()
                .ValidAtCurrentVersion()
                .OrderBy(s => s.AreaId)
                .ThenBy(s => s.Name)
                .Select(s => new SitesIndexSiteViewModel
                {
                    Id = s.Id,
                    AreaId = s.AreaId,
                    Name = s.Name,
                    ShortLocation = s.ShortLocation,
                    Type = s.Type,
                    Closed = s.Closed,
                    Conditions = s.Conditions,
                    Rating = s.Rating,
                    Height = s.Height,
                    LaunchNames = _context.Launch
                        .AsNoTracking()
                        .ValidAtCurrentVersion()
                        .Where(l => l.SiteId == s.Id && l.Name != null)
                        .Select(l => l.Name + (l.IsClosed ? " [Closed]" : ""))
                        .ToList(),
                })
                .ToListAsync();

            foreach (var area in areas)
                PopulateSitesIndexAreaViewModel(areaDict, area);

            foreach (var site in sites)
                areaDict[site.AreaId].Sites.Add(site);

            foreach (var area in areas)
            {
                // Add count of this area's sites to this area's counters and to all parent areas' counters.
                int openSitesCount = area.Sites.Count(s => string.IsNullOrEmpty(s.Closed));
                int closedSitesCount = area.Sites.Count(s => !string.IsNullOrEmpty(s.Closed));

                area.OpenSitesCount += openSitesCount;
                area.ClosedSitesCount += closedSitesCount;

                var thisArea = area;
                while (thisArea.ParentAreaId != null)
                {
                    thisArea = areaDict[(int)thisArea.ParentAreaId]; // Go to parent area

                    thisArea.OpenSitesCount += openSitesCount;
                    thisArea.ClosedSitesCount += closedSitesCount;
                }
            }

            return View(areas.Single(a => a.ParentAreaId == null));
        }

        private void PopulateSitesIndexAreaViewModel(IDictionary<int, SitesIndexAreaViewModel> areaDict, SitesIndexAreaViewModel area)
        {
            if (area.ParentAreaId == null)
                area.AncestorAreaNames = new List<string>();
            else
            {
                var parent = areaDict[(int)area.ParentAreaId];
                if (parent.AncestorAreaNames == null)
                    PopulateSitesIndexAreaViewModel(areaDict, parent);

                if (area.AncestorAreaNames == null)
                {
                    area.AncestorAreaNames = parent.AncestorAreaNames.Append(parent.FullName ?? parent.Name);
                    parent.Areas.Add(area);
                }
            }
        }

        // GET: Sites/Details/Airey
        public async Task<IActionResult> Details(string id, int? version = null, string editVersion = null) // TODO: Change all callers to use editVersion string instead of id.
        {
            // TODO: Does it ever make sense to specify both version and editVersion? Should probably just have one parameter instead of both version and editVersion.
            EditVersion editVersionObject = null;
            VersionTreeNodeIdentifier versionTreeNodeId = null;

            if (version != null)
                versionTreeNodeId = new VersionIdentifier((int)version);
            else if (editVersion != null)
            {
                versionTreeNodeId = EditVersionIdentifier.FromEditVersionString(editVersion, _context);
                editVersionObject = await _context.EditVersion
                    .Include(e => e.Status)
                    .SingleOrDefaultAsync(ev => ev.Id == ((EditVersionIdentifier)versionTreeNodeId).Id);
            }

            SiteInfo model;
            if (int.TryParse(id, out int siteId))
                model = await GetSiteViewModelAsync(_context, siteId, versionTreeNodeId);
            else
                model = await GetSiteViewModelAsync(_context, siteName: id, versionTreeNodeId);

            if (model == null)
                return RedirectToAction("Search", new { id, version, editVersion });
            //return NotFound();

            //if (site.Count() > 1)
            //    return View("Search", site);

            var parentAreaNames = AreaHelper.GetAreaPathNames(_context, model.Site.AreaId, versionTreeNodeId: versionTreeNodeId);
            ViewData.Add("Path", AreaHelper.AreaParentageDisplay(parentAreaNames));

            if (version != null)
            {
                var versionInfos = await _versionService.GetVersionsForSiteAsync(model.Site.Id);

                var thisAndPreceding = versionInfos
                    .Where(i => i.Id <= version)
                    .Reverse();
                var following = versionInfos
                    .Where(i => i.Id > version);

                model.LatestVersionId = _context.Version.Max(v => v.Id);
                model.VersionInfo = new NearVersionsInfo
                {
                    Preceding = thisAndPreceding.Skip(1).FirstOrDefault(),
                    This = thisAndPreceding.First(),
                    Next = following.FirstOrDefault(),
                    Latest = versionInfos.Reverse().First(),
                };
            }

            return View(model);
        }

        // TODO: Move to service
        public async Task<SiteInfo> GetSiteViewModelAsync(SiteGuideContext context, int siteId, VersionTreeNodeIdentifier versionTreeNodeIdentifier = null, HttpRequest request = null)
            => await GetSiteViewModelAsync(context, s => s.Id == siteId, versionTreeNodeIdentifier, request);
        public async Task<SiteInfo> GetSiteViewModelAsync(SiteGuideContext context, string siteName = null, VersionTreeNodeIdentifier versionTreeNodeIdentifier = null)
            => await GetSiteViewModelAsync(context, s => s.Name == siteName, versionTreeNodeIdentifier);

        private async Task<SiteInfo> GetSiteViewModelAsync(SiteGuideContext context, Func<Site, bool> siteSelector, VersionTreeNodeIdentifier versionTreeNodeIdentifier = null, HttpRequest request = null)
        {
            request ??= Request;
            var site = context.Site
                //.Include(s => s.Area)
                .Include(s => s.InsetMapPosition)
                .ValidAtVersionTreeNode(versionTreeNodeIdentifier)
                .SingleOrDefault(siteSelector);

            if (site == null)
                return null;

            var model = new SiteInfo()
            {
                Site = site,
                GMapsApiKey = _configuration.GetValue<string>("GMaps:ApiKey"),
                RequestedVersionTreeNode = versionTreeNodeIdentifier,
                SiteRootUrl = $"{request.Scheme}://{request.Host}",
            };
            model.Launches = context.Launch
                .Where(l => l.SiteId == model.Site.Id)
                .ValidAtVersionTreeNode(versionTreeNodeIdentifier)
                .ToList();
            model.MapShapes = context.SiteMapshape
                .Where(sm => sm.SiteId == model.Site.Id)
                .ValidAtVersionTreeNode(versionTreeNodeIdentifier)
                .ToList()
                .Join(
                    context.MapShape.Include(m => m.Category)
                        .ValidAtVersionTreeNode(versionTreeNodeIdentifier),
                    sm => sm.MapshapeId,
                    m => m.Id,
                    (sm, m) => m
                )
                .ToList();
            model.Contacts = context.SiteContact
                .Where(sc => sc.SiteId == model.Site.Id)
                .ValidAtVersionTreeNode(versionTreeNodeIdentifier)
                .ToList()
                .Join(
                    context.Contact
                        .ValidAtVersionTreeNode(versionTreeNodeIdentifier),
                    sc => sc.ContactId,
                    c => c.Id,
                    (sc, c) => new SiteContactInfo()
                    {
                        Contact = c,
                        IsContact = sc.IsContact,
                        IsResponsible = sc.IsResponsible,
                    }
                )
                .ToList();
            model.Videos = context.Video
                .Include(v => v.Type)
                .Where(v => v.SiteId == model.Site.Id)
                .ValidAtVersionTreeNode(versionTreeNodeIdentifier)
                .ToList();
            model.SeeAlsos = context.SeeAlso
                .Where(s => s.SiteId == model.Site.Id)
                .ValidAtVersionTreeNode(versionTreeNodeIdentifier)
                .ToList();

            model.AreaName = (await context.Area
                .ValidAtCurrentVersion()
                .SingleAsync(a => a.Id == site.AreaId))
                .Name;

            return model;
        }

        // GET: Sites/Search/stanwELl
        public async Task<IActionResult> Search(string id, int? version = null, string editVersion = null)
        {
            if (id is null)
                return View("Message", new MessageViewModel(MessageType.Warning, "Missing search term", $"No search term was provided."));

            var matches = await _siteService.FindSiteByNameAsync(id, version == null && editVersion == null ? null : VersionTreeNodeIdentifier.FromIdentifier(version != null ? version.ToString() : editVersion, _context));

            if (matches.Count == 1)
                return RedirectToAction("Details", new { id = matches[0].Name, version, editVersion });
            else
                return View(new SiteSearchResults { Matches = matches, SearchTerm = id });
        }

        //// GET: Sites/Create
        //[Authorize]
        //public IActionResult Create()
        //{
        //    throw new NotImplementedException();
        //    ViewData["AreaId"] = new SelectList(_context.Area, "Id", "Name");
        //    ViewData["InsetMapPositionId"] = new SelectList(_context.InsetMapPosition, "Id", "Name");
        //    return View();
        //}

        //// POST: Sites/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[Authorize]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("Id,AreaId,Name,Closed,Rating,Height,Type,Restrictions,ShortLocation,ExtendedLocation,Conditions,Description,Takeoff,Landing,Landowners,FlightComments,HazardsComments,InsetMapPositionId,SmallPicture,SmallPictureHref,SmallPictureTitle,LastReviewDate")] Site site)
        //{
        //    throw new NotImplementedException();
        //    //if (ModelState.IsValid)
        //    //{
        //    //    _context.Add(site);
        //    //    await _context.SaveChangesAsync();
        //    //    return RedirectToAction(nameof(Index));
        //    //}
        //    //ViewData["AreaId"] = new SelectList(_context.Area, "Id", "Name", site.AreaId);
        //    //ViewData["InsetMapPositionId"] = new SelectList(_context.InsetMapPosition, "Id", "Name", site.InsetMapPositionId);
        //    //return View(site);
        //}

        /// <param name="editVersion">Set this to EditController.CreateNewEditVersion if a new edit version should be created.</param>
        // GET: Sites/Edit/5/3
        [Authorize]
        //[HttpGet("{id}/Edit/{editVersion}")]       
        public async Task<IActionResult> Edit(int? id, [FromQuery] string editVersion)
        {
            if (id == null)
                return NotFound();

            var currentUser = await _userManager.GetUserAsync(HttpContext.User);
            if (editVersion == null)
            {
                // No edit id specified. Are there any existing draft edits for this user? If so, let the user choose, otherwise, create a new one.
                if (_editService.HasAnyDraft(currentUser))
                    return RedirectToAction(nameof(EditController.ChooseDraft), "Edit", new { chooseForUrl = Url.Action(nameof(Edit), new { id }) });
                else
                    editVersion = EditController.CreateNewEditVersion;
            }

            if (editVersion == EditController.CreateNewEditVersion)
            {
                var newEditVersion = await _editService.CreateDraftEditAsync(currentUser);
                return RedirectToAction(nameof(Edit), new { siteId = id, editVersion = newEditVersion.EditIdAndVersionNumber });
            }

            var editVersionIdentifier = EditVersionIdentifier.FromEditVersionString(editVersion, _context);
            if (editVersionIdentifier == null)
                return View("Message", new MessageViewModel(MessageType.Error, "Edit Version Not Found", $"The requested edit version \"{editVersion}\" does not seem to exist."));

            var latestEditVersion = await _context.EditVersion
                .Where(e => e.EditId == editVersionIdentifier.EditId)
                .OrderByDescending(e => e.EditVersionNumber)
                .FirstOrDefaultAsync();
            if (latestEditVersion == null)
                return NotFound(); // No such edit id
            if (latestEditVersion.EditVersionNumber != editVersionIdentifier.EditVersionNumber)
                return View("Message", new MessageViewModel(MessageType.Success, "Invalid request", "A newer version of this edit exists."));

            ViewBag.GmapsApiKey = _configuration.GetValue<string>("GMaps:ApiKey");

            if (latestEditVersion.StatusId != "D")
                // Change has already been accepted or rejected, nothing to do here.
                return View("Message", new MessageViewModel(MessageType.Error, "Invalid request", "The requested edit is currently not a draft and cannot be edited."));

            if (latestEditVersion.AuthorId != currentUser.Id)
                // Can't edit someone else's draft. 
                // TODO: We may want to enable editors to deal with a too long-lived draft?
                // TODO: For editors, should at least let the user know what's happening and who currently has a draft open.
                return Forbid();

            return View();
        }

        //// POST: Sites/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[Authorize]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,AreaId,Name,Closed,Rating,Height,Type,Restrictions,ShortLocation,ExtendedLocation,Conditions,Description,Takeoff,Landing,Landowners,FlightComments,HazardsComments,InsetMapPositionId,SmallPicture,SmallPictureHref,SmallPictureTitle,LastReviewDate")] Site site)
        //{
        //    if (id != site.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(site);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!SiteExists(site.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["AreaId"] = new SelectList(_context.Area, "Id", "Name", site.AreaId);
        //    ViewData["InsetMapPositionId"] = new SelectList(_context.InsetMapPosition, "Id", "Name", site.InsetMapPositionId);
        //    return View(site);
        //}

        //// GET: Sites/Delete/5
        //[Authorize]
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var site = await _context.Site
        //        .Include(s => s.Area)
        //        .Include(s => s.InsetMapPosition)
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (site == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(site);
        //}

        //// POST: Sites/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var site = await _context.Site.FindAsync(id);
        //    _context.Site.Remove(site);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        [AuthoriseSafaMember]
        public async Task<IActionResult> Create([FromQuery] string editVersion, [FromQuery] int? areaId)
        {
            var currentUser = await _userManager.GetUserAsync(HttpContext.User);
            if (editVersion == null)
            {
                // No edit id specified. Are there any existing draft edits for this user? If so, let the user choose, otherwise, create a new one.
                if (_editService.HasAnyDraft(currentUser))
                    return RedirectToAction(nameof(EditController.ChooseDraft), "Edit", new { chooseForUrl = Url.Action(nameof(Create)) });
                else
                    editVersion = EditController.CreateNewEditVersion;
            }

            if (editVersion == EditController.CreateNewEditVersion)
            {
                var newEditVersion = await _editService.CreateDraftEditAsync(currentUser);
                return RedirectToAction(nameof(Create), new { editVersion = newEditVersion.EditIdAndVersionNumber });
            }

            var editVersionIdentifier = EditVersionIdentifier.FromEditVersionString(editVersion, _context);
            if (editVersionIdentifier == null)
                return View("Message", new MessageViewModel(MessageType.Error, "Edit Version Not Found", $"The requested edit version \"{editVersion}\" does not seem to exist."));

            var latestEditVersion = await _context.EditVersion
                .Where(e => e.EditId == editVersionIdentifier.EditId)
                .OrderByDescending(e => e.EditVersionNumber)
                .FirstOrDefaultAsync();
            if (latestEditVersion == null)
                return NotFound(); // No such edit id
            if (latestEditVersion.EditVersionNumber != editVersionIdentifier.EditVersionNumber)
                return View("Message", new MessageViewModel(MessageType.Success, "Invalid request", "A newer version of this edit exists."));

            if (areaId == null)
            {
                var model = new RecursiveAreaModel
                {
                    AllAreasByParentId = _context.Area
                        .ValidAtCurrentVersion()
                        .ToLookup(a => a.ParentAreaId),

                    LinkHref = (area) => Url.Action(nameof(Create), new { areaId = area.Id, editVersion = latestEditVersion.EditIdAndVersionNumber }),
                };

                return View(model);
            }

            var newSite = await _editService.CreateRecord<Site>(latestEditVersion, setFieldsAsync: async (s) => s.AreaId = (int)areaId);

            return RedirectToAction(nameof(Edit), new { id = newSite.Id, editVersion = latestEditVersion.EditIdAndVersionNumber });
        }

        //private bool SiteExists(int id)
        //{
        //    return _context.Site.Any(e => e.Id == id);
        //}

        //API
        // Export data for Craig Payne paraglidingmap.com

        /// <response code="301">Permanent redirect to /api/Export</response>
        [HttpGet("/Sites/Export")]
        [ProducesResponseType(StatusCodes.Status301MovedPermanently)]
        public IActionResult ExportRedirect() => new RedirectResult("/api/Export", true);

        [HttpGet("/api/Export")]
        public async Task<IActionResult> Export([FromQuery] int? compareToVersion, [FromQuery] string compareToTime)
        {
            VersionInfo compareToVersionFromVersion = null, compareToVersionFromTime = null;
            if (compareToVersion != null)
            {
                compareToVersionFromVersion = await _versionService.GetVersionInfoAsync(compareToVersion);
                if (compareToVersionFromVersion == null)
                    return new BadRequestObjectResult($"No version was found matching parameter {nameof(compareToVersion)}.");
            }
            if (compareToTime != null)
            {
                if (!DateTime.TryParse(compareToTime, null, System.Globalization.DateTimeStyles.RoundtripKind, out var compareToTimeUtc))
                    return new BadRequestObjectResult($"Parameter {nameof(compareToTime)} must be in ISO 8601 format, e.g. 2023-04-25T02:07:34Z.");
                if (compareToTimeUtc.Kind != DateTimeKind.Utc)
                    return new BadRequestObjectResult($"Parameter {nameof(compareToTime)} must be a UTC date time, e.g. 2023-04-25T02:07:34Z.");
                compareToVersionFromTime = await _versionService.GetVersionInfoAsync(compareToTimeUtc);
                if (compareToVersionFromTime == null)
                    return new BadRequestObjectResult($"No version was found matching parameter {nameof(compareToTime)}.");
            }
            if (compareToVersionFromVersion != null && compareToVersionFromTime != null && compareToVersionFromVersion.Id != compareToVersionFromTime.Id)
                return new BadRequestObjectResult(new
                {
                    Message = $"Parameters {nameof(compareToVersion)} and {nameof(compareToTime)} have both been specified, but specify different versions.",
                    VersionFromCompareToVersion = $"Version {compareToVersionFromVersion.Id} published {compareToVersionFromVersion.PublishedTime}",
                    VersionFromCompareToTime = $"Version {compareToVersionFromTime.Id} published {compareToVersionFromTime.PublishedTime}",
                });
            var compareTo = (compareToVersionFromVersion ?? compareToVersionFromTime);

            var exportDataCompareTo = await GetExportDataAsync(compareTo.Id);
            var exportDataCurrent = await GetExportDataAsync();

            var versionInfo = await _versionService.GetVersionInfoAsync();

            return Ok(new
            {
                version = new
                {
                    versionInfo.Id,
                    versionInfo.Comment,
                    published = versionInfo.PublishedTime,
                },
                compareToVersion = compareTo == null ? null : new
                {
                    compareTo.Id,
                    compareTo.Comment,
                    published = compareTo.PublishedTime,
                },
                //sites,
                //areas,
                //mapShapes
            });
        }

        private async Task<dynamic> GetExportDataAsync(int? version = null)
        {
            var versionIdentifier = version == null ? null : new VersionIdentifier((int)version);
            var sites = new List<dynamic>();
            foreach (var site in _context.Site
                .ValidAtVersion(version)
                .OrderBy(s => s.Id)
                .ToList())
            {
                var siteData = await GetSiteViewModelAsync(_context, siteName: site.Name, versionIdentifier);
                sites.Add(new
                {
                    siteData.Site.Id,
                    siteData.Site.AreaId,
                    siteData.Site.Name,
                    siteData.Site.Closed,
                    siteData.Site.Rating,
                    siteData.Site.Height,
                    siteData.Site.Type,
                    siteData.Site.Restrictions,
                    siteData.Site.ShortLocation,
                    siteData.Site.ExtendedLocation,
                    siteData.Site.Conditions,
                    siteData.Site.Description,
                    siteData.Site.Takeoff,
                    siteData.Site.Landing,
                    siteData.Site.Landowners,
                    siteData.Site.FlightComments,
                    siteData.Site.HazardsComments,
                    InsetMapPosition = new
                    {
                        site.InsetMapPosition.Id,
                        site.InsetMapPosition.Name
                    },
                    siteData.Site.SmallPicture,
                    siteData.Site.SmallPictureTitle,
                    siteData.Site.SmallPictureHref,
                    Launches = siteData.Launches
                        .OrderBy(l => l.Id)
                        .Select(l => new
                        {
                            l.Id,
                            l.Name,
                            l.Closed,
                            l.Conditions,
                            l.Description,
                            l.Lat,
                            l.Lon,
                            l.EstaEmergencyMarker
                        }),
                    MapShapeIds = siteData.MapShapes
                        .OrderBy(m => m.Id)
                        .Select(s => s.Id),
                    Contacts = siteData.Contacts
                        .OrderBy(c => c.IsContact)
                        .ThenBy(c => c.IsResponsible)
                        .ThenBy(c => c.Contact.Id)
                        .Select(c => new
                        {
                            c.IsContact,
                            c.IsResponsible,
                            c.Contact.Id,
                            c.Contact.Name,
                            c.Contact.FullName,
                            c.Contact.Link,
                            c.Contact.Address,
                            c.Contact.Phone,
                            c.Contact.SafaClubId,
                        }),
                    Videos = siteData.Videos
                        .OrderBy(v => v.Id)
                        .Select(v => new
                        {
                            v.Id,
                            Type = new
                            {
                                v.Type.Id,
                                v.Type.Name
                            },
                            v.Identifier,
                        }),
                    SeeAlsos = siteData.SeeAlsos
                        .OrderBy(a => a.Id)
                        .Select(a => new
                        {
                            a.Id,
                            a.Name,
                            a.Link,
                            a.IsEntryStubForThis,
                        }),
                });
            }

            var areas = GetAreaInfo(_context.Area.ValidAtVersion(version).Single(a => a.ParentAreaId == null));

            var mapShapes = _context.MapShape
                .AsNoTracking()
                .ValidAtVersion(version)
                .OrderBy(s => s.Id)
                .Select(s => new
                {
                    s.Id,
                    s.AreaId,
                    s.Name,
                    s.Description,
                    Category = new
                    {
                        s.Category.Id,
                        s.Category.Name
                    },
                    s.CoordinatesList,
                    s.EstaEmergencyMarker,
                });

            return new
            {
                sites,
                areas,
                mapShapes,
            };
        }

        private dynamic GetAreaInfo(Area area)
        {
            var areaInfo = new { area.Id, area.Name, area.FullName, Areas = new List<dynamic>() };
            var childAreas = _context.Area
                .ValidAtCurrentVersion()
                .Where(a => a.ParentAreaId == area.Id)
                .OrderBy(a => a.Id)
                .ToList();
            foreach (var childArea in childAreas)
                areaInfo.Areas.Add(GetAreaInfo(childArea));
            return areaInfo;
        }
    }

    public class SiteSearchResults
    {
        public List<Site> Matches { get; set; }
        public string SearchTerm { get; set; }
    }
}
