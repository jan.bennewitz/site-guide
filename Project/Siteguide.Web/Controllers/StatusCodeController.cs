﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Dropbox.Api.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Logging;
using SiteGuide.Service;
using SiteGuide.Web.Models;
using SiteGuide.DAL;

namespace SiteGuide.Web.Controllers
{
    public class StatusCodeController : Controller
    {
        //private VersionService _versionService;

        public StatusCodeController(/*VersionService versionService*/)
        {
            //_versionService = versionService;
        }

        public async Task<IActionResult> IndexAsync(int? statusCode = null)
        {
            if (statusCode.HasValue)
                HttpContext.Response.StatusCode = statusCode.Value;

            return statusCode switch
            {
                404 or 500 => View(statusCode.Value.ToString()),

                null => new EmptyResult(),  // TODO: Is this even possible?

                _ => new StatusCodeResult((int)statusCode),
            };
        }

        public async Task<IActionResult> ThrowTestErrorAsync() => throw new Exception("Test exception");
    }
}
