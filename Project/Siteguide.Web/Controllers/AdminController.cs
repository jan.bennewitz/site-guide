﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiteGuide.Service;
using SiteGuide.Web.Services;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Helpers;

namespace SiteGuide.Web.Controllers
{
    [Authorize(Roles = Permissions.AdminRoleName)]
    public class AdminController : Controller
    {
        private readonly SiteGuideContext _context;
        private readonly BackgroundTasksService _backgroundTasksService;

        public AdminController(SiteGuideContext context, BackgroundTasksService backgroundTasksService)
        {
            _context = context;
            _backgroundTasksService = backgroundTasksService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(_backgroundTasksService.BackgroundTaskTimers);
        }

        // Commented out for now as the reference to DataMigrator was causing issues on publish
        //[HttpPost]
        //public IActionResult Import()
        //{
        //    //string dataFile = Path.Combine(Environment.CurrentDirectory,
        //    //    @"..\Generator\SiteGuideData\siteGuideData.xml");
        //    //using var data = new FileStream(dataFile, FileMode.Open, FileAccess.Read);

        //    //// Don't pass context itself, that will run a lot slower.
        //    //new DataMigrator.ImportJob(data, _context.Database.GetDbConnection().ConnectionString, DateTime.UtcNow, User.Identity.Name, "Initialised from admin page.")
        //    //    .Run();

        //    const string repoPath = @"C:\Users\Jan Bennewitz\Coding Projects\Site guide";
        //    const string dataFileRelativePath = @"Project/Generator/SiteGuideData/siteGuideData.xml";
        //    const string branch = "master";

        //    DataMigrator.Importer.ImportFromGitAsync(_context.Database.GetDbConnection().ConnectionString, repoPath, dataFileRelativePath, branch);

        //    return RedirectToAction("Index");
        //}

        [HttpPost]
        public async Task<IActionResult> RefreshCacheUserClaimableEdits()
        {
            await _backgroundTasksService.RebuildCacheUserClaimableEditsAsync();
            return RedirectToAction("Index");
        }

        //public async Task<IActionResult> UpdateCtafDataAsync()
        //{
        //    int recordsWritten = await _externalDataCacheService.UpdateDataAsync(ExtCacheEnum.Ctaf);
        //    return Ok($"Read and cached {recordsWritten} CTAFs.");
        //}
    }
}
