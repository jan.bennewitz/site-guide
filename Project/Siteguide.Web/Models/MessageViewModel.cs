using SiteGuide.Web.Controllers;
using SiteGuide.DAL.Models;
using System.Collections.Generic;

namespace SiteGuide.Web.Models {
    public class MessageViewModel {
        public MessageViewModel(MessageType type, string title, string text) {
            Type = type;
            Title = title;
            Text = text;
        }

        public string Title { get; set; }
        public string Text { get; set; }
        public MessageType Type { get; set; }

        public override string ToString() => $"{Type} \"{Title}\": \"{Text}\"";
    }

    public enum MessageType {
        Success,
        Warning,
        Error,
    }
}