﻿using SiteGuide.DAL.Models;
using SiteGuide.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteGuide.Web.Models
{
    public class EditsListViewModel
    {
        public IList<CustomButtonViewModel<EditSummaryListItem>> CustomButtons { get; set; }
        public IEnumerable<EditSummaryListItem> Edits { get; set; }
    }

    public class EditSummaryListItem
    {
        public int Id { get; set; }
        /// <summary>
        /// Must be sorted by EditVersionNumber asc
        /// </summary>
        public IList<EditVersion> EditVersions { get; set; }
        public int? CompareToVersionId { get; set; }

        //public EditVersion LatestEditVersion => EditVersions.First(ev => ev.EditVersionNumber == EditVersions.Max(ev1 => ev1.EditVersionNumber));
        public EditVersion LatestEditVersion => EditVersions.Last();
    }

    public class CustomButtonViewModel<T>
    {
        public Func<T, string> Href { get; set; }
        public string Text { get; set; }
    }
}
