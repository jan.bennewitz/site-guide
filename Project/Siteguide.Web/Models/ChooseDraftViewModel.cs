﻿using SiteGuide.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteGuide.Web.Models
{
    public class ChooseDraftViewModel : EditsListViewModel
    {
        public string ChooseForUrl { get; set; }
    }
}
