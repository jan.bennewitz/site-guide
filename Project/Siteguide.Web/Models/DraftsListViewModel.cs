﻿using SiteGuide.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteGuide.Web.Models
{
    public class DraftsListViewModel : EditsListViewModel
    {
        public bool IsReviews { get; internal set; }
    }
}
