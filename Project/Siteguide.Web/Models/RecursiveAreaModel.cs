﻿using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Siteguide.Web.Models
{
    public class RecursiveAreaModel
    {
        public Area? Area { get; set; }
        public ILookup<int?, Area> AllAreasByParentId { get; set; }
        public Func<Area, string> LinkHref { get; set; }

        public IList<RecursiveAreaModel> ChildAreas
            => AllAreasByParentId[Area?.Id]
                .Select(a => new RecursiveAreaModel { Area = a, AllAreasByParentId = AllAreasByParentId, LinkHref = LinkHref })
                .ToList();
    }
}
