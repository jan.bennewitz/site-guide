﻿using SiteGuide.Service.Helpers;
using SiteGuide.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteGuide.Web.Models
{
    public class SitesIndexAreaViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        internal int? ParentAreaId { get; set; }

        public IEnumerable<string> AncestorAreaNames { get; set; }
        public IList<SitesIndexAreaViewModel> Areas { get; set; }
        public IList<SitesIndexSiteViewModel> Sites { get; set; }
        public int OpenSitesCount { get; set; }
        public int ClosedSitesCount { get; set; }

        public string FullPath(string separator = " > ") => AreaHelper.AreaParentageDisplay(AncestorAreaNames.Skip(1).Append(FullName), separator);

        public override string ToString() => $"{Id} {Name}";
    }
}

