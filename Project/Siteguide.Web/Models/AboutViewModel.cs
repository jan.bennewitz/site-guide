using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SiteGuide.Web.Models
{
    public class AboutViewModel
    {
        public System.Version SoftwareVersion { get; set; }
        public DateTime SoftwareBuildDate { get; set; }
        public int DataVersion { get; set; }
        public DateTime DataVersionDateTime { get; set; }

        public IList<ExtCacheClub> StateAssociations { get; set; }
    }
}