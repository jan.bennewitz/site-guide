﻿using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteGuide.Web.Models
{
    public class SitesIndexSiteViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortLocation { get; set; }
        public string Type { get; set; }
        public string Closed { get; set; }
        public string Conditions { get; set; }
        public string Rating { get; set; }
        public string Height { get; set; }
        public IEnumerable<string> LaunchNames { get; set; }
        public int AreaId { get; set; }

        public override string ToString()
        {
            return $"{Id} {Name}";
        }
    }
}

