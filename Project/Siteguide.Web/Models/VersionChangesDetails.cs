﻿using System.Collections.Generic;
using SiteGuide.Service;
using SiteGuide.Service.Identifiers;

namespace SiteGuide.Web.Models
{
    public class ChangesViewModel
    {
        public VersionTreeNodeIdentifier ThisVersionId { get; set; }
        public int? CompareToVersionId { get; set; }
        public IEnumerable<ChangesGroup> GroupedChanges { get; set; }
        public bool ShowButtons { get; set; }
        public bool ShowEditButton { get; set; }
    }
}
