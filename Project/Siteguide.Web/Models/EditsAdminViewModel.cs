﻿using SiteGuide.DAL.Models;
using SiteGuide.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteGuide.Web.Models
{
    public class EditsAdminViewModel : EditsListViewModel
    {
        public EditVersionStatus Status { get; set; }
    }
}
