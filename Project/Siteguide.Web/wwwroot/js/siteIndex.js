window.addEventListener('load', (event) => {
    document.getElementById('searchBar').style.display = '';
    groupSitesByArea();

    if (!window.location.hash)
        document.getElementById('searchString').focus();

    if (document.getElementById('searchString').value == '') {
        document.getElementById('searchString').value = unescape(urlParam('q')).replace(/\+/g, " ");
        fromUrl = true;
    }

    filter(document.getElementById('searchString').value);
    if (fromUrl && matchCount == 1)
        document.location.href = lastMatchRow.getElementsByTagName("a")[0].href;
});

var innerTextSupported = (document.getElementsByTagName("body")[0].innerText != undefined) ? true : false;
var areaRows = new Array();
var matchCount;
var lastMatchRow;
var fromUrl = false;

function groupSitesByArea() {
    var rows = document.getElementById('sites').getElementsByTagName('tr');
    var currentParentArea;
    for (i = 0; i < rows.length; i++) {
        var row = rows[i];
        switch (row.getElementsByTagName('td').length) {
            case 1: //Area
                areaRows[areaRows.length] = row;
                currentParentArea = row;
                currentParentArea.siteRows = new Array();
                break;
            case 0: //Area headers
                currentParentArea.headerRow = row;
                break;
            default: //Site
                currentParentArea.siteRows[currentParentArea.siteRows.length] = row;
        }
    }
};

function filter(filterstring) {

    //            var theText = document.getElementById('sites').innerHTML;
    //            theText = theText.replace(/<b>/gi, "");
    //            theText = theText.replace(/<\/b>/gi, "");
    //            document.getElementById('sites').innerHTML = theText;

    filterstring = filterstring.toLowerCase();
    matchCount = 0;
    //var areaRows = $('#sites tr.header-area-name');
    for (i = 0; i < areaRows.length; i++) {
        var area = areaRows[i];
        var areaHasMatch = false;
        for (j = 0; j < area.siteRows.length; j++) {
            var site = area.siteRows[j];
            if (innerTextSupported)
                var siteText = site.innerText;
            else
                var siteText = site.textContent;
            if (siteText.toLowerCase().indexOf(filterstring) == -1)
                site.style.display = 'none';
            else {
                site.style.display = '';
                //                        var theText = document.getElementById('sites').innerHTML;
                //                        theText = theText.replace(/searchString/gi, "<span class=searchStringHighlight>$1</span>");
                //                        document.getElementById('sites').innerHTML = theText;

                matchCount++;
                areaHasMatch = true;;
                lastMatchRow = site;
            }
        }
        if (areaHasMatch) {
            area.style.display = '';
            area.headerRow.style.display = '';
        }
        else {
            area.style.display = 'none';
            area.headerRow.style.display = 'none';
        }
    }
    var resultText;

    if (filterstring == '')
        resultText = matchCount + ' sites';
    else if (matchCount == 0)
        resultText = 'No match';
    else if (matchCount == 1)
        resultText = '1 matching site';
    else
        resultText = matchCount + ' matching sites';

    document.getElementById('searchHitsCount').innerHTML = resultText;
    document.getElementById('searchSubmit').style.display = matchCount == 1 ? '' : 'none';
    document.getElementById('areas').style.display = filterstring == '' ? '' : 'none';
}

function ShowSearchResult() {
    if (matchCount == 1) {
        document.getElementById('searchForm').action = lastMatchRow.getElementsByTagName("a")[0].href;
        return true;
    }
    else
        return false;
}

function urlParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}
