﻿var map; // the google map
var oms; // the OverlappingMarkerSpiderfier
var types;
var sitesById;
var versionTreeNodeIdentifier = null;
var shadowImage;

var urlParams;
var geocodeSuccess = false; // indicates if q ('query') url param was successfully geocoded

var userLocationMarker;
var userLocationControlImg;
var userLocationReceived = false;
var mapLockedToUserLocation = false;

var infoWindow;
var markers = [];
var ctafData;

var maxZIndex = 1E9; // TODO:check if better solution available

var delayedClosePopover;
var historyLastSaved = null;
var isMapChangingFromCode = 0;
var delayedOnBoundsChangedHandler;
var pendingshowMarkerLabels = null;

const MAXMARKERLABELS = 1000; // Set very high - seems to work well.

async function initializeMap() { // This gets called once the gmap is loaded. Before this is called we can't assume that gmap classes are available.
    declareOverlayClasses();
    shadowImage = new google.maps.MarkerImage('css/images/mm_20_shadow.png', null, null, new google.maps.Point(6, 20));
    infoWindow = new google.maps.InfoWindow();

    // Define types
    var getStandardTypeFromColor = (name, color) => {
        return {
            name: name,
            image: 'css/images/mm_20_' + color + '.png', // default icon image for this type of site
            highlightImage: 'css/images/mm_20_' + color + '_hi.png', // highlighted icon image for this type of site
            fadeImage: 'css/images/mm_20_' + color + '_fade.png', // icon image for this type of site when not visible on map
            inactiveImage: 'css/images/mm_20_transparent_fade.png', // icon image for deselected marker type;
            isHidden: true // boolean - type is currently hidden
        };
    };

    types = [
        getStandardTypeFromColor('school', 'orange'),
        getStandardTypeFromColor('club', 'yellow'),
        getStandardTypeFromColor('open', 'green'),
        //types['microlight:  getStandardTypeFromColor("red"),
        getStandardTypeFromColor('closed', 'gray'),
        {
            name: 'car166',
            image: 'css/images/OrangeDisk.png',
            inactiveImage: 'css/images/FadedDisk.png',
            isHidden: true
        },
    ];

    urlParams = getUrlParams();

    isMapChangingFromCode++;
    map = new google.maps.Map(document.getElementById("map-canvas"),
        {
            zoom: 4,
            disableDoubleClickZoom: true,
            scaleControl: true,
            mapTypeControl: true,
            mapTypeControlOptions: {
                position: google.maps.ControlPosition.TOP_LEFT
            },
            fullscreenControl: true,
            fullscreenControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT
            },

            center: new google.maps.LatLng(-25, 136), // arbitrary default position to init map
            mapTypeId: urlParams.MapTypeId,
        }
    );

    // Create the DIV to hold the userLocationControl and call the UserLocationControl()
    // constructor passing in this DIV.
    var userLocationControlDiv = document.createElement('div');
    UserLocationControl(userLocationControlDiv, map);

    userLocationControlDiv.index = 2;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(userLocationControlDiv);

    // Create the DIV to hold the linkControl and call the linkControl()
    // constructor passing in this DIV.
    var linkControlDiv = document.createElement('div');
    LinkControl(linkControlDiv, map);

    linkControlDiv.index = 3;
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(linkControlDiv);

    var userLocationControlDiv = document.createElement('div');
    UserLocationControl(userLocationControlDiv, map);

    var versionControlDiv = document.createElement('div');
    VersionControl(versionControlDiv, map);
    versionControlDiv.index = 5;
    map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(versionControlDiv);
    //$('#versionControl').hide();

    var typesControlDiv = document.createElement('div');
    typesControlDiv.index = 4;
    TypeControl(typesControlDiv);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(typesControlDiv);

    oms = new OverlappingMarkerSpiderfier(map, { markersWontMove: true, nearbyDistance: 14 });

    google.maps.event.addListener(map, 'click', hideDetails); // clicking anywhere on the map unpins the currently pinned marker
    google.maps.event.addListener(map, 'bounds_changed', ensureQueuedShowMarkerLabels);
    google.maps.event.addListener(map, 'dragstart', disableUserLocationTracking);

    google.maps.event.addListener(map, 'bounds_changed', () => {
        // We don't need to push state on every pixel move, introduce a delay so we push state only once bounds have stopped changing for a moment.
        // TODO: Maybe move this delay into pushState itself?
        clearTimeout(delayedOnBoundsChangedHandler);
        delayedOnBoundsChangedHandler = setTimeout(pushState, 300);
    });
    google.maps.event.addListener(map, 'maptypeid_changed', pushState);

    try {
        var response = await fetch(`Map/Data${(urlParams.Vnid == null ? '' : `?versionTreeNodeIdentifier=${urlParams.Vnid}`)}`);
    }
    catch (error) {
        debugger;
    }

    var data = await response.json();
    await dataLoaded(data);

    setTimeout(() => updateStateFromUrl(urlParams)); // Let the map finish setting up before doing this - otherwise the layer menu items aren't there yet when we need them.

    //var typeToggles = get$('show-types').getElementsByTagName('li');
    //for (i = 0; i < typeToggles.length; i++) {
    //    typeToggles[i].onclick = function () { toggleTypeVis(this.id) };
    //    typeToggles[i].onmouseover = function () { this.className = 'highlight' };
    //    typeToggles[i].onmouseout = function () { this.className = '' };
    //    setTypeHeaderImage(typeToggles[i].id);
    //}

    //showHideCAR166();

    // Enable popover
    $(function () {
        $('[data-toggle="popover"]').popover()
    });

    addEventListener('popstate', (e) => {
        updateStateFromUrl(getUrlParams());
    });

    // Attach modals etc to map so they show when using fullscreen
    google.maps.event.addListenerOnce(map, 'tilesloaded', function () {
        document.getElementById('map-canvas').firstChild.appendChild(document.getElementById("customMapControls"));
    });

    isMapChangingFromCode--;
}

function updateStateFromUrl(urlParams) {
    if ($('#typesControl .btn-group')
        .find('[data-type="open"]').length != 1) {
        // TODO: Handle better - the menu html hasn't been initialised yet
        setTimeout(() => updateStateFromUrl(urlParams), 50); // HACK: Retry in a moment
        return;
    }

    isMapChangingFromCode++;
    //set which types are shown
    for (var i = 0; i < types.length; i++) {
        var type = types[i];
        var isTypeHidden = !urlParams.Types.some((t) => t.toLowerCase() == type.name);
        type.isHidden = isTypeHidden;
        $('#typesControl .btn-group')
            .find(`[data-type='${type.name}']`)
            .toggleClass('active', !isTypeHidden);

        toggleTypeVis(type.name, !isTypeHidden);
    }

    setVersionTreeNodeIdentifier(urlParams.Vnid);

    map.setMapTypeId(urlParams.MapTypeId == null ? google.maps.MapTypeId.TERRAIN : urlParams.MapTypeId);

    // set initial viewport if specified by url params (if not, we'll show all markers once they're loaded)
    if (urlParams.Center) {
        if (urlParams.Zoom) {
            map.setCenter(urlParams.Center);
            map.setZoom(urlParams.Zoom);
        } else if (urlParams.Span)
            setMapCenterAndSpan(urlParams.Center, urlParams.Span);
        else {
            map.setCenter(urlParams.Center);
            map.setZoom(8); // default if just center specified
        }
    }

    if (urlParams.Query) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': urlParams.Query, 'region': 'AU' }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.fitBounds(results[0].geometry.viewport);
                geocodeSuccess = true;
            } else {
                alert(`Could not geocode the location '${urlParams.Query}': ${status}`);
                debugger;
            }
        });
    }

    isMapChangingFromCode--;
}

async function dataLoaded(data) {
    sitesById = new Map(data.sites.map(i => [i.id, i]));
    for (var i = 0; i < data.sites.length; i++) {
        var s = data.sites[i];
        for (var j = 0; j < s.launches.length; j++) {
            var l = s.launches[j];
            let type = (s.isClosed || l.isClosed) ? 'closed' : 'open';
            let name = s.name + (l.name == null ? '' : ' - ' + l.name);
            setUpMarker(l.lat, l.lon, type, { name, s, l });
        }
    }

    for (var i = 0; i < data.clubs.length; i++) {
        var c = data.clubs[i];
        let type;
        switch (c.category) {
            case 'c':
            case 'a': type = 'club'; break;
            case 's': type = 'school'; break;
        }
        setUpMarker(c.lat, c.lng, type, { name: c.name, c });
    }

    for (var i = 0; i < data.shapes.length; i++)
        setUpShape(data.shapes[i], data.mapShapeCategories);

    var pinShape = markers.find(m => m.data.name == urlParams.Pin);

    if (urlParams.Center || geocodeSuccess) {
        // the viewport has already been specified. If a launch has been specified, just pin it without panning
        if (pinShape)
            setPin(pinShape);
    }
    else if (pinShape) {
        // center the map on the specified launch
        if (urlParams.Zoom)
            map.setZoom(urlParams.Zoom);
        else if (urlParams.Span)
            setMapCenterAndSpan(pinShape.position, urlParams.Span);
        else
            map.setZoom(12); // default zoom level for showing a launch

        setPin(pinShape);
    }
    else {
        // there is no initial launch and no center, so we'll ignore any span or zoom level and just show all non-hidden markers
        var bounds = new google.maps.LatLngBounds;
        for (var i = 0; i < markers.length; i++) {
            if (!types.find((t) => t.name == markers[i].data.type).isHidden)
                bounds.extend(markers[i].position);
        }
        if (!bounds.isEmpty()) map.fitBounds(bounds);
    }

    setVersionTreeNodeIdentifier(data.versionInfo);
    toggleCar166();

    showMarkerLabels();
}

function setVersionTreeNodeIdentifier(value) {
    console.info('setversionTreeNodeIdentifier', value);
    versionTreeNodeIdentifier = value;
    if (versionTreeNodeIdentifier != null) {
        var el = document.getElementById('versionControlText');
        if (el)
            el.getElementsByClassName('card-title')[0].innerHTML = versionTreeNodeIdentifier.includes('.')
                ? `Showing <a href='/Edit/${versionTreeNodeIdentifier.split('.')[0]}'>edit version ${versionTreeNodeIdentifier}</a>`
                : `Showing <a href='/Versions/Details/${versionTreeNodeIdentifier}'>version ${versionTreeNodeIdentifier}</a>`
        else debugger; // Why does this happen?
    }
    $('#versionControl').toggle(versionTreeNodeIdentifier != null);

    // TODO: if versionTreeNodeIdentifier changed we need to reload the data
}

function showMarkerLabels() {
    var visBounds = map.getBounds();
    if (!visBounds)
        return;

    var removeMarkerLabel = function (marker) {
        if (marker.markerLabel) {
            marker.markerLabel.setMap(null);
            marker.markerLabel = null;
        }
    }
    var visibleMarkers = [];
    for (i = 0; i < markers.length; i++) {
        var marker = markers[i];

        if (!types.find(t => t.name == marker.data.type).isHidden && marker.spiderFormat != OverlappingMarkerSpiderfier.markerStatus.SPIDERFIABLE && visBounds.contains(marker.getPosition()))
            visibleMarkers.push(marker);
        else
            removeMarkerLabel(marker);
    }

    var showMarkers = visibleMarkers.length < MAXMARKERLABELS;
    for (var i = 0; i < visibleMarkers.length; i++) {
        if (showMarkers) {
            if (!visibleMarkers[i].markerLabel) // Create label if not already shown
                visibleMarkers[i].markerLabel = new MarkerLabelOverlay(visibleMarkers[i].data.l ? visibleMarkers[i].data.l.name ?? visibleMarkers[i].data.s.name : visibleMarkers[i].data.name, visibleMarkers[i].position);
        }
        else
            if (visibleMarkers[i].markerLabel) // Remove label if already shown
                removeMarkerLabel(visibleMarkers[i]);
    }
}
function setUpMarker(lat, lng, type, data) {
    var marker = new google.maps.Marker({
        icon: types.find((t) => t.name == type).image,
        shadow: shadowImage,
        position: new google.maps.LatLng(lat, lng),
        data: data,
    });

    marker.data.type = type;
    let popoverName;
    let popoverDesc;
    let url;
    switch (type) {
        case 'open':
        case 'closed':
            popoverName = data.s.name;
            popoverDesc = data.l.name ?? '';
            url = '/Sites/Details/' + data.s.name;
            break;
        case 'club':
        case 'school':
            popoverName = data.name;
            popoverDesc = '';
            url = data.c.url;
            if (url && !/^https?:\/\//i.test(url)) {
                url = 'http://' + url;
            }
    }
    marker.data.url = url;

    google.maps.event.addListener(marker, 'spider_click', () => {
        setTimeout(() => { showDetails(marker); }, 300); // HACK: Give the dblclick event time to get handled - this could be improved I hope
    });
    google.maps.event.addListener(marker, 'spider_format', (v) => {
        marker.spiderFormat = v;
        ensureQueuedShowMarkerLabels();
    });

    google.maps.event.addListener(marker, 'dblclick', () => {
        pushState();
        window.location.href = url;
    });
    google.maps.event.addListener(marker, 'mouseover', (e) => { showPopover(e, popoverName, popoverDesc); })
    google.maps.event.addListener(marker, 'mouseout', () => { hidePopover() });

    marker.setVisible(!types.find((t) => t.name == type).isHidden);

    markers.push(marker);

    //google.maps.event.addListener(marker, 'mouseover', function (e) { hoveringMarker = index; markerHighlight(marker, true); });
    //google.maps.event.addListener(marker, 'mouseout', function (e) { hoveringMarker = null; markerDeHighlight(marker); });
    ////google.maps.event.addListener(marker, 'click', function () { setPin(index, false, true) });
    //oms.addListener('click', function (marker) { setPin(marker.index, false, true) });
    oms.addMarker(marker);
}

function ensureQueuedShowMarkerLabels() {
    if (!pendingshowMarkerLabels)
        pendingshowMarkerLabels = setTimeout(() => {
            showMarkerLabels();
            pendingshowMarkerLabels = null;
        });
}

function setUpShape(shapeData, categories) {
    var style = categories.find(c => c.name === shapeData.category);

    if (style.dimensions == 2) {
        var polys = [];
        for (var s = 0; s < shapeData.coordinates.length; s++) {
            polys[s] = decodeLine(shapeData.coordinates[s]);
        }

        shape = new google.maps.Polygon({
            map: map,
            paths: polys,
            fillColor: style.color,
            fillOpacity: .2,
            strokeColor: style.color,
            strokeOpacity: .4,
            strokeWeight: 1
        });
    }
    else {
        for (var s = 0; s < shapeData.coordinates.length; s++) {
            shape = new google.maps.Polyline({
                map: map,
                path: decodeLine(shapeData.coordinates[s]),
                strokeColor: style.color,
                strokeOpacity: .6,
                strokeWeight: 1
            });
        }
    }

    var title = shapeData.name == null ? shapeData.category : `${shapeData.category}: "${shapeData.name}"`;
    var description = shapeData.description;
    if (shapeData.siteIds && shapeData.siteIds.length > 0) {
        if (description)
            description += '<hr/>';

        var sitesText = '';
        for (var i = 0; i < shapeData.siteIds.length; i++) {
            var site = sitesById.get(shapeData.siteIds[i]);
            sitesText += `<li><span class="launch${site.isClosed ? ' closed' : ''}">${site.name}</span></li>`;
        }
        description += `<ul class="launch_list">${sitesText}</ul>`;
    }
    google.maps.event.addListener(shape, 'mouseover', function (e) {
        showPopover(e, title, description);
        this.setOptions({ strokeWeight: 3 });
    })
    google.maps.event.addListener(shape, 'mousemove', positionPopover);
    let delay = style.dimensions == 2 ? 0 : 2000; // delay hiding popover of polylines to make the UX less fiddly
    google.maps.event.addListener(shape, 'mouseout', function () {
        hidePopover(delay);
        this.setOptions({ strokeWeight: 1 });
    });
    google.maps.event.addListener(shape, 'click', function () { showMapShapeDetailsModal(title, description) });
}
function showMapShapeDetailsModal(title, description) {
    hidePopover();
    var modal = $('#mapShapeDetailsPopup')
    modal.find('.modal-title').text(title);
    modal.find('.modal-body').html(description);
    modal.modal();
}
function showPopover(e, title, description) {
    // TODO: Mobile case? Maybe this - just disable popups?
    if (typeof (e.domEvent.pageY) == 'undefined')
        return;
    if (delayedClosePopover != null) {
        hidePopover(0);
        clearTimeout(delayedClosePopover);
        delayedClosePopover = null;
    }
    $('#popoverProxy').prop('title', title);
    $('#popoverProxy').data('html', true);
    $('#popoverProxy').data('content', description ?? '');
    positionPopover(e);
};

function positionPopover(e) {
    $("#popoverProxy").css({ top: e.domEvent.offsetY ?? e.domEvent.changedTouches[0].offsetX, left: e.domEvent.offsetX ?? e.domEvent.changedTouches[0].offsetY });
    $('#popoverProxy').popover({ container: e.domEvent.currentTarget }); // Required to see the popover when map is fullscreen
    $('#popoverProxy').popover('show');
};
function hidePopover(delay = 0) {
    delayedClosePopover = setTimeout(() => {
        $('#popoverProxy').popover('dispose');
    }, delay);
};

async function setPin(pinShape) {
    map.panTo(pinShape.position);
    //await showDetails(pinShape);
}

async function showDetails(item) { // item is marker or CTAF circle
    var isAerodrome = item.aerodrome != undefined;
    var name = isAerodrome ? item.aerodrome.name : item.data.name;
    var isWide;
    var showSpinner = false;
    var url;
    var urlButtonText;
    var categoryText;
    var detailsGetter;

    if (isAerodrome) { // Aerodrome
        isWide = false;
        url = item.aerodrome.ersaFacUrl;
        urlButtonText = 'Open ERSA FAC';
        categoryText = `${item.aerodrome.category} Aerodrome`;
        detailsGetter = async () => {
            return `Code: ${item.aerodrome.code}<br/>CTAF: ${item.aerodrome.ctaf} MHz<br/>ERSA FAC: <small><a href='${item.aerodrome.ersaFacUrl}'>${item.aerodrome.ersaFacUrl}</a></small></br></br><small>A 10nm circle around the aerodrome is shown. Data sourced from XCAustralia Aircheck who in turn source it from Airservices Australia - please <a href='https://xcaustralia.org/aircheck/aircheck.php'>contact Aircheck</a> or <a href='https://www.airservicesaustralia.com/about-us/contact-us/'>Airservices</a> for any corrections.</small>`;
        };
    }
    else if (item.data.type == 'open' || item.data.type == 'closed') { // Launch
        isWide = true;
        showSpinner = true;
        url = item.data.url;
        urlButtonText = 'Full Details';
        categoryText = `${item.data.type == 'open' ? '' : capitalizeFirstLetter(item.data.type)} Launch`.trim();
        detailsGetter = async () => {
            try {
                let request = await fetch(`/Map/LaunchSummary/${item.data.l.id}`);
                return await request.text();
            }
            catch (error) {
                debugger;
            }
        };
    }
    else { // School or club
        isWide = false;
        url = item.data.url;
        urlButtonText = 'Web Page';
        switch (item.data.c.category) {
            case 'c': categoryText = 'Club'; break;
            case 'a': categoryText = 'State Association'; break;
            case 's': categoryText = 'School'; break;
        }
        detailsGetter = async () => {
            return `Phone: ${item.data.c.phone}<br/>Email: <a href='mailto:${item.data.c.email}'>${item.data.c.email}</a><br/>Web: <a href='${item.data.c.url}'>${item.data.c.url}</a>${item.data.c.category != "s" && item.data.c.safaCode ? `<br/>SAFA info: <a href="https://www.safa.asn.au/club-info/${item.data.c.safaCode}">www.safa.asn.au/club-info/${item.data.c.safaCode}</a>` : ''}</br></br><small>Club, state association and school data is maintained by SAFA - please <a href='https://www.safa.asn.au/contact'>contact SAFA</a> for any corrections.</small>`;
        };
    }

    var modal = $('#detailsPopup');
    modal.find('.badge').text(categoryText);
    modal.find('.modal-title').text(name);
    modal.find('#openMarkerUrlButton').toggleClass('disabled', !url);
    modal.find('#openMarkerUrlButton').attr('href', url);
    modal.find('.modal-body .details').empty();
    modal.find('.modal-dialog').toggleClass('modal-xl', isWide);
    modal.modal();
    modal.find('.modal-body .details-spinner').toggle(showSpinner);
    modal.find('#openMarkerUrlButton').text(urlButtonText);
    modal.find('.modal-body .details').html(await detailsGetter());
    modal.find('.modal-body .details-spinner').hide();
}
var hideDetails = () => infoWindow.close();

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function pushState() {
    if (isMapChangingFromCode > 0)
        return;

    var url = getUrlToThisMap();
    if (history.state == url || window.location.href == url) {
        return; // No change.
    }
    let timestamp = new Date().getTime();
    const cooldownMs = 3000;
    if (historyLastSaved != null && (timestamp - historyLastSaved) < cooldownMs) {
        history.replaceState(null, '', url);
    }
    else {
        history.pushState(null, '', url);
    }
    historyLastSaved = timestamp;
}
function getUrlParams() {
    var url = new URL(window.location.href);
    var urlParam = (name) => url.searchParams.get(name);

    // url parameters, first three are equivalent to Google Maps params:
    var llParam = urlParam('ll'); // lat, lon
    var zParam = urlParam('z');  // zoom
    var spnParam = urlParam('spn'); // span - lat, lon
    var pinParam = urlParam('pin'); // pinned launch name
    var typeParam = urlParam('type'); // shown types (e.g. 'open,closed')
    var qParam = urlParam('q'); // query - name of area to be shown via geocoding
    var mapTypeParam = urlParam('mapType'); // map type - roadmap, satellite, hybrid or terrain
    mapTypeParam = mapTypeParam == null ? google.maps.MapTypeId.TERRAIN : mapTypeParam; // default
    var versionTreeNodeIdentifierParam = urlParam('vnid'); // versionTreeNodeIdentifier - either int for a version or int.int for an editVersion
    var center;
    if (llParam) {
        var coords = llParam.split(",");
        center = new google.maps.LatLng(coords[0], coords[1]);
    };

    var zoom;
    if (zParam) {
        zoom = parseFloat(zParam);
        if (isNaN(zoom)) zoom = null;
    }

    var span;
    if (spnParam) {
        coords = spnParam.split(",");
        span = new google.maps.LatLng(coords[0], coords[1]);
    };

    var types;
    if (typeParam != null) {
        if (typeParam == '')
            types = [];
        else
            types = typeParam.split(",");
    }
    var mapTypeId;
    if (mapTypeParam == null)
        mapTypeId = null;
    else
        switch (mapTypeParam.substring(0, 1)) {
            case 'r': mapTypeId = google.maps.MapTypeId.ROADMAP; break;
            case 's': mapTypeId = google.maps.MapTypeId.SATELLITE; break;
            case 'h': mapTypeId = google.maps.MapTypeId.HYBRID; break;
            case 't': mapTypeId = google.maps.MapTypeId.TERRAIN; break;
            default: null;
        }

    if (types == null)
        types = ['school', 'club', 'open', 'closed']; //default if unspecified

    return {
        "Center": center,
        "Zoom": zoom,
        "Span": span,
        "Query": qParam,
        "Pin": pinParam,
        "Types": types,
        "MapTypeId": mapTypeId,
        "Vnid": versionTreeNodeIdentifierParam,
    }
}

function setMapCenterAndSpan(center, span) {
    normalisedSpanLng = (span.lng() + 360) % 360;
    map.fitBounds(new google.maps.LatLngBounds(
        new google.maps.LatLng(center.lat() - span.lat() / 2, center.lng() - normalisedSpanLng / 2),
        new google.maps.LatLng(center.lat() + span.lat() / 2, center.lng() + normalisedSpanLng / 2)
    ));
}

// Decode an encoded polyline string into an array of coordinates.
// Adapted from http://code.google.com/apis/maps/documentation/utilities/include/polyline.js
function decodeLine(encoded) {
    var len = encoded.length;
    var index = 0;
    var array = [];
    var lat = 0;
    var lng = 0;

    while (index < len) {
        var b;
        var shift = 0;
        var result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        var dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;

        shift = 0;
        result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        var dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;

        array.push(new google.maps.LatLng(lat * 1e-5, lng * 1e-5));
    }

    return array;
}

function UserLocationControl(controlDiv, map) {
    var controlUI = MapCustomControl('Show my position');
    controlDiv.appendChild(controlUI);

    //controlUI.style.marginBottom = '10px';

    userLocationControlImg = document.createElement('img');
    userLocationControlImg.style.padding = '7px'; // TODO: Move all these into the css file?
    userLocationControlImg.EnabledImageSrc = 'css/images/UserLocationButtonEnabled24.png';
    userLocationControlImg.DisabledImageSrc = 'css/images/UserLocationButtonDisabled24.png';
    userLocationControlImg.src = userLocationControlImg.DisabledImageSrc;
    controlUI.appendChild(userLocationControlImg);

    controlUI.addEventListener('click', enableUserLocationTracking);
}

function LinkControl(controlDiv, map) {
    var controlUI = MapCustomControl('Link to this map');
    controlDiv.appendChild(controlUI);

    var linkControlImg = document.createElement('img');
    linkControlImg.style.padding = '7px';
    linkControlImg.src = 'css/images/links-24.png';
    controlUI.appendChild(linkControlImg);
    controlUI.parentNode.style.paddingBottom = '11px';

    controlUI.addEventListener('click', showLinkPopup);
}

function VersionControl(controlDiv, map) {
    var controlUI = MapCustomControl('', 'versionControl');
    controlDiv.appendChild(controlUI);

    //var linkControlImg = document.createElement('img');
    //linkControlImg.style.padding = '7px';
    //linkControlImg.src = 'css/images/links-24.png';
    //controlUI.appendChild(linkControlImg);
    controlUI.parentNode.style.padding = '11px';
    //var versionTextDiv = document.createElement('img');
    //linkControlImg.style.padding = '7px';
    //linkControlImg.src = 'css/images/links-24.png';
    //controlUI.appendChild(linkControlImg);
    var textDiv = document.createElement('div');
    textDiv.innerHTML = '<div class="card"><div id="versionControlText" class="card-body"><h5 class="card-title">Showing ...</h5><p class="card-text">Data may differ from current data.</p><a href="#" class="btn btn-primary mx-auto d-block">Show current</a></div></div>';
    controlUI.appendChild(textDiv);
    $(textDiv).find('.btn').click(showCurrent);
    //controlUI.addEventListener('click', showLinkPopup);
}

function showCurrent() {
    window.open(getUrlToThisMap(true), "_self");
}
function TypeControl(controlDiv) {
    var controlUI = MapCustomControl('Types to display');
    controlDiv.appendChild(controlUI);
    controlUI.style.margin = '11px 10px 11px';
    var typesList = document.getElementById('typesControl');
    controlUI.appendChild(typesList);
    $(document).on('click', '#typesControl .dropdown-menu', function (e) { // Prevent the dropdown menu from closing when a click happens inside it.
        e.stopPropagation();
    });
}

function MapCustomControl(title, id) {
    // Set CSS for the control border.
    var controlUI = document.createElement('div');
    controlUI.id = id;
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '1px solid #fff';
    controlUI.style.marginRight = '10px';
    controlUI.style.borderRadius = '2px';
    controlUI.style.boxShadow = 'rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px';
    controlUI.style.cursor = 'pointer';
    controlUI.title = title;
    return controlUI;
}

function userLocationUpdate(position) {
    userLocationMarker.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));

    if (mapLockedToUserLocation)
        map.setCenter(userLocationMarker.position);
}

function enableUserLocationTracking() {
    if (userLocationReceived)
        enableUserLocationTrackingPositionKnown();
    else
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                userLocationReceived = true;

                if (!userLocationMarker) {// first call
                    userLocationMarker = new google.maps.Marker({
                        icon: {
                            url: 'css/images/UserLocationMarker.png',
                            anchor: new google.maps.Point(12, 12)
                        },
                        position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                        map: map,
                        zIndex: maxZIndex + 1 // in front of any other marker
                    });
                    navigator.geolocation.watchPosition(userLocationUpdate);
                }

                enableUserLocationTrackingPositionKnown()
            }, function () {
                //handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            alert('Sorry, your browser does not support geolocation.');
            debugger;
        }
}

function enableUserLocationTrackingPositionKnown() {
    map.setCenter(userLocationMarker.position);
    map.setZoom(12);

    userLocationControlImg.src = userLocationControlImg.EnabledImageSrc;
    mapLockedToUserLocation = true;
}

function disableUserLocationTracking() {
    mapLockedToUserLocation = false;
    userLocationControlImg.src = userLocationControlImg.DisabledImageSrc;
}

function showLinkPopup() {
    $('#linkPopup').modal({}); // TODO: Replace with standard modal

    var url = getUrlToThisMap();
    $('#linkByArea').val(url);
    $('#testLink').attr('href', url);
    $('#embed').val('<iframe src="' + getUrlToThisMap() + '" width="600" height="400" />');
}

function getUrlToThisMap(excludeVersion) {
    orderOfNumber = (num) => Math.floor(Math.log(Math.abs(num)) / Math.LN10);

    var bounds = map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();
    var spnFactor = .8;
    var spnLat = spnFactor * (ne.lat() - sw.lat());
    var spnLng = (spnFactor * (ne.lng() - sw.lng()) + 360) % 360; // in case we're crossing the date line
    var llScale = -Math.max(orderOfNumber(spnLat), orderOfNumber(spnLng)) + 3; // Four significant digits
    var spn = floorNumber(spnLat, llScale) + ',' + floorNumber(spnLng, llScale); // Round down to avoid map zooming out
    var center = map.getCenter();
    var ll = roundNumber(center.lat(), llScale) + ',' + roundNumber(center.lng(), llScale);
    //var z = map.getZoom();
    //var pin = // handled below
    var showingTypes = [];
    for (var i = 0; i < types.length; i++)
        if (!types[i].isHidden)
            showingTypes.push(types[i].name);
    var type = showingTypes.join(',');
    if (!type) debugger; // Go to https://siteguide.org.au/Map?mapType=terrain&type=school,club to provoke this
    var mapTypeId = map.getMapTypeId();
    var url = window.location.origin + window.location.pathname + '?mapType=' + mapTypeId + '&type=' + type + /*(pinnedMarker == null ? '' : ('&pin=' + encodeURIComponent(markers[pinnedMarker].Name))) + */ '&ll=' + ll + '&spn=' + spn + (excludeVersion || versionTreeNodeIdentifier == null ? '' : '&vnid=' + versionTreeNodeIdentifier);
    return url;
}

function floorNumber(num, scale) {
    return roundOrFloorNumber(num, scale, Math.floor);
}
function roundNumber(num, scale) {
    return roundOrFloorNumber(num, scale, Math.round);
}
function roundOrFloorNumber(num, scale, op) { // http://stackoverflow.com/a/12830454
    if (("" + num).indexOf("e") == -1) {
        return +(op(num + "e" + scale) + "e" + -scale);
    } else {
        var arr = ("" + num).split("e");
        var sig = ""
        if (+arr[1] + scale > 0) {
            sig = "+";
        }
        return +(op(+arr[0] + "e" + sig + (+arr[1] + scale)) + "e" + -scale);
    }
}

function typeVisOnClick(e) {
    setTimeout(() => {
        var el = e.target.closest('label.btn');
        toggleTypeVis(el.dataset.type, el.querySelector('input').checked)
        pushState();
    });
}
function toggleTypeVis(typeName, isEnabled) {
    oms.unspiderfy();
    var type = types.find((t) => t.name == typeName);
    type.isHidden = !isEnabled ?? !type.isHidden;

    if (typeName == 'car166')
        toggleCar166();
    else
        setMarkerVisibilities(typeName);

    showMarkerLabels();
}

function setMarkerVisibilities(typeName) {
    //var visBounds = map.getBounds();
    //if (!visBounds) return;

    const typeMarkers = markers.filter(m => m.data.type == typeName);
    for (i = 0; i < typeMarkers.length; i++) {
        var marker = typeMarkers[i];
        marker.setVisible(!types.find((t) => t.name == typeName).isHidden);
    }
}

async function toggleCar166() {
    if (types.find((t) => t.name == "car166").isHidden) {
        if (ctafData) {
            // Hide CTAFs
            for (var n = 0; n < ctafData.aerodromes.length; n++) {
                aerodrome = ctafData.aerodromes[n];
                aerodrome.circle.setMap(null);
                //aerodrome.circle = null;
                aerodrome.Label.setMap(null);
                //aerodrome.Label = null;
            }
        }
    }
    else {
        // Set up CTAF stuff if not yet done
        if (ctafData == undefined) {
            try {
                var response = await fetch('Map/CtafData');
            }
            catch (error) {
                debugger;
            }
            ctafData = await response.json();

            for (var n = 0; n < ctafData.aerodromes.length; n++) {
                let aerodrome = ctafData.aerodromes[n];
                aerodrome.position = new google.maps.LatLng(aerodrome.lat, aerodrome.lng);

                aerodrome.circle = new google.maps.Circle({
                    center: aerodrome.position,
                    fillColor: "#E56717",
                    fillOpacity: .3,
                    strokeWeight: 0.01,
                    radius: 18520, // 10 nautical miles
                    map: map
                });
                aerodrome.circle.aerodrome = aerodrome;
                aerodrome.Label = new Car166LabelOverlay(aerodrome.name + " " + aerodrome.ctaf, aerodrome.position);
                aerodrome.ersaFacUrl = "http://www.airservicesaustralia.com/aip/current/ersa/FAC_" + aerodrome.code + "_" + ctafData.ersaEffectiveDate + ".pdf";

                google.maps.event.addListener(aerodrome.circle, 'mouseover', function () {
                    highlightAerodrome(this.aerodrome, true);
                });
                google.maps.event.addListener(aerodrome.circle, 'mouseout', function () {
                    highlightAerodrome(this.aerodrome, false);
                });
                google.maps.event.addListener(aerodrome.circle, 'click', function () {
                    showDetails(this);
                });
                google.maps.event.addListener(aerodrome.circle, 'dblclick', function () {
                    window.open(aerodrome.ersaFacUrl, '_blank'); // new window
                });
            }
        }
        else
            for (var n = 0; n < ctafData.aerodromes.length; n++) {
                let aerodrome = ctafData.aerodromes[n];
                aerodrome.circle.setMap(map);
                aerodrome.Label.setMap(map);
            }
    }
}

function highlightAerodrome(aerodrome, isHighlighted) {
    aerodrome.circle.setOptions({
        strokeWeight: (isHighlighted) ? 1 : 0.01
    });
    aerodrome.Label.div_.style.fontWeight = (isHighlighted) ? 'bold' : '';
    aerodrome.Label.positionDiv();
}

function declareOverlayClasses() { // This is a workaround for issues with the sequence the scripts load in. The declarations depend on the gmap lib having loaded, so can only execute as part of the gmap loaded callback.
    window.LabelOverlay = class LabelOverlay extends google.maps.OverlayView {
        constructor(name, position, doScaleFontSize, verticalOffset, shadowColour) {
            super();
            this.name = name;
            this.position = position;
            this.setMap(map);

            this.doScaleFontSize = doScaleFontSize;
            this.verticalOffset = verticalOffset;
            this.shadowColour = shadowColour;
        }

        onAdd() {
            var div = document.createElement("div");
            div.style.position = "absolute";
            div.style.textAlign = "center";
            div.style.verticalAlign = "center";
            //div.style.transform = "rotate(-30deg)";
            //div.style.color = 'MidnightBlue';
            if (this.shadowColour)
                div.style.textShadow = `-1px 0 ${this.shadowColour}, 0 1px ${this.shadowColour}, 1px 0 ${this.shadowColour}, 0 -1px ${this.shadowColour}`; // https://css-tricks.com/adding-stroke-to-web-text/
            div.style.fontWeight = "450";
            div.appendChild(document.createTextNode(this.name));

            this.div_ = div;

            this.getPanes().overlayLayer.appendChild(div);
        }

        draw() {
            //this.positionDiv(); // TODO: Ideally we don't have to call positionDiv twice
            //setTimeout(() => {
            if (this.doScaleFontSize)
                this.div_.style.fontSize = Math.pow(1.25, map.getZoom() + 4) + 'px';
            else
                this.div_.style.fontSize = '12px';
            this.positionDiv();
            //}); // The timeout is there to let the map scale first, this looks less jarring.
        }

        positionDiv() {
            var point = this.getProjection().fromLatLngToDivPixel(this.position);
            var div = this.div_;
            div.style.left = point.x - div.clientWidth / 2 + 'px';
            div.style.top = (this.verticalOffset ?? 0) + point.y - div.clientHeight / 2 + 'px';
        }

        onRemove = function () {
            this.div_.parentNode.removeChild(this.div_);
            this.div_ = null;
        }
    }

    window.MarkerLabelOverlay = class MarkerLabelOverlay extends LabelOverlay {
        constructor(name, position) {
            super(name, position, false, 8, 'Gold');
        }
    }
    window.Car166LabelOverlay = class Car166LabelOverlay extends LabelOverlay {
        constructor(name, position) {
            super(name, position, true);
        }
    }
}

//// TODO: Test this works, maybe report error to server so it can be logged
//window.addEventListener("error", function (e) {
//    alert("Error: " + e.error.message);
//    console.error(e);
//    debugger;
//    return false;
//})

//window.addEventListener('unhandledrejection', function (e) {
//    alert("Unhandled rejection: " + e.reason.message);
//    console.error(e);
//    debugger;
//    return false;
//})
