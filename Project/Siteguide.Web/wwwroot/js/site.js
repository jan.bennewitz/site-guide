﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

//import { each } from "jquery";

document.addEventListener("DOMContentLoaded", async function () {
    linkifyAndRenderTags();
});

function linkifyAndRenderTags(el) {
    linkify(el);

    const customTags = [
        {
            tagName: 'ChangeDetails',
            render: async (element) => {
                await fetchPartial(element, `/DetailsOfEditPartial/${element.dataset.node}?compareToVersion=${element.dataset.compareToVersion}&showViewButtons=${element.dataset.showViewButtons}&showEditButton=${element.dataset.showEditButton}`);
            },
        },
        {
            tagName: 'VersionsList',
            render: async (element) => {
                await fetchPartial(element, `/VersionsListPartial/${element.dataset.site}`);
            },
        },
        {
            tagName: 'LocalTime',
            render: element => {
                const utcTime = new Date(element.dataset.time + (element.dataset.time.endsWith('Z') ? '' : 'Z'));
                var language = window.navigator.userLanguage || window.navigator.language;
                element.innerHTML = `<span title='${utcTime.toLocaleString(language, {
                    timeStyle: "full",
                    dateStyle: "full"
                })}'>${utcTime.toLocaleString(language, {
                    timeStyle: "short",
                    dateStyle: "short"
                })}</span>`;
            },
        }
    ];

    customTags.forEach(t => {
        const elements = (el ?? document).getElementsByTagName(t.tagName);
        if (elements.length === 0)
            return;
        for (let element of elements) {
            try {
                t.render(element);
            }
            catch (error) {
                console.error(error);
                element.innerHTML = "Error: " + error;
            }
        }
    });

    async function fetchPartial(el, url) {
        el.innerHTML = "<div class='card mt-2'><div class='card-header'><div class='spinner-border' role='status'><span class='sr-only'><h5>Loading...</h5></span></div></div></div>";
        try {
            const response = await fetch(url);
            const responseHtml = await response.text();
            el.innerHTML = responseHtml;
        } catch (error) {
            console.error(error);
            el.innerHTML = `<div class='card mt-2'><div class='card-header error-message'>${error}</div></div>`;
        }
        linkifyAndRenderTags(el);
    }

    function linkify(el) {
        var rows = (el ?? document).querySelectorAll('.linkify tr');

        for (i = 0; i < rows.length; i++) {
            var links = rows[i].getElementsByTagName('a');
            if (links.length > 0 && links[0].href != '') {
                const url = links[0].href;
                rows[i].onclick = function (e) {
                    //this.className = "";
                    document.location.href = url;
                };
                rows[i].style.cursor = 'pointer';
            }
        }
    }
}
