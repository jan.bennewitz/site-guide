﻿Vue.component('text-edit', {
    props: [
        'value',
        'id',
        'name',
        'displayname',
    ],
    template: '#text-edit-template'
})
Vue.component('html-edit', {
    props: [
        'value',
        'id',
        'name',
        'displayname',
    ],
    template: '#html-edit-template'
})

var app = new Vue({
    el: '#app',
    data: {
        data: null,

        pendingServerCall: true,
        serverCallNotifications: [],

        insetMapPositions: null,
        videoTypes: null,
        contacts: null,
        mapShapeCategories: null,

        mapApiKey: null,

        // Modal data:
        addContactModal: {
            contactFilter: null,
            filteredContacts: null,
            associatedContactIds: null,
        },
        editingContact: null,
        editingLaunch: null,
        editingMapshape: null,
        newVideo: {
            type: {
                id: null,
                name: null
            },
            identifier: null,
        },
        shapeMapShapes: {},
        shapeMap: null,
        shapeMapVertexMarker: null,
        shapeMapMenu: null,
        shapeMapActiveMapShape: null,
        shapeMapActiveShape: null,
        shapeMapActivePoint: null,
        mapshapeAssociatedSites: null,
    },
    created: function () {
        var urlParts = window.location.href.split('/');
        this.loadSiteData(urlParts[urlParts.length - 1]);
    },
    mounted: function () {
        $('#addVideoModal').on('shown.bs.modal', function (e) {
            $('#newVideoIdentifier').focus();
        });
    },
    methods: {
        loadSiteData: async function (siteId) {
            this.pendingServerCall = true;
            const response = await fetch(`/api/Edit/Site/${siteId}`);
            if (!response.ok)
                app.handleRequestError(`Load failed with code ${response.status} ${response.statusText}`);
            const data = await response.json();
            app.loadDataFromResponseData(data);
            setTimeout(function () {
                var textareas = Array.from(document.getElementsByTagName('textarea'));
                for (let textarea of textareas) {
                    let easyMDE = new EasyMDE({ element: textarea, minHeight: "100px", spellChecker: false });
                    easyMDE.codemirror.on("change", function () {
                        easyMDE.isDirty = true;
                    });
                    easyMDE.codemirror.on("blur", function () {
                        if (easyMDE.isDirty == true) {
                            app.saveField(easyMDE.codemirror.getTextArea(), easyMDE.value());
                            easyMDE.isDirty = false;
                        }
                    });
                };
            }, 0);
            app.pendingServerCall = false;
        },
        handleRequestError: function (error) {
            setTimeout(alert(error), 10); // TODO: Make nicer
            console.log(error);
            debugger;
        },
        loadDataFromResponseData: function (data) {
            app.data = data.siteData;
            app.insetMapPositions = data.insetMapPositions;
            app.videoTypes = data.videoTypes;
            app.contacts = data.contacts;
            app.mapShapeCategories = data.mapShapeCategories;
            app.mapApiKey = data.mapApiKey;

            Vue.set(app.data.site, 'isClosedSelect', (typeof app.data.site.closed === 'undefined' || app.data.site.closed === null || app.data.site.closed === "") ? 'o' : 'c');
            app.data.launches.forEach(function (l) {
                Vue.set(l, 'isClosedSelect', (typeof app.data.site.closed === 'undefined' || l.closed === null || l.closed === "") ? 'o' : 'c');
            });
            if (app.videoTypes)
                app.data.videos.forEach(function (v) {
                    v.type = app.videoTypes.find(t => t.id == v.typeId);
                });

            setTimeout(app.loadShapeMap, 0);
        },

        commitEdit: function () {
            window.open(`/Edit/${app.data.requestedVersionTreeNode.editId}`, "_self")
            //app.post(`/api/Edit/Commit/${app.data.requestedVersionTreeNode.editId}`, null, "Committing", "Committed");
        },
        onDeleteEntity: async function (evt) {
            const el = evt.target;
            const entityEl = el.closest('[data-entity-type]');
            const entityType = entityEl.getAttribute('data-entity-type');
            const id = entityEl.getAttribute('data-entity-id');
            // Delete on server
            await app.deleteEntity(entityType, id);
            // remove on the client:
            var set = app.getEntitySet(entityType);
            const index = set.findIndex(i => i.id == id);
            set.splice(index, 1);
        },
        post: async function (url, data = null, descriptionPresentParticiple = 'Saving', descriptionPastParticiple = 'Saved') {
            let notification = this.raiseNotification(descriptionPresentParticiple, descriptionPastParticiple);
            var response;
            response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: data == null ? null : JSON.stringify(data),
            });
            this.resolveNotification(notification, response.ok);
            if (!response.ok)
                //app.handleRequestError(`Save failed with code ${response.status} ${response.statusText}`);
                return null;

            const responseText = await response.text();
            return responseText === '' ? null : JSON.parse(responseText);
        },
        raiseNotification: function (descriptionPresentParticiple, descriptionPastParticiple) {
            let notification = { descriptionPresentParticiple, descriptionPastParticiple, status: 'pending' };
            this.serverCallNotifications.push(notification);
            return notification;
        },
        resolveNotification: function (notification, didSucceed = true) {
            notification.status = didSucceed ? 'succeeded' : 'failed';
            setTimeout(() => this.serverCallNotifications.splice(this.serverCallNotifications.indexOf(notification), 1), didSucceed ? 2000 : 8000);
        },
        deleteEntity: (entityType, primaryId, secondaryId) =>
            app.post('/api/Edit/DeleteEntity',
                {
                    editId: app.data.requestedVersionTreeNode.editId,
                    entity: entityType,
                    primaryId,
                    secondaryId,
                }),
        getEntitySet: function (entityType) {
            switch (entityType.toUpperCase()) {
                case 'LAUNCH': return app.data.launches;
                case 'SEEALSO': return app.data.seeAlsos
                case 'VIDEO': return app.data.videos;
                case 'CONTACT': return app.contacts;
                case 'MAPSHAPE': return null;
                default: throw 'Unhandled entityType.';
            }
        },
        add: async function (entityType) {
            const newEntity = await app.post('/api/Edit/AddEntity', {
                editId: app.data.requestedVersionTreeNode.editId,
                entity: entityType,
                siteId: app.data.site.id,
            })
            const set = app.getEntitySet(entityType);
            if (set !== null)
                set.push(newEntity);

            return newEntity;
        },
        addLaunch: async function () {
            const newLaunch = await app.add('launch');
            app.editLaunchLocation(newLaunch);
        },
        showAddContactModal: function () {
            app.filterContacts();
            $('#addContactModal').modal('show');
            app.addContactModal.contactFilter = "";
        },
        showEditContactModal: function (contact) {
            app.editingContact = contact;
            $('#editContactModal').modal('show');
        },
        showEditMapshapeModal: function (mapshape) {
            app.editingMapshape = mapshape;
            // Modal doesn't work while in fullscreen. May be able to fix, but for now just exit fullscreen:
            app.exitFullscreen();
            $('#editMapshapeModal').modal('show');
        },
        exitFullscreen: function () {
            var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
                (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
                (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
                (document.msFullscreenElement && document.msFullscreenElement !== null);
            if (isInFullScreen) {
                if (document.exitFullscreen)
                    document.exitFullscreen();
                else if (document.webkitExitFullscreen)
                    document.webkitExitFullscreen();
                else if (document.mozCancelFullScreen)
                    document.mozCancelFullScreen();
                else if (document.msExitFullscreen)
                    document.msExitFullscreen();
            }
        },
        onMapshapeCategoryUpdated: function (mapshape) {
            activeShapeIndex = app.shapeMapShapes[mapshape.id].findIndex(s => s == app.shapeMapActiveShape);
            // remove current shapes from map and replace with new ones.
            const shapes = [...app.shapeMapShapes[mapshape.id]];
            app.removeMapshapeFromMap(mapshape);
            shapes.forEach(s => app.createShapeFromData(mapshape, s.getPath()));
            app.setMapshapeEditMode(mapshape, false, app.shapeMapShapes[mapshape.id][activeShapeIndex], app.shapeMapActivePoint);
        },
        removeMapshapeFromMap(mapshape) {
            app.shapeMapShapes[mapshape.id].forEach(s => {
                s.setEditable(false);
                s.setMap(null);
                if (s.clickTargetShape)
                    s.clickTargetShape.setMap(null);
            });
            app.shapeMapShapes[mapshape.id] = [];
        },
        showRemoveMapshapeModal: async function (mapshape) {
            // Get associated site names and ids
            const data = await fetch(`/api/Edit/MapshapeSites/${mapshape.id}?editId=${app.data.requestedVersionTreeNode.editId}`);
            app.mapshapeAssociatedSites = data;
            app.editingMapshape = mapshape;
            // Modal doesn't work while in fullscreen. May be able to fix, but for now just exit fullscreen:
            app.exitFullscreen();
            $('#removeMapshapeModal').modal();
        },
        disassociateMapshape: async function (mapshape) {
            // Remove on server, ...
            await app.deleteEntity('siteMapshape', app.data.site.id, mapshape.id);
            // ... then remove on client.
            app.exitMapshapeEditMode();
            app.data.mapShapes.splice(app.data.mapShapes.indexOf(mapshape), 1);
            app.removeMapshapeFromMap(mapshape);
        },
        deleteMapshape: async function (mapshape) {
            await app.disassociateMapshape(mapshape);
            await app.deleteEntity('mapshape', mapshape.id);
        },
        filterContacts: function () {
            // Exclude contacts already associated with this site.
            app.addContactModal.associatedContactIds = app.data.contacts.map(c => c.contact.id);
            //const unassociatedContacts = app.contacts.filter(c => !associatedContactIds.includes(c.id));

            if (app.addContactModal.contactFilter == null || app.addContactModal.contactFilter == '')
                app.addContactModal.filteredContacts = app.contacts;
            else
                app.addContactModal.filteredContacts = app.contacts.filter(c =>
                    (c.name != null && c.name.toUpperCase().includes(app.addContactModal.contactFilter.toUpperCase()))
                    || (c.fullName != null && c.fullName.toUpperCase().includes(app.addContactModal.contactFilter.toUpperCase()))
                );
        },
        editLaunchLocation: function (launch) {
            var getDefaultCoords = function () {
                var launchesWithCoords = app.data.launches.filter(l => l.lat != 0 && l.lon != 0);
                if (launchesWithCoords.length == 0)
                    return { lat: -25, lng: 136 } // fallback to 'Australia' - could improve to show parent area

                var bounds = new google.maps.LatLngBounds();
                for (i in launchesWithCoords)
                    bounds.extend(new google.maps.LatLng(launchesWithCoords[i].lat, launchesWithCoords[i].lon));
                return bounds.getCenter();
            };

            const coords = launch.lat == 0 && launch.lon == 0
                ? getDefaultCoords()
                : {
                    lat: launch.lat,
                    lng: launch.lon
                };
            const map = new google.maps.Map(document.getElementById("launchMap"), {
                center: coords,
                zoom: 15,
                mapTypeId: 'hybrid'
            });
            var marker = new google.maps.Marker({
                position: coords,
                map,
                draggable: true
            });

            var self = this;
            map.addListener('click', function (evt) {
                marker.setPosition({
                    lat: evt.latLng.lat(),
                    lng: evt.latLng.lng()
                });
                self.onLaunchSetNewCoords(evt);
            });
            marker.addListener('dragend', this.onLaunchSetNewCoords);
            app.editingLaunch = launch;
            $('#editLaunchModal').modal('show');
        },
        onLaunchSetNewCoords: async (evt) => {
            let newLat = parseFloat(evt.latLng.lat().toFixed(5));
            let newLon = parseFloat(evt.latLng.lng().toFixed(5));
            await app.post('/api/Edit/LaunchCoordinates', {
                editId: app.data.requestedVersionTreeNode.editId,
                launchId: app.editingLaunch.id,
                lat: newLat,
                lon: newLon,
            });
            app.editingLaunch.lat = newLat;
            app.editingLaunch.lon = newLon;
        },
        loadShapeMap: function () {
            var bounds = new google.maps.LatLngBounds();
            app.data.launches.forEach(l => bounds.extend(new google.maps.LatLng(l.lat, l.lon)));
            app.data.mapShapes
                .filter(shape => shape.coordinates)
                .forEach(shape => app.parseToPaths(shape.coordinates)
                    .forEach(path => path
                        .forEach(point => bounds.extend(point))));

            const map = new google.maps.Map(document.getElementById("shapeMap"), {
                center: bounds.getCenter(),
                zoom: 13,
                mapTypeId: 'hybrid'
            });

            map.fitBounds(bounds);
            var zoom = map.getZoom();
            map.setZoom(zoom > 15 ? 15 : zoom);

            app.shapeMap = map;

            var mapshapesListControlDiv = document.createElement('div');
            var controlUI = document.createElement('div');
            controlUI.title = 'Mapshapes';
            controlUI.id = "mapshapesListControl";
            controlUI.style.width = "300px";
            controlUI.style.fontSize = "small";
            app.sizeMapshapesListControl();
            mapshapesListControlDiv.appendChild(controlUI);
            controlUI.appendChild(document.getElementById('mapshapesControlContainer'));
            map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(mapshapesListControlDiv);
            google.maps.event.addListener(map, 'bounds_changed', app.sizeMapshapesListControl); // Resize when map resizes.
            google.maps.event.addListener(map, 'click', (e) => {
                if (app.shapeMapActiveShape != null)
                    app.addPointToActiveShape(e.latLng)
            });

            class ShapeMenu extends google.maps.OverlayView {
                constructor() {
                    super();
                    this.div_ = document.createElement("div");
                    this.div_.className = "shape-menu";
                    this.div_.innerHTML = "Delete Point [Del]";
                    const menu = this;
                    google.maps.event.addDomListener(this.div_, "click", (e) => {
                        app.removeActiveShapePoint();
                        this.close();
                        e.stopPropagation();
                    });
                }
                onAdd() {
                    app.shapeMapMenu = this;
                    const map = this.getMap();
                    this.getPanes().floatPane.appendChild(this.div_);
                    // mousedown anywhere on the map except on the menu div will close the
                    // menu.
                    this.divListener_ = google.maps.event.addDomListener(
                        map.getDiv(),
                        "mousedown",
                        (e) => {
                            if (e.target != app.shapeMapMenu.div_) {
                                app.shapeMapMenu.close();
                            }
                        },
                        true
                    );
                }
                onRemove() {
                    if (this.divListener_) {
                        google.maps.event.removeListener(this.divListener_);
                    }
                    this.div_.parentNode.removeChild(this.div_);
                    // clean up
                    this.set("position", null);
                }
                close() {
                    this.setMap(null);
                }
                draw() {
                    const position = this.get("position");
                    const projection = this.getProjection();

                    if (!position || !projection) {
                        return;
                    }
                    const point = projection.fromLatLngToDivPixel(position);
                    this.div_.style.top = point.y + "px";
                    this.div_.style.left = point.x + "px";
                }
                /**
                 * Opens the menu at a vertex of a given path.
                 */
                open(map, path, vertex) {
                    this.set("position", path.getAt(vertex));
                    this.setMap(map);
                    this.draw();
                }
            }
            app.shapeMapMenu = new ShapeMenu();

            google.maps.event.addDomListener(document, 'keyup', function (e) {
                app.shapeMapMenu.close();
                switch (e.code) {
                    case 'Delete':
                        if (app.shapeMapActivePoint !== null)
                            app.removeActiveShapePoint();
                        break;
                    case 'Escape':
                        app.exitMapshapeEditMode();
                        break;
                }
            });

            app.shapeMapVertexMarker = new google.maps.Marker({});

            for (var i = 0; i < app.data.launches.length; i++) {
                let launch = app.data.launches[i];
                var marker = new google.maps.Marker({
                    map: map,
                    label: launch.name ?? "[Unnamed launch]",
                    position: new google.maps.LatLng(launch.lat, launch.lon),
                });
            }
            app.data.mapShapes
                .forEach((shapeData) => app.parseToPaths(shapeData.coordinates)
                    .forEach(p => app.createShapeFromData(shapeData, p)));
        },
        createShapeFromData: function (shapeData, path = []) {
            let cat = app.mapShapeCategories.find(c => c.id === shapeData.categoryId);
            shapeData.category = cat;
            let shape, clickTargetShape;
            if (cat.dimensions == 2) {
                shape = new google.maps.Polygon({
                    map: app.shapeMap,
                    path: path,
                    fillColor: cat.color,
                    fillOpacity: .2,
                    strokeColor: cat.color,
                    strokeOpacity: .4,
                    strokeWeight: 1,
                });
            }
            else { // cat.dimensions == 1
                shape = new google.maps.Polyline({
                    map: app.shapeMap,
                    path: path,
                    strokeColor: cat.color,
                    strokeOpacity: .6,
                    strokeWeight: 1,
                });
                clickTargetShape = new google.maps.Polyline({ // Add a wider, near-invisible line so the user has a click target wider than a single pixel.
                    map: app.shapeMap,
                    path: path,
                    strokeColor: cat.color,
                    strokeOpacity: .01,
                    strokeWeight: 20,
                });
                shape.clickTargetShape = clickTargetShape;
            }
            shape.description = shapeData.description ?? cat.name;
            shape.mapshape = shapeData;
            if (app.shapeMapShapes[shapeData.id] == null)
                app.shapeMapShapes[shapeData.id] = [];
            app.shapeMapShapes[shapeData.id].push(shape);


            google.maps.event.addListener(shape, "click", (e) => app.onShapeClick(shape, e));
            if (clickTargetShape)
                google.maps.event.addListener(clickTargetShape, "click", (e) => app.onShapeClick(shape, e)); // Treat clicks on click target shape as if they were clicks on shape itself.

            return shape;
        },
        onShapeClick: function (shape, e) {
            if (shape.getEditable() !== true)
                app.setMapshapeEditMode(shape.mapshape);
            else {
                // Check if click was on a vertex control point
                if (e.vertex == undefined)
                    app.addPointToActiveShape(e.latLng);
                else {
                    app.shapeMapActiveShape = shape;
                    app.shapeMapActivePoint = e.vertex;
                    app.setShapeMapVertexMarker();
                    app.shapeMapMenu.open(app.shapeMap, shape.getPath(), e.vertex);
                }
            }
        },
        exitMapshapeEditMode: function () {
            if (app.shapeMapActiveShape) {
                app.shapeMapShapes[app.shapeMapActiveMapShape.id].forEach(s => s.setEditable(false));
                if (app.shapeMapActiveShape.clickTargetShape)
                    app.shapeMapActiveShape.clickTargetShape.setMap(app.shapeMap); // We hid the clickTargetShape during editing to stop it from intercepting click events - enable it again.
                app.shapeMapActiveMapShape = null;
                app.shapeMapActiveShape = null;
                app.shapeMapActivePoint = null;
                app.setShapeMapVertexMarker();
            }
        },
        removeActiveShapePoint: function () {
            app.shapeMapActiveShape.getPath().removeAt(app.shapeMapActivePoint);
        },
        addPointToActiveShape: function (latLng) {
            if (app.shapeMapActivePoint !== null)
                app.shapeMapActivePoint++;
            else
                app.shapeMapActivePoint = 0;
            app.shapeMapActiveShape.getPath().insertAt(app.shapeMapActivePoint, latLng); // insert and make this the new active point
            app.setShapeMapVertexMarker();
        },
        sizeMapshapesListControl: function () {
            const el = document.getElementById('mapshapesList');
            if (el)
                el.style.maxHeight = (app.shapeMap.getDiv().children[0].clientHeight - 150) + 'px';
        },
        parseToPaths: (pathData) =>
            (pathData ?? "").split('|')
                .map(path =>
                    path == ''
                        ? []
                        : path.split(' ').map(coord => {
                            [lng, lat] = coord.split(',');
                            return {
                                lat: Number.parseFloat(lat),
                                lng: Number.parseFloat(lng)
                            };
                        })),
        setMapshapeEditMode: function (mapshape, pan = true, activeShape = null, activePoint = 0) {
            var bounds = new google.maps.LatLngBounds();
            if (app.shapeMapActiveMapShape)
                app.shapeMapShapes[app.shapeMapActiveMapShape.id].forEach(s => {
                    s.setEditable(false);
                    if (s.clickTargetShape)
                        s.clickTargetShape.setMap(app.shapeMap); // We hid the clickTargetShape during editing to stop it from intercepting click events - enable it again.
                });
            app.shapeMapActiveMapShape = mapshape;
            app.shapeMapActiveShape = activeShape ?? app.shapeMapShapes[mapshape.id][0];
            app.shapeMapActivePoint = app.shapeMapActiveShape.getPath().length > 0 ? activePoint : null;
            app.setShapeMapVertexMarker();
            let hasPoints = false;
            app.shapeMapShapes[mapshape.id].forEach(shape => {
                shape.setEditable(true);
                if (shape.clickTargetShape)
                    shape.clickTargetShape.setMap(null); // Hide the clickTargetShape during editing to stop it from intercepting click events.
                path = shape.getPath();
                ["insert_at", "remove_at", "set_at"].forEach(evt => path.addListener(evt, (vertex) => {
                    if (evt !== "remove_at")
                        app.shapeMapActivePoint = vertex;
                    else {
                        // Move active point
                        if (app.shapeMapActivePoint > 0)
                            app.shapeMapActivePoint--;

                        // Clear active point if we have removed the last point.
                        if (app.shapeMapActiveShape.getPath().length == 0)
                            app.shapeMapActivePoint = null;
                    }
                    if (shape.clickTargetShape)
                        shape.clickTargetShape.setPath(path); // Keep the clickPath in sync
                    app.setShapeMapVertexMarker();
                    app.saveMapshapeCoords(mapshape);
                }));

                const points = path.getArray();
                if (points.length > 0) {
                    hasPoints = true;
                    points.forEach(p => bounds.extend(p));
                }
            });
            if (pan) {
                if (hasPoints)
                    app.shapeMap.panTo(bounds.getCenter());
                document.getElementById('mapshapesListItem' + mapshape.id).scrollIntoView({ block: 'nearest' });
            }
            app.setShapeMapVertexMarker();
        },
        addMapshape: async function () {
            const newMapshape = await app.add('mapshape');
            await app.associateMapshape(newMapshape);
            app.data.mapShapes.push(newMapshape);
            app.createShapeFromData(newMapshape);
            setTimeout(() => { // Give Vue a chance to update the UI
                app.setMapshapeEditMode(newMapshape);
                app.showEditMapshapeModal(newMapshape);
            }, 0);
        },
        addSegment: function (mapshape) {
            var emptyShape = app.shapeMapShapes[mapshape.id].find(s => s.getPath().getArray().length == 0);
            if (!emptyShape)
                emptyShape = app.createShapeFromData(mapshape);
            app.setMapshapeEditMode(mapshape, false, emptyShape, 0);
        },
        setShapeMapVertexMarker: function () {
            if (app.shapeMapActivePoint === null)
                app.shapeMapVertexMarker.setMap(null); // Hide marker
            else {
                app.shapeMapVertexMarker.setMap(app.shapeMap); // Show marker
                const path = app.shapeMapActiveShape.getPath();
                const thisPoint = path.getAt([app.shapeMapActivePoint]);
                // Point towards next point (away from last if last point of line or up if only point).
                var bearing;
                const isPolyline = !app.shapeMapActiveShape.fillColor; // There may be a nicer way to do this?
                if (path.length == 1) // No other points? Just point north.
                    bearing = 0;
                else if (isPolyline && app.shapeMapActivePoint == path.length - 1) { // Last point of polyline? Point away from prev point.
                    const prevPoint = path.getAt([app.shapeMapActivePoint - 1]);
                    bearing = google.maps.geometry.spherical.computeHeading(thisPoint, prevPoint) + 180;
                }
                else { // Default case - point to next point.
                    const nextPoint = path.getAt([(app.shapeMapActivePoint + 1) % path.length]);
                    bearing = google.maps.geometry.spherical.computeHeading(thisPoint, nextPoint);
                }
                app.shapeMapVertexMarker.setIcon({
                    path: 'M-2 -5 L0 -10 L2 -5',
                    scale: 1,
                    rotation: bearing,
                    anchor: { x: .5, y: 0 }, // adjust for the slightly off-center built-in edit points.
                    strokeWeight: 2,
                    strokeColor: "gold",
                    zIndex: -9000000000000000,
                });
                app.shapeMapVertexMarker.setPosition(thisPoint);
            }
        },
        saveMapshapeCoords(mapshape) {
            const nonEmptyPaths = app.shapeMapShapes[mapshape.id]
                .map(s => s.getPath().getArray())
                .filter(path => path.length != 0);
            mapshape.coordinates = nonEmptyPaths.map(path => path.map(pt => pt.lng() + ',' + pt.lat()).join(' ')).join('|');
            this.saveFieldValue(
                'mapshape',
                'coordinates',
                mapshape.coordinates,
                mapshape.id,
            );
        },
        showAddVideoModal: function (videoType) {
            app.newVideo.type = videoType;
            app.newVideo.identifier = null;
            $('#addVideoModal').modal('show');
        },
        addVideo: async function () {
            const newVideo = await app.post('/api/Edit/AddVideo', {
                editId: app.data.requestedVersionTreeNode.editId,
                siteId: app.data.site.id,
                typeId: app.newVideo.type.id,
                identifier: app.newVideo.identifier,
            });
            newVideo.type = app.videoTypes.find(t => t.id == newVideo.typeId);
            app.data.videos.push(newVideo);

            $('#addVideoModal').modal('hide');
        },
        createContactAndEdit: async function () {
            $('#addContactModal').modal('hide');
            const newContact = await app.add('contact');
            app.associateContact(newContact);
            app.showEditContactModal(newContact);
        },
        associateContact: async function (contact) {
            var newSiteContact = await app.post('/api/Edit/AddAssociation', {
                editId: app.data.requestedVersionTreeNode.editId,
                entity: 'siteContact',
                siteId: app.data.site.id,
                foreignKeyId: contact.id,
            });
            app.data.contacts.push({
                contact: contact,
                isContact: newSiteContact.isContact,
                isResponsible: newSiteContact.isResponsible,
            });

            $('#addContactModal').modal('hide');
        },
        associateMapshape: async function (mapshape) { // TODO: refactor with associateContact
            await app.post('/api/Edit/AddAssociation', {
                editId: app.data.requestedVersionTreeNode.editId,
                entity: 'siteMapshape',
                siteId: app.data.site.id,
                foreignKeyId: mapshape.id,
            });
        },
        disassociateContact: async function (contact) {
            // Remove on server, ...
            await app.deleteEntity('siteContact', app.data.site.id, contact.contact.id);
            // ... then remove on client.
            app.data.contacts.splice(app.data.contacts.indexOf(contact), 1);
        },
        fieldValueChanged: function (evt) {
            const el = evt.target;
            if (el.matches('[type="checkbox"]'))
                app.saveField(el, el.checked);
            else
                app.saveField(el);
        },
        isClosedChanged: function (evt) {
            const el = evt.target;
            const entityEl = el.closest('[data-entity-type]');
            const entity = entityEl.getAttribute('data-entity-type');
            const id = entityEl.getAttribute('data-entity-id');
            this.saveFieldValue(
                entity,
                'closed',
                el.value == 'o'
                    ? null
                    : entity == 'site'
                        ? app.data.site.closed
                        : app.data.launches.find(l => l.id == id).closed,
                id,
            );
        },
        saveField: function (el, value) {
            const entityEl = el.closest('[data-entity-type]');
            this.saveFieldValue(
                entityEl.getAttribute('data-entity-type'),
                el.getAttribute('name'),
                (typeof value === 'undefined') ? el.value : value,
                entityEl.getAttribute('data-entity-id'),
            );
        },
        associationFieldValueChanged: function (evt) {
            const el = evt.target;
            const entityEl = el.closest('[data-entity-type]');
            const entity = entityEl.getAttribute('data-entity-type');
            const associatedId = entityEl.getAttribute('data-associated-entity-id');
            const siteId = app.data.site.id;
            const field = el.getAttribute('name');
            const value = el.matches('[type="checkbox"]') ? el.checked : el.value;

            this.saveFieldValue(entity, field, value, siteId, associatedId);
        },
        saveFieldValue: function (entity, field, value, primaryId, secondaryId = null) {
            app.post('/api/Edit/Field', {
                editId: app.data.requestedVersionTreeNode.editId,
                entity: entity,
                primaryId: primaryId,
                secondaryId: secondaryId,
                field: field,
                value: value,
            });
        },
    },
})