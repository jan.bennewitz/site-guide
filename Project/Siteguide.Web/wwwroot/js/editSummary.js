﻿//document.addEventListener("DOMContentLoaded", async function () {
//    onLoaded();
//});

$('#actionConfirmationModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var action = button.data('action');
    var actionDesc = button.text();
    var role = button.data('role');
    var rolehumanised = button.data('rolehumanised');

    var modal = $(this)
    modal.find('#action').val(action);
    modal.find('#role').val(role);
    modal.find('.modal-title').text(actionDesc + (role ? (' as ' + rolehumanised) : ''));
    var actionConfirmButton = modal.find('#actionConfirmButton');
    actionConfirmButton.find('#actionConfirmButtonText').text(actionDesc);
    actionConfirmButton.removeClass("btn-primary btn-secondary btn-success btn-danger btn-warning btn-info btn-light btn-dark").addClass('btn-' + button.data('bootstrapclass'));
    modal.find('#editVersionCommentGroup').toggle(!['ClaimForReview', 'Abandon'].includes(action));
    modal.find('#editVersionCommentLabel').text(action != 'Reject' ? 'Description of your changes' : 'Reason for rejection');
    modal.find('#editVersionCommentHelpBlock').toggle(action != 'Reject');
    modal.find('#editVersionCommentHelpBlockRejection').toggle(action == 'Reject');
    modal.find('#versionCommentGroup').toggle(action === 'Accept');
    modal.find('#abandonText').toggle(action == 'Abandon');
})

function onSubmit(e) {
    document.getElementById('actionForm').requestSubmit();
    $('fieldset').prop('disabled', true);
    $('#actionConfirmButtonSpinner').prop('hidden', false);
}
