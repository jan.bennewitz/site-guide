﻿using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SiteGuide.Service.Helpers;
using System.Net;

namespace Siteguide.Web.Authorisation
{
    public class AuthoriseSafaMemberFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = context.HttpContext.User;

            // Not authenticated? Ask user to log in.
            if (!user.Identity.IsAuthenticated)
            {
                context.Result = new ChallengeResult();
                return;
            }

            // Not authorised? 503.
            if (!(Permissions.IsSafaMember(user) || Permissions.IsAdmin(user)))
            {
                // TODO: Could pass message back to user "This page is only accessible to SAFA members."
                context.Result = new ForbidResult();
                //context.Result = new ObjectResult("You must be a SAFA member or administrator to access this page.")
                //{
                //    StatusCode = (int)HttpStatusCode.Forbidden
                //};
                return;
            }
        }
    }
}
