﻿using Microsoft.AspNetCore.Mvc;

namespace Siteguide.Web.Authorisation
{
    public class AuthoriseSafaMemberAttribute : TypeFilterAttribute
    {
        public AuthoriseSafaMemberAttribute() : base(typeof(AuthoriseSafaMemberFilter))
        {
        }
    }
}
