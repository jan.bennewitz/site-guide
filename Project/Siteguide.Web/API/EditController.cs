﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
//using System.Web.Http;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using SiteGuide.Web.Models;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using Humanizer;
using SiteGuide.DAL;
using SiteGuide.Service.Identifiers;
using Microsoft.Extensions.Logging;
using SiteGuide.Service;
using SiteGuide.Service.Helpers;
using SiteGuide.Web.Controllers;
using System.Threading;
using SiteGuide.DAL.Extensions;
using Siteguide.Service.Helpers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SiteGuide.Web.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class EditController : ControllerBase
    {
        private static readonly HttpClient _httpClient = new();

        private readonly EditService _editService;
        private readonly SiteGuideContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IMapper _autoMapper;
        private readonly SitesController _sitesController;
        private readonly ILogger _logger;

        public EditController(IConfiguration configuration, SiteGuideContext context, UserManager<IdentityUser> userManager, IMapper autoMapper, SitesController sitesController, ILogger<EditController> logger, EditService editService)
        {
            _context = context;
            _userManager = userManager;
            _configuration = configuration;
            _autoMapper = autoMapper;
            _sitesController = sitesController;
            _logger = logger;
            _editService = editService;
        }

        // GET: api/Edit/Site/5
        [HttpGet("Site/{siteId}")]
        public async Task<IActionResult> GetSite([FromRoute] int siteId, [FromQuery] string editVersion)
        {
            var currentUser = (await _userManager.GetUserAsync(HttpContext.User));
            var editVersionIdentifier = EditVersionIdentifier.FromEditVersionString(editVersion, _context);
            var editVersionObj = await _context.EditVersion
                .WhereIsDraftBy(currentUser)
                .SingleOrDefaultAsync(v => v.Id == editVersionIdentifier.Id);

            if (editVersionObj == null)
                return NotFound();

            SiteInfo model = await _sitesController.GetSiteViewModelAsync(_context, siteId, new EditVersionIdentifier(editVersionObj, _context), Request); // TODO: We will need to make sure version matches editVersion

            if (model == null)
                return NotFound();

            //var areas = _context.Area.Select(a => new { a.Id, a.FullName });
            var insetMapPositions = await _context.InsetMapPosition
                .Select(a => new { a.Id, a.Name })
                .ToListAsync();
            var videoTypes = await _context.VideoType
                .OrderBy(v => v.SortOrder)
                .ToListAsync();
            var contacts = _context.Contact.ValidAtUnacceptedEditVersion(editVersionIdentifier)
                .OrderBy(c => c.Name)
                .ToList();

            return Ok(new
            {
                siteData = model,
                //areas = await areas.ToListAsync(),
                insetMapPositions,
                videoTypes,
                contacts,
                mapApiKey = _configuration.GetValue<string>("GMaps:ApiKey"),
                mapShapeCategories = await _context.MapShapeCategory.ToListAsync(),
            });
        }

        [HttpGet("MapshapeSites/{mapshapeId}")]
        public async Task<IActionResult> GetMapshapesSites([FromRoute] int mapshapeId, [FromQuery] int editId)
            => await WithDraftEditAsync(editId, async (editVersion) =>
            {
                var editVersionIdentifier = new EditVersionIdentifier(editVersion, _context);
                var siteIds = _context.SiteMapshape
                    .ValidAtUnacceptedEditVersion(editVersionIdentifier)
                    .Where(sm => sm.MapshapeId == mapshapeId)
                    .Select(sm => sm.SiteId)
                    .ToArray();
                return Ok(
                    _context.Site
                        .ValidAtUnacceptedEditVersion(editVersionIdentifier)
                        .Where(s => siteIds.Contains(s.Id))
                        .Select(s => new { id = s.Id, name = s.Name })
                        .ToArray());
            });

        [HttpPost("DeleteEntity")]
        public async Task<IActionResult> DeleteEntity([FromBody] DeleteEntityInfo data)
            => data.Entity.ToUpperInvariant() switch
            {
                //"SITE" => await DeleteEntity<Site>(data),
                "LAUNCH" => await DeleteEntity<Launch>(data),
                "VIDEO" => await DeleteEntity<Video>(data),
                "SEEALSO" => await DeleteEntity<SeeAlso>(data),
                "SITECONTACT" => await DeleteEntity<SiteContact>(data),
                "MAPSHAPE" => await DeleteEntity<MapShape>(data),
                "SITEMAPSHAPE" => await DeleteEntity<SiteMapshape>(data),
                //TODO: contacts
                //DeleteEntity(_context.Contact, editVersionId, current.Contacts, data.Contacts);

                _ => throw new ArgumentException($"Unhandled entity \"{data.Entity}\""),
            };

        private async Task<IActionResult> DeleteEntity<T>(DeleteEntityInfo info) where T : class, IRecord, new()
            => await WithDraftEditAsync(info.EditId, async (editVersion) =>
            {
                var dbSet = _context.Set<T>();

                var selector = GetIdOrManyToManySelector<T>(info);

                SemaphoreSlim? semaphoreSlim;
                if (typeof(IId).IsAssignableFrom(typeof(T)))
                    semaphoreSlim = _editRecordDeleteOrCreateSemaphores[typeof(T)]; // This is only an issue for records which have an id.
                else
                    semaphoreSlim = null; // No need
                try
                {
                    if (semaphoreSlim != null)
                        await semaphoreSlim.WaitAsync();

                    var existingRecord = await dbSet
                        .ValidAtCurrentVersion()
                        .SingleOrDefaultAsync(selector);

                    var existingEdit = dbSet
                        .Where(i => i.EditVersionId == editVersion.Id)
                        .SingleOrDefault(selector);

                    if (existingRecord == null)
                    {
                        if (existingEdit == null)
                            throw new ArgumentException("No matching record found.");
                        else
                            dbSet.Remove(existingEdit); // We're removing a record that was only added in this edit, so just remove the record of the addition.
                    }
                    else
                    {
                        if (existingEdit == null)
                            // We're removing an existing record and there is no existing edit record, so create a delete record.
                            CreateEditRecord(dbSet, editVersion.Id, existingRecord, true);
                        else
                            existingEdit.IsDelete = true; // We're removing an existing record and there is an existing edit record, so change that one to be a delete.
                    }
                    await _context.SaveChangesAsync().ConfigureAwait(false);

                    return base.Ok();
                }
                finally
                {
                    semaphoreSlim?.Release();
                }
            }).ConfigureAwait(false);

        [HttpPost("AddEntity")]
        public async Task<IActionResult> AddEntity([FromBody] AddEntityInfo data)
            => data.Entity.ToUpperInvariant() switch
            {
                //"SITE" => await addEntity<Site>(data),
                "LAUNCH" => await AddEntity<Launch>(data),
                //"VIDEO" => await addEntity<Video>(data), // Special case handled elsewhere.
                "SEEALSO" => await AddEntity<SeeAlso>(data),
                "CONTACT" => await AddEntity<Contact>(data),
                "MAPSHAPE" => await AddEntity<MapShape>(data),

                _ => throw new ArgumentException($"Unhandled entity \"{data.Entity}\""),
            };

        [HttpPost("AddAssociation")]
        public async Task<IActionResult> AddSiteManyToMany([FromBody] AddSiteManyToManyInfo data)
            => data.Entity.ToUpperInvariant() switch
            {
                "SITECONTACT" => await AddEntity<SiteContact>(data),
                "SITEMAPSHAPE" => await AddEntity<SiteMapshape>(data),

                _ => throw new ArgumentException($"Unhandled entity \"{data.Entity}\""),
            };

        // TODO: Move to edit service
        private static readonly Dictionary<Type, SemaphoreSlim> _editRecordDeleteOrCreateSemaphores = new Type[]{
            typeof(Site) ,
            typeof(Launch),
            typeof(Video),
            typeof(SeeAlso),
            typeof(Contact),
            typeof(MapShape),
        }.ToDictionary(i => i, i => new SemaphoreSlim(1, 1));

        private async Task<IActionResult> AddEntity<T>(AddEntityInfo data, Func<T, Task> setFieldsAsync = null) where T : class, IRecord, new()
            => await WithDraftEditAsync(data.EditId, async (editVersion) =>
            {
                var newItem = await _editService.CreateRecord(editVersion, data.SiteId, (data as AddSiteManyToManyInfo)?.ForeignKeyId, setFieldsAsync);
                return Ok(newItem);
            })
            .ConfigureAwait(false);

        [HttpPost("AddVideo")]
        public async Task<IActionResult> AddVideo([FromBody] AddVideoInfo data)
        => await AddEntity<Video>(
            new AddEntityInfo
            {
                EditId = data.EditId,
                SiteId = data.SiteId
            },
            async (video) =>
            {
                video.TypeId = data.TypeId;
                video.Identifier = data.Identifier;
                await SetVideoThumbnailAsync(video);
            }).ConfigureAwait(false);

        [HttpPost("Field")]
        public async Task<IActionResult> PostField([FromBody] EditFieldInfo data)
            => data.Entity.ToUpperInvariant() switch
            {
                "SITE" => await SaveField<Site>(data),
                "LAUNCH" => await SaveField<Launch>(data),
                "VIDEO" => await SaveField<Video>(data),
                "SEEALSO" => await SaveField<SeeAlso>(data),
                "SITECONTACT" => await SaveField<SiteContact>(data),
                "CONTACT" => await SaveField<Contact>(data),
                "MAPSHAPE" => await SaveField<MapShape>(data),
                // "SITEMAPSHAPE": No need to handle as SiteMapshape records do not have any attached additional information.

                _ => throw new ArgumentException($"Unhandled entity \"{data.Entity}\""),
            };

        private async Task<IActionResult> SaveField<T>(EditFieldInfo data) where T : class, IRecord, new()
            => await WithDraftEditAsync(data.EditId, async (editVersion) =>
            {
                // get the matching record
                IRecord record = await GetOrCreateEditRecordAsync(_context.Set<T>(), data, editVersion.Id);

                // Ensure this is an editable field
                var allowedProps = IRecordExtensions.GetRecordPayloadProps<T>();

                // update the field
                var prop = allowedProps.SingleOrDefault(p => p.Name.ToUpperInvariant() == data.Field.ToUpperInvariant())
                    ?? throw new ArgumentException($"Property \"{data.Field}\" is not a payload property of \"{typeof(T).Name}\".");
                object value = Type.GetTypeCode(prop.PropertyType) switch
                {
                    TypeCode.String => prop.IsRecordFieldMarkdown() ? Sanitise(data.Value) : data.Value,
                    TypeCode.Boolean => bool.Parse(data.Value),
                    TypeCode.Int32 => int.Parse(data.Value),
                    TypeCode.Byte => byte.Parse(data.Value),

                    _ => throw new NotImplementedException(),
                };
                prop.SetValue(record, value);

                await _context.SaveChangesAsync();

                return Ok();
            });

        private static string Sanitise(string markdown)
        {
            var sanitiser = new Sanitiser();
            var result = sanitiser.SanitizeMarkdown(markdown);

            return result;
        }

        [HttpPost("LaunchCoordinates")]
        public async Task<IActionResult> SaveLaunchCoordinates([FromBody] EditLaunchCoordsInfo data)
            // TODO: Should probably just make saveField accept an array of field/value pairs.
            => await WithDraftEditAsync(data.EditId, async (editVersion) =>
            {
                // get the matching record
                var launch = await GetOrCreateEditRecordAsync(_context.Launch, new EditFieldInfo { PrimaryId = data.LaunchId }, editVersion.Id);
                launch.Lat = data.Lat;
                launch.Lon = data.Lon;

                await _context.SaveChangesAsync();

                return base.Ok();
            });

        /// <summary>
        /// Ensures the current user has a draft edit with the given edit id and passes it to the function.
        /// </summary>
        private async Task<IActionResult> WithDraftEditAsync(int editId, Func<EditVersion, Task<IActionResult>> funcAsync)
        {
            var editVersion = await GetUserDraftEditVersionAsync(editId);

            if (editVersion == null)
                return NotFound("Found no draft edit with this id for this user.");

            return await funcAsync(editVersion)
                .ConfigureAwait(false);
        }

        private async Task<T> GetOrCreateEditRecordAsync<T>(DbSet<T> dbSet, IRecordIdInfo info, int editVersionId) where T : class, IRecord, new()
        {
            var selector = GetIdOrManyToManySelector<T>(info);

            SemaphoreSlim? semaphoreSlim;
            if (typeof(IId).IsAssignableFrom(typeof(T)))
                semaphoreSlim = _editRecordDeleteOrCreateSemaphores[typeof(T)]; // This is only an issue for records which have an id.
            else
                semaphoreSlim = null; // No need
            try
            {
                if (semaphoreSlim != null)
                    await semaphoreSlim.WaitAsync();

                T existingEdit = await dbSet
                .WhereEditVersion(editVersionId)
                .SingleOrDefaultAsync(selector);
                if (existingEdit != null)
                    return existingEdit;

                // This item has no edit yet. Create one based on the currently valid record.
                var currentRecord = await dbSet
                    .ValidAtCurrentVersion()
                    .SingleOrDefaultAsync(selector);

                return CreateEditRecord(dbSet, editVersionId, currentRecord, false);
            }
            finally
            {
                semaphoreSlim?.Release();
            }
        }


        private async Task<EditVersion> GetUserDraftEditVersionAsync(int editId)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            return await _context.EditVersion
                .WhereIsDraftBy(user)
                .SingleOrDefaultAsync(e => e.EditId == editId);
        }

        private static Expression<Func<T, bool>> GetIdOrManyToManySelector<T>(IRecordIdInfo idInfo) where T : class, IRecord, new()
        {
            Expression<Func<T, bool>> selector;
            if (typeof(IId).IsAssignableFrom(typeof(T)))
                selector = r =>
                    ((IId)r).Id == idInfo.PrimaryId;
            else if (typeof(T) == typeof(SiteContact))
                selector = r =>
                    ((ISiteContact)r).SiteId == idInfo.PrimaryId
                    && ((ISiteContact)r).ContactId == idInfo.SecondaryId;
            else if (typeof(T) == typeof(SiteMapshape))
                selector = r =>
                    ((ISiteMapshape)r).SiteId == idInfo.PrimaryId
                    && ((ISiteMapshape)r).MapshapeId == idInfo.SecondaryId;
            else
                throw new ArgumentException($"Type must implement {nameof(IId)}, {nameof(ISiteContact)} or {nameof(ISiteMapshape)}");
            return selector;
        }

        private T CreateEditRecord<T>(DbSet<T> dbSet, int editVersionId, T currentItem, bool isDelete) where T : class, IRecord, new()
        {
            var newItem = new T()
            {
                EditVersionId = editVersionId,
                IsDelete = isDelete,
            };
            _autoMapper.Map(currentItem, newItem);

            dbSet.Add(newItem);

            return newItem;
        }

        //[HttpPost("Site")]
        //public async Task<IActionResult> SaveSite([FromBody] SiteInfo data)
        //    => await WithDraftEditAsync(data.EditId, async (editVersion) =>
        //    {
        //        int siteId = data.Site.Id;

        //        await ReplaceChangesAsync(_context.Site, s => s.Id == siteId, editVersion.Id, new List<Site> { data.Site });
        //        await ReplaceChangesAsync(_context.Launch, l => l.SiteId == siteId, editVersion.Id, data.Launches);
        //        await ReplaceChangesAsync(_context.Video, v => v.SiteId == siteId, editVersion.Id, data.Videos);
        //        await ReplaceChangesAsync(_context.SeeAlso, sa => sa.SiteId == siteId, editVersion.Id, data.SeeAlsos);
        //        //TODO: Mapshapes and contacts
        //        //ReplaceChanges(_context.MapShape, editVersionId, current.MapShapes, data.MapShapes);
        //        //ReplaceChanges(_context.SiteMapshape, editVersionId, current.SiteMapshapes, data.SiteMapshapes);
        //        //ReplaceChanges(_context.Contact, editVersionId, current.Contacts, data.Contacts);
        //        //ReplaceChanges(_context.SiteContact, editVersionId, current.SiteContacts, data.SiteContacts);

        //        await _context.SaveChangesAsync();

        //        return Ok(data); // TODO: May not need to return any data.
        //    }).ConfigureAwait(false);

        //private async Task ReplaceChangesAsync<T>(DbSet<T> dbSet, Func<T, bool> filter, int editVersionId, IEnumerable<T> newItems) where T : class, IRecord, IId, new()
        //{
        //    var currentItems = dbSet.ValidAtVersion()
        //        .Where(filter)
        //        .ToList(); // TODO: Can we make this faster by evaluating filter server side?

        //    // Remove old for the specified filter (typically, this site)
        //    var oldChanges = dbSet
        //        .WhereEditVersion(editVersionId)
        //        .Where(filter);
        //    dbSet.RemoveRange(oldChanges);

        //    // Compare to current version. If identical, remove change record, if different, update or create change record.
        //    var preservedItems = currentItems
        //        .Join(newItems, c => c.Key, n => n.Key, (c, n) => new { c, n }) // TODO: Just do this in one hit and use a full outer join
        //        .ToList();

        //    foreach (var preservedItem in preservedItems)
        //        if (!preservedItem.c.IsRecordContentIdentical(preservedItem.n))
        //            await AddNewEditRecordAsync(editVersionId, dbSet, preservedItem.n, false);

        //    var addedItems = newItems.Where(n => !currentItems.Any(c => c.Key.Equals(n.Key)))
        //        .ToList();

        //    if (addedItems.Any())
        //    {
        //        int temp = dbSet
        //       //.Where(i => !oldChanges.Contains(i)) // super slow for some reason. replace with following line.
        //       .Max(i => i.Id);
        //        var oldChangesRecordIds = oldChanges.Select(c => c.RecordId);
        //        int maxId = dbSet
        //            //.Where(i => !oldChanges.Contains(i)) // super slow for some reason. replace with following line.
        //            .Where(i => !oldChangesRecordIds.Contains(i.RecordId))
        //            .Max(i => i.Id);

        //        foreach (var addedItem in addedItems)
        //            await AddNewEditRecordAsync(editVersionId, dbSet, addedItem, false, ++maxId);
        //    }

        //    var deletedItems = currentItems.Where(c => !newItems.Any(n => n.Key.Equals(c.Key)))
        //        .ToList();
        //    foreach (var deletedItem in deletedItems)
        //        await AddNewEditRecordAsync(editVersionId, dbSet, deletedItem, true);
        //}

        //private async Task AddNewEditRecordAsync<T>(int editVersionId, DbSet<T> dbSet, T dataItem, bool isDelete, int? id = null) where T : class, IRecord, IId, new()
        //{
        //    if (id != null)
        //    {
        //        // Adding a new item.
        //        switch (dataItem)
        //        {
        //            case Video video:
        //                await SetVideoThumbnailAsync(video);
        //                break;
        //        }
        //    }

        //    T newItem = CreateEditRecord(dbSet, editVersionId, dataItem, isDelete);

        //    SetReferenceObjectsToNull(newItem);

        //    if (id != null)
        //        newItem.Id = (int)id;
        //}

        private async Task SetVideoThumbnailAsync(Video video)
        {
            if (video.ThumbnailUrl != null)
                return;

            int typeId = video.TypeId;
            VideoType videoType = _context.VideoType.Single(t =>
                t.Id == typeId
            );
            switch (videoType.Name.ToUpperInvariant())
            {
                case "VIMEO":
                    const string unknownVideoThumbnail = "https://i.vimeocdn.com/video/000000000_100x75.webp";

                    // Set when creating video entry and store in Video.ThumbUrl.
                    var response = await _httpClient.GetAsync($"http://vimeo.com/api/v2/video/{video.Identifier}.json");
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string contentText = await response.Content.ReadAsStringAsync();
                        var data = (JArray)JsonConvert.DeserializeObject(contentText);
                        string thumbnailUrl = data[0]["thumbnail_medium"].ToString();
                        video.ThumbnailUrl = thumbnailUrl;
                    }
                    else
                    {
                        // Fallback for unknown video
                        video.ThumbnailUrl = unknownVideoThumbnail;
                    }
                    //JsonConvert.DeserializeObject(
                    break;

                case "YOUTUBE":
                case "DAILYMOTION":
                    break; // not required

                default:
                    throw new NotImplementedException();
            }
        }
    }
}
