﻿using Microsoft.AspNetCore.Html;
using SiteGuide.DAL.Models;
using System;

namespace SiteGuide.Web.Helpers
{
    public static class EditVersionStatusHelper
    {
        public static string BootstrapClass(string statusId) => statusId switch
        {
            "D" => "dark",
            "S" => "warning",
            "A" => "success",
            "R" => "danger",

            _ => throw new NotImplementedException($"Unhandled {nameof(statusId)}\"{statusId}\"."),
        };

        public static string StatusDescription(EditVersion editVersion)
            => editVersion.StatusId switch
            {
                "A" => "Accepted",
                "R" => "Rejected",
                "D" => editVersion.EditVersionNumber == 1
                    ? "Draft"
                    : "Draft Review",
                "S" => editVersion.EditVersionNumber == 1
                    ? "Submitted for review"
                    : "Submitted for further review",

                _ => throw new NotImplementedException(),
            };
    }
}
