﻿using Microsoft.AspNetCore.Html;
using System;

namespace SiteGuide.Web.Helpers
{
    public static class LocalTimeHelper
    {
        public static HtmlString LocalTime(DateTime utcTime) => new HtmlString($"<LocalTime class='badge badge-localtime' data-time='{utcTime:O}'></LocalTime>");
        //public static HtmlString LocalTime(DateTime utcTime) => new HtmlString($"<span class=\"badge badge-light\"><LocalTime data-time='{utcTime:O}'></LocalTime></span>");
    }
}
