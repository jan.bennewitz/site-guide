﻿using Microsoft.AspNetCore.Html;
using SiteGuide.Service;
using System;

namespace SiteGuide.Web.Helpers {
    public static class EditActionHelper {
        public static string BootstrapClass(EditAction editAction) => editAction.Name switch {
            nameof(EditActions.SubmitForReview) => "primary",
            nameof(EditActions.Abandon) => "danger",
            nameof(EditActions.ClaimForReview) => "primary",
            nameof(EditActions.Accept) => "success",
            nameof(EditActions.Reject) => "danger",
            nameof(EditActions.ForwardToSubmitter) => "primary",

            _ => throw new NotImplementedException($"Unhandled {nameof(editAction.Name)}\"{editAction.Name}\"."),
        };
    }
}
