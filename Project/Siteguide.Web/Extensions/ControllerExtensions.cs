﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;
using SiteGuide.Web.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SiteGuide.Web.Extensions
{
    public static class ControllerExtensions
    {
        public static void AddMessage(this Controller controller, MessageType type, string title, string text)
            => AddMessage(controller, new MessageViewModel(type, title, text));

        public static void AddMessage(this Controller controller, MessageViewModel message)
            => AddMessage(controller.TempData, message);

        public static void AddMessage(this PageModel pageModel, MessageType type, string title, string text)
            => AddMessage(pageModel, new MessageViewModel(type, title, text));

        public static void AddMessage(this PageModel pageModel, MessageViewModel message)
            => AddMessage(pageModel.TempData, message);

        private static void AddMessage(ITempDataDictionary tempData, MessageViewModel message)
        {
            if (tempData["Messages"] == null)
                tempData["Messages"] = new List<string>();
            ((IList<string>)tempData["Messages"]).Add(JsonConvert.SerializeObject(message));
        }
    }
}
