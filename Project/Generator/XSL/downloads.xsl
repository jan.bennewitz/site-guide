<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml"
				  media-type="text/html"
				  doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
				  doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
				  indent="yes"/>

	<xsl:param name="maxSitesPerPage" select="20" />
	<xsl:param name="timestampSuffix" />

	<xsl:include href="common.xsl"/>

	<xsl:template match="/">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<xsl:call-template name="common_head">
				<xsl:with-param name="title">Australian National Site Guide - Print or Download</xsl:with-param>
			</xsl:call-template>
			<body>
				<xsl:call-template name="common_menu-top"/>
				<xsl:call-template name="common_header_site-guide"/>
				<div id="wrapper-content">
					<div id="wrapper-content2">
						<xsl:call-template name="common_menu-page_siteguide"/>

						<div class="content">
							<h2 id="print-or-download">Print or Download the Site Guide</h2>
							<p>To limit the size of individual files and printouts, the guide is broken up into the following sections:</p>
							<ul>
								<!--								<xsl:for-each select="siteGuideData//area[.//siteFreeFlight[not(closed)] and ((count(.//siteFreeFlight[not(closed)]) &lt;= $maxSitesPerPage and (count(..//siteFreeFlight[not(closed)]) &gt; $maxSitesPerPage or parent::siteGuideData)) or count(.//siteFreeFlight[not(closed)]) &gt; $maxSitesPerPage and not(area))]">-->
								<xsl:for-each select="siteGuideData/area">
									<xsl:sort select="@name"/>
									<xsl:call-template name="area"/>
								</xsl:for-each>
							</ul>
							<h2 id="other-downloads">Other downloads</h2>
							<p>
								Please contact the <a href="mailto:webmaster@siteguide.org.au">webmaster</a> if you need a custom version of any of these files or have feedback.
							</p>
							<ul>
								<li>
									Kmz file to view the site guide in Google Earth:
									<a href="siteguide.kmz" title="Google Earth kmz">
										<img alt="" src="Images/GEIcon.png"/>
										<xsl:text>Google Earth kmz</xsl:text>
									</a>
								</li>
								<li>
									<p>
										Landings, no landing zones etc. in OpenAir format for flight instruments, XCSoar, SeeYou Navigator etc.:
										<a title="OpenAir file">
											<xsl:attribute name="href">
												<xsl:text>siteGuide.OpenAir</xsl:text>
												<xsl:value-of select="$timestampSuffix"/>
												<xsl:text>.txt</xsl:text>
											</xsl:attribute>
											<xsl:attribute name="download">
												<xsl:text>siteGuide.OpenAir</xsl:text>
												<xsl:value-of select="$timestampSuffix"/>
												<xsl:text>.txt</xsl:text>
											</xsl:attribute>
											<img alt="" src="Images/OA.png" />
											<xsl:text>OpenAir file</xsl:text>
										</a>
									</p>
									<p>
										Combined file with sub-FL125 Airspace:
										<a title="OpenAir file including airspace">
											<xsl:attribute name="href">
												<xsl:text>siteGuideAndSubFl125Airspace.OpenAir</xsl:text>
												<xsl:value-of select="$timestampSuffix"/>
												<xsl:text>.txt</xsl:text>
											</xsl:attribute>
											<xsl:attribute name="download">
												<xsl:text>siteGuideAndSubFl125Airspace.OpenAir</xsl:text>
												<xsl:value-of select="$timestampSuffix"/>
												<xsl:text>.txt</xsl:text>
											</xsl:attribute>
											<img alt="" src="Images/OA.png" />
											<xsl:text>Combined OpenAir file</xsl:text>
										</a>
									</p>
									<p>
										<a href="https://docs.google.com/document/d/1lfMe2K1Znh5NLY4D5-cz99xMVsXxX4dRG5ZOZNp_LXY">Installation guide for XCSoar</a>
									</p>
									<p>
										<a href="https://kb.naviter.com/en/kb/custom-airspace/">Installation guide for SeeYou Navigator</a>
									</p>
									<p>
										Combined file with sub-FL125 Airspace for Flyskyhy:
										<a title="OpenAir file including airspace for Flyskyhy">
											<xsl:attribute name="href">
												<xsl:text>siteGuideAndSubFl125Airspace.FlySkyHy.OpenAir</xsl:text>
												<xsl:value-of select="$timestampSuffix"/>
												<xsl:text>.txt</xsl:text>
											</xsl:attribute>
											<xsl:attribute name="download">
												<xsl:text>siteGuideAndSubFl125Airspace.FlySkyHy.OpenAir</xsl:text>
												<xsl:value-of select="$timestampSuffix"/>
												<xsl:text>.txt</xsl:text>
											</xsl:attribute>
											<img alt="" src="Images/OA.png" />
											<xsl:text>Flyskyhy combined OpenAir file</xsl:text>
										</a>
									</p>
									<p>
										For XCTrack, use this version:
										<a title="OpenAir file for XCTrack">
											<xsl:attribute name="href">
												<xsl:text>siteGuide.XCTrack.OpenAir</xsl:text>
												<xsl:value-of select="$timestampSuffix"/>
												<xsl:text>.txt</xsl:text>
											</xsl:attribute>
											<xsl:attribute name="download">
												<xsl:text>siteGuide.XCTrack.OpenAir</xsl:text>
												<xsl:value-of select="$timestampSuffix"/>
												<xsl:text>.txt</xsl:text>
											</xsl:attribute>
											<img alt="" src="Images/OA.png" />
											<xsl:text>XCTrack OpenAir file</xsl:text>
										</a>
									</p>
									<p>
										<img alt="" src="Images/Siteguide data on XCSoar.jpg" />
									</p>
								</li>
								<li>
									Landings, no landing zones etc. in OziExplorer wpt format for flight instruments, XCSoar, GPS receivers etc.:
									<a title="OziExplorer wpt file">
										<xsl:attribute name="href">
											<xsl:text>siteGuide.OziExplorer</xsl:text>
											<xsl:value-of select="$timestampSuffix"/>
											<xsl:text>.wpt</xsl:text>
										</xsl:attribute>
										<xsl:attribute name="download">
											<xsl:text>siteGuide.OziExplorer</xsl:text>
											<xsl:value-of select="$timestampSuffix"/>
											<xsl:text>.wpt</xsl:text>
										</xsl:attribute>
										<img alt="" src="Images/ozi.png" />
										<xsl:text>OziExplorer wpt file</xsl:text>
									</a>
								</li>
								<li>
									Landings, no landing zones etc. for Garmin GPS receivers (76C, 76CSx, 60Cx etc.):
									<ul>
										<li>
											As waypoints: <a href="siteGuide.gdb" download="siteGuide.gdb" title="Garmin gdb file">
												<xsl:text>Garmin gdb file</xsl:text>
											</a> for Garmin MapSource and BaseCamp
										</li>
										<li>
											<a href="siteGuide.Garmin.img" download="siteGuide.Garmin.img" title="Garmin img file">
												<xsl:text>Garmin img file</xsl:text>
											</a> for upload using <a href="https://www.google.com.au/search?q=sendmap+garmin">sendmap</a>
											(available <a href="Files/sendmap20.exe">here</a>)
										</li>
									</ul>
									<p>
										<img alt="" src="Images/Siteguide data on a 76C.png" />
									</p>
								</li>
							</ul>
							<xsl:call-template name="common_contribute"/>
						</div>
					</div>
				</div>
				<xsl:call-template name="common_footer"/>
			</body>
		</html>
	</xsl:template>

	<xsl:template name="area">
		<xsl:choose>
			<xsl:when test=".//siteFreeFlight[not(closed)] and ((count(.//siteFreeFlight[not(closed)]) &lt;= $maxSitesPerPage and (count(..//siteFreeFlight[not(closed)]) &gt; $maxSitesPerPage or parent::siteGuideData)) or count(.//siteFreeFlight[not(closed)]) &gt; $maxSitesPerPage and not(area))">
				<li xmlns="http://www.w3.org/1999/xhtml">
					<a>
						<xsl:attribute name="href">
							<xsl:text>Site%20Guide%20-%20</xsl:text>
							<xsl:call-template name="common_areaQualifiedName"/>
							<xsl:text>.html</xsl:text>
						</xsl:attribute>
						<xsl:call-template name="common_areaQualifiedName">
							<xsl:with-param name="full" select="'Y'"/>
						</xsl:call-template>
					</a>
					<xsl:text> (</xsl:text>
					<xsl:value-of select="count(.//siteFreeFlight[not(closed)])"/>
					<xsl:text> site</xsl:text>
					<xsl:if test="count(.//siteFreeFlight[not(closed)]) >1">
						<xsl:text>s</xsl:text>
					</xsl:if>
					<xsl:text>)</xsl:text>
				</li>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="area">
					<xsl:sort select="@name"/>
					<xsl:call-template name="area"/>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>