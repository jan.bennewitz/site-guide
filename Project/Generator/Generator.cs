using Geo;
using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Xsl;
//using VHPASiteGuide.Schema;
using static VHPASiteGuide.DrawSettings;

namespace VHPASiteGuide
{
    public class Generator
    {
        public event ProgressEventHandler ProgressEvent;

        private readonly string
          sourceDir,
          dataDir,
          xslDir,
          targetDir,
          sitesDir;

        public Generator(string SourceDir, string TargetDir)
        {
            sourceDir = SourceDir;
            dataDir = Path.Combine(sourceDir, "SiteGuideData");
            xslDir = Path.Combine(sourceDir, "XSL");
            targetDir = TargetDir;
            if (targetDir.EndsWith("\\"))
                targetDir = targetDir.Remove(targetDir.Length - 1);
            sitesDir = targetDir + "\\Sites";

            // Required after migrating .Net Framework 4 to .Net 5:  https://github.com/dotnet/runtime/issues/26969#issuecomment-408175222
            AppContext.SetSwitch("Switch.System.Xml.AllowDefaultResolver", true);
        }

        public void GenerateSpecificSites(string[] SiteNames)
        {
            Generate(SiteNames);
        }

        public void GenerateAll()
        {
            Generate();
        }

        public void DeleteSitePage(string SiteName)
        {
            File.Delete(SitePageFileName(SiteName));
        }

        private void Generate(string[] SiteNames = null)
        {
            string timestampSuffix = "." + DateTime.Now.ToString("yyyyMMdd");

            XmlDocument xmlSites = LoadSiteGuideData();

            Directory.CreateDirectory(targetDir);

            AddCalculatedData(xmlSites);

            //Generate index pages
            //all sites
            RunSimpleXsl(xslDir + "\\sitesindex.xsl", xmlSites, targetDir + "\\sitesindex.html", null);
            //open sites
            XsltArgumentList argsOpenIndex = new XsltArgumentList();
            argsOpenIndex.AddParam("restrictToType", string.Empty, "open");
            RunSimpleXsl(xslDir + "\\sitesindex.xsl", xmlSites, targetDir + "\\sitesopenindex.html", argsOpenIndex);
            //closed sites
            XsltArgumentList argsClosedIndex = new XsltArgumentList();
            argsClosedIndex.AddParam("restrictToType", string.Empty, "closed");
            RunSimpleXsl(xslDir + "\\sitesindex.xsl", xmlSites, targetDir + "\\sitesclosedindex.html", argsClosedIndex);

            ////microlight sites
            //runSimpleXsl(xslDir + "\\sitesmicrolightindex.xsl", xmlSites, targetDir + "\\sitesmicrolightindex.html", null);

            string airspace = GetAirspace(dataDir + "\\ExternalCache\\airspace.OpenAir.txt", "https://xcaustralia.org/download/class_all_NO_fia_&_e%20class%20freq.bnd.php", TimeSpan.FromDays(1));

            //Generate site page(s)
            Directory.CreateDirectory(sitesDir);
            XslCompiledTransform sitePageTransform = new XslCompiledTransform();
            sitePageTransform.Load(xslDir + "\\sitespage.xsl");
            if (SiteNames == null)
            {
                XmlNodeList sites = xmlSites.SelectNodes("//siteFreeFlight");
                RaiseProgressEvent("Generate " + sites.Count + " site pages...");
                foreach (XmlNode site in sites)
                {
                    GenerateSitePage(xmlSites, sitePageTransform, site);
                }
            }
            else
            {
                foreach (string siteName in SiteNames)
                {
                    RaiseProgressEvent("Generate site page for \"" + siteName + "\"");
                    XmlNode site = xmlSites.SelectSingleNode("//siteFreeFlight[name=\"" + siteName + "\"]");

                    GenerateSitePage(xmlSites, sitePageTransform, site);
                }
            }

            //Generate OpenAir file
            var openAirEquivalentAirspaceClasses = new Dictionary<string, string> {
                    { "Landing", "LZ" },
                    { "Access", "G"},
                    { "Feature", "G"},
                    { "No Landing", "GP"}, // Glider prohibited
                    { "Powerline", "Q"}, // Danger
                    { "Hazard", "Q"},
                    { "Hazard Area", "Q"},
                    { "No Launching", "R"}, // Restricted
                    { "Emergency Landing", "R"},
                    { "No Fly Zone", "P"}, //Prohibited
                };
            string fileName = Path.Combine(targetDir, "siteGuide.OpenAir.txt");
            GenerateOpenAirFile(xmlSites, fileName, openAirEquivalentAirspaceClasses);
            CopyWithTimestamp(fileName, timestampSuffix);
            fileName = Path.Combine(targetDir, "siteGuideAndSubFl125Airspace.OpenAir.txt");
            GenerateCombinedFile(xmlSites, fileName, openAirEquivalentAirspaceClasses, airspace);
            CopyWithTimestamp(fileName, timestampSuffix);

            //Generate FlySkyHy OpenAir file
            // Same except for this
            openAirEquivalentAirspaceClasses["Landing"] = "LZ";
            openAirEquivalentAirspaceClasses["Powerline"] = "OBS";
            fileName = Path.Combine(targetDir, "siteGuide.FlySkyHy.OpenAir.txt");
            GenerateOpenAirFile(xmlSites, fileName, openAirEquivalentAirspaceClasses, false);
            CopyWithTimestamp(fileName, timestampSuffix);

            openAirEquivalentAirspaceClasses = new Dictionary<string, string> {
                    { "Landing", "LZ" },
                    { "Access", "SRZ"},
                    { "Feature", "SRZ"},
                    { "No Landing", "GP"},
                    { "Powerline", "OBS"},
                    { "Hazard", "Q"},
                    { "Hazard Area", "Q"},
                    { "No Launching", "GP"},
                    { "Emergency Landing", "R"},
                    { "No Fly Zone", "R"},
                };
            fileName = Path.Combine(targetDir, "siteGuideAndSubFl125Airspace.FlySkyHy.OpenAir.txt");
            GenerateCombinedFile(xmlSites, fileName, openAirEquivalentAirspaceClasses, airspace);
            CopyWithTimestamp(fileName, timestampSuffix);

            //Generate XCTrack OpenAir file
            openAirEquivalentAirspaceClasses = new Dictionary<string, string> {
                    { "Landing", "W" },
                    { "Access", "W"},
                    { "Feature", "W"},
                    { "No Landing", "GP"},
                    { "Powerline", "Q"},
                    { "Hazard", "Q"},
                    { "Hazard Area", "Q"},
                    { "No Launching", "Q"},
                    { "Emergency Landing", "Q"},
                    { "No Fly Zone", "P"},
                };
            fileName = Path.Combine(targetDir, "siteGuide.XCTrack.OpenAir.txt");
            GenerateOpenAirFile(xmlSites, fileName, openAirEquivalentAirspaceClasses);
            CopyWithTimestamp(fileName, timestampSuffix);
            //Generate airspaces definition file for https://airspace.xcontest.org
            GenerateXCTrackJsonFile(xmlSites, Path.Combine(targetDir, "siteGuide.airspace_xcontest_org.json"));
            //Generate OziExplorer wpt file
            fileName = Path.Combine(targetDir, "siteGuide.OziExplorer.wpt");
            GenerateOziExplorerWptFile(xmlSites, fileName);
            CopyWithTimestamp(fileName, timestampSuffix);
            //Generate Garmin gdb file
            RaiseProgressEvent("Generate Garmin gdb file");
            //var procGpsBabel = Process.Start(
            //    Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "libs", "GpsBabel", "gpsbabel.exe"),
            //    "-w -r -t -i ozi -f \"" + Path.Combine(targetDir, "siteGuide.OziExplorer.wpt") + "\" -o gdb -F \"" + Path.Combine(targetDir, "siteGuide.gdb") + "\"");
            GenerateMapSourceFile(xmlSites, Path.Combine(targetDir, "siteGuide.gdb"));
            //Generate Polish format .mp intermediary file
            GeneratePolishFormatFile(xmlSites, Path.Combine(targetDir, "siteGuide.PolishFormat.mp"));
            // Generate Garmin img file
            RaiseProgressEvent("Generate Garmin img file");
            Process.Start(
                Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "libs", "cGPSmapper", "cgpsmapper.exe"),
            "\"" + Path.Combine(targetDir, "siteGuide.PolishFormat.mp") + "\" -o \"" + Path.Combine(targetDir, "siteGuide.Garmin.img") + "\"");

            //Generate kmz
            // for Google Earth
            GenerateKmz(xslDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesAll.kmz", null);

            // for Google Earth - freeflight only
            XsltArgumentList argsFreeflight = new XsltArgumentList();
            argsFreeflight.AddParam("subset", string.Empty, "freeflight");
            GenerateKmz(xslDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesFreeflight.kmz", argsFreeflight);

            //// for Google Earth - microlight only
            //XsltArgumentList argsMicrolight = new XsltArgumentList();
            //argsMicrolight.AddParam("subset", string.Empty, "microlight");
            //generateKmz(xslDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesMicrolight.kmz", argsMicrolight);

            // for Google Earth - closed only
            XsltArgumentList argsClosed = new XsltArgumentList();
            argsClosed.AddParam("subset", string.Empty, "closed");
            GenerateKmz(xslDir + "\\siteskml.xsl", xmlSites, targetDir + "\\sitesClosed.kmz", argsClosed);

            // for Google Earth - CAR166
            string Car166Json = dataDir + "\\VHFAerodromes.json";
            string ersaDatesJson = dataDir + "\\ERSA effective dates.json";
            GenCAR166Kml(Car166Json, ersaDatesJson, targetDir + "\\CAR166.kmz");
            // Also copy JSON for site guide map
            FileInfo fi = new FileInfo(Car166Json);
            fi.CopyTo(Path.Combine(targetDir, fi.Name), true);
            fi = new FileInfo(ersaDatesJson);
            fi.CopyTo(Path.Combine(targetDir, fi.Name), true);

            // for map page
            RunSimpleXsl(xslDir + "\\sitesForGMaps.xsl", xmlSites, targetDir + "\\sitesForGMaps.xml", null);

            //Print/download guide
            int maxSitesPerPage = 20; // max sites per 'download or print' page
            string areasWithPrintGuidesXPath = "siteGuideData//area[" +
              ".//siteFreeFlight[not(closed)] and " +
              "(" +
                "(" +
                  "count(.//siteFreeFlight[not(closed)]) <= " + maxSitesPerPage + " and " +
                  "(count(..//siteFreeFlight[not(closed)]) > " + maxSitesPerPage + " or parent::siteGuideData)" +
                ") " +
                "or count(.//siteFreeFlight[not(closed)]) > " + maxSitesPerPage + " and not(area)" +
              ")" +
            "]";
            XmlNodeList areasWithPrintGuides = xmlSites.SelectNodes(areasWithPrintGuidesXPath);

            //Generate print index
            XsltArgumentList argsDownloads = new XsltArgumentList();
            argsDownloads.AddParam("maxSitesPerPage", string.Empty, maxSitesPerPage);
            argsDownloads.AddParam("timestampSuffix", string.Empty, timestampSuffix);
            RunSimpleXsl(xslDir + "\\downloads.xsl", xmlSites, targetDir + "\\downloads.html", argsDownloads);

            //Generate downloadable/printable site guides
            XslCompiledTransform printGuideTransform = new XslCompiledTransform();
            printGuideTransform.Load(xslDir + "\\sitesForPrint.xsl");
            GeneratePrintGuides(xmlSites, printGuideTransform, areasWithPrintGuides);

            //Include site guide data
            FileInfo siteGuideDataXml = new FileInfo(dataDir + "\\siteGuideData.xml");
            siteGuideDataXml.CopyTo(Path.Combine(targetDir, siteGuideDataXml.Name), true);
            FileInfo siteGuideDataXsd = new FileInfo(dataDir + "\\siteGuideData.xsd");
            siteGuideDataXsd.CopyTo(Path.Combine(targetDir, siteGuideDataXsd.Name), true);

            RaiseProgressEvent("Generated site guide.");
        }

        private string GetAirspace(string fileName, string url, TimeSpan maxAge)
        {
            if (File.Exists(fileName) && DateTime.UtcNow - File.GetLastWriteTimeUtc(fileName) <= maxAge)
                return File.ReadAllText(fileName); // We have recent cached data. Use that.
            else
            {
                // Retrieve airspace data from url.
                var response = new System.Net.Http.HttpClient()
                    .GetAsync(url)
                    .Result;
                if (!response.IsSuccessStatusCode)
                    throw new ApplicationException($"Can not retrieve data: {response.StatusCode} {response.ReasonPhrase}");

                var content = response.Content.ReadAsStringAsync().Result;

                Directory.CreateDirectory(Path.GetDirectoryName(fileName));
                File.WriteAllText(fileName, content); // Cache the data.
                return content;
            }
        }

        private void GenerateCombinedFile(XmlDocument xmlSites, string outFile, Dictionary<string, string> openAirEquivalentAirspaceClasses, string airspaceData)
        {
            var resultBuilder = new StringBuilder();

            // Filter high airspaces
            var airspaces = SplitIntoAirspaceSources(airspaceData);
            foreach (var airspace in airspaces)
            {
                var lines = airspace.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                var al = lines.FirstOrDefault(l => l.TrimStart().StartsWith("AL FL"));
                if (al != null)
                {
                    var alFl = int.Parse(al.TrimStart().Substring("AL FL".Length));
                    if (alFl >= 125) // FL125 = 3800m
                        continue;
                }
                resultBuilder.Append(airspace);
            }

            var siteGuideDataOpenAir = GenerateOpenAirContent(xmlSites, openAirEquivalentAirspaceClasses);

            resultBuilder.Append(siteGuideDataOpenAir);

            File.WriteAllText(outFile, resultBuilder.ToString());
        }

        private static List<string> SplitIntoAirspaceSources(string source)
        {
            var lines = source.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
            var airspaceSources = new List<string>();
            var sourceBuilder = new StringBuilder();
            foreach (var line in lines)
            {
                if (line.TrimStart().StartsWith("AC "))
                {
                    var airspaceSource = sourceBuilder.ToString();
                    airspaceSources.Add(airspaceSource);
                    sourceBuilder.Clear();
                }
                sourceBuilder.AppendLine(line);
            }
            airspaceSources.Add(sourceBuilder.ToString());
            return airspaceSources;
        }

        private static void CopyWithTimestamp(string fileName, string suffix)
        {
            File.Copy(fileName, AddSuffixToFileName(fileName, suffix), true);
        }

        private static string AddSuffixToFileName(string fileName, string suffix)
        {
            return Path.Combine(Path.GetDirectoryName(fileName), Path.GetFileNameWithoutExtension(fileName) + suffix + Path.GetExtension(fileName));
        }

        private void GenerateOpenAirFile(XmlDocument xmlSites, string outFile, Dictionary<string, string> openAirEquivalentAirspaceClasses, bool linesAsPolys = true)
        {
            RaiseProgressEvent("Generate " + new FileInfo(outFile).Name);
            StringBuilder result = GenerateOpenAirContent(xmlSites, openAirEquivalentAirspaceClasses, linesAsPolys);

            //XmlNodeList launches = xmlSites.SelectNodes("siteGuideData//launch");

            File.WriteAllText(outFile, result.ToString());
        }

        private StringBuilder GenerateOpenAirContent(XmlDocument xmlSites, Dictionary<string, string> openAirEquivalentAirspaceClasses, bool linesAsPolys = true)
        {

            //Closest to an OpenAir spec I could find: http://www.winpilot.com/UsersGuide/UserAirspace.asp
            var result = new StringBuilder();
            XmlNodeList mapshapes = xmlSites.SelectNodes("siteGuideData//mapShape[coordinates and not(@category='Access')]");
            foreach (XmlNode mapshape in mapshapes)
            {
                string name = mapshape.SelectSingleNode("name")?.InnerText;
                string category = mapshape.Attributes["category"].Value;
                //                string description = mapshape.SelectSingleNode("description")?.InnerText;

                var drawSettings = DrawSettings.ShapeColours[category];

                //result.AppendLine("T" + (drawSettings.Dimension == 1 ? "O" : "C") + " "
                //    + category
                //    + (!String.IsNullOrWhiteSpace(name) ? ": " + name : ""));
                //var color = System.Drawing.ColorTranslator.FromHtml("#" + drawSettings.Colour);
                //result.AppendLine("SP 0 1 " + color.R + " " + color.G + " " + color.B); // 0 =Solid line, 1 pxel wide
                //if (drawSettings.Dimension == 2)
                //    result.AppendLine("SB -1,-1,-1"); // Make polygon interior transparent
                //result.AppendLine("V Z=100"); // Show these shapes at zoom levels up to 100km. TODO: Make this smarter and dependent on mapShape size

                if (!openAirEquivalentAirspaceClasses.TryGetValue(category, out string openAirEquivAirspaceClass))
                    throw new ApplicationException("Unrecognised category: " + category);

                XmlNodeList polys = mapshape.SelectNodes("coordinates");
                foreach (XmlNode poly in polys)
                {
                    result.AppendLine("AC " + openAirEquivAirspaceClass);
                    result.AppendLine("AN " + category + (!string.IsNullOrWhiteSpace(name) ? ": " + name.Trim().Replace("\r", " ").Replace("\n", " ") : ""));
                    result.AppendLine("AL SFC");
                    result.AppendLine("AH 100 ft AGL");
                    var coords = poly.InnerText.Trim().Split(new string[] { " ", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    if (linesAsPolys)
                    {
                        if (coords.Length == 2)
                        {
                            // HACK: add an extra dummy point in the centre to keep xcsoar happy - it complains if a poly has < 3 points. https://github.com/XCSoar/XCSoar/issues/1011#issuecomment-1245473637
                            var latLngStart = coords[0].Split(',');
                            var latLngEnd = coords[1].Split(',');
                            float lngDeg = (float.Parse(latLngStart[0]) + float.Parse(latLngEnd[0])) / 2;
                            float latDeg = (float.Parse(latLngStart[1]) + float.Parse(latLngEnd[1])) / 2; // This is a good quick and dirty approximation if the line is small relative to the earth and does not cross the 180E/W meridian - seems a pretty safe assumption.

                            var coordsList = coords.ToList();
                            coordsList.Insert(1, lngDeg + "," + latDeg);
                            coords = coordsList.ToArray();
                        }
                        foreach (string coord in coords)
                            AddOpenAirCoord(result, coord, "DP");
                        if (drawSettings.Dimension == 1) // Close the polygon by reversing the path
                            foreach (string coordString in coords.Skip(1).Reverse().Skip(1))
                                AddOpenAirCoord(result, coordString, "DP");
                    }
                    else
                    {
                        string type;
                        if (drawSettings.Dimension == 1)
                        {
                            result.AppendLine("V W=0.054"); // Corridor width of 100m or 0.054 nautical miles
                            type = "DY";
                        }
                        else
                            type = "DP";

                        foreach (string coord in coords)
                            AddOpenAirCoord(result, coord, type);
                    }

                    result.AppendLine();
                }
            }

            return result;
        }

        private void GenerateXCTrackJsonFile(XmlDocument xmlSites, string outFile)
        {
            RaiseProgressEvent("Generate " + new FileInfo(outFile).Name);

            var result = new
            {
                airspaces = new List<XCTrackAirspace>(),
                oaname = "siteguide.org.au " + DateTime.Now.ToString("yyyy-MM-dd H:mm zzz"),
                oadescription = "Australian national site guide data: Landing, no-landing and emergency landing areas, significant powerlines and other data. See https://siteguide.org.au .",
            };

            XmlNodeList mapshapes = xmlSites.SelectNodes("siteGuideData//mapShape[coordinates and not(@category='Access')]");
            foreach (XmlNode mapshape in mapshapes)
            {
                string name = mapshape.SelectSingleNode("name")?.InnerText;
                string category = mapshape.Attributes["category"].Value;
                string description = mapshape.SelectSingleNode("description")?.InnerText;

                var drawSettings = DrawSettings.ShapeColours[category];

                var airchecktypes = new Dictionary<string, string>{
                    { "Landing", "inverse" },
                    { "Access", "inverse" },
                    { "Feature", "inverse" },
                    { "No Landing", "restrict"},
                    { "Powerline", "obstacle"},
                    { "Hazard", "ignore"},
                    { "Hazard Area", "ignore"},
                    { "No Launching", "ignore"},
                    { "Emergency Landing", "ignore"},
                    { "No Fly Zone", "restrict"},
                };

                var shortCategories = new Dictionary<string, string>{
                    { "Landing", "LZ" },
                    { "Access", "Access" },
                    { "Feature", "Feature" },
                    { "No Landing", "NoLZ"},
                    { "Powerline", "Powerline"},
                    { "Hazard", "Haz"},
                    { "Hazard Area", "Haz"},
                    { "No Launching", "NoLaunch"},
                    { "Emergency Landing", "EmgyLZ"},
                    { "No Fly Zone", "NoFly"},
                };

                XmlNodeList polys = mapshape.SelectNodes("coordinates");
                foreach (XmlNode poly in polys)
                {
                    var airspace = new XCTrackAirspace
                    {
                        airlower = new XCTrackAispaceVerticalBound { height = 0, type = "AGL" },
                        airupper = new XCTrackAispaceVerticalBound { height = 100, type = "AGL" },
                        airchecktype = airchecktypes[category],
                        aircatpg = true,
                        airname = shortCategories[category] + (!string.IsNullOrWhiteSpace(name) ? ": " + CollapseWhitespace(name) : ""),
                        airclass = shortCategories[category],
                        components = new List<double[]>(),
                        airparams = new object(),
                        descriptions = description == null ? new object() : new { en = CollapseWhitespace(description) },
                        airpen = new List<int> { 0, 2, drawSettings.Colour.R, drawSettings.Colour.G, drawSettings.Colour.B },
                        airbrush = new List<int> { drawSettings.Colour.R, drawSettings.Colour.G, drawSettings.Colour.B },
                    };

                    var coords = poly.InnerText.Trim().Split(new string[] { " ", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string coord in coords)
                        AddJsonCoordinate(airspace, coord);
                    if (drawSettings.Dimension == 1 && airspace.airchecktype != "obstacle") // close the polygon by reversing the path
                        foreach (string coord in coords.Skip(1).Reverse().Skip(1))
                            AddJsonCoordinate(airspace, coord);

                    result.airspaces.Add(airspace);
                }
            }

            //XmlNodeList launches = xmlSites.SelectNodes("siteGuideData//launch");

            File.WriteAllText(outFile, Newtonsoft.Json.JsonConvert.SerializeObject(result));
        }

        private static string CollapseWhitespace(string text)
        {
            return text == null ? null : Regex.Replace(text, @"\s+", " ").Trim();
        }

        private static void AddJsonCoordinate(XCTrackAirspace airspace, string coord)
        {
            var latLng = coord.Split(',');
            airspace.components.Add(new double[2] { double.Parse(latLng[1]), double.Parse(latLng[0]) });
        }

        private struct XCTrackAirspace
        {
            public string airchecktype;
            public ICollection<int> airpen;
            public ICollection<int> airbrush;
            public XCTrackAispaceVerticalBound airupper;
            public XCTrackAispaceVerticalBound airlower;
            public const object airacttime = null;
            public const object airendtime = null;
            public const object airstarttime = null;
            public object airparams;
            public string airname;
            public const object airautoid = null;
            public const object airemail = null;
            public ICollection<double[]> components;
            public bool aircatpg;
            public string airclass;
            public object descriptions;
        }

        private struct XCTrackAispaceVerticalBound
        {
            public string type;
            public int height;
        }

        private void GenerateOziExplorerWptFile(XmlDocument xmlSites, string outFile)
        {
            RaiseProgressEvent("Generate " + new FileInfo(outFile).Name);

            //http://www.oziexplorer3.com/eng/help/fileformats.html
            var result = new StringBuilder();
            result.AppendLine("OziExplorer Waypoint File Version 1.1");
            result.AppendLine("WGS 84");
            result.AppendLine("Reserved");
            result.AppendLine("Reserved");

            XmlNodeList mapshapes = xmlSites.SelectNodes("siteGuideData//mapShape[coordinates]");
            foreach (XmlNode mapshape in mapshapes)
            {
                string name = mapshape.SelectSingleNode("name")?.InnerText;
                if (name != null)
                    name = OziExplorerWptFileTextClean(name);
                string category = mapshape.Attributes["category"].Value;
                //                string description = mapshape.SelectSingleNode("description")?.InnerText;

                ShapeDrawSetting drawSettings = DrawSettings.ShapeColours[category];
                if (drawSettings.Dimension != 1)
                {
                    int color = OziExplorerColour(drawSettings.Colour);

                    //switch (category)
                    //{
                    //    case "Landing":
                    //    case "Access":
                    //    case "Feature":
                    //        openAirEquivAirspaceClass = "G";
                    //        break;

                    //    case "No Landing":
                    //        openAirEquivAirspaceClass = "GP"; // Glider prohibited
                    //        break;

                    //    case "Powerline":
                    //    case "Hazard":
                    //    case "Hazard Area":
                    //        openAirEquivAirspaceClass = "Q"; //Danger
                    //        break;

                    //    case "No Launching":
                    //    case "Emergency Landing":
                    //        openAirEquivAirspaceClass = "R"; //Restricted
                    //        break;

                    //    case "No Fly Zone":
                    //        openAirEquivAirspaceClass = "P"; //Prohibited
                    //        break;

                    //    default:
                    //        throw new ApplicationException("Unrecognised category: " + category);
                    //}

                    XmlNodeList polys = mapshape.SelectNodes("coordinates");
                    foreach (XmlNode poly in polys)
                    {
                        Track track = Track.CreateFromPointsData(poly.InnerText.Trim(), false);
                        if (track.Points.Count > 2)
                        {
                            LatLng centroid = track.GetCentroid();
                            //result.AppendLine("AC " + openAirEquivAirspaceClass);
                            //result.AppendLine("AN " + category + (!String.IsNullOrWhiteSpace(name) ? ": " + name : ""));
                            //result.AppendLine("AL SFC");
                            //result.AppendLine("AH 100 ft");
                            //var coords = poly.InnerText.Split(new string[] { " ", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                            //foreach (string coord in coords)
                            //    AddOpenAirCoord(result, coord);
                            //if (drawSettings.Dimension == 1) // OpenAir has no line element in the airspace format, so close the polygon by reversing the path
                            //    foreach (string coordString in coords.Skip(1).Reverse().Skip(1))
                            //        AddOpenAirCoord(result, coordString);
                            result.AppendLine($"-1,{category},{centroid.lat},{centroid.lng},,0,1,3,0,{color},{name},0,0,0,-777,6,0,17");
                        }
                    }
                }
            }

            //XmlNodeList launches = xmlSites.SelectNodes("siteGuideData//launch");

            File.WriteAllText(outFile, result.ToString());
        }

        private void GeneratePolishFormatFile(XmlDocument xmlSites, string outFile)
        {
            //http://www.gpsinformation.org/adamnewham/article1/mission_trails_img.txt

            RaiseProgressEvent("Generate " + new FileInfo(outFile).Name);

            var result = new StringBuilder();
            result.AppendLine(@";Generated " + DateTime.Now.ToString() + @"
[IMG ID]
ID=62831853
Name=siteguide.org.au
LBLcoding=6
Codepage=1252
Transparent=Y
MG=N
Numbering=N
Routing=N
Lock=N
ProductCode=1
CopyRight=none
Elevation=m
TreSize=1311
RgnLimit=1024
PreProcess=F
Levels=5
Level0=24
Level1=21
Level2=18
Level3=15
Level4=12
Zoom0=0
Zoom1=1
Zoom2=2
Zoom3=3
Zoom4=4
[END]");

            var garminTypes = new Dictionary<string, int>()
            {
                //["Landing"] = 24, 
                //["Emergency Landing"] = 40,
                //["No Landing"] = 12,
                //["No Fly Zone"] = 12,
                //["Feature"] = 77,
                //["No Launching"] = 12,
                ["Landing"] = 1, // 1: Heavy red, 2 orange, 31 light orange, 8&9 thin red, good for no landing
                ["Emergency Landing"] = 2,
                ["No Landing"] = 8,
                ["No Fly Zone"] = 8,
                ["Hazard"] = 8,
                ["Hazard Area"] = 8,
                ["Feature"] = 2,
                ["No Launching"] = 8,
            };

            // http://magex.sourceforge.net/doc/cGPSmapper-UsrMan-v02.4.pdf
            // Polylines:
            // Hex Decimal Description
            // 0x01 1 Major Highway-thick
            // 0x02 2 Principal Highway-thick
            // 0x03 3 Principal Highway-medium
            // 0x04 4 Arterial Road–medium
            // 0x05 5 Arterial Road-thick
            // 0x06 6 Road - thin
            // 0x07 7 Alley - thick
            // 0x08 8 Ramp
            // 0x09 9 Ramp
            // 0x0a 10 Unpaved Road-thin
            // 0x0b 11 Major Highway Connector - thick
            // 0x0c 12 Roundabout
            // 0x14 20 Railroad
            // 0x15 21 Shoreline
            // 0x16 22 Track / Trail
            // 0x18 24 Stream - thin
            // 0x19 25 Time - Zone
            // 0x1a 26 Ferry
            // 0x1b 27 Ferry
            // 0x1c 28 Political Boundary
            // 0x1d 29 County Boundary
            // 0x1e 30 International Boundary
            // 0x1f 31 River
            // 0x20 32 Land Contour(thin)
            // 0x21 33 Land Contour(medium)
            // 0x22 34 Land Contour(thick)
            // 0x23 35 Depth Contour(thin)
            // 0x24 36 Depth Contour(medium)
            // 0x25 37 Depth Contour(thick)
            // 0x26 38 Intermittent River
            // 0x27 39 Airport Runway
            // 0x28 40 Pipeline
            // 0x29 41 Power line
            // 0x2a 42 Marine Boundary(no line)
            // 0x2b 43 Marine Hazard(no line)
            //
            // Polygons:
            //Hex Decimal Description
            // 0x01 1 City
            // 0x02 2 City
            // 0x03 3 City
            // 0x04 4 Military
            // 0x05 5 Car Park(Parking Lot)
            // 0x06 6 Parking Garage
            // 0x07 7 Airport
            // 0x08 8 Shopping Centre
            // 0x09 9 Marina
            // 0x0a 10 University
            // 0x0b 11 Hospital
            // 0x0c 12 Industrial
            // 0x0d 13 Reservation
            // 0x0e 14 Airport Runway
            // 0x13 19 Man made area
            // 0x14 20 National park
            // 0x15 21 National park
            // 0x16 22 National park
            // 0x17 23 City Park
            // 0x18 24 Golf
            // 0x19 25 Sport
            // 0x1a 26 Cemetery
            // 0x1e 30 State Park
            // 0x1f 31 State Park
            // 0x28 40 Ocean
            // 0x3b 59 Blue - Unknown
            // 0x32 50 Sea
            // 0x3b 59 Blue - Unknown
            // 0x3c 60 Lake
            // 0x3d 61 Lake
            // 0x3e 62 Lake
            // 0x3f 63 Lake
            // 0x40 64 Lake
            // 0x41 65 Lake
            // 0x42 66 Lake
            // 0x43 67 Lake
            // 0x44 68 Lake
            // 0x45 69 Blue - Unknown
            // 0x46 70 River
            // 0x47 71 River
            // 0x48 72 River
            // 0x49 73 River
            // 0x4b 75 Background
            // 0x4c 76 Intermittent River/ Lake
            // 0x4d 77 Glacier
            // 0x4e 78 Orchard or plantation
            // 0x4f 79 Scrub
            // 0x50 80 Woods
            // 0x51 81 Wetland
            // 0x52 82 Tundra
            // 0x53 83 Flats

            foreach (XmlNode mapshape in xmlSites.SelectNodes("siteGuideData//mapShape[coordinates]"))
            {
                string name = mapshape.SelectSingleNode("name")?.InnerText;
                if (name != null)
                    name = OziExplorerWptFileTextClean(name);

                string category = mapshape.Attributes["category"].Value;
                ShapeDrawSetting drawSettings = DrawSettings.ShapeColours[category];

                if (drawSettings.Dimension != 1)
                    foreach (XmlNode poly in mapshape.SelectNodes("coordinates"))
                    {
                        Track track = Track.CreateFromPointsData(poly.InnerText.Trim(), false);
                        if (track.Points.Count >= 3)
                        {
                            result.AppendLine();
                            result.AppendLine("[POLYLINE]");
                            result.AppendLine("Type=0x" + garminTypes[category].ToString("x4"));
                            result.AppendLine("Label=" + (category + (name != null ? " - " + name : "")).ToUpperInvariant());
                            result.AppendLine("Levels=3");
                            result.AppendLine("Data0=" + string.Join(",", track.Points.Select(p => $"({Math.Round(p.lat, 5)},{Math.Round(p.lng, 5)})")));
                            result.AppendLine("[END]");
                        }
                    }
            }

            File.WriteAllText(outFile, result.ToString());
        }

        private static int OziExplorerColour(Color colour) => colour.B * 0x10000 + colour.G * 0x100 + colour.R;

        private static string OziExplorerWptFileTextClean(string text) => text.Replace("\r", "").Replace("\n", "").Replace(",", ((char)209).ToString()).Trim();

        private void AddOpenAirCoord(StringBuilder result, string coord, string type)
        {
            var latLng = coord.Split(',');
            float lngDeg = float.Parse(latLng[0]);
            float latDeg = float.Parse(latLng[1]);
            result.AppendLine($"{type} {CoordToDMMSSH(latDeg, "N", "S")} {CoordToDMMSSH(lngDeg, "E", "W")}"); //e.g. 38:56:00.00 N 120:02:00.00 W
        }

        /// <summary>Convert decimal coordinate to e.g. 38:56:00.00 N or 120:02:00.00 W</summary>
        private string CoordToDMMSSH(float coord, string posHemisphere, string negHemisphere)
        {
            //http://stackoverflow.com/questions/3249700/convert-degrees-minutes-seconds-to-decimal-coordinates
            double sec = Math.Round(coord * 3600, 2);
            int deg = (int)sec / 3600;
            sec = Math.Abs(sec % 3600);
            int min = (int)sec / 60;
            sec %= 60;
            return Math.Abs(deg) + ":" + min.ToString("D2") + ":" + sec.ToString("00.00") + " " + (coord < 0 ? negHemisphere : posHemisphere);
        }

        private void GenerateMapSourceFile(XmlDocument xmlSites, string outFile)
        {
            RaiseProgressEvent("Generate " + new FileInfo(outFile).Name);

            var categorySymbolCodes = new Dictionary<string, byte> {
                { "Landing", 0x0F },
                { "Feature", 0x12 },
                { "No Landing", 0x10 },
                { "No Launching", 0xDC },
                { "Emergency Landing", 0xAF },
                { "No Fly Zone", 0x41 },
                { "Hazard", 0x8C },
                { "Hazard Area", 0x8C }
            };
            var categoryCodes = new Dictionary<string, string> {
                { "Landing", "LZ" },
                { "Feature", "Feature" },
                { "No Landing", "NoLZ" },
                { "No Launching", "NoLaunch" },
                { "Emergency Landing", "EmergLZ" },
                { "No Fly Zone", "NoFly" },
                { "Hazard", "Haz" },
                { "Hazard Area", "Haz" }
            };

            var namesUsed = new List<string>();

            using BinaryWriter writer = new BinaryWriter(File.Open(outFile, FileMode.Create));
            // Header
            writer.Write(new byte[] { 0x4D, 0x73, 0x52, 0x63, 0x66, 0x00, 0x02, 0x00,
                    0x00, 0x00, 0x44, 0x6D, 0x00, 0x1B, 0x00, 0x00,
                    0x00, 0x41, 0x67, 0x02, 0x73, 0x71, 0x61, 0x00,
                    0x53, 0x65, 0x70, 0x20, 0x32, 0x31, 0x20, 0x32,
                    0x30, 0x30, 0x39, 0x00, 0x31, 0x35, 0x3A, 0x33,
                    0x33, 0x3A, 0x35, 0x32, 0x00, 0x4D, 0x61, 0x70,
                    0x53, 0x6F, 0x75, 0x72, 0x63, 0x65, 0x00 });

            var timestamp = BitConverter.GetBytes(Convert.ToInt32((DateTime.Now.ToLocalTime() - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds));

            foreach (XmlNode mapshape in xmlSites.SelectNodes("siteGuideData//mapShape[coordinates]"))
            {
                string name = mapshape.SelectSingleNode("name")?.InnerText.Trim();
                if (name != null)
                    name = OziExplorerWptFileTextClean(name); // TODO: Required?
                string category = mapshape.Attributes["category"].Value;
                string description = Regex.Replace(mapshape.SelectSingleNode("description")?.InnerText.Trim() ?? "", @"\s+", " ");

                ShapeDrawSetting drawSettings = DrawSettings.ShapeColours[category];
                if (drawSettings.Dimension != 1)
                {
                    string categoryCode = categoryCodes[category];
                    foreach (XmlNode poly in mapshape.SelectNodes("coordinates"))
                    {
                        Track track = Track.CreateFromPointsData(poly.InnerText.Trim(), false);

                        string baseName = categoryCode + (name == null ? "" : ":" + name);

                        string uniqueName = baseName;
                        int i = 0;
                        while (namesUsed.Contains(uniqueName))
                        {
                            i++;
                            uniqueName = baseName + "." + i;
                        }
                        namesUsed.Add(uniqueName);

                        LatLng centroid = track.GetCentroid();
                        var waypointData = Encoding.ASCII.GetBytes(uniqueName)
                        .Concat(new byte[] {
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
                                0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF })
                        .Concat(BitConverter.GetBytes(Convert.ToInt32(centroid.lat / 180 * int.MaxValue)))
                        .Concat(BitConverter.GetBytes(Convert.ToInt32(centroid.lng / 180 * int.MaxValue)))
                        .Concat(new byte[] { 0x00 })
                        .Concat(Encoding.ASCII.GetBytes(description ?? ""))
                        .Concat(new byte[] { 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 })
                        .Concat(new byte[] { categorySymbolCodes[category] })
                        .Concat(new byte[] {
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01 })
                        .Concat(timestamp)
                        .Concat(new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 });

                        writer.Write(BitConverter.GetBytes(waypointData.Count()));
                        writer.Write((byte)0x57);
                        writer.Write(waypointData.ToArray());
                    }
                }
            }

            //Footer
            writer.Write(new byte[] { 0x02, 0x00, 0x00, 0x00, 0x56, 0x00, 0x01 });
        }

        private static void AddCalculatedData(XmlDocument xmlSites)
        {
            PolylineEncoder polyEncoderDetail = new PolylineEncoder(18, 2, 1e-5, true);

            var allMapShapeNodes = xmlSites.SelectNodes("//mapShape");
            var mapShapeBounds = new LatLngBounds[allMapShapeNodes.Count];

            //Assign an id to each mapShape and calculate bounds of each shape
            int mapShapeId = 0;
            foreach (XmlNode mapShapeNode in allMapShapeNodes)
            {
                mapShapeNode.Attributes.Append(xmlSites.CreateAttribute("id")).Value = mapShapeId.ToString();

                mapShapeBounds[mapShapeId] = new LatLngBounds();

                foreach (XmlNode coordNode in mapShapeNode.SelectNodes("coordinates"))
                {
                    Track track = Track.CreateFromPointsData(coordNode.InnerText.Trim(), false);

                    //encode mapShape coordinates
                    XmlNode encodedCoordsNode = mapShapeNode.AppendChild(xmlSites.CreateElement("encodedCoordinates"));
                    encodedCoordsNode.InnerText = polyEncoderDetail.DPEncode(track)["encodedPoints"];
                    encodedCoordsNode.Attributes.Append(xmlSites.CreateAttribute("level")).InnerText = "detail";

                    mapShapeBounds[mapShapeId].expand(track.GetBounds());
                }

                mapShapeId++;
            }

            //Process maps for each site with relevant mapShapes
            foreach (XmlNode siteNode in xmlSites.SelectNodes("//siteFreeFlight[mapShape|mapShapeRef]"))
            {
                var siteMapShapeBounds = new List<LatLngBounds>();
                foreach (XmlNode mapShapeNode in GetMapShapesInclRef(siteNode))
                {
                    siteMapShapeBounds.Add(mapShapeBounds[Convert.ToInt32(mapShapeNode.Attributes["id"].Value)]);
                    //XmlNode mapShapeBoundsNode = mapShapeNode.AppendChild(xmlSites.CreateElement("boundingBox"));
                    //mapShapeBoundsNode.Attributes.Append(xmlSites.CreateAttribute("latMin")).InnerText = mapShapeBounds[i].latMin.ToString();
                    //mapShapeBoundsNode.Attributes.Append(xmlSites.CreateAttribute("latMax")).InnerText = mapShapeBounds[i].latMax.ToString();
                    //mapShapeBoundsNode.Attributes.Append(xmlSites.CreateAttribute("lngMin")).InnerText = mapShapeBounds[i].lngMin.ToString();
                    //mapShapeBoundsNode.Attributes.Append(xmlSites.CreateAttribute("lngMax")).InnerText = mapShapeBounds[i].lngMax.ToString();

                }
                var maps = GetMaps(siteMapShapeBounds, 445, 445);
                LatLngBounds overviewMap = maps[0];

                int overviewMapZoom = overviewMap.GetGMapZoom(445, 445);
                //siteNode.Attributes.Append(xmlSites.CreateAttribute("overviewMapZoom")).InnerText = overviewMapZoom.ToString();

                PolylineEncoder polyEncoderOverviewMap = new PolylineEncoder(18, 2, 8e-6 * Math.Pow(2, 19 - overviewMapZoom), true);

                //XmlNode overviewMapNode = siteNode.AppendChild(xmlSites.CreateElement("overviewMap"));
                //overviewMapNode.Attributes.Append(xmlSites.CreateAttribute("centerLat")).InnerText = allShapesBounds.Center.lat.ToString();
                //overviewMapNode.Attributes.Append(xmlSites.CreateAttribute("centerLng")).InnerText = allShapesBounds.Center.lng.ToString();
                //overviewMapNode.Attributes.Append(xmlSites.CreateAttribute("spanLat")).InnerText = (allShapesBounds.latMax - allShapesBounds.latMin).ToString();
                //overviewMapNode.Attributes.Append(xmlSites.CreateAttribute("spanLng")).InnerText = (allShapesBounds.lngMax - allShapesBounds.lngMin).ToString();


                string mapShapesUrl = GetMapShapesUrl(siteNode, polyEncoderOverviewMap);
                //if (staticMapUrl.Length > 2000) //2048 length limit - the xslt will add further to this.
                //{
                //    // Try again using a larger value of verysmall:
                //    PolylineEncoder polyEncoderOverviewMapLessDetail = new PolylineEncoder(1, 2, 1e-5 * Math.Pow(2, 19 - overviewMapZoom), true);
                //    staticMapUrl = getStaticMapUrl(siteNode, polyEncoderOverviewMapLessDetail);
                if (mapShapesUrl.Length > 8000) // limit has benn increased from 2048
                    throw new ApplicationException("Static Maps API URL too long.");
                //}
                siteNode.AppendChild(xmlSites.CreateElement("mapShapesUrl")).InnerText = mapShapesUrl;
            }

            //encode launch coordinates ** currently not used **
            //Track launchCoords = new Track();
            //foreach (XmlNode markerNode in xmlSites.SelectNodes("//siteFreeFlight/launch|//siteMicrolight"))
            //{
            //    launchCoords.AddTrackpoint(markerNode.SelectSingleNode("lat").InnerText, markerNode.SelectSingleNode("lon").InnerText);
            //}
            //string encodedCoords = PolylineEncoder.CreateEncodings(launchCoords, 0, 1)["encodedPoints"];
            //xmlSites.DocumentElement.AppendChild(xmlSites.CreateElement("encodedMarkerCoordinates")).InnerText = encodedCoords;
        }

        private static string GetMapShapesUrl(XmlNode siteNode, PolylineEncoder polyEncoderOverviewMap)
        {
            var result = new StringBuilder();
            //            result.Append(String.Format("https://maps.google.com/maps/api/staticmap?key=AIzaSyARig6PIprQX7r4jp4oj4u8_-ZHwY-Qtn0&size={0}x{0}&maptype=hybrid&markers=icon:http:/siteguide.org.au/Images/mm_20_green.png", size));
            result.Append(GetMarkers(siteNode, "[not(closed|../closed)]", "https://siteguide.org.au/Images/mm_20_green.png"));
            result.Append(GetMarkers(siteNode, "[closed|../closed]", "https://siteguide.org.au/Images/mm_20_gray.png"));
            //TODO: Add any other launches in view, e.g. Ben Nevis to Sugarloaf?
            var mapShapeNodes = GetMapShapesInclRef(siteNode);
            foreach (XmlNode mapShapeNode in mapShapeNodes)
            {
                BuildGMapUrl(polyEncoderOverviewMap, result, mapShapeNode);
            }

            return result.ToString();
        }

        private static string GetMarkers(XmlNode siteNode, string selector, string icon)
        {
            XmlNodeList launchNodes = siteNode.SelectNodes("launch" + selector);
            if (launchNodes.Count == 0)
                return "";

            StringBuilder result = new StringBuilder($"&markers=icon:{icon}");
            foreach (XmlNode launchNode in launchNodes)
            {
                string lat = launchNode.SelectSingleNode("lat").InnerText;
                string lon = launchNode.SelectSingleNode("lon").InnerText;

                result.Append($"|{lat},{lon}");
            }
            return result.ToString();
        }

        //Get all mapShapes of this siteFreeFlight as well as all referenced mapShapes
        private static IEnumerable<XmlNode> GetMapShapesInclRef(XmlNode siteNode)
        {
            IEnumerable<XmlNode> mapShapeNodes = siteNode.SelectNodes("mapShape").Cast<XmlNode>();
            foreach (XmlNode mapShapeRefNode in siteNode.SelectNodes("mapShapeRef"))
            {
                if ((mapShapeRefNode.Attributes["site"] == null && mapShapeRefNode.Attributes["mapShape"] == null)
                || (mapShapeRefNode.Attributes["site"] != null && mapShapeRefNode.Attributes["mapShape"] != null))
                    throw new ApplicationException("MapshapeRef must specify exactly one of mapShape or site.");

                XmlNodeList refMapShapeNodes;
                if (mapShapeRefNode.Attributes["site"] != null)
                {
                    refMapShapeNodes = siteNode.SelectNodes(string.Format("//siteFreeFlight[name=\"{0}\"]/mapShape", mapShapeRefNode.SelectSingleNode("@site").Value));
                }
                else
                {
                    string refName = mapShapeRefNode.SelectSingleNode("@mapShape").Value;
                    refMapShapeNodes = siteNode.SelectNodes($"//mapShape[name=\"{refName}\"]");

                    if (refMapShapeNodes.Count == 0)
                        throw new ApplicationException($"mapShapeRef not found: \"{refName}\"");
                    if (refMapShapeNodes.Count > 1)
                        throw new ApplicationException("Multiple mapShapeRef matches found.");
                }
                mapShapeNodes = mapShapeNodes.Cast<XmlNode>().Concat(refMapShapeNodes.Cast<XmlNode>());
            }
            return mapShapeNodes;
        }

        private static void BuildGMapUrl(PolylineEncoder polyEncoderOverviewMap, StringBuilder result, XmlNode mapShapeNode)
        {
            var shapeDrawSettings = DrawSettings.ShapeColours[mapShapeNode.Attributes["category"].Value];
            string colour = (shapeDrawSettings.Colour.R * 0x10000 + shapeDrawSettings.Colour.G * 0x100 + shapeDrawSettings.Colour.B).ToString("X6").ToLowerInvariant();

            foreach (XmlNode coords in mapShapeNode.SelectNodes("coordinates"))
            {
                Track track = Track.CreateFromPointsData(coords.InnerText.Trim(), false);
                Dictionary<string, string> encodedTrack = polyEncoderOverviewMap.DPEncode(track);

                result.Append("&path=weight:1|");
                if (shapeDrawSettings.Dimension == 2 && encodedTrack["encodedLevels"].Length > 2) // Don't bother filling the area in if it's been reduced to a line - shorter url this way
                    result.Append($"fillcolor:0x{colour}33|");
                result.Append($"color:0x{colour}{(shapeDrawSettings.Dimension == 1 ? "99" : "66")}|enc:{encodedTrack["encodedPoints"]}");
            }
        }

        private static List<LatLngBounds> GetMaps(List<LatLngBounds> shapesBounds, int pixelWidth, int pixelHeight)
        {
            var result = new List<LatLngBounds>();

            //overview map
            var overviewMap = new LatLngBounds();
            foreach (var bounds in shapesBounds)
            {
                overviewMap.expand(bounds);
            }
            result.Add(overviewMap);

            var unrepresented = GetUnrepresented(shapesBounds, overviewMap, pixelWidth, pixelHeight);

            //Sort shapes by size descending
            unrepresented.Sort((bounds1, bounds2) => Math.Sign(Math.Max(bounds2.SizeLat, bounds2.SizeLng) - Math.Max(bounds1.SizeLat, bounds1.SizeLng)));

            while (unrepresented.Count > 0)
            {
                var detailMap = unrepresented[0]; //Start with largest bounds
                var includedShapes = new List<LatLngBounds> { detailMap };

                unrepresented.RemoveAt(0);

                //Expand to include next largest shape as long as this doesn't drop any contained shape under minimum size
                int i = 0;
                while (i < unrepresented.Count())
                {
                    var expandedMap = new LatLngBounds();
                    expandedMap.expand(detailMap);
                    expandedMap.expand(unrepresented[i]);
                    if (GetUnrepresented(includedShapes, expandedMap, pixelWidth, pixelHeight).Count == 0)
                    {
                        detailMap = expandedMap;
                        includedShapes.Add(unrepresented[i]);
                        unrepresented.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }

                result.Add(detailMap);
            }
            return result;
        }

        //Return the list of bounds which would be drawn too small on the specified map
        private static List<LatLngBounds> GetUnrepresented(List<LatLngBounds> shapesBounds, LatLngBounds mapBounds, int pixelWidth, int pixelHeight)
        {
            //detail maps
            const int MinimumShapeSizePixels = 20;
            var detailShapeBounds = new List<LatLngBounds>();
            foreach (var shapeBounds in shapesBounds)
            {
                if (shapeBounds.GMapSizeInPixel(mapBounds.GetGMapZoom(pixelWidth, pixelHeight)) < MinimumShapeSizePixels)
                    detailShapeBounds.Add(shapeBounds);
            }
            return detailShapeBounds;
        }

        private void GeneratePrintGuides(XmlDocument xmlSites, XslCompiledTransform printGuideTransform, XmlNodeList areas)
        {
            foreach (XmlNode area in areas)
            {
                XmlWriter pageResult = XmlWriter.Create(targetDir + "\\Site Guide - " + AreaFullName(area) + ".html", printGuideTransform.OutputSettings);
                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("area", string.Empty, area);
                printGuideTransform.Transform(xmlSites, args, pageResult);
                pageResult.Close();
            }
        }

        public void ExtractData()
        {
            XmlDocument xmlSites = LoadSiteGuideData();

            string targetDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); //@"D:\Desktop";

            Directory.CreateDirectory(targetDir);

            StringBuilder result = new StringBuilder();
            result.AppendLine(string.Join(",", new string[]{
                    "Site name",
                    //"Site short location",
                    //"Site extended location",
                    "Site land owners",
                    //"Site contact",
                    "Site responsible",
                    "Launch name",
                    //"Launch description",
                    "Launch lat",
                    "Launch lon"
                    }));

            //XmlNodeList launches = xmlSites.SelectNodes("//area[@name='VIC']//launch");
            XmlNodeList launches = xmlSites.SelectNodes("//area//launch");
            foreach (XmlNode launch in launches)
            {
                result.AppendLine(string.Join(",", FieldsContents(launch, new string[]{
                    "../name",
                    //"../shortlocation",
                    //"../extendedlocation",
                    "../landowners",
                    //"../contactName",
                    "../responsibleName",
                    "name",
                    //"description",
                    "lat",
                    "lon"
                    })));
            }

            RaiseProgressEvent("Extracted data.");

            using StreamWriter outfile = new StreamWriter(targetDir + @"\Launches.csv");
            outfile.Write(result.ToString());
        }

        private string[] FieldsContents(XmlNode node, string[] selectors)
        {
            string[] result = new string[selectors.Count()];
            for (int i = 0; i < selectors.Count(); i++)
            {
                string selector = selectors[i];
                XmlNode field = node.SelectSingleNode(selector);
                if (field != null)
                {
                    result[i] = "\"" + field.InnerText.Replace("\n", "").Replace("\"", "'").Trim() + "\"";
                }
                else
                    result[i] = "";
            }
            return result;
        }

        private static string AreaFullName(XmlNode area)
        {
            string areaName = area.Attributes.GetNamedItem("name").InnerText;
            if (area.ParentNode.Name == "area")
                return AreaFullName(area.ParentNode) + " - " + areaName;
            else
                return areaName;
        }

        private void GenerateSitePage(XmlDocument xmlSites, XslCompiledTransform sitePageTransform, XmlNode site)
        {
            XmlWriter pageResult = XmlWriter.Create(SitePageFileName(site.SelectSingleNode("name").InnerText), sitePageTransform.OutputSettings);
            GenerateSitePage(xmlSites, sitePageTransform, site, pageResult);
        }

        public static void GenerateSitePage(XmlDocument xmlSites, XslCompiledTransform sitePageTransform, XmlNode site, XmlWriter pageResult)
        {
            XsltArgumentList args = new XsltArgumentList();
            args.AddParam("siteName", string.Empty, site.SelectSingleNode("name").InnerText);
            sitePageTransform.Transform(xmlSites, args, pageResult);
            pageResult.Close();
        }

        private string SitePageFileName(string siteName)
        {
            return sitesDir + "\\" + siteName + ".html";
        }

        private XmlDocument LoadSiteGuideData()
        {
            //var ser = new System.Xml.Serialization.XmlSerializer(typeof(siteGuideDataType));
            //var siteGuideData = (siteGuideDataType)ser.Deserialize(new StreamReader(dataDir + "\\siteGuideData.xml"));

            //Load and validate
            RaiseProgressEvent("Load data ...");
            XmlReaderSettings settings = new XmlReaderSettings() { ValidationType = ValidationType.Schema };
            settings.Schemas.Add(null, dataDir + "\\siteGuideData.xsd");
            //settings.ValidationEventHandler += new ValidationEventHandler(ValidationEventHandler);

            XmlReader reader = XmlReader.Create(dataDir + "\\siteGuideData.xml", settings);
            XmlDocument xmlSites = new XmlDocument();
            xmlSites.Load(reader);
            reader.Close();

            xmlSites.Validate(new ValidationEventHandler(ValidationEventHandler));
            RaiseProgressEvent("Data loaded and valid.");
            return xmlSites;
        }

        private void GenerateKmz(string transformFile, XmlDocument xmlSites, string targetFile, XsltArgumentList args)
        {
            RaiseProgressEvent("Generate " + new FileInfo(targetFile).Name);
            XslCompiledTransform transform = new XslCompiledTransform();
            transform.Load(transformFile);
            MemoryStream kml = new MemoryStream();
            XmlWriter result = XmlWriter.Create(kml, transform.OutputSettings);
            transform.Transform(xmlSites, args, result);
            result.Close();
            byte[] buffer = new byte[(int)kml.Length];
            kml.Position = 0;
            kml.Read(buffer, 0, (int)kml.Length);
            KmlSaveAsKmz(buffer, targetFile);
        }

        private void RunSimpleXsl(string transformFile, XmlDocument source, string targetFile, XsltArgumentList args)
        {
            RaiseProgressEvent("Generate " + new FileInfo(targetFile).Name);
            var transform = new XslCompiledTransform(true);
            transform.Load(transformFile);
            XmlWriter result = XmlWriter.Create(targetFile, transform.OutputSettings);
            transform.Transform(source, args, result);
            result.Close();
        }

        public void CopyIncludeDir(string includeDir)
        {
            RaiseProgressEvent("Copy Include directory");
            CopyDir(new DirectoryInfo(includeDir), new DirectoryInfo(targetDir));
        }

        private static void CopyDir(DirectoryInfo source, DirectoryInfo target)
        {
            //A simple directory copy.

            //ignore svn source control dirs
            if (source.Name == ".svn") return;

            //Copy all files
            foreach (FileInfo fi in source.GetFiles())
            {
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }

            // Copy all subdirectories
            foreach (DirectoryInfo dir in source.GetDirectories())
            {
                CopyDir(dir, target.CreateSubdirectory(dir.Name));
            }
        }

        public int ReadCanungraAreas(string sourceFile, string outputFile)
        {
            //Load and validate
            RaiseProgressEvent("Load data ...");
            XmlReader reader = XmlReader.Create(sourceFile);
            XmlDocument xmlSource = new XmlDocument();
            xmlSource.Load(reader);
            reader.Close();

            var xmlOut = new XDocument(new XElement("root"));

            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlSource.NameTable);
            nsmgr.AddNamespace("x", xmlSource.DocumentElement.NamespaceURI);
            var polyFolder = xmlSource.SelectSingleNode("//x:Folder[x:name=\"Landing and No Landing Polygons\"]", nsmgr);
            var allPolys = polyFolder.SelectNodes("x:Placemark", nsmgr);
            foreach (XmlNode poly in allPolys)
            {
                string name = poly.SelectSingleNode("x:name", nsmgr).InnerText;

                var mapShape = new XElement("mapShape",
                        new XElement("name", name),
                        new XElement("description", new XCData("Imported from the 'Landing and No Landing Polygons' section of <a href='http://www.chgc.asn.au/Resources/Online-Maps'>http://www.chgc.asn.au/Resources/Online-Maps</a>"))
                    );

                double area = 0;
                foreach (XmlNode coords in poly.SelectNodes(".//x:coordinates", nsmgr))
                {
                    string coordinates = Regex.Replace(coords.InnerText, @"\s{2,}", " ").Trim();
                    mapShape.Add(new XElement("coordinates", coordinates));
                    area += Track.CreateFromPointsData(coordinates, false).GetArea();
                }

                var styleMapUrl = poly.SelectSingleNode("x:styleUrl", nsmgr).InnerText;
                Debug.Assert(styleMapUrl.First() == '#');

                var styleMap = xmlSource.SelectSingleNode("//x:StyleMap[@id='" + styleMapUrl.Substring(1) + "']", nsmgr);
                string normalStyleUrl = styleMap.SelectSingleNode("x:Pair[x:key='normal']/x:styleUrl", nsmgr).InnerText;

                var style = xmlSource.SelectSingleNode("//x:Style[@id='" + normalStyleUrl.Substring(1) + "']", nsmgr);
                string lineColorHex = style.SelectSingleNode("x:LineStyle/x:color", nsmgr).InnerText;
                string a = lineColorHex.Substring(0, 2);
                string b = lineColorHex.Substring(2, 2);
                string g = lineColorHex.Substring(4, 2);
                string r = lineColorHex.Substring(6, 2);

                var lineColor = Color.FromArgb((int)Convert.ToUInt32(a, 16), (int)Convert.ToUInt32(r, 16), (int)Convert.ToUInt32(g, 16), (int)Convert.ToUInt32(b, 16));
                double distanceFromRed = BasicColorDistance(lineColor, Color.Red);
                double distanceFromYellow = BasicColorDistance(lineColor, Color.Yellow);

                string category;
                if (distanceFromYellow < 90)
                {
                    Debug.Assert(area > 0);
                    category = "Landing";
                }
                else if (distanceFromRed < 90)
                {
                    if (area == 0)
                        category = "Powerline";
                    else
                        category = "No Landing";
                }
                else
                    throw new ArgumentException("Could not map polygon color to mapshape category.");

                mapShape.Add(new XAttribute("category", category));

                xmlOut.Root.Add(mapShape);
            }

            xmlOut.Save(outputFile);
            return allPolys.Count;
        }

        private double BasicColorDistance(Color color1, Color color2)
        {
            return Math.Sqrt(Math.Pow(color1.B - color2.B, 2) + Math.Pow(color1.G - color2.G, 2) + Math.Pow(color1.R - color2.R, 2));
        }

        static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    Console.WriteLine("Error: {0}", e.Message);
                    break;
                case XmlSeverityType.Warning:
                    Console.WriteLine("Warning {0}", e.Message);
                    break;
            }

        }

        void RaiseProgressEvent(string description)
        {
            ProgressEvent?.Invoke(this, new ProgressEventArgs(description));
        }

        static void KmlSaveAsKmz(byte[] kmlBytes, string outFile)
        {
            using (System.IO.MemoryStream ms = new MemoryStream())
            using (ZipOutputStream zs = new ZipOutputStream(ms))
            {
                zs.SetLevel(6);
                zs.IsStreamOwner = true;

                //create zipped kml entry
                ZipEntry zipEntry = new ZipEntry("Doc.kml")
                {
                    DateTime = DateTime.Now,
                    Size = kmlBytes.Length
                };

                zs.PutNextEntry(zipEntry);
                zs.Write(kmlBytes, 0, kmlBytes.Length);
                zs.CloseEntry();

                zs.Finish();

                //write to disk
                using FileStream kmz = new FileStream(outFile, FileMode.Create);
                kmz.Write(ms.ToArray(), 0, (int)ms.Length);
                zs.Close();
            }
            // For debug/comparison purposes, also save as kml
            string kmlFilename;
            if (outFile.EndsWith(".kmz"))
                kmlFilename = outFile.Substring(0, outFile.Length - 4) + ".kml";
            else
                kmlFilename = outFile + ".kml";

            using FileStream kml = new FileStream(kmlFilename, FileMode.Create);
            kml.Write(kmlBytes, 0, kmlBytes.Length);
        }

        public void GenCAR166Kml(string JSONFilePath, string ErsaDatesFilePath, string targetFileName)
        {
            TextReader reader = new StreamReader(JSONFilePath);
            var aerodromes = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(reader.ReadToEnd());
            reader.Close();

            //Data source: https://www.airservicesaustralia.com/industry-info/aeronautical-information-management/document-amendment-calendar/
            reader = new StreamReader(ErsaDatesFilePath);
            var ersaDateStrings = JsonConvert.DeserializeObject<List<string>>(reader.ReadToEnd());
            reader.Close();
            IEnumerable<DateTime> ersaDates = ersaDateStrings.Select(s => DateTime.Parse(s));
            DateTime? ersaDate = ersaDates.Where(d => d <= DateTime.Today).Max();
            if (ersaDate == null)
                throw new ApplicationException("No valid ERSA date found.");

            DateTime lastErsaDate = ersaDates.Max();
            RaiseProgressEvent("Using ERSA date: " + ((DateTime)ersaDate).ToShortDateString());
            RaiseProgressEvent("Last known ERSA date: " + lastErsaDate.ToShortDateString());
            if (lastErsaDate <= DateTime.Today)
                RaiseProgressEvent($"Warning: Using last known ERSA date. Please update {ErsaDatesFilePath} from https://www.airservicesaustralia.com/industry-info/aeronautical-information-management/document-amendment-calendar/ .");

            var kmlBuilder = new StringBuilder();
            kmlBuilder.AppendLine(
        @"<?xml version='1.0' encoding='UTF-8'?>
<kml xmlns='http://www.opengis.net/kml/2.2'>
<Folder>
<Style id='CAR166Disk'><PolyStyle><color>7f1767E5</color><colorMode>normal</colorMode><outline>0</outline></PolyStyle></Style>
<name>CAR 166</name><open>1</open>
<description>10 nm circles around CAR 166 Aerodromes. Inofficial, no claim to accuracy. Not for navigation.</description>");

            foreach (Dictionary<string, object> aerodrome in aerodromes)
            {
                LatLng center = new LatLng(double.Parse(aerodrome["Lat"].ToString()), double.Parse(aerodrome["Lng"].ToString()));
                List<LatLng> circlePoints = center.CirclePoints(18520, 40);

                kmlBuilder.AppendLine(
        $@"<Placemark id='{aerodrome["Code"]}'>
<name>{aerodrome["Name"]} ({aerodrome["Code"]})</name>
<description><![CDATA[CTAF {aerodrome["CTAF"]}<br/>{aerodrome["Category"]}<br/><a href='http://www.airservicesaustralia.com/aip/current/ersa/FAC_{aerodrome["Code"]}_{ersaDate:ddMMMyyyy}.pdf'>FAC</a>]]></description>
<styleUrl>#CAR166Disk</styleUrl>
<Polygon><outerBoundaryIs><LinearRing><coordinates>");
                foreach (LatLng circlePoint in circlePoints)
                    kmlBuilder.AppendLine(circlePoint.lng + "," + circlePoint.lat);
                kmlBuilder.AppendLine("</coordinates></LinearRing></outerBoundaryIs></Polygon></Placemark>");
            }
            kmlBuilder.AppendLine("</Folder></kml>");

            byte[] buffer = new UTF8Encoding().GetBytes(kmlBuilder.ToString());
            KmlSaveAsKmz(buffer, targetFileName);
        }

        public int UpdateCAR166DataFromFile(string openAirSourceFileName)
        {
            string source = File.ReadAllText(openAirSourceFileName);
            string target = dataDir + "\\VHFAerodromes.json";

            string pattern = @"\r\n\r\n(?:.+\r\n)+AN (?<name>.+) (?<CTAF>[0-9]*\.?[0-9]+) (?:\*TWR HRS-SEE ERSA\* )?(?<state>.+) \((?<code>.+)\) (?<category>.+)\r\n(?:.+\r\n)+V X=(?<latDeg>.+):(?<latMin>.+):(?<latSec>.+) (?<latHem>.)  (?<lngDeg>.+):(?<lngMin>.+):(?<lngSec>.+) (?<lngHem>.)\r\n";

            //var result = new List<object>();
            var json = new StringBuilder("[");

            var matches = Regex.Matches(source, pattern);
            foreach (Match match in matches)
            {
                // Workaround issue in source file:
                var category = match.Groups["category"].Value.Trim();
                switch (category)
                {
                    case "CERT":
                    case "REG":
                    case "MIL":
                    case "UNCR":
                    case "JOINT": // E.g. Townsville
                        break;
                    case "WREG":
                        category = "REG";
                        break;
                    case "WCERT":
                        category = "CERT";
                        break;
                    default:
                        throw new ApplicationException("Unrecognised category.");
                }

                if (category != "UNCR")
                {
                    switch (match.Groups["state"].Value)
                    {
                        case "NSW":
                        case "VIC":
                        case "ACT":
                        case "QLD":
                        case "SA":
                        case "NT":
                        case "WA":
                        case "TAS":
                        case "OTH": // "Other"? Chrismas Island
                            break;
                        default:
                            throw new ArgumentException("State not recognised. Value was: " + match.Groups["state"].Value);
                    }
                    var latLng = new LatLng(
                        match.Groups["latHem"].Value[0],
                        int.Parse(match.Groups["latDeg"].Value),
                        int.Parse(match.Groups["latMin"].Value),
                        int.Parse(match.Groups["latSec"].Value),
                        match.Groups["lngHem"].Value[0],
                        int.Parse(match.Groups["lngDeg"].Value),
                        int.Parse(match.Groups["lngMin"].Value),
                        int.Parse(match.Groups["lngSec"].Value)
                        );
                    //result.Add(new
                    //{
                    //    Name = match.Groups["name"].Value,
                    //    CTAF = match.Groups["CTAF"].Value,
                    //    Category = match.Groups["category"].Value,
                    //    Lat = latLng.lat,
                    //    Lng = latLng.lng
                    //});
                    if (json.Length > 1)
                        json.Append(",\r\n");
                    json.Append($"{{\"Code\":\"{match.Groups["code"].Value}\",\"Name\":\"{match.Groups["name"].Value}\",\"CTAF\":\"{match.Groups["CTAF"].Value}\",\"Category\":\"{category}\",\"Lat\":\"{Math.Round(latLng.lat, 4)}\",\"Lng\":\"{Math.Round(latLng.lng, 4)}\"}}");
                }
            }
            json.Append("]");
            File.WriteAllText(target, json.ToString());

            return matches.Count;
        }
    }

    public class ProgressEventArgs : EventArgs
    {
        public ProgressEventArgs(string text) { Text = text; }
        public string Text { get; private set; }
    }

    public delegate void ProgressEventHandler(object sender, ProgressEventArgs e);
}