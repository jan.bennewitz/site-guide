﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace VHPASiteGuide
{
    public static class DrawSettings
    {
        public static Dictionary<string, ShapeDrawSetting> ShapeColours { get; }

        static DrawSettings()
        {
            ShapeColours = new ShapeDrawSetting[] {
                new ShapeDrawSetting("Landing", 2, Color.Blue),
                new ShapeDrawSetting("No Landing", 2, Color.Red),
                new ShapeDrawSetting("No Launching", 2, Color.FromArgb(0xff, 0x7f, 0)),
                new ShapeDrawSetting("Emergency Landing", 2, Color.FromArgb(255, 127, 0)),
                new ShapeDrawSetting("No Fly Zone", 2, Color.Red),
                new ShapeDrawSetting("Hazard", 1, Color.Red),
                new ShapeDrawSetting("Hazard Area", 2, Color.Red),
                new ShapeDrawSetting("Feature", 2, Color.FromArgb(0xff, 0x7f, 0)),
                new ShapeDrawSetting("Access", 1, Color.Blue),
                new ShapeDrawSetting("Powerline", 1, Color.Red),
            }
            .ToDictionary(i => i.Class);
        }

        public struct ShapeDrawSetting
        {
            public string Class;
            public byte Dimension;
            public Color Colour;
            public ShapeDrawSetting(string @class, byte dimension, Color colour) { Class = @class; Dimension = dimension; Colour = colour; }
        }
    }
}