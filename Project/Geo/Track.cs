﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

namespace Geo
{
    public class Track
    {
        public List<LatLng> Points { get; }

        public Track()
        {
            Points = new List<LatLng>();
        }

        public void AddPoint(LatLng point)
            => Points.Add(point);

        public void AddPoint(double lat, double lng)
            => Points.Add(new LatLng(lat, lng));

        public void AddPoint(string lat, string lng, IFormatProvider culture)
            => AddPoint(Convert.ToDouble(lat, culture), Convert.ToDouble(lng, culture));

        public void AddPoint(string lat, string lng)
        {
            IFormatProvider culture = CultureInfo.InvariantCulture;
            AddPoint(Convert.ToDouble(lat, culture), Convert.ToDouble(lng, culture));
        }

        public LatLngBounds GetBounds()
        {
            LatLngBounds bounds = new LatLngBounds(Points[0]);
            foreach (LatLng point in Points)
            {
                if (!bounds.contains(point))
                    bounds.expand(point);
            }
            return bounds;
        }

        public static Track CreateFromPointsData(string pointsData, bool latFirst, bool closeTrack = false)
            => CreateFromPointsData(pointsData, CultureInfo.InvariantCulture, latFirst, closeTrack);
        /**
       * @param points
       *            set the points that should be encoded all points have to be in
       *            the following form: latitude, longitude[separator] (if latFirst) or longitude, latitude[separator] (if !latFirst)
       */
        public static Track CreateFromPointsData(string pointsData, IFormatProvider culture, bool latFirst, bool closeTrack = false)
        {
            Track trk = new Track();
            char[] pointSeparators = new char[] { ' ', '\n' };
            char[] coordSeparators = new char[] { ',' };

            string[] points = pointsData.Split(pointSeparators, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < points.Length; i++)
            {
                string[] coords = points[i].Split(coordSeparators);
                if (latFirst)
                    trk.AddPoint(coords[0], coords[1], culture);
                else
                    trk.AddPoint(coords[1], coords[0], culture);
            }
            if (closeTrack && points.Length > 0)
            {
                var firstPoint = trk.Points[0];
                var lastPoint = trk.Points[trk.Points.Count - 1];
                if (!(lastPoint.lat == firstPoint.lat && lastPoint.lng == firstPoint.lng))
                    trk.AddPoint(firstPoint.lat, firstPoint.lng);
            }

            return trk;
        }

        public static Track CreateFromKmlLineString(string pointsData)
            => CreateFromKmlLineString(pointsData, CultureInfo.InvariantCulture);

        /// <param name="pointsData">All points have to be in the following form: Longitude,Latitude[,Altitude(ignored)]</param>
        public static Track CreateFromKmlLineString(string pointsData, IFormatProvider culture)
        {
            Track trk = new Track();
            char[] pointSeparators = new char[] { ' ', '\n' };
            char[] coordSeparators = new char[] { ',' };

            string[] points = pointsData.Split(pointSeparators);

            for (int i = 0; i < points.Length; i++)
            {
                string[] coords = points[i].Split(coordSeparators);
                trk.AddPoint(coords[0], coords[1], culture);
            }
            return trk;
        }

        public LatLng GetCentroid()
        {
            // If it's only two points, the below code fails, simply return the middle point.
            if (Points.Count == 2)
                return Points[0].GoTo(Points[0].BearingToR(Points[1]), Points[0].DistanceTo(Points[1]) / 2);

            //http://stackoverflow.com/questions/5271583/center-of-gravity-of-a-polygon
            // Not the most legible code...
            LatLng off = Points[0];
            double twicearea = 0;
            double x = 0;
            double y = 0;
            LatLng p1, p2;
            double f;
            for (int i = 0, j = Points.Count - 1; i < Points.Count; j = i++)
            {
                p1 = Points[i];
                p2 = Points[j];
                f = (p1.lat - off.lat) * (p2.lng - off.lng) - (p2.lat - off.lat) * (p1.lng - off.lng);
                twicearea += f;
                x += (p1.lat + p2.lat - 2 * off.lat) * f;
                y += (p1.lng + p2.lng - 2 * off.lng) * f;
            }
            f = twicearea * 3;

            return new LatLng(x / f + off.lat, y / f + off.lng);
        }

        public double GetArea()
        {
            if (Points.Count < 3)
                return 0;

            double determinant(double x1, double y1, double x2, double y2) => x1 * y2 - x2 * y1;
            double area = determinant(Points[Points.Count - 1].lng, Points[Points.Count - 1].lat, Points[0].lng, Points[0].lat);
            for (int i = 1; i < Points.Count; i++)
                area += determinant(Points[i - 1].lng, Points[i - 1].lat, Points[i].lng, Points[i].lat);
            return area / 2;
        }
    }
}