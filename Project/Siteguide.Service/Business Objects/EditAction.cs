﻿using Humanizer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service {
    /// <summary>
    /// RequiresEditRole: Requires at least one of the specified roles.
    /// </summary>
    public record EditAction(string Name, IEnumerable<string> ValidForStatus, IEnumerable<EditActionRole> RequiresEditRole) {
        public override string ToString() => $"{Name} ({string.Join(",", ValidForStatus)}) [{(RequiresEditRole.Any() ? RequiresEditRole.Humanize("or") : "No role requirement")}]";
    }

    public static class EditActions {
        public static List<EditAction> All => new()
        {
            SubmitForReview,
            Abandon,
            ClaimForReview,
            Accept,
            Reject,
            ForwardToSubmitter,
        };
        public static EditAction SubmitForReview => new(nameof(SubmitForReview), new[] { "D" }, Array.Empty<EditActionRole>() /*Anyone can do this.*/);
        public static EditAction Abandon => new(nameof(Abandon), new[] { "D" }, Array.Empty<EditActionRole>() /*Anyone can do this.*/);
        //ClaimForReviewAsDirectAdmin: Only submitted edits can be claimed for review.
        public static EditAction ClaimForReview => new(nameof(ClaimForReview), new[] { "S" }, new[] { EditActionRole.DirectAdmin, EditActionRole.IndirectAdmin, EditActionRole.SystemAdmin });
        //Accept/RejectAsDirect/Indirect/SystemAdmin: Only drafts and submitted edits can be accepted/rejected.
        public static EditAction Accept => new(nameof(Accept), new[] { "D", "S" }, new[] { EditActionRole.DirectAdmin, EditActionRole.IndirectAdmin, EditActionRole.SystemAdmin });
        public static EditAction Reject => Accept with { Name = nameof(Reject) };
        public static EditAction ForwardToSubmitter => Accept with { Name = nameof(ForwardToSubmitter) };
    }

    public enum EditActionRole {
        DirectAdmin,
        IndirectAdmin,
        SystemAdmin,
    }
}
