﻿using SiteGuide.Service.Views.Kml.Shared;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Version = SiteGuide.DAL.Models.Version;

namespace SiteGuide.Service.Views.Kml.SitesKml
{
    public class SitesKmlViewModel
    {
        public Version Version { get; set; }
        public EditVersion EditVersion { get; set; }
        public bool IsClosedSites { get; set; }
        public IEnumerable<KmlStyle> Styles { get; set; }
        public IEnumerable<Site> Sites { get; set; }
        public ILookup<int, Launch> Launches { get; set; }
        public ILookup<int, SeeAlso> SeeAlsos { get; set; }
        public ILookup<int, MapShape> SiteMapshapes { get; set; }
        public ILookup<int, ContactInfo> Contacts { get; set; }
        public IEnumerable<MapShape> SharedMapshapes { get; set; }
        public string SiteUrl { get; set; }
        public string ImagesFolderPath { get; set; }
    }

    public class KmlStyle
    {
        public string Id { get; set; }
        public string LineColor { get; set; }
        public int LineWidth { get; set; }
        public string PolygonColor { get; set; }
    }

}
