﻿using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service.Views.Kml.Shared
{
    public class ContactListViewModel
    {
        public string Title { get; set; }
        public IEnumerable<Contact> Contacts { get; set; }
    }
}
