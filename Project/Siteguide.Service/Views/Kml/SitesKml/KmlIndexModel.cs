﻿namespace SiteGuide.Service.Views.Kml.Shared
{
    public class KmlIndexModel
    {
        public string ControllerPath { get; set; }
        public string Host { get; set; }
    }
}
