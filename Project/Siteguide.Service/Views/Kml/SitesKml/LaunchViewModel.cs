﻿using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service.Views.Kml.Shared
{
    public class LaunchViewModel
    {
        public Site Site { get; set; }
        public Launch Launch { get; set; }
        public IEnumerable<SeeAlso> SeeAlsos { get; set; }
        public IEnumerable<ContactInfo> Contacts { get; set; }
        public string SiteUrl { get; set; }
    }

    public class ContactInfo
    {
        public bool IsContact;
        public bool IsResponsible;
        public Contact Contact;
    }
}
