﻿using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service.Views.Kml.Car166Kml
{
    public class Car166KmlViewModel
    {
        public DateOnly ErsaDate { get; set; }
        public IEnumerable<ExtCacheCtaf> Ctafs { get; set; }
    }
}
