﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiteGuide.Service.Views.Emails;

namespace SiteGuide.Service.Views.Emails
{
    public class CallbackEmailViewModel : EmailViewModel
    {
        public CallbackEmailViewModel(string siteUrl, string callbackUrl, string salutation) : base(siteUrl, null)
        {
            CallbackUrl = callbackUrl;
            Salutation = salutation;
        }

        public string CallbackUrl { get; set; }
        public string Salutation { get; set; }
    }
}
