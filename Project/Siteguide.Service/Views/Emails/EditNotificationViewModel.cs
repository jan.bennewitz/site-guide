﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service.Views.Emails
{
	public class EditNotificationViewModel : EmailViewModel
	{
		public EditNotificationViewModel(string siteUrl, EditAction action, int editId, string salutation) : base(siteUrl, null)
		{
            Action = action;
			EditId = editId;
            Salutation = salutation;
        }

        public EditAction Action { get; set; }
		public int EditId { get; set; }
        public string Salutation { get; set; }

        public string ActionDescriptionPastParticiple => Action.Name switch
		{
			nameof(EditActions.SubmitForReview) => "submitted",
			nameof(EditActions.Accept) => "accepted",
			nameof(EditActions.Reject) => "rejected",
			nameof(EditActions.Abandon) => "abandoned",

			_ => throw new NotImplementedException()
		};
	}
}
