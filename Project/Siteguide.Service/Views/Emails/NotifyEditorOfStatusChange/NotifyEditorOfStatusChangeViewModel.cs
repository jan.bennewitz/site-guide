﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service.Views.Emails.NotifyEditorOfStatusChange
{
    public class NotifyEditorOfStatusChangeViewModel : EditNotificationViewModel
    {
        public NotifyEditorOfStatusChangeViewModel(string siteUrl, EditAction action, int editId, string salutation, string editorReason, string actionByUserEmail, string additionalInformation = null) : base(siteUrl, action, editId, salutation)
        {
            EditorReason = editorReason;
            ActionByUserEmail = actionByUserEmail;
            AdditionalInformation = additionalInformation;
        }

        public string EditorReason { get; set; }
        public string ActionByUserEmail { get; set; }
        public string AdditionalInformation { get; set; }
    }
}
