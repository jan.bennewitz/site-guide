﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service.Views.Emails.PasswordReset
{
    public class PasswordResetViewModel : EmailViewModel
    {
        public PasswordResetViewModel(string siteUrl, string callbackUrl, string salutation) : base(siteUrl, null)
        {
            CallbackUrl = callbackUrl;
            Salutation = salutation;
        }

        public string CallbackUrl { get; set; }
        public string Salutation { get; set; }
    }
}
