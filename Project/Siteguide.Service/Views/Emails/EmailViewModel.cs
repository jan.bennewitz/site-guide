﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service.Views.Emails
{
    public class EmailViewModel
    {
        public EmailViewModel(string siteUrl, string hiddenPreheaderText)
        {
            SiteUrl = siteUrl;
            HiddenPreheaderText = hiddenPreheaderText;
        }

        public string SiteUrl { get; set; }
        public string HiddenPreheaderText { get; set; }
    }
}
