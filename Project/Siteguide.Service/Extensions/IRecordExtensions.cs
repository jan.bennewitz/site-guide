﻿using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace SiteGuide.Service.Extensions
{
    public static class IRecordExtensions
    {
        public static bool IsRecordContentIdentical<T>(this T rec1, T rec2) where T : class, IRecord
        {
            if (rec1 == null || rec2 == null)
                return rec1 == rec2;

            return !GetRecordContentComparisons(rec1, rec2)
                .Any(p => p.IsDifferent);
        }

        /// <summary>
        /// Compare only those properties of the specified IRecord type which are 'content' as opposed to shared IRecord 'header'.
        /// </summary>
        public static IEnumerable<RecordPropertyComparison> GetRecordContentComparisons<T>(T rec1, T rec2) where T : class, IRecord
        {

            return GetRecordPayloadProps<T>()
                .Select(propInfo =>
                {
                    object val1 = propInfo.GetValue(rec1, null);
                    object val2 = propInfo.GetValue(rec2, null);

                    return new RecordPropertyComparison
                    {
                        IsDifferent = val1 != val2 && (val1 == null || !val1.Equals(val2)),
                        Name = propInfo.Name,
                        Type = propInfo.PropertyType,
                        Value1 = val1,
                        Value2 = val2,
                    };
                });
        }

        public static IEnumerable<PropertyInfo> GetRecordPayloadProps<T>() where T : class, IRecord
        {
            var recordHeaderProps = typeof(IRecord).GetProperties() // exclude these.
                .Select(p => p.Name)
                .ToList();

            return typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(propInfo =>
                    !recordHeaderProps.Contains(propInfo.Name)
                    && (propInfo.PropertyType.IsValueType || propInfo.PropertyType == "".GetType())
                    && propInfo.CanWrite /* Filter out calculated properties such as Site.IsClosed */);
        }

        public static T CopyRecordContentFrom<T>(this T target, T source) where T : class, IRecord
        {
            if (target == null) throw new ArgumentNullException(nameof(target));
            if (source == null) throw new ArgumentNullException(nameof(source));

            Type type = typeof(T);
            var ignore = typeof(IRecord).GetProperties()
                    .Select(p => p.Name)
                    .ToList();

            foreach (var propInfo in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                if (!ignore.Contains(propInfo.Name) && (propInfo.PropertyType.IsValueType || propInfo.PropertyType == "".GetType()))
                {
                    object newValue = propInfo.GetValue(source, null);
                    if (propInfo.CanWrite)
                        propInfo.SetValue(target, newValue);
                }

            return target;
        }

        //public static bool IsManyToMany<T>() where T : class, IRecord => typeof(T) == typeof(SiteMapshape) || typeof(T) == typeof(SiteContact);

    }

    public class RecordPropertyComparison
    {
        public string Name { get; set; }
        public Type Type { get; set; }
        public bool IsDifferent { get; set; }
        public object Value1 { get; set; }
        public object Value2 { get; set; }

        public override string ToString() => $"{Name} [{Type.Name}]: {Value1} {(IsDifferent ? $"-> {Value2}" : "[Unchanged]")}";
    }

}
