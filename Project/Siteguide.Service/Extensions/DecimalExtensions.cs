﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SiteGuide.Service.Extensions
{
    public static class DecimalExtensions
    {
        /// <summary>
        /// The exponent part of the decimal is reduced to just what is needed. Calling ToString() on the output decimal will write the number without any trailing 0.
        /// https://stackoverflow.com/questions/4525854/remove-trailing-zeros/7983330#7983330
        /// </summary>
        public static decimal Normalize(this decimal value) => value / 1.000000000000000000000000000000000m;

        //private static decimal Round(double d, int significantDigits) // Round to significant digits https://stackoverflow.com/a/374470/3479465
        //{
        //    if (d == 0) return 0;
        //    var scale = Math.Pow(10, (int)(Math.Floor(Math.Log10(Math.Abs(d))) + 1));
        //    return ((decimal)scale * (decimal)Math.Round(d / scale, significantDigits)).Normalize();
        //}
    }
}
