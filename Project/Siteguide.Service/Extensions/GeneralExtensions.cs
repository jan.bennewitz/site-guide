﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SiteGuide.Service.Extensions
{
    public static class GeneralExtensions
    {
        public static bool IsOneOf<T>(this T item, params T[] array)
            => Array.Exists(array, i => EqualityComparer<T>.Default.Equals(i, item));
    }
}
