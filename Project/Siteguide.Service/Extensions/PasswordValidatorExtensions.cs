﻿using Humanizer;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SiteGuide.Service.Extensions
{
    public static class PasswordValidatorExtensions
    {
        public static string ToDescription(this PasswordOptions passwordOptions)
        {
            var letterClauses = new List<string>();
            if (passwordOptions.RequireUppercase) 
                letterClauses.Add("upper-case letters");
            if (passwordOptions.RequireLowercase) 
                letterClauses.Add("lower-case letters");
            if (passwordOptions.RequireDigit) 
                letterClauses.Add("numbers");
            if (passwordOptions.RequireNonAlphanumeric) 
                letterClauses.Add("special characters");

            var clauses = new List<string>();

            clauses.Add($"be at least {passwordOptions.RequiredLength} characters long");

            if (letterClauses.Any())
                clauses.Add($"include {letterClauses.Humanize()}");

            if (passwordOptions.RequiredUniqueChars > 1)
                clauses.Add($"contain at least {passwordOptions.RequiredUniqueChars} unique characters");

            var result = $"Passwords must {clauses.Humanize()}.";
            return result;
        }
    }
}
