﻿using Microsoft.AspNetCore.Identity;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace SiteGuide.Service.Extensions
{
    public static class EditVersionExtensions
    {
        public static IQueryable<EditVersion> WhereIsDraftBy(this IQueryable<EditVersion> editVersion, IdentityUser user)
            => editVersion.Where(e =>
                e.StatusId == "D"
                && e.AuthorId == user.Id);
    }
}
