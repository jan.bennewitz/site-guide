﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SiteGuide.Service.Extensions
{
    public static class TextExtensions
    {
        public static string SplitCamelCase(this string str) // Thanks to https://stackoverflow.com/a/5796793
        {
            return Regex.Replace(
                Regex.Replace(
                    str,
                    @"(\P{Ll})(\P{Ll}\p{Ll})",
                    "$1 $2"
                ),
                @"(\p{Ll})(\P{Ll})",
                "$1 $2"
            );
        }
    }
}
