﻿using SiteGuide.DAL.Models;
using SiteGuide.Service.Identifiers;
using System;
using System.Collections.Generic;
using System.Linq;
using Version = SiteGuide.DAL.Models.Version;

namespace SiteGuide.Service.Extensions
{
    public static class LinqExtensions
    {
        /// <summary>
        /// Filter for current version of the record.
        /// </summary>
        public static IQueryable<T> ValidAtCurrentVersion<T>(this IQueryable<T> queryable) where T : IRecord
            => queryable.Where(s =>
                    s.ValidFromVersion != null
                    && s.ValidToVersion == null
                );

        /// <summary>
        /// Filter for specified version of the record or current if no version specified.
        /// </summary>
        /// <param name="version">
        /// Version id or null for current.
        /// </param>
        public static IQueryable<T> ValidAtVersion<T>(this IQueryable<T> queryable, int? version = null) where T : IRecord
        {
            return (version == null)
                ? // current/latest version
                queryable.ValidAtCurrentVersion()
                : // specified version
                queryable.Where(s =>
                    s.ValidFromVersion <= version
                    && (s.ValidToVersion == null || s.ValidToVersion >= version)
                );
        }
        public static IQueryable<T> ValidAtUnacceptedEditVersion<T>(this IQueryable<T> queryable, EditVersionIdentifier editVersionIdentifier) where T : IRecord
        {
            // Apply edit version changes to reference version

            IQueryable<T> referenceVersionRecords;
            if (editVersionIdentifier.ReferenceVersion == null) // The initial version's accepted edit version has no reference version.
                referenceVersionRecords = queryable.Where(i => false);
            else
                referenceVersionRecords = queryable.ValidAtVersion(editVersionIdentifier.ReferenceVersion);

            var editRecords = queryable.WhereEditVersion(editVersionIdentifier.Id);

            // TODO: Using a join might be overkill - just use editversion where available, version otherwise, and handle deletes?
            return referenceVersionRecords
                .FullOuterJoin(
                    editRecords,
                    b => b.Key,
                    e => e.Key,
                    (b, e, id) => new { Key = id, Base = b, Edit = e }
                )
                .Where(i => i.Edit == null || !(bool)i.Edit.IsDelete)
                .Select(i =>
                    i.Edit != null ? i.Edit // Addition or Edit
                    : i.Base // Unchanged
                    ).AsQueryable();
        }

        public static IQueryable<T> ValidAtVersionTreeNode<T>(this IQueryable<T> queryable, VersionTreeNodeIdentifier versionTreeNodeId) where T : IRecord
            => versionTreeNodeId switch
            {
                null => queryable.ValidAtCurrentVersion(),
                VersionIdentifier versionIdentifier => queryable.ValidAtVersion(versionIdentifier.Id),
                EditVersionIdentifier editVersionIdentifier => queryable.ValidAtUnacceptedEditVersion(editVersionIdentifier),
                _ => throw new NotImplementedException(),
            };
        //public static IQueryable<T> ValidAtVersionOrUnacceptedEditVersion<T>(this IQueryable<T> queryable, VersionTreeNodeIdentifier versionTreeNode) where T : IRecord
        //    => versionTreeNode switch
        //    {
        //        VersionIdentifier versionIdentifier => ValidAtVersion(queryable, versionIdentifier.Id),
        //        EditVersionIdentifier editVersionIdentifier => ValidAtUnacceptedEditVersion(queryable, editVersionIdentifier.EditId, editVersionIdentifier.EditVersionNumber),
        //    };


        /// <summary>
        /// Filter for specified EditVersionId
        /// </summary>
        public static IQueryable<T> WhereEditVersion<T>(this IQueryable<T> records, int editVersionId) where T : IRecord
            => records.Where(c => c.EditVersionId == editVersionId);

        /// <summary>
        /// Return only trunk records, that is, records that have been accepted as an official version at some point.
        /// </summary>
        public static IEnumerable<T> ValidAtAnyVersion<T>(this IEnumerable<T> dbSet) where T : IRecord
            => dbSet.Where(r => r.ValidFromVersion != null);

        /// <summary>
        /// Return those records whose validity overlaps validity of the specified record
        /// </summary>
        public static IEnumerable<T1> ValidityOverlaps<T1, T2>(this IEnumerable<T1> dbSet, T2 record) where T1 : IRecord where T2 : IRecord
            => dbSet.Where(i =>
                (i.ValidFromVersion <= record.ValidToVersion || record.ValidToVersion == null)
                && (record.ValidFromVersion <= i.ValidToVersion || i.ValidToVersion == null));

        /// <summary>
        /// Get data with specified edits applied
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="baseVersion">The version number to apply the edit to - null for current.</param>
        public static IEnumerable<T> ApplyEditVersion<T>(this IQueryable<T> records, int? baseVersion = null, int? editVersionId = null) where T : IRecord
        {
            var versionData = records.ValidAtVersion(baseVersion);

            if (editVersionId == null)
                return versionData;
            else
                return versionData
                    .FullOuterJoin(records.WhereEditVersion((int)editVersionId), h => h.Key, c => c.Key, (h, c, id) => (c == null) ? h : c)
                    .Where(r => r.IsDelete != true); // Remove deleted records
        }

        public static Version Current(this IQueryable<Version> version)
            => version.Single(v => v.Id == version.Max(l => l.Id));

        public static Version Get(this IQueryable<Version> version, int? versionId = null)
            => versionId == null
                ? version.Current()
                : version.SingleOrDefault(v => v.Id == versionId); // null if requested version does not exist

        /// <summary>
        /// Return versions that modified the specified records.
        /// </summary>
        public static IEnumerable<int> GetModifyingVersions<T>(this IEnumerable<T> dbSet) where T : IRecord
            => dbSet
                    .Where(r => r.ValidFromVersion != null)
                    .Select(r => (int)r.ValidFromVersion)
                .Union(dbSet
                    .Where(r => r.ValidToVersion != null)
                    .Select(r => (int)r.ValidToVersion + 1))
                .Distinct();

        #region Outer Join, thanks to https://stackoverflow.com/a/13503860
        public static IEnumerable<TResult> FullOuterGroupJoin<TA, TB, TKey, TResult>(
        this IEnumerable<TA> a,
        IEnumerable<TB> b,
        Func<TA, TKey> selectKeyA,
        Func<TB, TKey> selectKeyB,
        Func<IEnumerable<TA>, IEnumerable<TB>, TKey, TResult> projection,
        IEqualityComparer<TKey> cmp = null)
        {
            cmp = cmp ?? EqualityComparer<TKey>.Default;
            var alookup = a.ToLookup(selectKeyA, cmp);
            var blookup = b.ToLookup(selectKeyB, cmp);

            var keys = new HashSet<TKey>(alookup.Select(p => p.Key), cmp);
            keys.UnionWith(blookup.Select(p => p.Key));

            var join = from key in keys
                       let xa = alookup[key]
                       let xb = blookup[key]
                       select projection(xa, xb, key);

            return join;
        }

        // https://stackoverflow.com/a/39020068/3479465
        public static IEnumerable<TResult> LeftOuterJoin<TLeft, TRight, TKey, TResult>(this IEnumerable<TLeft> left, IEnumerable<TRight> right, Func<TLeft, TKey> leftKey, Func<TRight, TKey> rightKey,
        Func<TLeft, TRight, TResult> result)
        {
            return left.GroupJoin(right, leftKey, rightKey, (l, r) => new { l, r })
                 .SelectMany(
                     o => o.r.DefaultIfEmpty(),
                     (l, r) => new { lft = l.l, rght = r })
                 .Select(o => result.Invoke(o.lft, o.rght));
        }

        public static IEnumerable<TResult> FullOuterJoin<TA, TB, TKey, TResult>(
            this IEnumerable<TA> a,
            IEnumerable<TB> b,
            Func<TA, TKey> selectKeyA,
            Func<TB, TKey> selectKeyB,
            Func<TA, TB, TKey, TResult> projection,
            TA defaultA = default(TA),
            TB defaultB = default(TB),
            IEqualityComparer<TKey> cmp = null)
        {
            cmp = cmp ?? EqualityComparer<TKey>.Default;
            var alookup = a.ToLookup(selectKeyA, cmp);
            var blookup = b.ToLookup(selectKeyB, cmp);

            var keys = new HashSet<TKey>(alookup.Select(p => p.Key), cmp);
            keys.UnionWith(blookup.Select(p => p.Key));

            var join = from key in keys
                       from xa in alookup[key].DefaultIfEmpty(defaultA)
                       from xb in blookup[key].DefaultIfEmpty(defaultB)
                       select projection(xa, xb, key);

            return join;
        }
        #endregion
    }
}
