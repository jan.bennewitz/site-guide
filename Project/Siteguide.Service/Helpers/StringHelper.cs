﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SiteGuide.Service.Helpers
{
    public static class StringHelper
    {
        public static string JoinNotEmpty(string separator, params string[] parts)
            => string.Join(separator, parts.Where(i => !string.IsNullOrWhiteSpace(i)));
    }
}
