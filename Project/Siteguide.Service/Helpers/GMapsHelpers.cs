﻿using Geo;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Html;
using SiteGuide.Service.Extensions;

namespace SiteGuide.Service.Helpers
{
    public static class GMapsHelper
    {
        private const string _iconsFolder = "/css/images/";
        private const string _iconClosed = "mm_20_gray.png";
        private const string _iconOpen = "mm_20_green.png";

        public static string GetMainStaticMapUrl(SiteInfo model, int sizeX = 270, int sizeY = 200)
        {
            GetLaunchesSpanAndCenterPoint(model.Launches, out decimal spanLat, out decimal spanLon, out decimal centerLat, out decimal centerLon, 5, .1m);

            return $@"https://maps.google.com/maps/api/staticmap?key={model.GMapsApiKey}&visible={centerLat - spanLat / 2},{centerLon - spanLon / 2}|{centerLat + spanLat / 2},{centerLon + spanLon / 2}&size={sizeX}x{sizeY}&maptype=terrain{GetEncodedMarkers(model.Launches, model.Site.IsClosed, model.SiteRootUrl)}";
        }
        public static string GetInsetStaticMapUrl(SiteInfo model, int sizeX = 80, int sizeY = 80, int zoom = 5)
        {
            GetLaunchesSpanAndCenterPoint(model.Launches, out decimal spanLat, out decimal spanLon, out decimal centerLat, out decimal centerLon, 5, .2m);
            return $@"https://maps.google.com/maps/api/staticmap?key={model.GMapsApiKey}&zoom={zoom}&size={sizeX}x{sizeY}&markers=icon:{model.SiteRootUrl + _iconsFolder}{(model.Site.IsClosed ? _iconClosed : _iconOpen)}|{centerLat},{centerLon}";
        }
        public static string GetCoordinatesStaticMapUrl(string latLng, bool isRemoved, string apiKey, int sizeX = 240, int sizeY = 240, int zoom = 16)
        {
            return $@"https://maps.google.com/maps/api/staticmap?key={apiKey}&zoom={zoom}&size={sizeX}x{sizeY}&maptype=hybrid&markers=size:tiny|color:{(isRemoved ? "red" : "green")}|{latLng}";
        }
        public static HtmlString GetSiteMapHref(SiteInfo model)
        {
            string result = $"/Map?type=open{(!string.IsNullOrWhiteSpace(model.Site.Closed) ? ",closed" : "")}&amp;";
            if (model.Launches.Count() == 1)
            {
                result += $"pin={model.Site.Name}";
                string launchName = model.Launches.Single().Name;
                if (!string.IsNullOrWhiteSpace(launchName))
                    result += " - " + launchName;
            }
            else
            {
                GetLaunchesSpanAndCenterPoint(model.Launches, out decimal spanLat, out decimal spanLon, out decimal centerLat, out decimal centerLon, 5, .2m);
                result += $"spn={spanLat},{spanLon}&amp;ll={centerLat},{centerLon}";
            }
            return new HtmlString(result);
        }

        public static HtmlString GetStaticMapUrl(IEnumerable<Launch> launches, bool isSiteClosed, IEnumerable<MapShape> mapShapes, string gMapsApiKey, string siteRootUrl, int size = 445)
        {
            var result = new StringBuilder(GetEncodedMarkers(launches, isSiteClosed, siteRootUrl));
            //TODO: Add any other launches in view, e.g. Ben Nevis to Sugarloaf?

            return GetStaticMapUrl(mapShapes, gMapsApiKey, size, result);
        }

        public static HtmlString GetStaticMapUrl(string[] removed, string[] unchanged, string[] added, int size, ShapeDrawSetting drawSetting, string apiKey)
        {
            var visibleArea = new LatLngBounds();
            foreach (var poly in removed)
                visibleArea.expand(Track.CreateFromPointsData(poly, false).GetBounds());
            foreach (var poly in unchanged)
                visibleArea.expand(Track.CreateFromPointsData(poly, false).GetBounds());
            foreach (var poly in added)
                visibleArea.expand(Track.CreateFromPointsData(poly, false).GetBounds());

            int zoom = visibleArea.GetGMapZoom(size, size);

            var polyEncoder = new PolylineEncoder(18, 2, 8e-6 * Math.Pow(2, 19 - zoom), true);
            var result = new StringBuilder();
            drawSetting.LineThickness = 1;
            AddEncodedShape(polyEncoder, result, unchanged, drawSetting);
            var defaultFillApha = drawSetting.FillAlpha;
            drawSetting.LineAlpha = 0xff;
            drawSetting.LineColour = Color.Red;
            drawSetting.FillAlpha = 0;
            AddEncodedShape(polyEncoder, result, removed, drawSetting);
            drawSetting.LineColour = Color.Lime;
            drawSetting.FillAlpha = defaultFillApha;
            AddEncodedShape(polyEncoder, result, added, drawSetting);

            return new HtmlString($@"https://maps.google.com/maps/api/staticmap?key={apiKey}&size={size}x{size}&maptype=hybrid" + result.ToString());
        }

        public static HtmlString GetStaticMapUrl(MapShape mapshape, string apiKey, int size = 445, LatLngBounds? overrideVisibleArea = null)
            => GetStaticMapUrl(new MapShape[] { mapshape }, apiKey, size, overrideVisibleArea: overrideVisibleArea);

        private static HtmlString GetStaticMapUrl(IEnumerable<MapShape> mapshapes, string apiKey, int size, StringBuilder result = null, LatLngBounds? overrideVisibleArea = null)
        {
            result ??= new StringBuilder();

            LatLngBounds visibleArea = overrideVisibleArea ?? GetBounds(mapshapes);

            int zoom = visibleArea.GetGMapZoom(445, 445);

            if (overrideVisibleArea != null)
                result.Append($"&center={((LatLngBounds)overrideVisibleArea).Center.lat},{((LatLngBounds)overrideVisibleArea)!.Center.lng}&zoom={zoom - 2}");

            var polyEncoderOverviewMap = new PolylineEncoder(18, 2, 8e-6 * Math.Pow(2, 19 - zoom), true);
            foreach (var shape in mapshapes)
                AddEncodedShape(polyEncoderOverviewMap, result, shape.CoordinatesList, new ShapeDrawSetting(shape.Category));

            return new HtmlString($@"https://maps.google.com/maps/api/staticmap?key={apiKey}&size={size}x{size}&maptype=hybrid" + result.ToString());
        }

        private static string GetEncodedMarkers(IEnumerable<Launch> launches, bool isSiteClosed, string siteRoot)
        {
            return EncodeMarkers(launches.Where(l => !(l.IsClosed || isSiteClosed)), siteRoot + _iconsFolder + _iconOpen)
                + EncodeMarkers(launches.Where(l => l.IsClosed || isSiteClosed), siteRoot + _iconsFolder + _iconClosed);
        }

        public static LatLngBounds GetBounds(MapShape mapShape)
            => GetBounds(new MapShape[] { mapShape });

        public static LatLngBounds GetBounds(IEnumerable<MapShape> mapShapes)
        {
            var bounds = new LatLngBounds() { };

            foreach (var mapShape in mapShapes)
                foreach (var coord in mapShape.CoordinatesList)
                    if (!string.IsNullOrWhiteSpace(coord))
                    {
                        Track track = Track.CreateFromPointsData(coord.Trim(), false);
                        bounds.expand(track.GetBounds());
                    }

            return bounds;
        }

        ////Return the list of bounds which would be drawn too small on the specified map
        //private static List<LatLngBounds> GetUnrepresented(List<LatLngBounds> shapesBounds, LatLngBounds mapBounds, int pixelWidth, int pixelHeight)
        //{
        //    //detail maps
        //    const int MinimumShapeSizePixels = 20;
        //    var detailShapeBounds = new List<LatLngBounds>();
        //    foreach (var shapeBounds in shapesBounds)
        //    {
        //        if (shapeBounds.GMapSizeInPixel(mapBounds.GetGMapZoom(pixelWidth, pixelHeight)) < MinimumShapeSizePixels)
        //            detailShapeBounds.Add(shapeBounds);
        //    }
        //    return detailShapeBounds;
        //}

        private static void AddEncodedShape(PolylineEncoder polyEncoder, StringBuilder result, string[] coordinatesList, ShapeDrawSetting drawSetting)
        {
            foreach (string coords in coordinatesList)
            {
                Track track = Track.CreateFromPointsData(coords.Trim(), false, drawSetting.Dimension == 2);
                EncodeTrack(polyEncoder, result, drawSetting, track);
            }
        }

        private static string GetRGBString(Color colour) => (colour.R * 0x10000 + colour.G * 0x100 + colour.B).ToString("X6").ToLowerInvariant();

        private static void EncodeTrack(PolylineEncoder polyEncoder, StringBuilder result, ShapeDrawSetting drawSetting, Track track)
        {
            var encodedTrack = polyEncoder.DPEncode(track);

            bool isArea = drawSetting.Dimension == 2 && encodedTrack["encodedLevels"].Length > 2;

            result.Append($"&path=weight:{drawSetting.LineThickness}|");
            if (isArea) // Don't bother filling the area in if it's been reduced to a line
                result.Append($"fillcolor:0x{GetRGBString(drawSetting.FillColour)}{drawSetting.FillAlpha:X2}|");
            result.Append($"color:0x{GetRGBString(drawSetting.LineColour)}{drawSetting.LineAlpha:X2}|enc:{Uri.EscapeDataString(encodedTrack["encodedPoints"])}");
        }

        private static string EncodeMarkers(IEnumerable<Launch> launches, string icon)
        {
            if (!launches.Any())
                return "";

            var result = new StringBuilder($"&markers=icon:{icon}");
            foreach (var launch in launches)
                result.Append($"|{launch.Lat.Normalize()},{launch.Lon.Normalize()}");

            return result.ToString();
        }

        private static void GetLaunchesSpanAndCenterPoint(IEnumerable<Launch> launches, out decimal spanLat, out decimal spanLon, out decimal centerLat, out decimal centerLon, int roundToDecimalPlaces = 5, decimal minSpan = .1m)
        {
            decimal minLat = 0;
            decimal maxLat = 0;
            decimal minLon = 0;
            decimal maxLon = 0;

            if (launches.Any())
            {
                minLat = launches.Min(l => l.Lat);
                maxLat = launches.Max(l => l.Lat);
                minLon = launches.Min(l => l.Lon);
                maxLon = launches.Max(l => l.Lon);
            }

            spanLat = Math.Max(decimal.Round(maxLat - minLat, roundToDecimalPlaces), minSpan); // This will not deal with spans that straddle the 180 meridian
            spanLon = Math.Max(decimal.Round(maxLon - minLon, roundToDecimalPlaces), minSpan);

            centerLat = decimal.Round((maxLat + minLat) / 2, roundToDecimalPlaces);
            centerLon = decimal.Round((maxLon + minLon) / 2, roundToDecimalPlaces);
        }
    }
}
