﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siteguide.Service.Helpers;

/// <summary>
/// If this object exists for longer than the set limit it will log an error with detailed timing information.
/// Usage:
///
///public Asset AddAsset(int customerId, Asset model)
///{
///    using var perf = new PerformanceLogger(nameof(AddAsset), $"CustomerId={customerId}, model.AssetId={model.AssetId}", 3, logger);
///    using (var db = Db.Get())
///    {
///        perf.Log("Asset.Add");
///        db.Assets.Add(model);
///        perf.Log(nameof(RegisterCustomerAssetDescriptionUsage));
///        RegisterCustomerAssetDescriptionUsage(db, customerId, model.Description);
///
///        perf.Log("SaveChanges");
///        db.SaveChanges();
///        perf.Log(nameof(db.UpdateCache_SingleAsset));
///        db.UpdateCache_SingleAsset(model.Id);
///    }
///
///    return model;
///}
/// </summary>
public class PerformanceLogger : IDisposable
{
    private readonly StringBuilder _log;
    private readonly double _limitSeconds;
    private readonly Stopwatch _stopwatch;
    private bool disposedValue;
    private readonly ILogger _logger;
    private readonly string _name;
    private readonly string _parameterInfo;

    public PerformanceLogger(string name, string parameterInfo, double limitSeconds, ILogger logger)
    {
        _logger = logger;
        _name = name;
        _parameterInfo = parameterInfo;
        _log = new StringBuilder();
        _limitSeconds = limitSeconds;

        _stopwatch = new Stopwatch();
        _stopwatch.Start();

        Log($"Begin {name}");
    }

    public void Log(string text) => _log.AppendLine($"{_stopwatch.Elapsed.TotalSeconds:N3}: {text}");

    public void Log(string text, IEnumerable<string> messages) => Log($"{text}\r\n{string.Join("\r\n", messages.Select(m => $">\t{m}"))}");

    public object GetLog() => _log.ToString();

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                _stopwatch.Stop();
                Log("Finished");
                var elapsedSeconds = (double)_stopwatch.ElapsedMilliseconds / 1000;

                if (elapsedSeconds > _limitSeconds)
                    _logger.LogWarning($"Performance Failure. {_name} took {elapsedSeconds}s which exceeds the threshold of {_limitSeconds}s. {_parameterInfo}{Environment.NewLine}Log: {Environment.NewLine}{_log}");
                else
                    _logger.LogTrace($"Performance Success. {_name} took {elapsedSeconds}s which is within the threshold of {_limitSeconds}s. {_parameterInfo}{Environment.NewLine}Log: {Environment.NewLine}{_log}");
            }

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
