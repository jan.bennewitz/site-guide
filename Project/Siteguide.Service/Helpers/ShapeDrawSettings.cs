﻿using AngleSharp.Common;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace SiteGuide.Service.Helpers
{
    public struct ShapeDrawSetting
    {
        public string ClassName { get; set; }
        public byte Dimension { get; set; }
        public byte FillAlpha { get; set; }
        public Color FillColour { get; set; }
        public byte LineAlpha { get; set; }
        public Color LineColour { get; set; }
        public int LineThickness { get; set; }

        public ShapeDrawSetting(string @class, byte dimension, Color colour)
        {
            ClassName = @class;
            Dimension = dimension;
            FillAlpha = 0x33;
            FillColour = colour;
            LineAlpha = Dimension == 1 ? (byte)0x99 : (byte)0x66;
            LineColour = FillColour;
            LineThickness = 1;
        }

        public ShapeDrawSetting(MapShapeCategory category) : this(category.Name, (byte)category.Dimensions, ColorTranslator.FromHtml(category.Color))
        { }
    }
}