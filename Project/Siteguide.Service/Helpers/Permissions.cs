﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Security.Claims;

namespace SiteGuide.Service.Helpers
{
    public static class Permissions
    {
        public const string SafaMemberNumberClaimType = "SAFA Member";
        public const string AdminRoleName = "Administrator";

        public static bool MaySeeAuthorName(ClaimsPrincipal user)
            => IsSafaMember(user) || IsAdmin(user);

        public static bool MayAddNewSite(ClaimsPrincipal user)
            => IsSafaMember(user) || IsAdmin(user);

        public static bool IsSafaMember(ClaimsPrincipal user)
            => user.HasClaim(c => c.Type == SafaMemberNumberClaimType);

        public static bool IsAdmin(ClaimsPrincipal user)
            => user.IsInRole(AdminRoleName);
    }
}