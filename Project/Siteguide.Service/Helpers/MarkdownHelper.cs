﻿using MarkdownSharp;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteGuide.Service.Helpers
{
    /// <summary>
    /// Helper class for transforming Markdown. Modified from https://blog.dantup.com/2011/03/an-asp-net-mvc-htmlhelper-extension-method-for-markdown-using-markdownsharp/
    /// The Markdown instance was static, but this doesn't work if multiple threads are calling this at once:
    /// "An unhandled exception has occurred while executing the request. System.Collections.Generic.KeyNotFoundException: The given key ' H1582867552H' was not present in the dictionary."
    /// </summary>
    public static class MarkdownHelper
    {
        /// <summary>
        /// Transforms a string of Markdown into HTML.
        /// </summary>
        /// <param name="helper">HtmlHelper - Not used, but required to make this an extension method.</param>
        /// <param name="markdown">The Markdown that should be transformed.</param>
        /// <returns>The HTML representation of the supplied Markdown.</returns>
        public static HtmlString FromMarkdown<T>(this IHtmlHelper<T> helper, string markdown) where T : class
        {
            Markdown markdownTransformer = new();

            // Transform the supplied text (Markdown) into HTML.
            string html = markdownTransformer.Transform(markdown);

            // Wrap the html in an HtmlString otherwise it'll be HtmlEncoded and displayed to the user as HTML :(
            return new HtmlString(html);
        }
    }
}

