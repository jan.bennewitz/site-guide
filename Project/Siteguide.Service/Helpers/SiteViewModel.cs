﻿using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Version = SiteGuide.DAL.Models.Version;
using SiteGuide.Service.Identifiers;
using SiteGuide.Service;

namespace SiteGuide.Service.Helpers
{
    public class SiteInfo
    {
        public Site Site { get; set; }
        public IEnumerable<Launch> Launches { get; set; }
        public IEnumerable<MapShape> MapShapes { get; set; }
        public IEnumerable<SiteContactInfo> Contacts { get; set; }
        public IEnumerable<Video> Videos { get; set; }
        public IEnumerable<SeeAlso> SeeAlsos { get; set; }
        public string AreaName { get; set; }
        public NearVersionsInfo VersionInfo { get; set; }
        public VersionTreeNodeIdentifier RequestedVersionTreeNode { get; set; }
        public int LatestVersionId { get; set; }
        public string GMapsApiKey { get; set; }
        public string SiteRootUrl { get; set; }
    }

    public class LaunchInfo
    {
        public Site Site { get; set; }
        public Launch Launch { get; set; }
        //public IEnumerable<MapShape> MapShapes { get; set; }
        public IEnumerable<SiteContactInfo> Contacts { get; set; }
        //public IEnumerable<Video> Videos { get; set; }
        //public IEnumerable<SeeAlso> SeeAlsos { get; set; }
        //public string AreaName { get; set; }
        //public NearVersionsInfo VersionInfo { get; set; }
        //public VersionTreeNodeIdentifier RequestedVersionTreeNode { get; set; }
        //public int LatestVersionId { get; set; }
        //public string GMapsApiKey { get; set; }
    }

    public class NearVersionsInfo
    {
        public VersionInfo Preceding { get; set; }
        public VersionInfo This { get; set; }
        public VersionInfo Next { get; set; }
        public VersionInfo Latest { get; set; }
    }
    public class SiteContactInfo
    {
        public Contact Contact { get; set; }
        public bool IsContact { get; set; }
        public bool IsResponsible { get; set; }
    }
}
