﻿using Ganss.Xss;
using MarkdownSharp;
using ReverseMarkdown;
using SiteGuide.Service.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Siteguide.Service.Helpers
{
    public class Sanitiser : HtmlSanitizer
    {
        public Sanitiser()
        {
            AllowedSchemes.Add("mailto");
            AllowedSchemes.Add("tel");
            RemovingTag += Sanitizer_RemovingTag;
            RemovingAttribute += Sanitizer_RemovingAttribute;
        }

        public string SanitizeMarkdown(string markdown)
        {
            if (string.IsNullOrEmpty(markdown))
                return markdown;

            Markdown markdownTransformer = new();
            string html = markdownTransformer.Transform(markdown);

            string sanitizedHtml = Sanitize(html);

            Converter htmlToMarkdownConverter = new();
            var result = htmlToMarkdownConverter.Convert(sanitizedHtml);

            return result;
        }

        private static void Sanitizer_RemovingAttribute(object sender, RemovingAttributeEventArgs e)
        {
            if (IsEmbeddedGmapOrYoutube(e.Tag) && new[] { "frameborder", "allowfullscreen" }.Contains(e.Attribute.Name))
                e.Cancel = true;
            //else if (new[] { "hoodies", "inline-left", "inline-right", "alertIcon" }.Contains(e.Tag.ClassName) || e.Attribute.Name == "s%20point%20beach.html'")
            //    e.Cancel = false;
            //else
            //{
            //    // check
            //}
        }

        private static void Sanitizer_RemovingTag(object sender, RemovingTagEventArgs e)
        {
            if (IsEmbeddedGmapOrYoutube(e.Tag))
                e.Cancel = true;
            //else
            //{
            //}
        }
        private static bool IsEmbeddedGmapOrYoutube(AngleSharp.Dom.IElement e)
        {
            string src = e.GetAttribute("src");
            return e.NodeName.ToUpperInvariant() == "IFRAME" && (src.StartsWith("https://www.google.com/maps/embed?") || src.StartsWith("https://www.youtube.com/embed/"));
        }
    }
}
