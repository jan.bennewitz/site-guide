﻿using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using SiteGuide.Service.Extensions;
using SiteGuide.DAL;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using Humanizer;
using SiteGuide.Service.Services;
using System.Configuration;

namespace SiteGuide.Service
{
    public class BackgroundTaskTimer
    {
        public Timer Timer { get; protected set; }
        public string TaskId { get; protected set; }
        public TimeSpan TaskRerunInterval { get; protected set; }
        public DateTime? LastUpdateUtc { get; protected set; }
        public DateTime NextUpdateUtc { get; protected set; }
        public long CurrentFailuresCount { get; protected set; }
        public TimeSpan InitialRetryInterval { get; protected set; }
        public double SubsequentRetryMultiplier { get; protected set; }

        internal void Stop() => Timer.Change(Timeout.Infinite, 0);

        public void Dispose() => Timer.Dispose();

    }
    public class BackgroundTaskTimer<T> : BackgroundTaskTimer, IDisposable
    {
        private readonly ILogger _logger;
        private readonly IServiceProvider _services;

        public Func<T, Task> ActionAsync { get; }
        public BackgroundTaskTimer(string taskId, IServiceProvider services, BackgroundTaskTimerSettings settings, ILogger logger, Func<T, Task> actionAsync)
        {
            TaskId = taskId;

            if (string.IsNullOrEmpty(settings.StaleAge)) throw new ConfigurationErrorsException($"{nameof(settings.StaleAge)} must be configured.");
            if (string.IsNullOrEmpty(settings.InitialRetryDelay)) throw new ConfigurationErrorsException($"{nameof(settings.InitialRetryDelay)} must be configured.");
            if (settings.SubsequentRetryMultiplier == 0) throw new ConfigurationErrorsException($"{nameof(settings.SubsequentRetryMultiplier)} must be configured.");

            TaskRerunInterval = TimeSpan.Parse(settings.StaleAge);
            InitialRetryInterval = TimeSpan.Parse(settings.InitialRetryDelay);
            SubsequentRetryMultiplier = settings.SubsequentRetryMultiplier;

            _logger = logger;
            _services = services;

            using var scope = _services.CreateScope();
            var scopedContext = scope.ServiceProvider.GetRequiredService<SiteGuideContext>();
            LastUpdateUtc = scopedContext.BackgroundTask.SingleOrDefault(c => c.Name == TaskId)?.LastUpdateUtc;
            NextUpdateUtc = LastUpdateUtc == null ? DateTime.UtcNow : ((DateTime)LastUpdateUtc).Add(TaskRerunInterval);
            var waitTime = NextUpdateUtc - DateTime.UtcNow;
            if (waitTime < TimeSpan.Zero)
                waitTime = TimeSpan.Zero;
            Timer = new Timer(async (object state) => await RunNowAsync(), null, waitTime, TaskRerunInterval);
            ActionAsync = actionAsync;
        }

        /// <param name="retryOnFailure">
        /// If called externally, out of turn, then we may or may not want to ignore a failure and not reset the timer. 
        /// If called from the timer, this should always be true, otherwise after a failure the timer will wait the full TaskRerunInterval.
        /// </param>
        public async Task RunNowAsync(bool retryOnFailure = true)
        {
            try
            {
                using var scope = _services.CreateScope();
                var t = scope.ServiceProvider
                    .GetRequiredService<T>();
                await ActionAsync(t);

                LastUpdateUtc = DateTime.UtcNow;

                _logger.LogInformation($"Updated cache \"{TaskId}\". Next update in {TaskRerunInterval.Humanize(3)}.");

                Timer.Change(TaskRerunInterval, TaskRerunInterval);
                NextUpdateUtc = DateTime.UtcNow.Add(TaskRerunInterval);
                CurrentFailuresCount = 0;
            }
            catch (Exception ex)
            {
                TimeSpan retryDelay = InitialRetryInterval * Math.Pow(SubsequentRetryMultiplier, CurrentFailuresCount);
                CurrentFailuresCount++;
                _logger.LogError(ex, $"Failed to execute background task \"{TaskId}\". {CurrentFailuresCount.Ordinal()} failure. Retry in {retryDelay.Humanize(3)}.");
                if (retryOnFailure)
                {
                    Timer.Change(retryDelay, TaskRerunInterval);
                    NextUpdateUtc = DateTime.UtcNow.Add(retryDelay);
                }
            }
        }
    }

    public class BackgroundTaskTimerSettings
    {
        public bool IsEnabled { get; set; } // TODO: BackgroundTask tasks are not disableable currently
        public string StaleAge { get; set; }
        public string InitialRetryDelay { get; set; }
        public double SubsequentRetryMultiplier { get; set; }
    }
}
