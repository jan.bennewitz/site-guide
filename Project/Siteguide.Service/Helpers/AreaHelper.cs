﻿using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using SiteGuide.Service.Identifiers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiteGuide.Service.Helpers
{
    public static class AreaHelper
    {
        public static List<string> GetAreaPathNames(SiteGuideContext context, int areaId, bool rootFirst = true, VersionTreeNodeIdentifier versionTreeNodeId = null)
        {
            var result = new List<string>();
            var areas = context.Area.ValidAtVersionTreeNode(versionTreeNodeId);
            //var areas = editVersion != null
            //    ? context.Area.ValidAtUnacceptedEditVersion((int)editVersion)
            //    : context.Area.ValidAtVersion(version);
            Area area = areas.Single(a => a.Id == areaId);

            while (area.ParentAreaId != null)
            {
                result.Add(area.FullName ?? area.Name);
                area = areas.Single(a => a.Id == area.ParentAreaId);
            }

            if (rootFirst)
                result.Reverse();

            return result;
        }

        public static string AreaParentageDisplay(IEnumerable<string> areaNames, string separator = " > ") => areaNames == null ? null : string.Join(separator, areaNames);
    }
}
