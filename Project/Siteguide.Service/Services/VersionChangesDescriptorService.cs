﻿using DiffMatchPatch;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using SiteGuide.Service.Identifiers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Version = SiteGuide.DAL.Models.Version;
using SiteGuide.Service;
using System.Diagnostics.CodeAnalysis;
using SiteGuide.Service.Helpers;
using Clipper2Lib;
using SiteGuide.DAL;
using System.Diagnostics;
using Geo;
using HashCode = System.HashCode;

namespace SiteGuide.Service
{
    public class VersionChangesDescriptorService
    {
        private readonly SiteGuideContext _context;
        private readonly diff_match_patch _diffMatchPatch = new();
        private readonly VersionService _versionService;
        private readonly IConfiguration _configuration;

        public VersionChangesDescriptorService(SiteGuideContext context, VersionService versionService, IConfiguration configuration)
        {
            _context = context;
            _versionService = versionService;
            _configuration = configuration;
        }

        public async Task<EditVersionChangesDetails> GetEditVersionChangesDetailsAsync(int editId, int? editVersionNumber, int? compareToVersion = null, IdentityUser currentUser = null)
        {
            var thisEditVersion = await _context.EditVersion
                .AsNoTracking()
                .Include(e => e.Status)
                .SingleOrDefaultAsync(ev => ev.EditId == editId && ev.EditVersionNumber == editVersionNumber);
            if (thisEditVersion is null)
                return null;
            int? thisEditAppliedAsVersion = thisEditVersion.StatusId == "A"
                                ? _context.Version
                                    .AsNoTracking()
                                    .Single(v => v.EditId == thisEditVersion.EditId).Id
                                : null;

            bool isEditableByCurrentUser = currentUser != null
                && thisEditVersion.StatusId == "D"
                && thisEditVersion.AuthorId == currentUser.Id;

            if (compareToVersion == null)
            {
                if (thisEditAppliedAsVersion != null) // By default, compare to version this edit was applied to, or to current version if not applied.
                    compareToVersion = thisEditAppliedAsVersion > 1 ? thisEditAppliedAsVersion - 1 : null;
                else
                    compareToVersion = _context.Version
                        .Max<Version, int?>(v => v.Id);
            }

            var compareToVersionInfo = await _versionService.GetVersionInfoAsync(compareToVersion);
            if (compareToVersionInfo is null)
                return null;

            // If we're comparing an applied edit, compare its version instead.
            VersionTreeNodeIdentifier thisVersionOrUnacceptedEditVersionId = (thisEditAppliedAsVersion != null)
                ? new VersionIdentifier((int)thisEditAppliedAsVersion)
                : new EditVersionIdentifier(thisEditVersion, _context);

            return new EditVersionChangesDetails
            {
                ThisEdit = thisEditVersion,
                ThisEditAppliedAsVersion = thisEditAppliedAsVersion,
                CompareToVersion = compareToVersionInfo,
                IsEditableByCurrentUser = isEditableByCurrentUser,
            };
        }

        public IEnumerable<ChangesGroup> GetGroupedChanges(VersionTreeNodeIdentifier thisVersionOrUnacceptedEditVersionId, int? baseVersionId)
        {
            var changeDescriptions = new List<ChangeDescription>();

            changeDescriptions.AddRange(GetVersionOrEditVersionChangeDescriptions(_context.Area, thisVersionOrUnacceptedEditVersionId, baseVersionId));
            changeDescriptions.AddRange(GetVersionOrEditVersionChangeDescriptions(_context.Site, thisVersionOrUnacceptedEditVersionId, baseVersionId));
            changeDescriptions.AddRange(GetVersionOrEditVersionChangeDescriptions(_context.Launch, thisVersionOrUnacceptedEditVersionId, baseVersionId));
            changeDescriptions.AddRange(GetVersionOrEditVersionChangeDescriptions(_context.Video, thisVersionOrUnacceptedEditVersionId, baseVersionId));
            changeDescriptions.AddRange(GetVersionOrEditVersionChangeDescriptions(_context.SeeAlso, thisVersionOrUnacceptedEditVersionId, baseVersionId));
            changeDescriptions.AddRange(GetVersionOrEditVersionChangeDescriptions(_context.Contact, thisVersionOrUnacceptedEditVersionId, baseVersionId));
            changeDescriptions.AddRange(GetVersionOrEditVersionChangeDescriptions(_context.MapShape, thisVersionOrUnacceptedEditVersionId, baseVersionId));
            changeDescriptions.AddRange(GetVersionOrEditVersionChangeDescriptions(_context.SiteContact, thisVersionOrUnacceptedEditVersionId, baseVersionId));
            changeDescriptions.AddRange(GetVersionOrEditVersionChangeDescriptions(_context.SiteMapshape, thisVersionOrUnacceptedEditVersionId, baseVersionId));

            // Remove all changes that are children of a (non-self) removed or added parent: (Is there a better way to do this?)
            var addedOrDeletedRecords = changeDescriptions
                .Where(c =>
                    (c.ChangeType == ChangeType.Addition || c.ChangeType == ChangeType.Removal)
                    && !(c.ParentType == c.RecordType && c.ParentId.Equals(c.RecordKey))) // Do not remove those that are their own parent.
                .Select(c => new RecordIdentifier
                {
                    Type = c.RecordType,
                    Key = c.RecordKey
                });

            changeDescriptions.RemoveAll(c => // Remove those whose parent record was added or deleted on the assumption that this information isn't that interesting.
                addedOrDeletedRecords.Contains(new RecordIdentifier
                {
                    Type = c.ParentType,
                    Key = c.ParentId
                }));

            // Collapse Addition and Associations of the same object under the same parent into a single AdditionAndAssociation record for legibility.
            // Same for removals/disassociations => RemovalAndDisassociation.
            var collapsibleChanges = changeDescriptions
                .Where(c => new[] { ChangeType.Addition, ChangeType.Association, ChangeType.Removal, ChangeType.Disassociation }.Contains(c.ChangeType))
                .GroupBy(c =>
                {
                    var isSiteChild = typeof(ISiteManyToMany).IsAssignableFrom(c.RecordType);
                    var recordKey = isSiteChild ? ((Tuple<int, int>)c.RecordKey).Item2 : c.RecordKey; // TODO - this isn't great
                    var recordType = isSiteChild
                        ? c.RecordType
                            .GetInterfaces()
                            .SingleOrDefault(i => i.IsGenericType && typeof(ISiteManyToMany).IsAssignableFrom(i))
                            .GenericTypeArguments.Single()
                        : c.RecordType;
                    return new
                    {
                        recordType,
                        recordKey,
                        parentType = c.ParentType,
                        parentId = c.ParentId
                    };
                })
                .Where(g => g.Count() > 1)
                .ToList();

            foreach (var collapsibleChange in collapsibleChanges)
            {
                var collapseToThis = collapsibleChange
                    .Single(c => typeof(ISiteManyToMany).IsAssignableFrom(c.RecordType));
                var removeThis = collapsibleChange
                    .Single(c => c != collapseToThis);
                switch (collapseToThis.ChangeType)
                {
                    case ChangeType.Association:
                        Debug.Assert(removeThis.ChangeType == ChangeType.Addition);
                        collapseToThis.ChangeType = ChangeType.AdditionAndAssociation;
                        break;
                    case ChangeType.Disassociation:
                        Debug.Assert(removeThis.ChangeType == ChangeType.Removal);
                        collapseToThis.ChangeType = ChangeType.RemovalAndDisassociation;
                        break;

                    default: throw new NotImplementedException();
                }

                changeDescriptions.Remove(removeThis);
            }

            var result = changeDescriptions
                .OrderBy(c => Array.IndexOf(new Type[] {
                    typeof(Site),
                    typeof(Contact),
                    typeof(MapShape),
                }, c.ParentType))
                .ThenBy(c => c.ParentId)
                .ThenBy(c => Array.IndexOf(new Type[] {
                    typeof(Site),
                    typeof(Launch),
                    typeof(Video),
                    typeof(SeeAlso),
                    typeof(SiteMapshape),
                    typeof(MapShape),
                    typeof(SiteContact),
                    typeof(Contact),
                }, c.RecordType))
                //.ThenBy(c => c.RecordKey)
                .GroupBy(c => new ParentIdentifier { Type = c.ParentType, Id = c.ParentId })
                .Select(g => g.Key.Type.Name switch
                    {
                        nameof(Area) => GetChangesGroup(_context.Area, thisVersionOrUnacceptedEditVersionId, baseVersionId, g),
                        nameof(Site) => GetChangesGroup(_context.Site, thisVersionOrUnacceptedEditVersionId, baseVersionId, g),
                        nameof(Contact) => GetChangesGroup(_context.Contact, thisVersionOrUnacceptedEditVersionId, baseVersionId, g),
                        nameof(MapShape) => GetChangesGroup(_context.MapShape, thisVersionOrUnacceptedEditVersionId, baseVersionId, g),

                        _ => throw new NotImplementedException(),
                    })
                .OrderBy(i => i.AreaParentage == null ? "" : string.Join('/', i.AreaParentage)) // HACK - good enough for most purposes but could think about a real solution
                .ThenBy(i => i.ParentDescription);

            return result;
        }

        private ChangesGroup GetChangesGroup<T>(DbSet<T> dbSet, VersionTreeNodeIdentifier newVersionId, int? oldVersionId, IGrouping<ParentIdentifier, ChangeDescription> grouping) where T : class, new()
        {
            object parent = dbSet switch
            {
                DbSet<Area> a => GetParent(a, newVersionId, oldVersionId, grouping),
                DbSet<Site> s => GetParent(s, newVersionId, oldVersionId, grouping),
                DbSet<Contact> c => GetParent(c, newVersionId, oldVersionId, grouping),
                DbSet<MapShape> m => GetParent(m, newVersionId, oldVersionId, grouping),

                _ => throw new NotImplementedException(),
            };
            string description;
            int? areaId;
            switch (parent)
            {
                case Area area:
                    description = area.FullName ?? area.Name;
                    areaId = area.ParentAreaId;
                    break;
                case Site site:
                    description = site.Name;
                    areaId = site.AreaId;
                    break;
                case Contact contact:
                    description = contact.Name;
                    areaId = null;
                    break;
                case MapShape mapShape:
                    description = GetShortHtmlDescription(mapShape, newVersionId.Identifier);
                    areaId = mapShape.AreaId;
                    break;

                default: throw new NotImplementedException();
            }

            return new ChangesGroup
            {
                ParentDescription = description,
                ParentType = grouping.Key.Type,
                ParentKey = grouping.Key.Id,
                AreaParentage = areaId == null ? null : AreaHelper.GetAreaPathNames(
                    _context,
                    (int)areaId,
                    versionTreeNodeId: newVersionId ?? new VersionIdentifier((int)oldVersionId)),
                ChangeDescriptions = grouping,
            };
        }

        private static IRecord GetParent<T>(DbSet<T> dbSet, VersionTreeNodeIdentifier newVersionId, int? oldVersionId, IGrouping<ParentIdentifier, ChangeDescription> grouping) where T : class, IRecord, IId, new()
            => dbSet
                .AsNoTracking()
                .ValidAtVersionTreeNode(newVersionId)
                .SingleOrDefault(r => r.Id == grouping.Key.Id)
                ??
                dbSet
                .AsNoTracking()
                .ValidAtVersion(oldVersionId)
                .Single(r => r.Id == grouping.Key.Id);

        private string GetShortHtmlDescription<T>(T record, string editVersionString) where T : IRecord
        {
            switch (record)
            {
                case MapShape mapShape:
                    mapShape.Category ??= _context.MapShapeCategory.Single(mc => mc.Id == mapShape.CategoryId);
                    string thumbnail;
                    if (string.IsNullOrEmpty(mapShape.Coordinates))
                    {
                        // This can happen while a new mapshape is added to a draft edit.
                        thumbnail = " [No coordinates]";
                    }
                    else
                    {
                        var mapUrl = $"/Map?mapType=hybrid&{GetRoundedBoundsString(mapShape, 4)}&vnid={editVersionString}";
                        var staticMapUrl = GMapsHelper.GetStaticMapUrl(mapShape, _configuration.GetValue<string>("GMaps:ApiKey"), 150);
                        thumbnail = $"<br /><a href='{mapUrl}'><img style='max-width: 100%' src='{staticMapUrl}'/></a>";
                    }

                    return mapShape.Category.Name + (mapShape.Name == null ? null : (": " + mapShape.Name)) + thumbnail;
                default:
                    throw new NotImplementedException();
            }
        }

        private static string GetRoundedBoundsString(MapShape mapShape, int significantDigits)
        {
            var bounds = new LatLngBounds();
            foreach (var coordinates in mapShape.CoordinatesList)
                bounds.expand(Track.CreateFromPointsData(coordinates, false).GetBounds());

            if (bounds.SizeLat == 0 && bounds.SizeLng == 0) // This happens if there is only a single point in the mapshape
                return $"ll={Math.Round(bounds.Center.lat, 5)},{Math.Round(bounds.Center.lng, 5)}&spn=.004,.004";

            static int OrderOfNumber(double num) => (int)Math.Floor(Math.Log10(num));
            var llScale = -Math.Max(OrderOfNumber((double)bounds.SizeLat), OrderOfNumber((double)bounds.SizeLng)) + significantDigits - 1;
            return $"ll={Math.Round(bounds.Center.lat, llScale)},{Math.Round(bounds.Center.lng, llScale)}&spn={Math.Round((double)bounds.SizeLat, llScale)},{Math.Round((double)bounds.SizeLng, llScale)}";
        }

        //private IEnumerable<ChangeDescription> GetVersionChangeDescriptions<T>(DbSet<T> dbSet, int newVersionId, int? oldVersionId)
        //    where T : class, IRecord, new()
        //    => GetVersionOrEditVersionChangeDescriptions(dbSet, new VersionOrEditVersionId { Type = VersionOrEditVersionIdType.Version, Id = newVersionId }, oldVersionId);

        //private IEnumerable<ChangeDescription> GetEditChangeDescriptions<T>(DbSet<T> dbSet, int editVersionId, int baseVersionId)
        //    where T : class, IRecord, new()
        //    => GetVersionOrEditVersionChangeDescriptions(dbSet, new VersionOrEditVersionId { Type = VersionOrEditVersionIdType.EditVersion, Id = editVersionId }, baseVersionId);

        private IEnumerable<ChangeDescription> GetVersionOrEditVersionChangeDescriptions<T>(DbSet<T> dbSet, VersionTreeNodeIdentifier newVersionOrUnacceptedEditVersionId, int? oldVersionId) where T : class, IRecord, new()
        {
            var newRecords = dbSet
                .AsNoTracking()
                .ValidAtVersionTreeNode(newVersionOrUnacceptedEditVersionId);

            var oldRecords = oldVersionId == null // E.g. we're comparing the first version to 'before the first version' i.e. show all changes, or compare to the empty set.
                ? dbSet.Where(i => false)
                : dbSet
                .AsNoTracking()
                .ValidAtVersion(oldVersionId);

            var changeInfoGroups = oldRecords.FullOuterJoin(newRecords, o => o.Key, n => n.Key, (o, n, id) => new RecordChangeInfo<T> { Key = id, Before = o, After = n })
                .Where(i => i.Before == null || i.After == null || !i.Before.IsRecordContentIdentical(i.After))
                .GroupBy(c =>
                    c.Before == null ? ChangeType.Addition
                    : c.After == null ? ChangeType.Removal
                    : ChangeType.Edit)
                .ToList();

            var changeDescriptions = new List<ChangeDescription>();

            var isManyToMany = typeof(ISiteManyToMany).IsAssignableFrom(typeof(T));
            foreach (var changeInfoGroup in changeInfoGroups)
            {
                switch (changeInfoGroup.Key)
                {
                    case ChangeType.Edit:
                        changeDescriptions.AddRange(
                            GetEditDescriptions(newVersionOrUnacceptedEditVersionId, oldVersionId, changeInfoGroup));
                        break;
                    case ChangeType.Addition:
                        changeDescriptions.AddRange(
                            changeInfoGroup
                                .Select(c => GetPropertyChangeDescription(newVersionOrUnacceptedEditVersionId, oldVersionId, isManyToMany ? ChangeType.Association : ChangeType.Addition, c)));
                        break;
                    case ChangeType.Removal:
                        changeDescriptions.AddRange(
                            changeInfoGroup
                                .Select(c => GetPropertyChangeDescription(newVersionOrUnacceptedEditVersionId, oldVersionId, isManyToMany ? ChangeType.Disassociation : ChangeType.Removal, c)));
                        break;
                }
            }
            return changeDescriptions;
        }

        private IEnumerable<ChangeDescription> GetEditDescriptions<T>(VersionTreeNodeIdentifier newVersionId, int? oldVersionId, IGrouping<ChangeType, RecordChangeInfo<T>> editInfos) where T : class, IRecord, new()
            => editInfos
                .SelectMany(c => // Bit of a hack to achieve 'select some'/'select where' in one step
                {
                    var recordContentComparisons = IRecordExtensions.GetRecordContentComparisons(c.Before, c.After);
                    var recordPropertyDiffs = recordContentComparisons
                        .Where(pc => pc.IsDifferent);

                    // Replace any Lat and Lon changes with a combined LatLon change.
                    if (recordPropertyDiffs.Where(d => (new[] { "Lat", "Lon" }).Contains(d.Name)).Any())
                    {
                        var latLonComparisons = recordContentComparisons
                            .Where(d => (new[] { "Lat", "Lon" }).Contains(d.Name))
                            .ToDictionary(c => c.Name);
                        var replacementRecordPropertyDiffs = recordPropertyDiffs.Where(d => !new[] { "Lat", "Lon" }.Contains(d.Name))
                            .ToList();
                        replacementRecordPropertyDiffs.Add(
                            new RecordPropertyComparison
                            {
                                Name = "Lat/Lon",
                                Type = typeof(string),
                                IsDifferent = true,
                                Value1 = $"{((decimal)latLonComparisons["Lat"].Value1).Normalize()},{((decimal)latLonComparisons["Lon"].Value1).Normalize()}",
                                Value2 = $"{((decimal)latLonComparisons["Lat"].Value2).Normalize()},{((decimal)latLonComparisons["Lon"].Value2).Normalize()}",
                            });

                        recordPropertyDiffs = replacementRecordPropertyDiffs;
                    }

                    return recordPropertyDiffs
                        .Select(pd => GetPropertyChangeDescription(newVersionId, oldVersionId, ChangeType.Edit, c, pd));
                });

        private string GetRecordPropertyDiffText<T>(RecordChangeInfo<T> c, RecordPropertyComparison propertyDiff, int? oldVersionId, VersionTreeNodeIdentifier newVersionId) where T : class, IRecord, new()
        {
            if (propertyDiff.Type == typeof(string))
            {
                switch (propertyDiff.Name)
                {
                    case "Coordinates":
                        var mapshapeBefore = (MapShape)(object)c.Before;
                        var mapshapeAfter = (MapShape)(object)c.After;
                        mapshapeBefore.Category = _context.MapShapeCategory.Single(mc => mc.Id == mapshapeBefore.CategoryId);
                        mapshapeAfter.Category = _context.MapShapeCategory.Single(mc => mc.Id == mapshapeAfter.CategoryId);

                        //var viewPort = GMapsHelper.GetBounds(mapshapeBefore);
                        //viewPort.expand(GMapsHelper.GetBounds(mapshapeAfter));
                        //details = $"<a href='TODO'><img class='del' src='{GMapsHelper.GetStaticMapUrl(mapshapeBefore, 200, viewPort)}'/></a> => <a href='TODO'><img class='ins' src='{GMapsHelper.GetStaticMapUrl(mapshapeAfter, 200, viewPort)}'/></a>";

                        var mapShapeBeforeClipperPolys = GetClipperPolys(mapshapeBefore.GetCoordinates());
                        var mapShapeAfterClipperPolys = GetClipperPolys(mapshapeAfter.GetCoordinates());

                        var drawSetting = new ShapeDrawSetting(mapshapeAfter.Category);
                        if (drawSetting.Dimension == 1)
                        {
                            //mapShapeBeforeClipperPolys = InflateMapShapeClipperPoly(mapShapeBeforeClipperPolys, )
                            // If we're comparing paths instead of polygons, turn them into polygons for clipper:
                            // TODO: Correct for non-squareness of coordinate grid - project to cartesian, inflate, then project back.
                            mapShapeBeforeClipperPolys = Clipper.InflatePaths(mapShapeBeforeClipperPolys, 1E-6, JoinType.Square, EndType.Square, precision: 7);
                            mapShapeAfterClipperPolys = Clipper.InflatePaths(mapShapeAfterClipperPolys, 1E-6, JoinType.Square, EndType.Square, precision: 7);
                        }
                        var unchanged = Clipper.Intersect(mapShapeBeforeClipperPolys, mapShapeAfterClipperPolys, FillRule.NonZero, 7);
                        var removed = Clipper.Difference(mapShapeBeforeClipperPolys, mapShapeAfterClipperPolys, FillRule.NonZero, 7);
                        var added = Clipper.Difference(mapShapeAfterClipperPolys, mapShapeBeforeClipperPolys, FillRule.NonZero, 7);

                        return $"<br/><img title='Deletions outlined bright red, additions outlined bright green.' src='{GMapsHelper.GetStaticMapUrl(GetCoordinateStrings(removed), GetCoordinateStrings(unchanged), GetCoordinateStrings(added), 600, drawSetting, _configuration.GetValue<string>("GMaps:ApiKey"))}'/></a>";

                    //case "SafaClubId":
                    //    details = $"<a href = '{_configuration.GetValue<string>("SAFA:ClubsPageUrl").Replace("{clubId}", (string)(propertyDiff.Value1 ?? propertyDiff.Value2))}'>{details}</a>";
                    //    break;

                    case "Lat/Lon":
                        var diffs = _diffMatchPatch.diff_main((string)propertyDiff.Value1 ?? "", (string)propertyDiff.Value2 ?? "");
                        _diffMatchPatch.diff_cleanupSemantic(diffs);
                        var gmapsApiKey = _configuration.GetValue<string>("GMaps:ApiKey");
                        //return _diffMatchPatch.diff_prettyHtml(diffs);
                        // TODO: Move this into a custom control?
                        return $"""
                            <div class='row'>
                                <div class='col'>
                                    <a href='https://www.google.com.au/maps/search/{propertyDiff.Value1}/data=!3m1!1e3'>
                                        <img title='Old location' src='{GMapsHelper.GetCoordinatesStaticMapUrl((string)propertyDiff.Value1, true, gmapsApiKey)}'/>
                                    </a>
                                    <br/>
                                    Old: {(string)propertyDiff.Value1}
                                </div>
                                <div class='col'>
                                    <a href='https://www.google.com.au/maps/search/{propertyDiff.Value2}/data=!3m1!1e3'>
                                        <img title='New location' src='{GMapsHelper.GetCoordinatesStaticMapUrl((string)propertyDiff.Value2, false, gmapsApiKey)}'/>
                                    </a>
                                    <br/>
                                    New: {(string)propertyDiff.Value2}
                                </div>
                            </div>
                            """;

                    default:
                        diffs = _diffMatchPatch.diff_main((string)propertyDiff.Value1 ?? "", (string)propertyDiff.Value2 ?? "");
                        _diffMatchPatch.diff_cleanupSemantic(diffs);
                        return _diffMatchPatch.diff_prettyHtml(diffs);
                }
            }
            else if (propertyDiff.Type == typeof(bool))
                return $"<del>{propertyDiff.Value1}</del><ins>{propertyDiff.Value2}</ins>";
            else
            {
                string oldVal, newVal;
                switch (propertyDiff.Name)
                {
                    case "AreaId":
                        oldVal = AreaHelper.AreaParentageDisplay(AreaHelper.GetAreaPathNames(_context, (int)propertyDiff.Value1, versionTreeNodeId: new VersionIdentifier((int)oldVersionId)));
                        newVal = AreaHelper.AreaParentageDisplay(AreaHelper.GetAreaPathNames(_context, (int)propertyDiff.Value2, versionTreeNodeId: newVersionId));
                        break;
                    case "CategoryId":
                        oldVal = _context.MapShapeCategory.Single(c => c.Id == (int)propertyDiff.Value1).Name;
                        newVal = _context.MapShapeCategory.Single(c => c.Id == (int)propertyDiff.Value2).Name;
                        break;
                    case "InsetMapPositionId":
                        oldVal = _context.InsetMapPosition.Single(p => p.Id == (int)propertyDiff.Value1).Name;
                        newVal = _context.InsetMapPosition.Single(p => p.Id == (int)propertyDiff.Value2).Name;
                        break;
                    case "AdminId":
                        oldVal = propertyDiff.Value1 is null ? "" : _context.Contact.ValidAtVersion(c.Before.ValidToVersion).Single(c => c.Id == (int)propertyDiff.Value1).FullNameOrName();
                        newVal = propertyDiff.Value2 is null ? "" : _context.Contact.ValidAtVersion(c.After.ValidFromVersion).Single(c => c.Id == (int)propertyDiff.Value2).FullNameOrName();
                        break;
                    default:
                        // TODO - Make sure we handle other non-string, non-bool cases. Mapshape.?, special case for MapShape.Coordinates, special case for launch coordinates
                        oldVal = propertyDiff.Value1.ToString();
                        newVal = propertyDiff.Value2.ToString();
                        break;
                }
                return $"<del>{oldVal}</del><ins>{newVal}</ins>";
            }
        }

        private static PathsD GetClipperPolys(List<List<Tuple<decimal, decimal>>> coords)
        {
            PathsD result = new();
            foreach (var sourcePath in coords)
                result.Add(Clipper.MakePath(sourcePath.SelectMany(pt => new double[] { (double)pt.Item1, (double)pt.Item2 }).ToArray()));
            return result;
        }

        private static string[] GetCoordinateStrings(PathsD polys)
            => polys.Select(p => string.Join(' ', p.Select(c => c.y + "," + c.x))).ToArray();

        /// <param name="oldVersionId">This can be null eg when looking at the initial edit.</param>
        private ChangeDescription GetPropertyChangeDescription<T>(
                VersionTreeNodeIdentifier newVersionId,
                int? oldVersionId,
                ChangeType changeType,
                RecordChangeInfo<T> changeInfo,
                RecordPropertyComparison propertyDiff = null)
            where T : class, IRecord, new()
        {
            var parent = GetParentRecord(newVersionId, oldVersionId, changeType, changeInfo);

            string overridePropertyName = propertyDiff?.Name switch
            {
                "SafaClubId" => "SAFA Club",
                "AreaId" => "Area",
                "CategoryId" => "Category",
                "InsetMapPositionId" => "Inset Map Position",
                "AdminId" => "Administrator",
                _ => null,
            };

            return new ChangeDescription
            {
                ParentType = parent.Type,
                ParentId = (int)parent.Key,
                RecordType = typeof(T),
                RecordKey = changeInfo.Key,
                RecordDescription = typeof(T) == parent.Type ? null : GetRecordDescription(newVersionId, oldVersionId, changeInfo),
                ChangeType = changeType,
                Item = overridePropertyName ?? propertyDiff?.Name,
                Details = changeType == ChangeType.Edit ? GetRecordPropertyDiffText(changeInfo, propertyDiff, oldVersionId, newVersionId) : null,
            };
        }

        private RecordIdentifier GetParentRecord<T>(VersionTreeNodeIdentifier newVersionId, int? oldVersionId, ChangeType changeType, RecordChangeInfo<T> changeInfo) where T : class, IRecord, new()
        {
            switch (changeInfo.After ?? changeInfo.Before)
            {
                case Area area:
                    return new RecordIdentifier { Type = typeof(Area), Key = area.Id };

                case ISiteChild siteChild:
                    return new RecordIdentifier { Type = typeof(Site), Key = siteChild.SiteId };

                case Site site:
                    if (changeType == ChangeType.Addition || changeType == ChangeType.Removal)
                        return new RecordIdentifier { Type = typeof(Area), Key = site.AreaId };
                    else
                        return new RecordIdentifier { Type = typeof(Site), Key = site.Id };

                case Contact contact:
                    // TODO: Assign to site if only one site, assign to smallest area encompassing all ever associated sites otherwise. Leave null if none.
                    var associatedSiteIds = _context.SiteContact
                        .AsNoTracking()
                        .ValidAtVersionTreeNode(changeType == ChangeType.Removal
                            ? new VersionIdentifier((int)oldVersionId)
                            : newVersionId)
                        .Where(a => a.ContactId == contact.Id)
                        .Select(a => a.SiteId)
                        .Distinct();
                    if (associatedSiteIds.Count() == 1)
                        return new RecordIdentifier { Type = typeof(Site), Key = associatedSiteIds.Single() };
                    else
                    {
                        return new RecordIdentifier
                        {
                            Type = typeof(Area),
                            Key = _context.Area
                            .ValidAtVersionTreeNode(changeType == ChangeType.Removal
                                ? new VersionIdentifier((int)oldVersionId)
                                : newVersionId)
                            .AsNoTracking()
                            .Single(a => a.ParentAreaId == null)
                                .Id
                        };
                    }

                case MapShape mapShape:
                    associatedSiteIds = _context.SiteMapshape
                        .AsNoTracking()
                        .ValidAtVersionTreeNode(changeType == ChangeType.Removal
                            ? new VersionIdentifier((int)oldVersionId)
                            : newVersionId)
                        .Where(a => a.MapshapeId == mapShape.Id)
                        .Select(a => a.SiteId)
                        .Distinct();
                    if (associatedSiteIds.Count() == 1)
                        return new RecordIdentifier { Type = typeof(Site), Key = associatedSiteIds.Single() };
                    else
                        return new RecordIdentifier { Type = typeof(Area), Key = mapShape.AreaId };

                default: throw new NotImplementedException($"Unhandled type {typeof(T).Name}.");
            }
        }

        /// <param name="oldVersionId">This can be null eg when looking at the initial edit.</param>
        private string GetRecordDescription<T>(VersionTreeNodeIdentifier newVersionId, int? oldVersionId, RecordChangeInfo<T> changeInfo) where T : class, IRecord, new()
        {
            string editVersionString;
            T record;
            if (changeInfo.After != null) // Describe the new version if it exists, otherwise the old version (which has been deleted if the new version doesn't exist).
            {
                editVersionString = newVersionId.Identifier;
                record = changeInfo.After;
            }
            else
            {
                editVersionString = oldVersionId.ToString();
                record = changeInfo.Before;
            }
            return record switch
            {
                Site s => s.Name,
                Launch l => l.Name,
                SeeAlso a => $"<a href='{a.Link}'>{a.Name}</a>",
                //Launch l => l.Name != null
                //    ? $"Launch \"{l.Name}\""
                //    : $"Unnamed launch", // Do something smart like call it just "Launch" if there's only one for this site before and after if edit, one before if delete or one after if addition? Possibly not worth the effort.
                //SeeAlso a =>
                //    $"SeeAlso \"{a.Name}\"",
                Video v => GetVideoRecordDescription(v),
                Contact c => c.Name,
                MapShape m => GetShortHtmlDescription(m, editVersionString),
                SiteContact sc => (sc.IsContact ? ("Contact" + (sc.IsResponsible ? "/" : "")) : "") + (sc.IsResponsible ? "Responsible" : "") + ": " + GetForeignKeyChangeRecord(newVersionId, oldVersionId, _context.Contact, sc.ContactId).Name,
                SiteMapshape sm => GetShortHtmlDescription(GetForeignKeyChangeRecord(newVersionId, oldVersionId, _context.MapShape, sm.MapshapeId), editVersionString),

                _ => throw new NotImplementedException($"Unhandled type {typeof(T).Name}."),
            };
        }

        private string GetVideoRecordDescription(Video v)
        {
            v.Type ??= _context.VideoType.Single(t => t.Id == v.TypeId);
            string thumbnailUrl = v.ThumbnailUrl
                ?? v.Type.ThumbnailUrl?.Replace("%id", v.Identifier)
                ?? "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Wikiversity-Mooc-Icon-Video.svg/200px-Wikiversity-Mooc-Icon-Video.svg.png";
            return $"<a href='{v.Type.LinkUrl.Replace("%id", v.Identifier)}'><img src='{thumbnailUrl}' /></a>"; //https://img.youtube.com/vi/sD3uYSI2V_g/default.jpg
        }

        /// <param name="oldVersionId">This can be null eg when looking at the initial edit.</param>
        private static T GetForeignKeyChangeRecord<T>(VersionTreeNodeIdentifier newVersionId, int? oldVersionId, DbSet<T> dbSet, int id) where T : class, IRecord, IId
            => (
            oldVersionId == null
                ? dbSet.ValidAtVersionTreeNode(newVersionId).ToList().Single(c => c.Id == id)
                : dbSet.ValidAtVersionTreeNode(newVersionId).ToList().SingleOrDefault(c => c.Id == id)
                ??
                dbSet.ValidAtVersion(oldVersionId).ToList().Single(c => c.Id == id)
            );

        public bool HasChanges(EditVersion editVersion)
        {
            int baseVersionId = _context.Version.Max(v => v.Id);
            return GetGroupedChanges(new EditVersionIdentifier(editVersion, _context), baseVersionId)
                .Any();
        }
    }

    public class VersionDetailsModel
    {
        public VersionInfo ThisVersion { get; set; }
        public VersionInfo CompareToVersion { get; set; }
        public bool NextVersionExists { get; set; }
        public bool PreviousVersionExists { get; set; }
    }

    public class EditVersionChangesDetails
    {
        public EditVersion ThisEdit { get; set; }
        public int? ThisEditAppliedAsVersion { get; set; }
        public VersionInfo CompareToVersion { get; set; }
        public bool IsEditableByCurrentUser { get; set; }
    }

    public class ChangesGroup
    {
        public string ParentDescription { get; set; }
        public Type ParentType { get; set; }
        public object ParentKey { get; set; }
        public IEnumerable<string> AreaParentage { get; set; }
        public IEnumerable<ChangeDescription> ChangeDescriptions { get; set; }

        public ChangeType ParentChangeType()
        {
            var parentFirstChangeDescription = ChangeDescriptions.FirstOrDefault(c => ParentType == c.RecordType);
            if (parentFirstChangeDescription == null) // No change to parent itself, only changes to child records. We'll count that as an edit of the parent.
                return ChangeType.Edit;
            else
                return parentFirstChangeDescription.ChangeType;
        }
    }
    public class ChangeDescription
    {
        public Type ParentType { get; set; }
        public int ParentId { get; set; }
        public Type RecordType { get; set; }
        public object RecordKey { get; set; }
        public string RecordDescription { get; set; }
        public ChangeType ChangeType { get; set; }
        public string Item { get; set; }
        public string Details { get; set; }

        public override string ToString() => $"{RecordType.Name} {RecordKey}: {ChangeType} [{Item}] {Details}";
    }
    internal class RecordIdentifier : IEquatable<RecordIdentifier>
    {
        public Type Type { get; set; }
        public object Key { get; set; }

        public override bool Equals(object obj) => Equals((RecordIdentifier)obj);

        public bool Equals([AllowNull] RecordIdentifier other) => other.Type == Type && other.Key.Equals(Key);

        public override int GetHashCode() => HashCode.Combine(Type, Key);
    }

    internal class ParentIdentifier : IEquatable<ParentIdentifier>
    {
        public Type Type { get; set; }
        public int Id { get; set; }

        public override bool Equals(object obj) => Equals((ParentIdentifier)obj);

        public bool Equals([AllowNull] ParentIdentifier other) => other.Type == Type && other.Id == Id;

        public override int GetHashCode() => HashCode.Combine(Type, Id);

        public override string ToString() => $"{Type.Name} {Id}";
    }

    internal class RecordChangeInfo<T> where T : class, IRecord, new()
    {
        public object Key { get; set; }
        public T Before { get; set; }
        public T After { get; set; }

        public override string ToString() => $"Change to {typeof(T).Name} {After ?? Before}";
    }
}
