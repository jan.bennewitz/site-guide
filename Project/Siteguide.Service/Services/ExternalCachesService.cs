﻿using HtmlAgilityPack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace SiteGuide.Service.Services
{
    public class ExternalCache
    {
        private static readonly HttpClient _httpClient = new();

        public IServiceScopeFactory _serviceScopeFactory;
        private readonly ILogger<ExternalCache> _logger;
        private readonly IConfiguration _configuration;

        public ExternalCache(IServiceScopeFactory serviceScopeFactory, ILogger<ExternalCache> logger, IConfiguration configuration)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _logger = logger;
            _configuration = configuration;
        }

        public async Task<bool> UpdateDataAsync(ExtCacheEnum cache)
        {
            try
            {
                switch (cache)
                {
                    case ExtCacheEnum.Ctaf:
                        {
                            var rows = await GetExternalDataCtafs().ConfigureAwait(false);
                            return UpdateExtCacheRecords(rows, c => c.Code, cache);
                        }
                    case ExtCacheEnum.Club:
                        {
                            var rows = await GetExternalDataClubs().ConfigureAwait(false);
                            return UpdateExtCacheRecords(rows, c => c.SafaCode, cache);
                        }
                    case ExtCacheEnum.ErsaEffectiveDate:
                        {
                            var rows = await GetExternalDataErsaEffectiveDates().ConfigureAwait(false);
                            return UpdateExtCacheRecords(rows, d => d.EffectiveDate, cache, false /* We have seen the current date missing from the Air Services page, so keep old dates. */);
                        }
                    case ExtCacheEnum.Editor:
                        {
                            var rows = await GetExternalDataEditors().ConfigureAwait(false);
                            return UpdateExtCacheRecords(rows, e => new { e.Club, e.SafaId }, cache);
                        }

                    default: throw new ArgumentException($"Unhandled value \"{cache}\".", nameof(cache));
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException($"Could not refresh {cache} cache.", ex);
            }
        }

        private static async Task<List<ExtCacheCtaf>> GetExternalDataCtafs()
        {
            HttpResponseMessage response = await GetExternalDataAsync("https://xcaustralia.org/download/class_ctaf.php")
                            .ConfigureAwait(false);
            var responseText = await response.Content.ReadAsStringAsync();

            var matches = Regex.Matches(responseText, @"\r\n\r\n(?:.+\r\n)+AN (?<name>.+) (?<CTAF>[0-9]*\.?[0-9]+) (?:\*TWR HRS-SEE ERSA\* )?(?<state>.+) \((?<code>.+)\) (?<category>.+)\r\n(?:.+\r\n)+V X=(?<latDeg>.+):(?<latMin>.+):(?<latSec>.+) (?<latHem>.)  (?<lngDeg>.+):(?<lngMin>.+):(?<lngSec>.+) (?<lngHem>.)\r\n");

            if (matches.Count == 0)
                throw new Exception("No data found.");

            var rows = new List<ExtCacheCtaf>();

            foreach (Match match in matches)
            {
                // Work around issue in source file:
                var category = match.Groups["category"].Value.Trim();
                switch (category)
                {
                    case "CERT":
                    case "REG":
                    case "MIL":
                    case "UNCR":
                    case "JOINT": // E.g. Townsville
                        break;
                    case "WREG":
                        category = "REG";
                        break;
                    case "WCERT":
                        category = "CERT";
                        break;
                    default:
                        throw new ApplicationException($"Unrecognised category \"{category}\".");
                }

                if (category == "UNCR")
                    continue;

                if (!new string[] { "NSW", "VIC", "ACT", "QLD", "SA", "NT", "WA", "TAS", "OTH" /* "Other" ? Christmas Island */}.Contains(match.Groups["state"].Value))
                    throw new ArgumentException("State not recognised. Value was: " + match.Groups["state"].Value);

                var latLng = new Geo.LatLng(
                    match.Groups["latHem"].Value[0],
                    int.Parse(match.Groups["latDeg"].Value),
                    int.Parse(match.Groups["latMin"].Value),
                    int.Parse(match.Groups["latSec"].Value),
                    match.Groups["lngHem"].Value[0],
                    int.Parse(match.Groups["lngDeg"].Value),
                    int.Parse(match.Groups["lngMin"].Value),
                    int.Parse(match.Groups["lngSec"].Value)
                    );

                rows.Add(new ExtCacheCtaf
                {
                    Code = match.Groups["code"].Value,
                    Name = match.Groups["name"].Value,
                    Category = category,
                    Ctaf = match.Groups["CTAF"].Value,
                    Lat = Math.Round(latLng.lat, 4),
                    Lng = Math.Round(latLng.lng, 4),
                });
            }

            return rows;
        }

        private async Task<List<ExtCacheClub>> GetExternalDataClubs()
        {
            string apiKey = _configuration["SafaApiKey"];
            var response = await GetExternalDataAsync($"https://members.safa.asn.au/hgfa_api.php?API_KEY={apiKey}&club_data=siteguide&data_format=json")
                            .ConfigureAwait(false);
            var responseText = await response.Content.ReadAsStringAsync();

            var rows = JArray.Parse(responseText)
                .ToObject<List<dynamic>>()
                .Select(i => new ExtCacheClub
                {
                    Name = i.Name,
                    Category = i.Category.Value switch
                    {
                        "assoc" => "a",
                        "club" => "c",
                        "school" => "s",

                        _ => throw new NotImplementedException($"Unrecognised category \"{i.Category}\".")
                    },
                    Address = i.Address.Value ?? "", // Could make this DB field nullable instead.
                    Lat = decimal.Round(decimal.Parse(i.Lat.Value), 6),
                    Lng = decimal.Round(decimal.Parse(i.Lng.Value), 6),
                    Phone = i.Phone,
                    SafaCode = i.SafaCode,
                    Email = i.Email,
                    Url = i.Url,
                })
                .ToList();
            return rows;
        }

        private static async Task<List<ExtCacheErsaEffectiveDate>> GetExternalDataErsaEffectiveDates()
        {
            var doc = await new HtmlWeb().LoadFromWebAsync("https://www.airservicesaustralia.com/industry-info/aeronautical-information-management/document-amendment-calendar/");

            var rows = new List<ExtCacheErsaEffectiveDate>();

            var sourceRows = doc.DocumentNode
                .SelectNodes("//article//tr");

            if (sourceRows == null || sourceRows.Count == 0)
                throw new Exception("No data found.");

            foreach (var sourceRow in sourceRows)
            {
                var headerCells = sourceRow.SelectNodes("th");
                if (headerCells != null)
                {
                    if (headerCells.Count > 4)
                        throw new Exception("Expected no more than four cells per row.");

                    if (headerCells[1].InnerText.Trim() != "AIRAC Effective Date")
                        throw new Exception("Expected header to be \"AIRAC Effective Date\" for fourth column.");
                }

                var cells = sourceRow.SelectNodes("td[@rowspan='4' or @rowspan='5']"); // If this proves too fragile consider searching for <strong> dates instead.
                if (cells != null)
                {
                    if (cells.Count > 2)
                        throw new Exception("Expected no more than two cells per row.");

                    if (cells.Count == 2)
                        rows.Add(new ExtCacheErsaEffectiveDate
                        {
                            EffectiveDate = DateOnly.ParseExact(cells[1].InnerText.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture),
                        });
                }
            }

            if (rows.Count == 0)
                throw new Exception("No Ersa Effective Date data found.");

            return rows;
        }

        private async Task<IEnumerable<ExtCacheEditor>> GetExternalDataEditors()
        {
            string apiKey = _configuration["SafaApiKey"];
            HttpResponseMessage response = await GetExternalDataAsync($"https://members.safa.asn.au/hgfa_api.php?CLUB=SGA&API_KEY={apiKey}&data_format=json")
                .ConfigureAwait(false);
            var responseText = await response.Content.ReadAsStringAsync();

            var rows = JArray.Parse(responseText)
                .ToObject<List<dynamic>>()
                .Select(i => new ExtCacheEditor
                {
                    Club = i.club == "hgfa" ? "SAFA" : i.club,
                    SafaId = i.hgfa,
                    Position = i.postition,
                    Name = i.name,
                    Mobile = i.mobile,
                    Email = i.email,
                    FirstName = i.fname,
                    Surname = i.sname,
                });
            return rows;
        }

        /// <returns>True if cache was updated, false if there were no changes.</returns>
        private bool UpdateExtCacheRecords<T>(IEnumerable<T> rows, Func<T, object> keySelector, ExtCacheEnum cacheType, bool deleteObsolete = true) where T : class
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<SiteGuideContext>(); // Can't just use a _context object from dependency injection. https://stackoverflow.com/questions/70181860/background-tasks-with-net-core-lifetime-and-di-injection
            var dbSet = context.Set<T>();
            var byKeyChange = dbSet
                .FullOuterJoin(
                    rows,
                    keySelector,
                    keySelector,
                    (o, n, k) => new OldNewRecordPair<T>(o, n))
                .ToLookup(j =>
                    j.NewRecord == null ? ChangeType.Removal
                    : j.OldRecord == null ? ChangeType.Addition
                    : ChangeType.Unchanged);

            var removed = byKeyChange[ChangeType.Removal].Select(e => e.OldRecord);
            var added = byKeyChange[ChangeType.Addition].Select(e => e.NewRecord);
            var changed = byKeyChange[ChangeType.Unchanged]
                .Where(e => AreRecordsDifferent(e.OldRecord, e.NewRecord));

            bool hasActionableChanges = (removed.Any() && deleteObsolete) || added.Any() || changed.Any();
            if (hasActionableChanges)
            {
                _logger.LogInformation($"External Cache {typeof(T).Name} synched: Changes found.");
                if (removed.Any() && deleteObsolete)
                {
                    _logger.LogInformation($"External Cache {typeof(T).Name} synched: Removing {string.Join(", ", removed)}");
                    context.RemoveRange(removed);
                }

                if (added.Any())
                {
                    _logger.LogInformation($"External Cache {typeof(T).Name} synched: Adding {string.Join(", ", added)}");
                    context.AddRange(added);
                }

                if (changed.Any())
                {
                    _logger.LogInformation($"External Cache {typeof(T).Name} synched: Updating {string.Join(", ", changed.Select(e => e.NewRecord))}");
                    foreach (var update in changed)
                    {
                        var record = dbSet
                            .ToList() // These tables are small and not updated often so this is acceptable. Not sure how to keep this generic otherwise.
                            .Single(e =>
                            keySelector(e).Equals(keySelector(update.OldRecord)));
                        CopyProperties(update.NewRecord, record);
                    }
                }
            }
            else if (removed.Any() && !deleteObsolete)
                _logger.LogInformation($"External Cache {typeof(T).Name} synched: Removed records found but ignored due to deleteObsolete parameter being set to false.");
            else
                _logger.LogInformation($"External Cache {typeof(T).Name} synched: No changes.");

            // Update/create record in BackgroundTask:
            var backgroundTaskRecord = context.BackgroundTask
                .SingleOrDefault(c => c.Name == cacheType.ToString());
            if (backgroundTaskRecord == null)
            {
                backgroundTaskRecord = new BackgroundTask { Name = cacheType.ToString() };
                context.BackgroundTask.Add(backgroundTaskRecord);
            }
            backgroundTaskRecord.LastUpdateUtc = DateTime.UtcNow;

            context.SaveChanges();

            return hasActionableChanges;
        }

        private static async Task<HttpResponseMessage> GetExternalDataAsync(string requestUri)
        {
            var response = await _httpClient
                .GetAsync(requestUri)
                .ConfigureAwait(false);
            if (!response.IsSuccessStatusCode)
                throw new ApplicationException($"Could not retrieve data: {response.StatusCode} {response.ReasonPhrase}\r\nUri: {requestUri}");

            return response;
        }

        private static async Task<XmlDocument> GetExternalXmlAsync(string url)
        {
            HttpResponseMessage response = await GetExternalDataAsync(url)
                .ConfigureAwait(false);
            if (!response.IsSuccessStatusCode)
                throw new ApplicationException($"Can not retrieve data: {response.StatusCode} {response.ReasonPhrase}");

            XmlDocument doc = new XmlDocument();
            using var responseStream = await response.Content.ReadAsStreamAsync();
            doc.Load(responseStream);
            return doc;
        }

        private static bool AreRecordsDifferent<T>(T rec1, T rec2, IList<string> ignoreProperties = null) where T : class
            => typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(propInfo =>
                    !(ignoreProperties != null && ignoreProperties.Contains(propInfo.Name))
                    && (propInfo.PropertyType.IsValueType || propInfo.PropertyType == "".GetType())
                    && propInfo.CanWrite /* Filter out any calculated properties */)
                .Any(propInfo =>
                {
                    object val1 = propInfo.GetValue(rec1, null);
                    object val2 = propInfo.GetValue(rec2, null);

                    return val1 != val2 && (val1 == null || !val1.Equals(val2));
                });

        private static T CopyProperties<T>(T source, T target, IList<string> ignoreProperties = null) where T : class
        {
            if (target == null) throw new ArgumentNullException(nameof(target));
            if (source == null) throw new ArgumentNullException(nameof(source));

            Type type = typeof(T);

            foreach (var propInfo in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                if (!(ignoreProperties != null && ignoreProperties.Contains(propInfo.Name))
                    && propInfo.CanWrite
                    && (propInfo.PropertyType.IsValueType || propInfo.PropertyType == "".GetType()))
                {
                    propInfo.SetValue(target, propInfo.GetValue(source, null));
                }

            return target;
        }
    }

    public enum ExtCacheEnum
    {
        Ctaf,
        Club,
        ErsaEffectiveDate,
        Editor,
    }

    public class OldNewRecordPair<T>
    {
        public OldNewRecordPair(T oldRecord, T newRecord)
        {
            OldRecord = oldRecord;
            NewRecord = newRecord;
        }
        public T OldRecord { get; set; }
        public T NewRecord { get; set; }
    }
}
