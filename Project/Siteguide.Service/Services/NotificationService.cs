﻿using Humanizer;
using Mailjet.Client.Resources;
using Mailjet.Client.TransactionalEmails;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SiteGuide.Service.Services;
using SiteGuide.Service.Views.Emails;
using SiteGuide.Service.Views.Emails.NotifyEditorOfStatusChange;
using SiteGuide.Service.Views.Emails.PasswordReset;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service
{
    public class NotificationService
    {
        private readonly string _sendToFolder;
        private readonly string _BccTo;
        private readonly bool _notificationsToRegisteredUsersOnly;
        private readonly string _fromAddress;
        private readonly string _fromName;
        private readonly string _environment;

        private readonly IEmailService _emailService;
        private readonly RazorViewToStringRenderer _razorViewToStringRenderer;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<IdentityUser> _userManager;
        public NotificationService(IOptionsMonitor<NotificationOptions> optionsAccessor, IEmailService emailService, RazorViewToStringRenderer razorViewToStringRenderer, IHttpContextAccessor httpContextAccessor, UserManager<IdentityUser> userManager)
        {
            var options = optionsAccessor.CurrentValue;
            _fromAddress = options.FromAddress;
            _fromName = options.FromName;
            _sendToFolder = options.SendToFolder;
            _BccTo = options.BccTo;
            _notificationsToRegisteredUsersOnly = options.NotificationsToRegisteredUsersOnly;
            _environment = options.Environment;

            _emailService = emailService;
            _razorViewToStringRenderer = razorViewToStringRenderer;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
        }

        private string GetSiteBaseUrl() => $"{_httpContextAccessor.HttpContext.Request.Scheme}://{_httpContextAccessor.HttpContext.Request.Host.ToUriComponent()}";

        public async Task NotifySubscribersAsync(IEnumerable<EditorInfo> editors, int editId, EditAction action, string actionByUserEmail)
        {
            // Notify all affected parties.
            // TODO: Add a user-editable subscription system.
            if (new string[] { nameof(EditActions.Accept), nameof(EditActions.Reject) }.Contains(action.Name))
                await NotifyAuthorOfStatusChange(editId, action, actionByUserEmail);
            if (new string[] { nameof(EditActions.Accept), nameof(EditActions.SubmitForReview) }.Contains(action.Name)) // No notification for rejections
                await NotifyEditorsOfStatusChange(editId, action, actionByUserEmail, editors);
        }

        private async Task NotifyAuthorOfStatusChange(int editId, EditAction action, string actionByUserEmail)
        {
            // Notify original submitter.
            Debug.WriteLine($"Notify submitter: {actionByUserEmail}");

            var model = new EditNotificationViewModel(GetSiteBaseUrl(), action, editId, GetSalutationFromEmail(actionByUserEmail));
            await SendEmailAsync(actionByUserEmail, $"A site guide edit you requested has been {model.ActionDescriptionPastParticiple}.", "NotifyAuthorOfStatusChange", model);
        }

        public async Task ConfirmAccountAsync(string email, string callbackUrl)
            => await SendEmailAsync(email, "Confirm your email", "ConfirmAccount", new CallbackEmailViewModel(GetSiteBaseUrl(), callbackUrl, GetSalutationFromEmail(email)), true);

        public async Task ConfirmChangeEmailAsync(string email, string callbackUrl)
            => await SendEmailAsync(email, "Confirm your changed email address", "ConfirmEmailChange", new CallbackEmailViewModel(GetSiteBaseUrl(), callbackUrl, GetSalutationFromEmail(email)), true);

        public async Task PasswordResetAsync(string email, string callbackUrl)
            => await SendEmailAsync(email, "Reset Password", "PasswordReset", new PasswordResetViewModel(GetSiteBaseUrl(), callbackUrl, GetSalutationFromEmail(email)), true);

        private async Task SendEmailAsync<T>(string recipientEmail, string subject, string view, T model, bool isAccountEmail = false)
        {
            //Use cases:
            //(Prod) Send as normal
            //(Test) Send to registered users only, BCC all to dedicated address
            //(Dev) BCC all to dedicated address
            //(Dev) Send all to folder

            bool suppressTo;
            if (_notificationsToRegisteredUsersOnly && !isAccountEmail)
            {
                var user = await _userManager.FindByEmailAsync(recipientEmail);
                suppressTo = user == null || !await _userManager.IsEmailConfirmedAsync(user);
            }
            else
                suppressTo = false;

            NameAndEmail to = suppressTo
                ? null
                : new NameAndEmail(recipientEmail, recipientEmail);
            NameAndEmail bcc = _BccTo is null
                ? null
                : new NameAndEmail(_BccTo, recipientEmail); // Use the original recipient's email as the name so it's obvious who this was meant for.

            if (to is null && bcc is null)
            {
                //_logger.Warn("Notification will be ignored - no recipients configured.");
                return; // Nothing to do.
            }

            // Not legal to send just to bcc without a to address:
            if (to is null)
            {
                to = bcc;
                bcc = null;
            }

            var email = new Email
            {
                From = new NameAndEmail(_fromAddress, _fromName),
                To = to,
                Bcc = bcc,
                Subject = _environment == Environments.Production ? subject : $"[{_environment}] {subject}", // Prepend environment if not prod
                HtmlBody = await _razorViewToStringRenderer.RenderViewToStringAsync($"/Views/Emails/{view}/{view}.cshtml", model),
            };

            if (_sendToFolder != null)
                SaveToFolder(email);
            else
                await _emailService.SendEmailAsync(email);
        }

        public async Task NotifyEditorsOfStatusChange(int editId, EditAction action, string editByUserEmail, IEnumerable<EditorInfo> editors, string additionalInformation = null)
        {
            Debug.WriteLine($"TODO: Notify all editors:\n{string.Join("\n", editors)}");
            foreach (var editor in editors.Where(e => e.Email != null)) // May want to exclude the user themselves. We're forced to exclude editors with no known email address.
            {
                string editorReason = editor
                    .Reasons
                    .Select(reason =>
                    reason.EditorForClubOrAssociation != null
                    ? $"registered with SAFA as a site guide editor for {reason.EditorForClubOrAssociation}, who are responsible for the {reason.ResponsibleForType.ToString().ToLower()} \"{reason.ResponsibleForName}\""
                    : $"listed as responsible for the {reason.ResponsibleForType.ToString().ToLower()} \"{reason.ResponsibleForName}\"")
                    .Distinct()
                    .Humanize();
                var model = new NotifyEditorOfStatusChangeViewModel(GetSiteBaseUrl(), action, editId, editor.Name ?? GetSalutationFromEmail(editor.Email), editorReason, editByUserEmail, additionalInformation);

                await SendEmailAsync(editor.Email, $"A site guide edit has been {model.ActionDescriptionPastParticiple}.", "NotifyEditorOfStatusChange", model);
            }
        }

        private static string GetSalutationFromEmail(string email) => email.Split('@').First();

        private void SaveToFolder(Email email)
        {
            if (!Directory.Exists(_sendToFolder))
                Directory.CreateDirectory(_sendToFolder);
            string path = Path.Combine(_sendToFolder, MakeValidFileName($"{DateTime.Now:yyyy-MM-dd HH-mm-ss} '{email.Subject}' {email.To}.txt"));
            using var file = File.CreateText(UniqueFileName(path));
            file.WriteLine($"From: {email.From}");
            file.WriteLine($"To: {email.To}");
            file.WriteLine($"Subject: {email.Subject}");
            file.WriteLine();
            file.WriteLine(email.HtmlBody);
            file.Close();
        }

        private static string MakeValidFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(Path.GetInvalidFileNameChars()));
            string invalidRegStr = $@"([{invalidChars}]*\.+$)|([{invalidChars}]+)";

            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
        }

        private static string UniqueFileName(string path)
        {
            string result = path;
            int tries = 1;
            while (File.Exists(result))
            {
                result = $"{Path.Combine(Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path))}({tries++}){Path.GetExtension(path)}";
            }
            return result;
        }
    }

    public class NotificationOptions
    {
        public string SendToFolder { get; set; }
        public string BccTo { get; set; }
        /// <summary>
        ///  If true, send non-account notifications to registered users only. Account emails such as password reset or account signup emails are no affected.
        /// </summary>
        public bool NotificationsToRegisteredUsersOnly { get; set; } = false;
        public string FromAddress { get; set; }
        public string FromName { get; set; }
        public string Environment { get; set; }
    }
}
