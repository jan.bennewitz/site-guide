﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SiteGuide.Web.Services;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SiteGuide.Service.Extensions;
using SiteGuide.DAL;
using SiteGuide.Service;
using Microsoft.Extensions.Hosting;
using Humanizer;
using System.Timers;
using Microsoft.AspNetCore.Identity;
using SiteGuide.Service.Services;
using Siteguide.Service.Helpers;

namespace SiteGuide.Web.Services
{

    public class BackgroundTasksService : IHostedService, IDisposable
    {
        private readonly IConfiguration _config;
        private readonly ILogger<BackgroundTasksService> _logger;
        private readonly IServiceProvider _services;

        public List<BackgroundTaskTimer> BackgroundTaskTimers { get; set; }

        public BackgroundTasksService(IConfiguration config, ILogger<BackgroundTasksService> logger, IServiceProvider services)
        {
            _config = config;
            _logger = logger;
            _services = services;

            BackgroundTaskTimers = new List<BackgroundTaskTimer>();
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Background Tasks Service is starting.");

            BackgroundTaskTimers.AddRange(
                (new[] {
                    (t: ExtCacheEnum.Ctaf, s: "Ctaf"),
                    (t: ExtCacheEnum.Club, s: "Club"),
                    (t: ExtCacheEnum.ErsaEffectiveDate, s: "Ersa"),
                    (t: ExtCacheEnum.Editor, s: "Editor"),
                })
                .Select(extCache =>
                    new BackgroundTaskTimer<ExternalCache>(
                        "External Cache: " + extCache.t.ToString().Humanize(LetterCasing.Title),
                        _services,
                        _config.GetSection($"ExternalDataCache:{extCache.s}").Get<BackgroundTaskTimerSettings>(),
                        _logger,
                        async (c) =>
                        {
                            bool wasUpdated = await c.UpdateDataAsync(extCache.t);
                            if (wasUpdated && extCache.t == ExtCacheEnum.Editor)
                                await RebuildCacheUserClaimableEditsAsync();
                        }))
            );

            BackgroundTaskTimerSettings backupSettings = _config.GetSection("Backup").Get<BackgroundTaskTimerSettings>();
            if (backupSettings.IsEnabled)
                BackgroundTaskTimers.Add(new BackgroundTaskTimer<IBackupService>(
                    "Database Backup",
                    _services,
                    backupSettings,
                    _logger,
                    async (s) => await s.BackupAsync()
                ));

            // Add more tasks here as needed.
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Background Tasks Service is stopping.");

            foreach (var timer in BackgroundTaskTimers)
                timer?.Stop();
        }

        public async Task RebuildCacheUserClaimableEditsAsync(int? editId = null)
        {
            using var performanceLogger = new PerformanceLogger(nameof(RebuildCacheUserClaimableEditsAsync), editId.ToString(), 3, _logger);
            using var scope = _services.CreateScope();
            using var context = scope.ServiceProvider.GetRequiredService<SiteGuideContext>();
            var editService = scope.ServiceProvider.GetRequiredService<EditService>();

            var userClaimableEdits = await editService.GetUserClaimableEditsInfoAsync(editId);

            if (editId is null)
                context.Database.ExecuteSqlRaw($"TRUNCATE TABLE \"{nameof(context.CacheUserClaimableEditsDetail)}\"");
            else
                context.RemoveRange(context.CacheUserClaimableEditsDetail.Where(i => i.ClaimableEditId == editId));

            context.AddRange(userClaimableEdits);
            await context.SaveChangesAsync();

            var grouped = context.CacheUserClaimableEditsDetail.GroupBy(i => i.UserId)
                .Select(g => new CacheUserClaimableEdits
                {
                    UserId = g.Key,
                    ClaimableEditsCount = g.Count(),
                    DebugInfo = string.Join(",", g.Select(g => g.ClaimableEditId)),
                });

            context.Database.ExecuteSqlRaw($"TRUNCATE TABLE \"{nameof(context.CacheUserClaimableEdits)}\"");
            await context.CacheUserClaimableEdits.AddRangeAsync(grouped);
            context.SaveChanges();

            // TODO: Confirm this completes if called synchronously
        }

        public void Dispose()
        {
            foreach (var timer in BackgroundTaskTimers)
                timer?.Dispose();
        }
    }
}