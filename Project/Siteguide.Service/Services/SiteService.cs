﻿using Humanizer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using SiteGuide.Service.Identifiers;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service
{
    public class SiteService
    {
        private readonly SiteGuideContext _context;
        //private readonly UserManager<IdentityUser> _userManager;

        public SiteService(SiteGuideContext context/*, UserManager<IdentityUser> userManager*/)
        {
            _context = context;
            //_userManager = userManager;
        }

        public async Task<List<Site>> FindSiteByNameAsync(string id, VersionTreeNodeIdentifier versionTreeNodeIdentifier = null)
        {
            var matches = _context.Site
                .AsNoTracking()
                //.Include(s => s.Area)
                .Include(s => s.InsetMapPosition) // Do we need this?
                .Where(s => s.Name.ToUpper().Contains(id.ToUpper()))
                .ValidAtVersionTreeNode(versionTreeNodeIdentifier)
                .ToList();
            return matches;
        }
    }
}
