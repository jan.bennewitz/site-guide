﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Npgsql;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service.Services
{
    public interface IBackupService
    {
        Task BackupAsync();
    }

    public class PostgresqlBackupService : IBackupService
    {
        // Modified from https://stackoverflow.com/questions/23026949/how-to-backup-restore-postgresql-using-code
        readonly string Set = RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? "set " : "export ";
        private readonly IConfiguration _config;
        private readonly IOnlineFileStorageService _onlineFileStorageService;

        public PostgresqlBackupService(IConfiguration config, IOnlineFileStorageService onlineFileStorageService)
        {
            _config = config;
            _onlineFileStorageService = onlineFileStorageService;
        }

        public async Task BackupAsync()
        {
            var localFileName = Path.GetTempFileName();
            var connStringBuilder = new NpgsqlConnectionStringBuilder(_config.GetConnectionString("SiteGuide"));
            await PostgreSqlDump(localFileName, connStringBuilder.Host, connStringBuilder.Port, connStringBuilder.Database, connStringBuilder.Username, connStringBuilder.Password);
            string remoteFileName = $"/Backup/{connStringBuilder.Host}_{connStringBuilder.Database}_{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}_{DateTime.UtcNow:yyyyMMdd-HHmmss}.bak";

            await _onlineFileStorageService.UploadAsync(localFileName, remoteFileName);
        }

        public async Task PostgreSqlDump(
            string outFile,
            string host,
            int port,
            string database,
            string user,
            string password)
        {
            string dumpCommand =
                 $"{Set}PGPASSWORD={password}\n" +
                 $"pg_dump" + " -Fc" + " -h " + host + " -p " + port + " -d " + database + " -U " + user + "";

            string batchContent = "" + dumpCommand + "  > " + "\"" + outFile + "\"" + "\n";
            if (File.Exists(outFile))
                File.Delete(outFile);

            await Execute(batchContent);
        }

        public async Task PostgreSqlRestore(
           string inputFile,
           string host,
           string port,
           string database,
           string user,
           string password)
        {
            string dumpCommand = $"{Set}PGPASSWORD={password}\n" +
                                 $"psql -h {host} -p {port} -U {user} -d {database} -c \"select pg_terminate_backend(pid) from pg_stat_activity where datname = '{database}'\"\n" +
                                 $"dropdb -h " + host + " -p " + port + " -U " + user + $" {database}\n" +
                                 $"createdb -h " + host + " -p " + port + " -U " + user + $" {database}\n" +
                                 "pg_restore -h " + host + " -p " + port + " -d " + database + " -U " + user + "";

            //psql command disconnect database
            //dropdb and createdb  remove database and create.
            //pg_restore restore database with file created with pg_dump command
            dumpCommand = $"{dumpCommand} {inputFile}";

            await Execute(dumpCommand);
        }

        private static Task Execute(string dumpCommand)
        {
            return Task.Run(() =>
            {

                string batFilePath = Path.Combine(Path.GetTempPath(), $"{Guid.NewGuid()}." + (RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? "bat" : "sh"));
                try
                {
                    string batchContent = "";
                    batchContent += $"{dumpCommand}";

                    File.WriteAllText(batFilePath, batchContent, Encoding.ASCII);

                    ProcessStartInfo info = ProcessInfoByOS(batFilePath);
                    info.RedirectStandardError = true;

                    using System.Diagnostics.Process proc = System.Diagnostics.Process.Start(info);


                    proc.WaitForExit();
                    var exitCode = proc.ExitCode;
                    string stderr = proc.StandardError.ReadToEnd();
                    if (exitCode != 0)
                        throw new Exception(stderr);

                    proc.Close();
                }
                finally
                {
                    File.Delete(batFilePath); // https://learn.microsoft.com/en-us/dotnet/api/system.io.file.delete?view=net-7.0#remarks "If the file to be deleted does not exist, no exception is thrown."
                }
            });
        }

        private static ProcessStartInfo ProcessInfoByOS(string batFilePath)
        {
            ProcessStartInfo info;
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                info = new ProcessStartInfo(batFilePath)
                {
                };
            }
            else
            {
                info = new ProcessStartInfo("sh")
                {
                    Arguments = $"{batFilePath}"
                };
            }

            info.CreateNoWindow = true;
            info.UseShellExecute = false;
            info.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory;
            info.RedirectStandardError = true;

            return info;
        }
    }
}
