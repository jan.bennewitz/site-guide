﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SiteGuide.Service;
using SiteGuide.Service.Services;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using SiteGuide.Service.Identifiers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiteGuide.Service.Helpers;

namespace SiteGuide.Service
{
    public class EditPermissionsService
    {
        private readonly SiteGuideContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly EditorResolver _editorResolver;

        public EditPermissionsService(SiteGuideContext context, UserManager<IdentityUser> userManager, EditorResolver editorResolver)
        {
            _context = context;
            _userManager = userManager;
            _editorResolver = editorResolver;
        }

        public async Task<FullAndPartialEditors> GetEditorsForEditVersionAsync(EditVersion editVersion)
        {
            // TODO: The logic in this class won't work for A or R edits which are against an older version.
            //int baseVersion;
            //if (new List<string> { "D", "S" }.Contains(editVersion.StatusId))
            //    baseVersion = _context.Version.Max(v => v.Id);
            //else {
            //    var editId = _context.EditVersion
            //        .Where(ev => ev.StatusId == "A" && ev.SubmittedTime <= DateTime.SpecifyKind((DateTime)editVersion.SubmittedTime, DateTimeKind.Utc))
            //        .OrderByDescending(ev => ev.SubmittedTime)
            //        .First()
            //        .EditId;
            //    baseVersion = _context.Version.Single(v => v.EditId == editId).Id;
            //}
            // Check if there are in fact any changes.
            if (!HasChanges(editVersion))
                return new FullAndPartialEditors
                {
                    Editors = new List<EditorInfo>(),
                    PartialEditors = new List<EditorInfo>(),
                    HasChanges = false,
                };

            /*
             * Versioned records:
            site			Has responsible
            launch			=> parent Site
            seealso			=> parent Site
            sitecontact		=> parent Site
            sitemapshape	=> parent Site
            video			=> parent Site
            area			Has admin, and parent area may have its own admin etc
            contact			=> Self, => Site via SiteContact ** To edit a contact, user only needs to be editor for at least one associated site, not all. Seems a safe assumption that this is safe. **
            mapshape		=> parent Area, => Site via SiteMapshape
            
            Changes to tables affect:

            Site
            Launch, SeeAlso, Video: Only parent site
            Contact: Affects multiple sites
            Mapshape: Affects multiple sites

            Edit by editor: Can change all data associated with site
            Edit by non-editor: Edit can only affect one site and associated data

            Can an edit affect more than one site (Editor only)? Maybe not for starters.

            Separate edit pages for contacts, mapshapes? Some may not be associated with a site at all, so probably required for mapshapes. Contacts are always reachable from sites and not relevant when not associated with a site so probably don't need a separate edit page.
            Non-editors would be able to edit mapshapes via the MapShape editor.

            Mapshape edit permissions: 
            - A mapshape can be edited by the editors for any associated sites and the editors of the mapshape's area, and both their parent areas.
            SiteMapshape edit permissions:
            - A mapshape can be added to or removed from a site by an editor of that site.

            Contact edit permissions:
            - A contact can be edited by anyone who can edit an associated site. 
            SiteContact edit permissions:
            - Any contacts can be added to or removed from any site by an editor of that site.

            Edit Areas? Later.
             */
            //var currentUser = await _userManager.GetUserAsync(HttpContext.User);
            //var currentUserSafaId = (await _userManager.GetLoginsAsync(currentUser)) 
            //    .SingleOrDefault(l => l.LoginProvider == "SAFA")
            //    ?.ProviderKey;

            // Make sure we take into account both 'after' state and 'before' state as appropriate
            // TODO: Review all this logic for deletes

            // *** Site, Launch, SeeAlso, Video, SiteContact, SiteMapshape: 
            // Need edit permission for ALL of these sites
            var siteBasedEditors = await GetSiteBasedEditorsAsync(editVersion);
            var contactsEditors = await GetContactsEditorsAsync(editVersion);
            var mapshapeEditors = await GetMapshapeEditorsAsync(editVersion);
            var areaEditors = await GetAreaEditorsAsync(editVersion);

            //throw new ValidationException("Edit has no changes.");

            var editorsIntersectAll = siteBasedEditors;
            editorsIntersectAll.AddRange(contactsEditors);
            editorsIntersectAll.AddRange(mapshapeEditors);
            editorsIntersectAll.AddRange(areaEditors);

            // TODO: Add note to edit page if user has no associated SAFA login saying we won't be able to match them.
            // TODO: Can we match on email too?

            if (!editorsIntersectAll.Any())
                throw new Exception($"{nameof(editorsIntersectAll)} is empty - this probably indicates an error in the logic.");

            var editors = EditorResolver.EditorsIntersect(editorsIntersectAll);
            var partialEditors = EditorResolver.EditorsUnion(editorsIntersectAll).Except(editors);

            return new FullAndPartialEditors
            {
                Editors = editors,
                PartialEditors = partialEditors,
                HasChanges = true,
            };
        }

        private bool HasChanges(EditVersion editVersion)
            => _context.RecordTables
                .Aggregate(false, (hasChangeSoFar, table) => hasChangeSoFar || table.WhereEditVersion(editVersion.Id).Any());

        private async Task<List<IEnumerable<EditorInfo>>> GetSiteBasedEditorsAsync(EditVersion editVersion)
        {
            var siteBasedEditors = new List<IEnumerable<EditorInfo>>();
            var siteIds = GetSiteIds(_context.Site, editVersion)
                .Union(GetOldAndNewParentSiteIds(_context.Launch, editVersion))
                .Union(GetOldAndNewParentSiteIds(_context.SeeAlso, editVersion))
                .Union(GetOldAndNewParentSiteIds(_context.SiteContact, editVersion))
                .Union(GetOldAndNewParentSiteIds(_context.SiteMapshape, editVersion))
                .Union(GetOldAndNewParentSiteIds(_context.Video, editVersion));
            // A user can edit a site exactly if:
            // - Local user account listed as editor for the site
            // - SAFA user is editor for the responsible club, SRA OR SAFA
            foreach (int siteId in siteIds) // Need edit permission for ALL of these sites.
            {
                IList<EditorInfo> thisSiteEditors = await _editorResolver.GetSiteEditorsAsync(siteId, editVersion);
                EditorResolver.EditorsSetClosest(thisSiteEditors);
                siteBasedEditors.Add(thisSiteEditors);
            }

            return siteBasedEditors;
        }

        private async Task<List<IEnumerable<EditorInfo>>> GetContactsEditorsAsync(EditVersion editVersion)
        {
            // Check permissions for changes to Contact. We have already checked permissions to siteContact above.
            // For each contact, get the current associated site and the contact id. Ignore the new associated site - this is taken care of above already (SiteContact changes). Editors are (user is contact) or (user can edit associated site).
            // This could probably be rewritten to be nicer using a join
            var editedContactsInfo = _context.Contact
                .WhereEditVersion(editVersion.Id) // The contact id can't be changed in an edit, so we don't need to get the current version.
                .Select(c => new
                {
                    ContactId = c.Id,
                    OldContactUserId = _context.Contact
                        .ValidAtCurrentVersion()
                        .Where(oc => oc.Id == c.Id)
                        .Select(oc => oc.UserId)
                        .SingleOrDefault(), // Will be null for a new mapshape
                    NewContactUserId = (bool)c.IsDelete ? null : c.UserId,
                    SiteIds = _context.SiteContact
                        .ValidAtCurrentVersion()
                        .Where(sc => sc.ContactId == c.Id)
                        .Select(sc => sc.SiteId)
                        .ToList()
                })
                .ToList();

            // For each contact: Need edit permission for AT LEAST ONE site OR for the user to be that contact.
            var contactsEditors = new List<IEnumerable<EditorInfo>>();
            IList<EditorInfo> rootAreaEditors = null;
            foreach (var editedContactInfo in editedContactsInfo)
            {
                var contactEditors = new List<EditorInfo>();

                // For contacts not associated with a site, use the newly associated site. 
                if (!editedContactInfo.SiteIds.Any())
                    editedContactInfo.SiteIds.AddRange(
                        _context.SiteContact
                            .Where(sc =>
                                sc.EditVersionId == editVersion.Id
                                && sc.ContactId == editedContactInfo.ContactId)
                            .Select(sc => sc.SiteId)
                            .ToList()
                        );

                foreach (var siteId in editedContactInfo.SiteIds)
                    contactEditors.AddRange(await _editorResolver.GetSiteEditorsAsync(siteId, editVersion));

                // For contacts not associated with an old or new site, fall back to the root area admin.
                if (!contactEditors.Any())
                {
                    if (rootAreaEditors is null) // Retrieve root area editor info
                    {
                        int rootAreaId = _context.Area
                            .ValidAtCurrentVersion()
                            .Single(a => a.ParentAreaId == null)
                            .Id;
                        var rootAreaResponsibles = _editorResolver.GetAreaResponsibles(rootAreaId);
                        rootAreaEditors = await _editorResolver.GetEditorsFromResponsibles(rootAreaResponsibles);
                    }
                    contactEditors.AddRange(rootAreaEditors);
                }

                /* If the changed contact is associated with a system user, add them as an editor, but not if the associated system user has changed.
                    Old		New		Editor
                    null	null	-
                    A		null	A
                    null	A		-
                    A		A		A
                    A		B		-
                */
                if (editedContactInfo.OldContactUserId != null && (editedContactInfo.OldContactUserId == editedContactInfo.NewContactUserId || editedContactInfo.NewContactUserId == null))
                {
                    contactEditors.Add(await _editorResolver.GetContactUser(editedContactInfo.OldContactUserId));
                }


                EditorResolver.EditorsSetClosest(contactEditors); // TODO - move this elsewhere, maybe later in the process?
                contactsEditors.Add(contactEditors);
            }

            return contactsEditors;
        }

        private async Task<IList<IEnumerable<EditorInfo>>> GetMapshapeEditorsAsync(EditVersion editVersion)
        {
            // *** Mapshape
            // - A mapshape can be edited by the editors for any associated sites and the editors of the mapshape's area, and both their parent areas.
            // - A mapshape can also be added to an area by an editor of any site in that area.
            // - A mapshape can be added to or removed from a site by an editor of that site - this is a change to sitemapshape and is handled above.
            var result = new List<IEnumerable<EditorInfo>>();
            var editedMapshapesWithAssociatedSites = _context.MapShape
                .WhereEditVersion(editVersion.Id)
                .Select(m => new
                {
                    MapshapeId = m.Id,
                    OldMapshapeAreaId = _context.MapShape
                        .ValidAtCurrentVersion()
                        .Where(om => om.Id == m.Id)
                        .Select(om => (int?)om.AreaId)
                        .SingleOrDefault(), // Will be null for a new mapshape
                    NewMapshapeAreaId = (bool)m.IsDelete ? (int?)null : m.AreaId,
                    SiteIds = _context.SiteMapshape
                        .ValidAtCurrentVersion()
                        .Where(sm => sm.MapshapeId == m.Id)
                        .Select(sm => sm.SiteId)
                        .ToList(),
                })
                .ToList();

            // mapshapeChangesSiteIds - Need edit permission for (AT LEAST ONE OF (site OR the area)) for each mapshape
            var editVersionIdentifier = new EditVersionIdentifier(editVersion, _context);
            foreach (var editedMapshapeWithAssociatedSites in editedMapshapesWithAssociatedSites)
            {
                var thisMapshapeEditorsUnionAll = new List<IEnumerable<EditorInfo>>();
                foreach (var siteId in editedMapshapeWithAssociatedSites.SiteIds)
                    thisMapshapeEditorsUnionAll.Add(
                        await _editorResolver.GetSiteEditorsAsync(siteId, editVersion));
                // Newly added mapshape? A mapshape can be added to an area by an editor of any site in that area.
                if (editedMapshapeWithAssociatedSites.OldMapshapeAreaId == null && editedMapshapeWithAssociatedSites.NewMapshapeAreaId != null)
                {
                    var areaSiteIds = _context.Site
                        .ValidAtCurrentVersion()
                        .Where(s => s.AreaId == editedMapshapeWithAssociatedSites.NewMapshapeAreaId)
                        .Select(s => s.Id)
                        .ToList();
                    foreach (int siteId in areaSiteIds)
                    {
                        IList<EditorInfo> thisSiteEditors = await _editorResolver.GetSiteEditorsAsync(siteId, editVersion);
                        thisMapshapeEditorsUnionAll.Add(thisSiteEditors);
                    }
                }
                var oldAndNewAreaIds = new int?[] { editedMapshapeWithAssociatedSites.OldMapshapeAreaId, editedMapshapeWithAssociatedSites.NewMapshapeAreaId }
                    .Where(i => i != null)
                    .Distinct();
                foreach (var areaId in oldAndNewAreaIds) // Generally one area, but could be two if mapshape has been assigned to new area.
                    thisMapshapeEditorsUnionAll.Add(await _editorResolver.GetAreaEditorsAsync((int)areaId));

                var thisMapshapeEditors = EditorResolver.EditorsUnion(thisMapshapeEditorsUnionAll);
                EditorResolver.EditorsSetClosest(thisMapshapeEditors);

                result.Add(thisMapshapeEditors);
            }

            return result;
        }

        private async Task<IList<IEnumerable<EditorInfo>>> GetAreaEditorsAsync(EditVersion editVersion)
        {
            var result = new List<IEnumerable<EditorInfo>>();
            var editedAreaIds = _context.Area
                .WhereEditVersion(editVersion.Id)
                .Select(a => a.Id)
                .ToList();
            foreach (var editedAreaId in editedAreaIds)
            {
                var editors = await _editorResolver.GetAreaEditorsAsync(editedAreaId, 0, editVersion);
                result.Add(editors);
            }
            return result;
        }

        /// <summary>
        ///  Site ids affected by the specified change. We can just look at the change records as this accounts for additions, deletions and edits. Id is the primary key so we don't need to worry about id changes.
        /// </summary>
        private static IList<int> GetSiteIds(DbSet<Site> sitesDbSet, EditVersion editVersion)
        => sitesDbSet.WhereEditVersion(editVersion.Id)
            .Select(s => s.Id)
            .Distinct()
            .ToList();

        /// <summary>
        ///  Parent site ids affected by the specified change - both from the old/current data and the newly changed data, since the change may alter the site id.
        /// </summary>
        /// // TODO: This will need to be an outer join to deal with additions
        private static List<int> GetOldAndNewParentSiteIds<T>(DbSet<T> dbSet, EditVersion editVersion) where T : class, ISiteChild, IRecord
        => dbSet.WhereEditVersion(editVersion.Id)
            .ToList() // TODO: This shouldn't be necessary?
            .LeftOuterJoin(dbSet.ValidAtCurrentVersion(), n => n.SiteId, o => o.SiteId, (n, o) => new { n.IsDelete, OldId = o?.SiteId, NewId = n.SiteId })
            .ToList()
            .SelectMany(i => new int?[] { i.OldId, (bool)i.IsDelete ? null : i.NewId })
            .Where(i => i != null)
            .Distinct()
            .Select(i => (int)i)
            .ToList();

        public async Task<UserPrivsForEdit> GetUserPrivilegesForEdit(IdentityUser user, List<EditVersion> editVersions, EditVersion editVersion, IList<EditorInfo> editors)
        {
            var userSafaId = (await _userManager.GetLoginsAsync(user))
                .SingleOrDefault(l => l.LoginProvider == "SAFA")
                ?.ProviderKey;

            // TODO: I think we can just check on userId and simplify things from there.
            var roles = await _userManager.GetRolesAsync(user);
            bool isSysAdmin = roles.Contains(Permissions.AdminRoleName);

            bool isOriginalAuthor = editVersions.First().AuthorId == user.Id;
            bool isAuthor = editVersion.AuthorId == user.Id;
            var editorsWhoAreCurrentUser = editors == null
                ? new List<EditorInfo>()
                : editors.Where(e =>
                    e.UserId == user.Id
                    || (e.Email == user.Email && user.EmailConfirmed)
                    || e.SafaId == userSafaId)
                .ToList();
            bool isDirectAdmin = editorsWhoAreCurrentUser.Any(e => e.IsClosest);
            bool isIndirectAdmin = editorsWhoAreCurrentUser.Any() && !isDirectAdmin;
            bool isReviewer = isDirectAdmin || isIndirectAdmin || isSysAdmin;

            var userPrivileges = new UserPrivsForEdit
            {
                IsDirectAdmin = isDirectAdmin,
                IsIndirectAdmin = isIndirectAdmin,
                IsSystemAdmin = isSysAdmin,
                IsDraftOwner = editVersion.StatusId == "D" && isAuthor,

                CanView = isSysAdmin || isAuthor || isReviewer || isOriginalAuthor,
                //CanViewDetails = editVersion.StatusId switch
                //{
                //    "D" => isAuthor,
                //    "S" => isAuthor || isUserSysAdmin || isReviewer || isOriginalAuthor,
                //    "A" or "R" => isUserSysAdmin || isReviewer || isOriginalAuthor /*?  TODO*/,

                //    _ => throw new NotImplementedException($"Unhandled {nameof(EditVersionStatus)} id \"{ editVersion.StatusId}\"."),
                //},

                EditorsWhoAreCurrentUser = editorsWhoAreCurrentUser,
            };
            return userPrivileges;
        }
    }

    public class FullAndPartialEditors
    {
        public IList<EditorInfo> Editors { get; set; }
        public IEnumerable<EditorInfo> PartialEditors { get; set; }
        public bool HasChanges { get; set; }
    }
}


public class UserPrivsForEdit
{
    public bool IsDirectAdmin { get; set; }
    public bool IsIndirectAdmin { get; set; }
    public bool IsSystemAdmin { get; set; }
    public bool CanView { get; set; }
    public bool IsDraftOwner { get; set; }
    public IList<EditorInfo> EditorsWhoAreCurrentUser { get; set; }

    public bool HasEditActionRole(EditActionRole r)
        => r switch
        {
            EditActionRole.DirectAdmin => IsDirectAdmin,
            EditActionRole.IndirectAdmin => IsIndirectAdmin,
            EditActionRole.SystemAdmin => IsSystemAdmin,
            _ => throw new NotImplementedException($"Unhandled {nameof(EditActionRole)} \"{r}\"."),
        };
}

public class Responsible
{
    public Contact Contact { get; set; }
    public int ResponsibleForId { get; set; }
    public string ResponsibleForName { get; set; }
    public ResponsibleForType ResponsibleForType { get; set; }
    public int Distance { get; set; }

    public override string ToString() => $"{Contact} responsible for {ResponsibleForType} {ResponsibleForId} \"{ResponsibleForName}\"";
}
