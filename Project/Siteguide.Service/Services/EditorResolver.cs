﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using SiteGuide.Service.Identifiers;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service
{
    public class EditorResolver
    {
        private readonly SiteGuideContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        private readonly Dictionary<int, IList<EditorInfo>> _editorsBySiteCache = new();
        private readonly Dictionary<int, IList<Responsible>> _responsiblesByAreaCache = new();

        public EditorResolver(SiteGuideContext context, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<EditorInfo> GetContactUser(string contactUserId)
        {
            var contactUser = await _userManager.FindByIdAsync(contactUserId);
            string contactUserName = contactUser.Email.Split('@').First();
            var contactEditor = new EditorInfo
            {
                Name = contactUserName,
                Email = contactUser.Email,
                SafaId = "TODO: We can look this up if required.", // TODO?
                Reasons = new List<EditorReason> {
                    new EditorReason {
                        ResponsibleForId = contactUser.Id,
                        ResponsibleForName = contactUserName,
                        ResponsibleForType = ResponsibleForType.Contact,
                        IsClosest = true,
                        Distance = 0,
                        EditorForClubOrAssociation = null,
                    }
                },
            };
            return contactEditor;
        }

        public async Task<IList<EditorInfo>> GetSiteEditorsAsync(int siteId, EditVersion editVersion)
        {
            if (!_editorsBySiteCache.TryGetValue(siteId, out var editors))
            {
                var responsibles = await GetSiteResponsiblesAsync(siteId, editVersion, _responsiblesByAreaCache);
                // We now have a list of responsible contacts. Resolve into editors: 
                editors = await GetEditorsFromResponsibles(responsibles);
                _editorsBySiteCache.Add(siteId, editors);
            }
            return editors;
        }

        private async Task<IList<Responsible>> GetSiteResponsiblesAsync(int siteId, EditVersion editVersion, Dictionary<int, IList<Responsible>> _cacheResponsiblesByArea)
        {
            // TODO: If we are changing the site area, we should also probably not make the old area responsible, but instead the closest common parent area between the old and the new area?
            List<Responsible> siteResponsibles;
            int areaId;

            var site = _context.Site
                .ValidAtCurrentVersion()
                .SingleOrDefault(s => s.Id == siteId)
                ?? _context.Site // The site may have been deleted, e.g. /Edit/1 , "The Escarpment". In that case, use the last existing version.
                .Where(s => s.Id == siteId)
                    .ValidAtAnyVersion()
                    .OrderByDescending(s => s.ValidToVersion)
                    .FirstOrDefault();

            if (site != null)
            {
                // Responsible users
                siteResponsibles = await _context.Contact
                    .ValidAtCurrentVersion()
                    .Join(
                        _context.SiteContact
                        .ValidAtCurrentVersion()
                        .Where(sc =>
                            sc.SiteId == siteId
                            && sc.IsResponsible
                        ),
                        c => c.Id,
                        sc => sc.ContactId,
                        (c, sc) => new { c, sc }
                    ).Select(r => new Responsible()
                    {
                        Contact = r.c,
                        ResponsibleForId = site.Id,
                        ResponsibleForName = site.Name,
                        ResponsibleForType = ResponsibleForType.Site,
                        Distance = 0,
                    })
                    .ToListAsync();

                areaId = site.AreaId;
            }
            else
            {
                // This is a brand new site
                siteResponsibles = new();
                areaId = _context.Site
                    .Single(s =>
                        s.Id == siteId
                        && s.EditVersionId == editVersion.Id)
                    .AreaId; // TODO - Maybe we should treat a new site as an edit to an area instead?
            }

            // Add responsibles for all parent areas
            var areaResponsibles = GetAreaResponsibles(areaId);
            return siteResponsibles.Union(areaResponsibles).ToList();
        }

        /// <param name="distance">Distance from site. 0 for the site itself, 1 for the immediate parent area, 2 for the parent area's parent area etc.</param>
        public IList<Responsible> GetAreaResponsibles(int areaId, int distance = 1, EditVersion editVersion = null)
        {
            if (!_responsiblesByAreaCache.TryGetValue(areaId, out var result))
            {
                var currentArea = _context.Area
                    .ValidAtCurrentVersion()
                    .SingleOrDefault(a => a.Id == areaId);
                var newArea = editVersion == null
                    ? null
                    : _context.Area
                        .ValidAtUnacceptedEditVersion(new EditVersionIdentifier(editVersion, _context))
                        .SingleOrDefault(a => a.Id == areaId);
                // Parent area responsibles:
                int? parentAreaId = (currentArea ?? newArea).ParentAreaId;
                if (parentAreaId != null)
                    result = GetAreaResponsibles((int)parentAreaId, distance + 1);
                else
                    result = new List<Responsible>();

                //This area's responsibles:
                Contact adminContact = currentArea == null
                    ? null
                    : _context.Contact
                        .ValidAtCurrentVersion()
                        .SingleOrDefault(c => c.Id == currentArea.AdminId);

                if (adminContact != null)
                    result = result.Prepend(new Responsible()
                    {
                        Contact = adminContact,
                        ResponsibleForId = currentArea.Id,
                        ResponsibleForName = currentArea.Name,
                        ResponsibleForType = ResponsibleForType.Area,
                        Distance = distance,
                    })
                    .ToList();

                _responsiblesByAreaCache.Add(areaId, result);
            }
            return result;
        }

        public async Task<IEnumerable<EditorInfo>> GetAreaEditorsAsync(int areaId, int distance = 1, EditVersion editVersion = null)
        {
            IEnumerable<Responsible> responsibles = GetAreaResponsibles(areaId, distance, editVersion);
            // We now have a list of responsible contacts. Resolve into editors: 
            IEnumerable<EditorInfo> editors = await GetEditorsFromResponsibles(responsibles);
            return editors;
        }

        public async Task<IList<EditorInfo>> GetEditorsFromResponsibles(IEnumerable<Responsible> responsibles)
        {
            // Some of the contacts are associations, resolve them via SAFA api.
            // Some are local accounts. Some are neither - warn about these.

            //var editorGroups = editors.GroupBy(e => new { hasSafaClubId = e.SafaClubId != null, hasUserId = e.UserId != null })
            //    .ToList();
            var clubResponsibles = responsibles
                .Where(r => r.Contact.SafaClubId != null)
                .ToList();
            var result = new List<EditorInfo>();
            foreach (var clubResponsible in clubResponsibles)
                result.AddRange(await GetClubEditorsAsync(clubResponsible));

            IEnumerable<Responsible> localEditorContacts = responsibles
                .Where(r => r.Contact.UserId != null);

            foreach (var localEditorContact in localEditorContacts)
            {
                var localUser = await _userManager.FindByIdAsync(localEditorContact.Contact.UserId);
                result.Add(new EditorInfo
                {
                    Name = localEditorContact.Contact.Name,
                    UserId = localUser.Id,
                    Email = localUser.Email,
                    SafaId = "TODO: We can look this up if required.", // TODO?
                    Reasons = new List<EditorReason> {
                        new EditorReason {
                            ResponsibleForId = localEditorContact.ResponsibleForId.ToString(),
                            ResponsibleForName = localEditorContact.ResponsibleForName,
                            ResponsibleForType = localEditorContact.ResponsibleForType,
                            IsClosest = true,
                            Distance = localEditorContact.Distance,
                            EditorForClubOrAssociation = null,
                        }
                    },
                });
            }

            var unresolvedResponsibles = responsibles
                .Where(r =>
                    r.Contact.SafaClubId == null
                    && r.Contact.UserId != null)
                .ToList();

            foreach (var unresolvedResponsible in unresolvedResponsibles)
            {
                result.Add(new EditorInfo
                {

                });
            }

            return result;

            /*
                -- Responsible contacts with no associated safa club or user:
                select distinct c.*
                from "Contact" c
                join "SiteContact" sc on sc."ContactId" = c."Id"
                where c."ValidFromVersion" is not null and c."ValidToVersion" is null
                and sc."ValidFromVersion" is not null and sc."ValidToVersion" is null
                and sc."IsResponsible"
                and (c."SafaClubId" is null or not exists (select * from "ExtCacheClub" xc where xc."SafaCode"=c."SafaClubId"))
                and c."UserName" is null
            */
        }

        private async Task<IList<EditorInfo>> GetClubEditorsAsync(Responsible clubResponsible)
        {
            const string MailToPrefix = "mailto:";

            var reasons = new List<EditorReason> { new EditorReason {
                        EditorForClubOrAssociation = clubResponsible.Contact.Name,
                        ResponsibleForId = clubResponsible.ResponsibleForId.ToString(),
                        ResponsibleForName = clubResponsible.ResponsibleForName,
                        ResponsibleForType = clubResponsible.ResponsibleForType,
                        //IsClosest = ,
                        Distance = clubResponsible.Distance,
                    }};

            var safaEditors = _context.ExtCacheEditor
                .Select(e => new { e.Club, e.SafaId, e.Email, e.Name })
                .Union(
                    _context.Editor
                        .Select(e => new { e.Club, e.SafaId, e.Email, e.Name })
                )
                .Where(e => e.Club == clubResponsible.Contact.SafaClubId)
                .ToList();

            var result = new List<EditorInfo>();

            if (safaEditors.Any())
                foreach (var safaEditor in safaEditors)
                {
                    // Match this editor to a local user using safa id and email address (if confirmed).
                    // If we have no matches, leave userId blank. If we have two matches, return two editors.
                    var safaUserId = (await _userManager.FindByLoginAsync("SAFA", safaEditor.SafaId))?.Id;
                    var localUserId = _userManager.Users
                        .SingleOrDefault(u => safaEditor.Email == u.Email && u.EmailConfirmed)
                        ?.Id;

                    var userIds = new[] { safaUserId, localUserId }
                        .Where(i => i != null)
                        .Distinct()
                        .ToList();
                    if (!userIds.Any())
                        userIds.Add(null);

                    var resolvedSafaEditors = userIds.Select(userId => new EditorInfo
                    {
                        Name = safaEditor.Name,
                        UserId = userId,
                        Email = safaEditor.Email,
                        SafaId = safaEditor.SafaId,
                        Reasons = reasons,
                    });

                    result.AddRange(resolvedSafaEditors);
                }
            else
                result.Add(new EditorInfo
                {
                    Name = clubResponsible.Contact.Name,
                    UserId = clubResponsible.Contact.UserId,
                    Email = clubResponsible.Contact.Link?.StartsWith(MailToPrefix) == true
                        ? clubResponsible.Contact.Link.Substring(MailToPrefix.Length)
                        : null,
                    SafaId = clubResponsible.Contact.SafaClubId,
                    Reasons = reasons,
                }); // TODO: Make sure this works the other end too, at the consumer

            return result;
        }

        public static void EditorsSetClosest(IList<EditorInfo> editors)
        {
            if (!editors.Any())
                return;

            var minDistance = editors.Min(e => e.MinReasonDistance);

            foreach (var editor in editors)
            {
                foreach (var reason in editor.Reasons)
                    reason.IsClosest = reason.Distance == minDistance;

                editor.IsClosest = editor.Reasons.Any(r => r.IsClosest);
            }
        }

        public static IEnumerable<EditorInfo> EditorsMergeDistinct(IEnumerable<EditorInfo> editors)
        {
            var grouped = editors.GroupBy(e => new
            {
                e.Name,
                e.Email,
                e.UserId,
                e.SafaId,
            });

            var merged = grouped.Select(g => new EditorInfo
            {
                Name = g.Key.Name,
                Email = g.Key.Email,
                UserId = g.Key.UserId,
                SafaId = g.Key.SafaId,
                Reasons = ReasonsMergeDistinct(g.SelectMany(e => e.Reasons)).ToList(),
                IsClosest = g.Any(e => e.IsClosest),
            });

            return merged;
        }

        public static IEnumerable<EditorReason> ReasonsMergeDistinct(IEnumerable<EditorReason> reasons)
        {
            var merged = reasons.DistinctBy(r => new
            {
                r.EditorForClubOrAssociation,
                r.ResponsibleForId,
                r.ResponsibleForName,
                r.ResponsibleForType,
                r.Distance,
            });

            return merged;
        }


        public static IList<EditorInfo> EditorsUnion(IList<IEnumerable<EditorInfo>> editorLists)
        {
            if (!editorLists.Any())
                return new List<EditorInfo>();

            var editorListsConsolidated = editorLists.Select(ConsolidateEditors);

            var result = editorListsConsolidated.First();

            foreach (var editorList in editorListsConsolidated.Skip(1))
            {
                result = result.FullOuterJoin(editorList, a => a, b => b, (a, b, key) => new EditorInfo
                {
                    Name = a != null ? a.Name : b.Name,
                    Email = a != null ? a.Email : b.Email,
                    UserId = a != null ? a.UserId : b.UserId,
                    SafaId = a != null ? a.SafaId : b.SafaId,
                    Reasons = a == null ? b.Reasons : b == null ? a.Reasons : a.Reasons.Union(b.Reasons).ToList(),
                    //IsClosest = a?.IsClosest == true && b?.IsClosest == true,
                });
            }
            //var result = editorLists // https://stackoverflow.com/questions/1674742/intersection-of-multiple-lists-with-ienumerable-intersect
            //    .Aggregate((previousList, nextList) => previousList.Union(nextList))
            //    .ToList();

            return result.ToList();
        }

        /// <summary>
        /// Merge any duplicate editors in the list.
        /// </summary>
        private static IEnumerable<EditorInfo> ConsolidateEditors(IEnumerable<EditorInfo> editors)
            => editors.ToLookup(i => i)
                .Select(l => new EditorInfo
                {
                    Name = l.Key.Name,
                    Email = l.Key.Email,
                    UserId = l.Key.UserId,
                    SafaId = l.Key.SafaId,
                    Reasons = l.SelectMany(i => i.Reasons).Distinct().ToList(),
                    IsClosest = l.Any(i => i.IsClosest),
                });

        public static List<EditorInfo> EditorsIntersect(IList<IEnumerable<EditorInfo>> editorLists)
        {
            if (!editorLists.Any())
                return new List<EditorInfo>();

            var distinctEditorLists = editorLists.Select(EditorsMergeDistinct);

            var result = distinctEditorLists.First();
            foreach (var editorList in distinctEditorLists.Skip(1))
            {
                result = result.Join(editorList, a => a, b => b, (a, b) => new EditorInfo
                {
                    Name = a.Name,
                    Email = a.Email,
                    UserId = a.UserId,
                    SafaId = a.SafaId,
                    Reasons = a.Reasons.Union(b.Reasons).ToList(),
                    IsClosest = a.IsClosest || b.IsClosest,
                });
            }
            //var result = editorLists // https://stackoverflow.com/questions/1674742/intersection-of-multiple-lists-with-ienumerable-intersect
            //    .Aggregate((previousList, nextList) => previousList.Intersect(nextList))
            //    .ToList();
            return result.ToList();
        }
    }

    public class EditorInfo : IEquatable<EditorInfo>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string SafaId { get; set; }
        public string UserId { get; set; }
        public IList<EditorReason> Reasons { get; set; }
        public bool IsClosest { get; set; }
        public int MinReasonDistance => Reasons.Min(r => r.Distance);

        public override string ToString() => $"{Name} [{(SafaId == null ? "No SAFA id" : $"SAFA id {SafaId}")}] {(Email == null ? "[No email]" : $"<{Email}>")} [{(UserId == null ? "no local user" : "local user")}]";//, for {ResponsibleForType.ToString().ToLower()} \"{ResponsibleFor}\"";
        public bool Equals([AllowNull] EditorInfo other)
        => other != null
            && Name == other.Name
            && Email == other.Email
            && SafaId == other.SafaId
            && UserId == other.UserId;
        public override bool Equals(object obj) => Equals(obj as EditorInfo);
        public override int GetHashCode() => (Name, Email, SafaId, UserId).GetHashCode();
    }

    public class EditorReason : IEquatable<EditorReason>
    {
        public string EditorForClubOrAssociation { get; set; }
        public string ResponsibleForId { get; set; }
        public string ResponsibleForName { get; set; }
        public ResponsibleForType ResponsibleForType { get; set; }
        public bool IsClosest { get; set; }
        public int Distance { get; set; }

        public string ShortDescription => EditorForClubOrAssociation ?? $"{ResponsibleForType.ToString().ToLower()} \"{ResponsibleForName}\"";

        public override string ToString() => $"{(IsClosest ? "Closest" : "Deferred")}-{Distance}: "
            + (EditorForClubOrAssociation == null
                ? $"Responsible for {ResponsibleForType.ToString().ToLower()} \"{ResponsibleForName}\""
                : $"SAFA site guide editor for {EditorForClubOrAssociation}, who are responsible for {ResponsibleForType.ToString().ToLower()} \"{ResponsibleForName}\"");
        public bool Equals([AllowNull] EditorReason other)
        => other != null
            && EditorForClubOrAssociation == other.EditorForClubOrAssociation
            && ResponsibleForId == other.ResponsibleForId
            && ResponsibleForType == other.ResponsibleForType
            && IsClosest == other.IsClosest
            && Distance == other.Distance;
        public override bool Equals(object obj) => Equals(obj as EditorInfo);
        public override int GetHashCode() => (EditorForClubOrAssociation, ResponsibleForId, ResponsibleForType, IsClosest, Distance).GetHashCode();
    }

    public enum ResponsibleForType
    {
        Site,
        Area,
        /// <summary>
        /// If a contact is associated with a user, that user is responsible for that contact (generally themselves).
        /// </summary>
        Contact,
    }

}
