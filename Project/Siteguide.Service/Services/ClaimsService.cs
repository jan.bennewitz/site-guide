﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SiteGuide.Service;
using SiteGuide.Service.Services;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using SiteGuide.Service.Identifiers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiteGuide.Service.Helpers;
using System.Security.Claims;

namespace SiteGuide.Service
{
    public class ClaimsService
    {
        private readonly UserManager<IdentityUser> _userManager;

        public ClaimsService(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task AddProviderClaimsAsync(IdentityUser user, ExternalLoginInfo info)
        {
            if (info.LoginProvider == "SAFA")
            {
                const string claimType = Permissions.SafaMemberNumberClaimType;
                var safaClaim = new Claim(claimType, info.ProviderKey);
                var existingSafaClaim = (await _userManager.GetClaimsAsync(user))
                    .SingleOrDefault(c => c.Type == claimType);
                if (existingSafaClaim == null)
                    await _userManager.AddClaimAsync(user, safaClaim);
                else
                    await _userManager.ReplaceClaimAsync(user, existingSafaClaim, safaClaim);
            }
        }

        public async Task RemoveProviderClaimsAsync(IdentityUser user, string loginProvider)
        {
            if (loginProvider == "SAFA")
            {
                const string claimType = Permissions.SafaMemberNumberClaimType;
                var existingSafaClaim = (await _userManager.GetClaimsAsync(user))
                    .SingleOrDefault(c => c.Type == claimType);
                if (existingSafaClaim != null)
                    await _userManager.RemoveClaimAsync(user, existingSafaClaim);
            }
        }
    }
}
