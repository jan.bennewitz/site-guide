﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Version = SiteGuide.DAL.Models.Version;

namespace SiteGuide.Service
{
    public class VersionService
    {
        private readonly SiteGuideContext _context;

        public VersionService(SiteGuideContext context)
        {
            _context = context;
        }

        private static readonly SemaphoreSlim _versionCreationSemaphore = new(1, 1);

        /// <returns>Were any changes made?</returns>
        public async Task<bool> ApplyEditAsync(int editId, string comment)
        {
            var editVersion = _context.EditVersion
                .Where(ev => ev.EditId == editId)
                .OrderByDescending(ev => ev.EditVersionNumber)
                .FirstOrDefault()
                ?? throw new KeyNotFoundException($"No Edit with id {editId} exists.");

            // Create new version
            // Since we need to get the next available id before saving this record, only one create operation can be allowed simultaneously. Otherwise two processes may get the same id before either saves a new record, and we end up with duplicates in the DB.
            try
            {
                await _versionCreationSemaphore.WaitAsync();

                int? currentVersionId = _context.Version
                .Max<Version, int?>(v => v.Id);
                var newVersion = new Version()
                {
                    Id = (currentVersionId ?? 0) + 1, // TODO: Make auto-increment?
                    EditId = editId,
                    Comment = comment,
                };

                bool wereAnyChangesMade = false;
                wereAnyChangesMade |= await ApplyChangesToDatabase(_context.Area, editVersion.Id, currentVersionId, newVersion.Id);
                wereAnyChangesMade |= await ApplyChangesToDatabase(_context.Site, editVersion.Id, currentVersionId, newVersion.Id);
                wereAnyChangesMade |= await ApplyChangesToDatabase(_context.Launch, editVersion.Id, currentVersionId, newVersion.Id);
                wereAnyChangesMade |= await ApplyChangesToDatabase(_context.SeeAlso, editVersion.Id, currentVersionId, newVersion.Id);
                wereAnyChangesMade |= await ApplyChangesToDatabase(_context.Video, editVersion.Id, currentVersionId, newVersion.Id);
                wereAnyChangesMade |= await ApplyChangesToDatabase(_context.SiteContact, editVersion.Id, currentVersionId, newVersion.Id);
                wereAnyChangesMade |= await ApplyChangesToDatabase(_context.Contact, editVersion.Id, currentVersionId, newVersion.Id);
                wereAnyChangesMade |= await ApplyChangesToDatabase(_context.SiteMapshape, editVersion.Id, currentVersionId, newVersion.Id);
                wereAnyChangesMade |= await ApplyChangesToDatabase(_context.MapShape, editVersion.Id, currentVersionId, newVersion.Id);

                if (wereAnyChangesMade) // Nothing to do if there are no changes to any records.
                {
                    _context.Version.Add(newVersion);

                    // This doubles up work already done, but leave it here for now as the importer uses it:
                    //Mark change as applied etc.
                    editVersion.StatusId = "A";
                    editVersion.SubmittedTime = DateTime.UtcNow;

                    //var changes = _context.ChangeTracker.Entries().Where(e => e.State != EntityState.Unchanged);

                    //Update all open edits
                    if (currentVersionId != null)
                        await RebaseDrafts((int)currentVersionId, editVersion.Id);
                }
                await _context.SaveChangesAsync(); // We want to save clean-up changes even if no real changes were made.

                return wereAnyChangesMade;
            }
            finally
            {
                _versionCreationSemaphore.Release();
            }
        }

        private async Task RebaseDrafts(int oldBaseVersion, int appliedEditVersionId)
        {
            // This crashes hard when called from the migrator asynchronously. Works when called synchronously. Bug in .net?
            //var draftEdits = await _context.EditVersion
            //    .Where(ev => ev.StatusId == EditVersionStatus.Draft.Id)
            //    .ToListAsync();
            var draftEdits = _context.EditVersion
                .Where(ev => ev.StatusId == EditVersionStatus.Draft.Id)
                .ToList();
            foreach (var draftEdit in draftEdits)
            {
                RebaseDraft(draftEdit, oldBaseVersion, appliedEditVersionId, _context.Site);
                RebaseDraft(draftEdit, oldBaseVersion, appliedEditVersionId, _context.Area);
                RebaseDraft(draftEdit, oldBaseVersion, appliedEditVersionId, _context.Site);
                RebaseDraft(draftEdit, oldBaseVersion, appliedEditVersionId, _context.Launch);
                RebaseDraft(draftEdit, oldBaseVersion, appliedEditVersionId, _context.SeeAlso);
                RebaseDraft(draftEdit, oldBaseVersion, appliedEditVersionId, _context.Video);
                RebaseDraft(draftEdit, oldBaseVersion, appliedEditVersionId, _context.SiteContact);
                RebaseDraft(draftEdit, oldBaseVersion, appliedEditVersionId, _context.Contact);
                RebaseDraft(draftEdit, oldBaseVersion, appliedEditVersionId, _context.SiteMapshape);
                RebaseDraft(draftEdit, oldBaseVersion, appliedEditVersionId, _context.MapShape);
            }
        }

        private void RebaseDraft<T>(EditVersion draftEdit, int oldBaseVersion, int appliedEditVersionId, DbSet<T> dbSet) where T : class, IRecord
        {
            var payloadProps = IRecordExtensions.GetRecordPayloadProps<T>();

            var recordChanges = dbSet
                .Where(r => r.EditVersionId == appliedEditVersionId)
                .ToList()
                .Join(dbSet.Where(r => r.EditVersionId == draftEdit.Id).ToList(), r => r.Key, r => r.Key, (n, d) => new { n, d, key = n.Key });

            foreach (var recordChange in recordChanges)
            {
                var oldBaseRecord = dbSet
                    .ValidAtVersion(oldBaseVersion)
                    .ToList()
                    .SingleOrDefault(r => r.Key.Equals(recordChange.key));

                if (oldBaseRecord == null)
                    continue; // This is an addition, no need to rebase.

                foreach (var prop in payloadProps)
                {
                    var oldBaseValue = prop.GetValue(oldBaseRecord);
                    var newValue = prop.GetValue(recordChange.n);
                    var draftValue = prop.GetValue(recordChange.d);

                    // if the field is changed by this version change:
                    if (!Equals(oldBaseValue, newValue))
                    {
                        // if the field is not changed by the edit, update the field.
                        if (Equals(oldBaseValue, draftValue))
                            prop.SetValue(recordChange.d, newValue);
                        else
                        {
                            //     else ignore, or we could try to be smart about it.
                            Debug.WriteLine($"Rebasing {typeof(T).Name} {recordChange.d}: Field \"{prop.Name}\" has been changed by both draft edit {draftEdit.Id} and edit version {appliedEditVersionId}.");
                        }
                    }
                }
            }
        }

        private static async Task<bool> ApplyChangesToDatabase<T>(DbSet<T> dbSet, int editVersionId, int? currentVersionId, int newVersionId)
            where T : class, IRecord
        {
            bool wereAnyChangesMade = false;

            List<T> currentRecords = dbSet.ValidAtCurrentVersion().Any()
                //? await dbSet.ValidAtVersion().ToListAsync()
                ? dbSet.ValidAtCurrentVersion().ToList()
                : new List<T>();

            foreach (var change in dbSet.WhereEditVersion(editVersionId).ToList())
            {
                T currentRecord = currentRecords.SingleOrDefault(s => change.Key.Equals(s.Key));

                if (currentRecord.IsRecordContentIdentical(change) && currentRecord.IsDelete == change.IsDelete)
                    dbSet.Remove(change); // change record is identical to existing record and therefore pointless, clean it up.
                else
                {
                    if (currentRecord != null) // currentRecord will be null for an addition
                        currentRecord.ValidToVersion = currentVersionId; // currentVersionId will be null for the initial version, but then there is no current item.

                    if (!(bool)change.IsDelete)
                        change.ValidFromVersion = newVersionId; // Make change valid from this version

                    wereAnyChangesMade = true;
                }
            }

            return wereAnyChangesMade;
        }

        public async Task RemoveEditAsync(int editId)
        {
            var editVersions = _context.EditVersion
                .Where(ev => ev.EditId == editId);

            var editVersionIds = editVersions
                .Select(ev => ev.Id)
                .ToList();

            RemoveEdits(editVersionIds, _context.Area);
            RemoveEdits(editVersionIds, _context.Contact);
            RemoveEdits(editVersionIds, _context.Launch);
            RemoveEdits(editVersionIds, _context.MapShape);
            RemoveEdits(editVersionIds, _context.SeeAlso);
            RemoveEdits(editVersionIds, _context.Site);
            RemoveEdits(editVersionIds, _context.SiteContact);
            RemoveEdits(editVersionIds, _context.SiteMapshape);
            RemoveEdits(editVersionIds, _context.Video);

            _context.RemoveRange(editVersions);
            await _context.SaveChangesAsync();
        }
        private static void RemoveEdits<T>(List<int> editVersionIds, DbSet<T> dbSet) where T : class, IRecord
            => dbSet.RemoveRange(dbSet.Where(i => editVersionIds.Contains((int)i.EditVersionId)));

        /// <summary>
        /// Get the VersionInfos for all versions matching the predicate.
        /// </summary>
        ///<param name="versionPredicate">The version inclusion predicate. If this returns true for a given version, that version will be included in the result. If the predicate is null, return all versions.</param>
        ///<returns>The returned VersionInfos include information about the EditVersions associated with these versions.</returns>
        public async Task<IList<VersionInfo>> GetVersionInfosAsync(Expression<Func<Version, bool>> versionPredicate = null)
        {
            var versions = _context.Version.AsNoTracking();

            if (versionPredicate != null)
                versions = versions.Where(versionPredicate);

            return await versions
                .GroupJoin(
                    _context.EditVersion.AsNoTracking(),
                    v => v.EditId,
                    ev => ev.EditId,
                    (v, evs) => new VersionInfo
                    {
                        Id = v.Id,
                        Comment = v.Comment,
                        EditVersions = evs.ToList(),
                    })
                .ToListAsync();
        }

        /// <summary>
        /// Get the VersionInfo for this version id.
        /// </summary>
        /// <param name="versionId">Specified version number, or null for latest</param>
        /// <returns>VersionInfo includes information about the EditVersions associated with this version.</returns>
        public async Task<VersionInfo> GetVersionInfoAsync(int? versionId = null)
        {
            Version version = _context.Version.AsNoTracking().Get(versionId);

            return version == null
                ? null // Null if requested version does not exist
                : new VersionInfo
                {
                    Id = version.Id,
                    Comment = version.Comment,
                    EditVersions = await _context.EditVersion
                        .AsNoTracking()
                        .Where(ev => ev.EditId == version.EditId)
                        .ToListAsync(),
                };
        }
        /// <summary>
        /// Get the VersionInfo for this version id.
        /// </summary>
        /// <param name="atTimeUtc">Specifies the time at which the requested version was the current version.</param>
        /// <returns>VersionInfo includes information about the EditVersions associated with this version. Returns null if the requested time is before the first version's publish date.</returns>
        public async Task<VersionInfo> GetVersionInfoAsync(DateTime atTimeUtc)
        {
            var versionId = await _context.Version
                .Join(
                    _context.EditVersion.Where(ev =>
                        ev.StatusId == "A"
                        && ev.SubmittedTime <= atTimeUtc
                    ),
                    v => v.EditId, ev => ev.EditId,
                    (v, ev) => v)
                .MaxAsync(v => (int?)v.Id);

            if (versionId is null)
                return null; // No matches. The requested time is before the first version's publish date.

            return await GetVersionInfoAsync(versionId);
        }

        public async Task<IEnumerable<VersionInfo>> GetVersionsForSiteAsync(int siteId)
        {
            var mapshapeVersions =
                    _context.SiteMapshape
                    .AsNoTracking()
                    .ValidAtAnyVersion()
                    .Where(s => s.SiteId == siteId)
                    .ToList()
                    .SelectMany(sm =>
                        _context.MapShape
                            .AsNoTracking()
                            .Where(m => m.Id == sm.MapshapeId)
                            .ValidityOverlaps(sm)
                            .Select(m => new RecordFromTo(
                                new[] { m.ValidFromVersion, sm.ValidFromVersion }.Max(),
                                new[] { m.ValidToVersion, sm.ValidToVersion }.Min()
                            )))
                    .GetModifyingVersions();

            var contactVersions =
                    _context.SiteContact
                    .AsNoTracking()
                    .ValidAtAnyVersion()
                    .Where(s => s.SiteId == siteId)
                    .ToList()
                    .SelectMany(sc =>
                        _context.Contact
                            .AsNoTracking()
                            .Where(m => m.Id == sc.ContactId)
                            .ValidityOverlaps(sc)
                            .Select(m => new RecordFromTo(
                                new[] { m.ValidFromVersion, sc.ValidFromVersion }.Max(),
                                new[] { m.ValidToVersion, sc.ValidToVersion }.Min()
                            )))
                    .GetModifyingVersions();

            var versionIds = _context.Site
                .AsNoTracking()
                .Where(s => s.Id == siteId)
                .GetModifyingVersions()
                .Union(
                    _context.Launch
                        .AsNoTracking()
                        .Where(s => s.SiteId == siteId)
                        .GetModifyingVersions())
                .Union(
                    _context.SeeAlso
                        .AsNoTracking()
                        .Where(s => s.SiteId == siteId)
                        .GetModifyingVersions())
                .Union(
                    _context.Video
                        .AsNoTracking()
                        .Where(s => s.SiteId == siteId)
                        .GetModifyingVersions())
                .Union(
                    _context.SiteContact
                    .AsNoTracking()
                    .Where(s => s.SiteId == siteId)
                    .GetModifyingVersions())
                .Union(
                    _context.SiteMapshape
                    .AsNoTracking()
                    .Where(s => s.SiteId == siteId)
                    .GetModifyingVersions())
                .Union(mapshapeVersions)
                .Union(contactVersions)
                .Distinct()
                .OrderBy(v => v)
                .ToList();

            var versionInfos = await GetVersionInfosAsync(v => versionIds.Contains(v.Id));

            return versionInfos;
        }
    }

    /// <summary>
    /// VersionId, Comment and EditVersion used for the version with this VersionId.
    /// </summary>
    public class VersionInfo
    {
        private IList<EditVersion> _editVersions;

        public int Id { get; set; }
        public string Comment { get; set; }
        public EditVersion AppliedEditVersion => EditVersions.Single(ev => ev.StatusId == "A");
        /// <summary>
        ///  Ordered sequentially
        /// </summary>
        public IList<EditVersion> EditVersions
        {
            get => _editVersions;
            set => _editVersions = value.OrderBy(ev => ev.EditVersionNumber).ToList();
        }
        public DateTime PublishedTime => (DateTime)AppliedEditVersion.SubmittedTime;

        public override string ToString() => $"{Id}: {PublishedTime}";
    }

    internal class RecordFromTo : IRecord
    {
        public int RecordId { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? EditVersionId { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public EditVersion EditVersion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool? IsDelete { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int? ValidFromVersion { get; set; }
        public int? ValidToVersion { get; set; }
        public object Key { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public RecordFromTo(int? fromVersion, int? toVersion)
        {
            ValidFromVersion = fromVersion;
            ValidToVersion = toVersion;
        }
    }

    public enum ChangeType
    {
        Unchanged,
        Edit,
        Addition,
        Removal,
        Association,
        Disassociation,
        AdditionAndAssociation,
        RemovalAndDisassociation,
    }
}
