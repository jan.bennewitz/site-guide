﻿using Dropbox.Api;
using Dropbox.Api.Files;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Npgsql;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service.Services
{
    public interface IOnlineFileStorageService
    {
        Task UploadAsync(string localFileName, string remoteFileName);
    }

    public class DropBoxService : IOnlineFileStorageService
    {
        private readonly IConfiguration _config;

        public DropBoxService(IConfiguration config)
        {
            _config = config;
        }

        public async Task UploadAsync(string localFileName, string remoteFileName)
        {
            using var dropboxClient = new DropboxClient(null, _config.GetValue<string>("DropBox:RefreshToken"), _config.GetValue<string>("DropBox:AppKey"), _config.GetValue<string>("DropBox:AppSecret"), new DropboxClientConfig());
            using var fileStream = File.Open(localFileName, FileMode.Open);
            var response = await dropboxClient.Files.UploadAsync(new UploadArg(remoteFileName), fileStream);
        }

        // TODO: Add functionality to delete older backups - e.g. keep the last 10 days of daily backups, then 3 months of weekly backups, then a year of monthly. Or something - could base on available space.
    }
}
