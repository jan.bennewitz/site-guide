﻿using Humanizer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using SiteGuide.Service.Extensions;
using SiteGuide.Service.Helpers;
using SiteGuide.Service.Identifiers;
using SiteGuide.Web.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SiteGuide.Service
{
    public class EditService
    {
        private readonly SiteGuideContext _context;
        private readonly EditPermissionsService _editPermissionsService;
        private readonly NotificationService _notificationService;
        private readonly VersionService _versionService;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly BackgroundTasksService _backgroundTasksService;

        private static readonly Dictionary<Type, SemaphoreSlim> _entityCreateSemaphores = new Type[]{
            typeof(Site) ,
            typeof(Launch),
            typeof(Video),
            typeof(SeeAlso),
            typeof(Contact),
            typeof(MapShape),
        }.ToDictionary(i => i, i => new SemaphoreSlim(1, 1));

        public EditService(SiteGuideContext context, EditPermissionsService editPermissionsService, NotificationService notificationService, VersionService versionService, UserManager<IdentityUser> userManager, BackgroundTasksService backgroundTasksService)
        {
            _context = context;
            _editPermissionsService = editPermissionsService;
            _notificationService = notificationService;
            _versionService = versionService;
            _userManager = userManager;
            _backgroundTasksService = backgroundTasksService;
        }

        public async Task<EditSummaryModel> GetEditSummaryModel(int editId, IdentityUser user = null)
        {
            var editVersions = await _context.EditVersion
                .Where(ev => ev.EditId == editId)
                .OrderBy(ev => ev.EditVersionNumber)
                .ToListAsync();

            if (!editVersions.Any())
                return null;

            var editVersion = editVersions.Last();
            editVersion.Status ??= _context.EditVersionStatus.Single(s => s.Id == editVersion.StatusId);

            var fullAndPartialEditors = await _editPermissionsService.GetEditorsForEditVersionAsync(editVersion);
            UserPrivsForEdit userPrivileges = user == null ? null : await _editPermissionsService.GetUserPrivilegesForEdit(user, editVersions, editVersion, fullAndPartialEditors.Editors);

            var possibleActions = (editVersion.StatusId == "D" && user.Id != editVersion.AuthorId)
                ? new List<EditAction>() // Only author can only take actions if draft.
                : EditActions.All
                    .Where(a =>
                        a.ValidForStatus.Contains(editVersion.StatusId)
                        && (!a.RequiresEditRole.Any() // No role required ...
                            || a.RequiresEditRole.Any(r => userPrivileges != null && userPrivileges.HasEditActionRole(r))) // ... or user has at least one of the required roles.
                    );

            int? acceptedAsVersion;
            if (editVersion.StatusId == "A")
            {
                acceptedAsVersion = _context.Version.SingleOrDefault(v => v.EditId == editVersion.EditId)?.Id;
                if (acceptedAsVersion is null)
                    throw new Exception($"No Version exists for accepted Edit {editVersion.EditId}");
            }
            else
                acceptedAsVersion = null;

            return new EditSummaryModel
            {
                EditVersion = editVersion,
                EditVersions = editVersions,
                Editors = fullAndPartialEditors.Editors,
                PartialEditors = fullAndPartialEditors.PartialEditors,
                PossibleActions = possibleActions,
                PossibleActionsByRole = possibleActions.ToLookup(a =>
                    a.RequiresEditRole
                    .Where(r => userPrivileges != null && userPrivileges.HasEditActionRole(r))
                    .OrderBy(r => r) // Get the "closest" role held by the user. This relies on the enum being in ascending order of "distance".
                    .Select(r => (EditActionRole?)r) // This enables us to get null if there are no roles.
                    .FirstOrDefault()),
                UserPrivileges = userPrivileges,
                AcceptedAsVersion = acceptedAsVersion,
            };
        }

        public async Task<EditVersion> CreateReviewEditVersionAsync(int editId, IdentityUser user, string userRoleDescription = null)
        {
            var baseEditVersion = _context.EditVersion
                .Where(ev => ev.EditId == editId)
                .OrderByDescending(ev => ev.EditVersionNumber)
                .First();
            if (baseEditVersion.StatusId != "S")
                throw new InvalidOperationException("Can only review an edit with \"Submitted\" status.");
            var newEditVersion = await AddNewEditVersionAsync(user, editId, userRoleDescription, (short)(baseEditVersion.EditVersionNumber + 1));

            // Copy existing edit
            CopyEditVersionRecords<Area>(baseEditVersion.Id, newEditVersion);
            CopyEditVersionRecords<Contact>(baseEditVersion.Id, newEditVersion);
            CopyEditVersionRecords<Launch>(baseEditVersion.Id, newEditVersion);
            CopyEditVersionRecords<MapShape>(baseEditVersion.Id, newEditVersion);
            CopyEditVersionRecords<SeeAlso>(baseEditVersion.Id, newEditVersion);
            CopyEditVersionRecords<Site>(baseEditVersion.Id, newEditVersion);
            CopyEditVersionRecords<SiteContact>(baseEditVersion.Id, newEditVersion);
            CopyEditVersionRecords<SiteMapshape>(baseEditVersion.Id, newEditVersion);
            CopyEditVersionRecords<Video>(baseEditVersion.Id, newEditVersion);

            await _context.SaveChangesAsync();

            await _backgroundTasksService.RebuildCacheUserClaimableEditsAsync(editId);

            //TODO: Notify other editors that this edit has been claimed?
            //await NotifyEditorsAsync(directAdmins, workflowInfo.EditVersion, currentUser);

            return newEditVersion;
        }

        public async Task<IEnumerable<CacheUserClaimableEditsDetail>> GetUserClaimableEditsInfoAsync(int? editId = null)
        {
            List<int> calculateForEditIds;
            var result = new List<CacheUserClaimableEditsDetail>();

            if (editId is null)
                calculateForEditIds = (await GetLatestEditVersionsAsync(e => e.StatusId == "S"))
                    .Select(r => r.EditId)
                    .ToList(); // All claimable review Ids
            else
            {
                var editVersion = (await GetLatestEditVersionsAsync(e => e.EditId == editId)).Single();
                if (editVersion.StatusId != "S")
                    // This edit isn't available for review anymore - return empty list.
                    return result;

                calculateForEditIds = new List<int> { (int)editId };
            }

            // TODO: Optimise:
            foreach (var claimableReviewId in calculateForEditIds)
            {
                var info = await GetEditSummaryModel(claimableReviewId);

                result.AddRange(
                    info.Editors
                        .Where(e => e.UserId != null)
                        .Select(e => new CacheUserClaimableEditsDetail { ClaimableEditId = claimableReviewId, UserId = e.UserId })
                );
            }

            return result;
        }

        public async Task<List<EditVersion>> GetLatestEditVersionsAsync(Expression<Func<EditVersion, bool>> filter)
        {
            var editIdAndMaxEditVersionNumber = _context.EditVersion
                .GroupBy(e => e.EditId)
                .Select(g => new { EditId = g.Key, EditVersionNumber = g.Max(e => e.EditVersionNumber) });
            return await _context.EditVersion
                .Join(editIdAndMaxEditVersionNumber, e => new { e.EditId, e.EditVersionNumber }, m => m, (e, m) => e)
                .Where(filter)
                .ToListAsync();
        }

        private void CopyEditVersionRecords<T>(int sourceEditVersionId, EditVersion targetEditVersion) where T : class, IRecord, new()
        {
            var dbSet = _context.Set<T>();
            dbSet.AddRange(dbSet
                .Where(i => i.EditVersionId == sourceEditVersionId)
                .Select(i => new T
                {
                    EditVersion = targetEditVersion,
                    IsDelete = i.IsDelete,
                }
                .CopyRecordContentFrom(i)));
        }

        public async Task SubmitDraftAsync(EditVersion editVersion, string comment, string authorRoleDesc, string editByUserEmail, IEnumerable<EditorInfo> allAdmins)
        {
            // TODO: If (!workflowInfo.EditVersion.HasChanges()) {message="Can't submit - no changes", return invalidoperation}}
            await FinaliseDraftAsync(editVersion.Id, EditVersionStatus.Submitted, comment, authorRoleDesc);

            IEnumerable<EditorInfo> recipients;
            string additionalInformation;

            var directAdmins = allAdmins.Where(e => e.IsClosest);
            var indirectAdmins = allAdmins.Where(e => !e.IsClosest);

            if (directAdmins.Any())
            {
                recipients = directAdmins;
                additionalInformation = null;
            }
            else
            {
                recipients = indirectAdmins;
                additionalInformation = "No direct admins are registered for this proposed edit.";
            }

            await _notificationService.NotifyEditorsOfStatusChange(editVersion.EditId, EditActions.SubmitForReview, editByUserEmail, recipients, additionalInformation);
        }

        public async Task AcceptDraftAsync(EditVersion editVersion, string editVersionComment, string versionComment, string authorRoleDesc, string acceptedByUserEmail, IEnumerable<EditorInfo> editors)
        {
            async Task doBeforeSaveChanges()
            {
                bool wereAnyChangesMade = await _versionService.ApplyEditAsync(editVersion.EditId, versionComment);

                if (!wereAnyChangesMade)
                    throw new Exception("No changes made - this probably indicates an error in the logic.");
            }

            await FinaliseDraftAsync(editVersion.Id, EditVersionStatus.Accepted, editVersionComment, authorRoleDesc, doBeforeSaveChanges);

            await _notificationService.NotifySubscribersAsync(editors, editVersion.EditId, EditActions.Accept, acceptedByUserEmail);
        }

        public async Task RejectDraftAsync(EditVersion editVersion, string comment, string authorRoleDesc, string acceptedByUserEmail, IEnumerable<EditorInfo> editors)
        {
            await FinaliseDraftAsync(editVersion.Id, EditVersionStatus.Rejected, comment, authorRoleDesc);

            await _notificationService.NotifySubscribersAsync(editors, editVersion.EditId, EditActions.Reject, acceptedByUserEmail);
        }

        private async Task FinaliseDraftAsync(int editVersionId, EditVersionStatus finalStatus, string comment, string authorRole, Func<Task> doAsyncBeforeSaveChanges = null)
        {
            var editVersion = _context.EditVersion
                .Single(ev => ev.Id == editVersionId);

            if (editVersion.StatusId != "D")
                throw new InvalidOperationException("The specified edit version is not a draft and can therefore not be changed.");
            if (finalStatus.Id == "D")
                throw new ArgumentException($"Final status must be a non-draft status. Valid statuses are {EditVersionStatus.All.Where(s => s.Id != "D").Select(s => s.Name).Humanize()}.", nameof(finalStatus));

            editVersion.Comment = comment;
            editVersion.AuthorRole = authorRole;
            editVersion.StatusId = finalStatus.Id;
            editVersion.SubmittedTime = DateTime.UtcNow;

            if (doAsyncBeforeSaveChanges != null)
                await doAsyncBeforeSaveChanges();

            await _context.SaveChangesAsync();

            await _backgroundTasksService.RebuildCacheUserClaimableEditsAsync(editVersion.EditId);
        }

        /// <summary>
        /// Delete the specified edit version.
        /// </summary>
        /// <param name="editVersionId"></param>
        /// <returns></returns>
        public async Task AbandonDraftAsync(int editVersionId, IEnumerable<EditorInfo> directAdmins)
        {
            var editVersion = _context.EditVersion.Single(ev => ev.Id == editVersionId);
            if (editVersion.StatusId != "D")
                throw new InvalidOperationException("The specified edit version is not a draft and can therefore not be abandoned.");

            foreach (var table in _context.RecordTables)
                _context.RemoveRange(table.WhereEditVersion(editVersionId));
            _context.Remove(editVersion);

            await _context.SaveChangesAsync();

            // If this is EditVersion .1 then we need not do anything more. If this is .2 or later, treat like a forward.
            if (editVersion.EditVersionNumber != 1)
                await _notificationService.NotifyEditorsOfStatusChange(editVersion.EditId, EditActions.Abandon, editVersion.AuthorName, directAdmins);
        }

        private static readonly SemaphoreSlim _draftEditCreationSemaphore = new(1, 1);

        /// <summary>
        /// Create a new draft edit
        /// </summary>
        /// <returns>The newly create draft edit</returns>
        public async Task<EditVersion> CreateDraftEditAsync(IdentityUser authorUser)
        {
            // Since we need to get the next available id before saving this record, only one create operation can be allowed simultaneously. Otherwise two processes may get the same id before either saves a new record, and we end up with duplicates in the DB.
            try
            {
                await _draftEditCreationSemaphore.WaitAsync();
                int nextEditId = _context.EditVersion
                    .Select(v => v.EditId)
                    .ToList() // This seems necessary to work around EF bug? https://github.com/dotnet/efcore/issues/17783
                    .DefaultIfEmpty(0)
                    .Max() + 1;
                var editVersion = await AddNewEditVersionAsync(authorUser, nextEditId);
                await _context.SaveChangesAsync();
                return editVersion;
            }
            finally
            {
                _draftEditCreationSemaphore.Release();
            }
        }

        private async Task<EditVersion> AddNewEditVersionAsync(IdentityUser author, int editId, string userRoleDescription = null, short editVersionNumber = 1)
        {
            var safaNumber = (await _userManager.GetClaimsAsync(author)).SingleOrDefault(c => c.Type == Permissions.SafaMemberNumberClaimType)?.Value;
            var editVersion = new EditVersion()
            {
                AuthorId = author.Id,
                AuthorName = safaNumber == null ? author.Email : $"SAFA#{safaNumber} <{author.Email}>",
                AuthorRole = userRoleDescription,
                EditId = editId,
                EditVersionNumber = editVersionNumber,
                InitialisedTime = DateTime.UtcNow,
                StatusId = "D",
            };
            _context.EditVersion.Add(editVersion);

            return editVersion;
        }

        public IEnumerable<EntityValidationResult> Validate(int editVersionId)
        {
            var results = new List<EntityValidationResult>();
            results.AddRange(Validate<Site>(editVersionId, s => s.Name));

            // TODO: Add any others, implement IValidatableObject
            return results;
        }

        public bool HasAnyDraft(IdentityUser currentUser)
            => _context.EditVersion
                .WhereIsDraftBy(currentUser)
                .Any();

        private IEnumerable<EntityValidationResult> Validate<T>(int editVersionId, Func<T, string> itemDescriptor) where T : class, IRecord, IValidatableObject, new()
        {
            var results = new List<EntityValidationResult>();

            var items = _context.Set<T>()
                .Where(i => i.EditVersionId == editVersionId);

            foreach (var item in items)
            {
                var validationResults = item.Validate(null);
                if (validationResults.Any())
                    results.Add(new EntityValidationResult
                    {
                        EntityName = $"{typeof(T).Name} \"{itemDescriptor(item)}\"",
                        ValidationResults = validationResults,
                    });
            }

            return results;
        }

        public async Task<T> CreateRecord<T>(EditVersion editVersion, int? siteId = null, int? foreignKeyId = null, Func<T, Task> setFieldsAsync = null) where T : class, IRecord, new()
        {
            var newItem = new T()
            {
                EditVersionId = editVersion.Id,
                IsDelete = false,
                ValidFromVersion = null,
                ValidToVersion = null,
            };

            // Since we need to get the next available id before saving this record, for each record table only one create operation can be allowed simultaneously. Otherwise two processes may get the same id before either saves a new record, and we end up with duplicates in the DB.
            var semaphoreSlim = newItem is IId
                ? _entityCreateSemaphores[typeof(T)] // This is only an issue for records which have an id.
                : null; // No need

            try
            {
                if (semaphoreSlim != null)
                    await semaphoreSlim.WaitAsync();

                if (newItem is ISiteChild siteChildItem)
                    siteChildItem.SiteId = (int)siteId;

                switch (newItem)
                {
                    case IId idItem:
                        var maxId = await _context.Set<T>()
                            .MaxAsync(i => (int?)((IId)i).Id);

                        idItem.Id = (maxId ?? 0) + 1;
                        break;

                    case ISiteManyToMany siteManyToManyItem:
                        siteManyToManyItem.ForeignKeyId = (int)foreignKeyId;
                        break;

                    default: throw new ArgumentException("Unhandled type.");
                }

                // Special cases:
                switch (newItem)
                {
                    case Site site:
                        site.InsetMapPositionId = _context.InsetMapPosition.Single(i => i.Name == "Bottom Right").Id;
                        break;
                    case MapShape mapShape: // Add to current site's area and set default category.
                        mapShape.AreaId = _context.Site
                            .ValidAtUnacceptedEditVersion(new EditVersionIdentifier(editVersion, _context))
                            .Single(s => s.Id == siteId)
                            .AreaId;
                        mapShape.CategoryId = _context.MapShapeCategory.Single(c => c.Name == "Landing").Id;
                        break;
                    case SiteContact siteContact: // Set defaults.
                        siteContact.IsContact = true;
                        siteContact.IsResponsible = false;
                        break;
                }

                if (setFieldsAsync != null)
                    await setFieldsAsync(newItem);

                SetDefaultValuesForMissingRequiredFields(newItem);

                // Delete any existing delete record for this edit version and key.
                // This is only an issue for ISiteManyToMany records.
                // This is necessary when within an edit version an existing association is deleted and then re-created.
                if (newItem is ISiteManyToMany newAssociation)
                {
                    var existingAssociation = _context.Set<T>()
                        .Where(i => i.EditVersionId == editVersion.Id)
                        .ToList()
                        .SingleOrDefault(i =>
                            ((ISiteManyToMany)i).SiteId == newAssociation.SiteId
                            && ((ISiteManyToMany)i).ForeignKeyId == newAssociation.ForeignKeyId);
                    if (existingAssociation != null)
                        _context.Remove(existingAssociation);
                }

                _context.Add(newItem);

                await _context.SaveChangesAsync();

                return newItem;
            }
            finally
            {
                semaphoreSlim?.Release();
            }
        }

        private static void SetDefaultValuesForMissingRequiredFields<T>(T newItem) where T : class, IRecord
        {
            // Set default values for required string fields
            foreach (var prop in IRecordExtensions.GetRecordPayloadProps<T>())
                if (prop.PropertyType == typeof(string) && prop.GetValue(newItem) == null && Attribute.IsDefined(prop, typeof(RequiredAttribute)))
                    prop.SetValue(newItem, "[Required]");
        }
    }

    public class EntityValidationResult
    {
        public string EntityName { get; set; }
        public IEnumerable<ValidationResult> ValidationResults { get; set; }
    }

    public class EditSummaryModel
    {
        public EditVersion EditVersion { get; set; }
        public IList<EditVersion> EditVersions { get; set; }
        public IEnumerable<EditorInfo> Editors { get; set; }
        public IEnumerable<EditorInfo> PartialEditors { get; set; }
        public UserPrivsForEdit UserPrivileges { get; set; }
        public IEnumerable<EditAction> PossibleActions { get; set; }
        public ILookup<EditActionRole?, EditAction> PossibleActionsByRole { get; set; }
        public int? AcceptedAsVersion { get; set; }
    }
}
