﻿using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service.Identifiers
{
    //public class VersionTreeNodeIdentifier
    //{
    //    public VersionOrUnacceptedEditVersionIdType Type { get; set; }
    //    [Obsolete("Do not expose internal ids - replace with editId and versionId? or something...")]
    //    public int? Id { get; set; }

    //    public override string ToString() => $"{Type} {Id}";
    //}

    //public enum VersionOrUnacceptedEditVersionIdType
    //{
    //    Version,
    //    UnacceptedEditVersion,
    //}

    public class VersionTreeNodeIdentifier
    {
        public static VersionTreeNodeIdentifier FromIdentifier(string identifier, SiteGuideContext context)
            => identifier.Contains(".")
                ? EditVersionIdentifier.FromEditVersionString(identifier, context)
                : new VersionIdentifier(int.Parse(identifier));

        public string Identifier => this switch
        {
            EditVersionIdentifier evi => evi.IdAndVersionNumber,
            VersionIdentifier vi => vi.Id.ToString(),

            _ => throw new NotImplementedException(),
        };
    }

    public class VersionIdentifier : VersionTreeNodeIdentifier
    {
        public VersionIdentifier(int id)
        {
            Id = id;
        }

        public int Id { get; set; }

        public override string ToString() => $"[{Id}]";
    }

    public class EditVersionIdentifier : VersionTreeNodeIdentifier
    {
        public EditVersionIdentifier(EditVersion editVersion, SiteGuideContext context)
        {
            Id = editVersion.Id;
            EditId = editVersion.EditId;
            EditVersionNumber = editVersion.EditVersionNumber;

            var latestEditVersionNumber = context.EditVersion
                .Where(ev => ev.EditId == editVersion.EditId)
                .Max(ev => ev.EditVersionNumber);
            var latestEditVersion = context.EditVersion
                .Single(ev =>
                    ev.EditId == editVersion.EditId
                    && ev.EditVersionNumber == latestEditVersionNumber);

            ReferenceVersion = latestEditVersion.StatusId switch
            {
                "D" or "S" => context.Version
                    .OrderByDescending(v => v.Id)
                    .First()
                    .Id, // Always based on current.
                "A" => context.Version
                    .SingleOrDefault(i =>
                        i.Id == context.Version
                            .Single(v => v.EditId == editVersion.EditId)
                            .Id - 1)
                    ?.Id, // Based on version immediately preceding version it was accepted as. Ref version of initial version is null.
                "R" => GetReferenceVersionForRejectedEditVersion(latestEditVersion, context), // Relative to version current at rejection.
            };
        }

        private int? GetReferenceVersionForRejectedEditVersion(EditVersion rejectedEditVersion, SiteGuideContext context)
        {
            var rejectedTime = DateTime.SpecifyKind((DateTime)rejectedEditVersion.SubmittedTime, DateTimeKind.Utc); // Need to specify kind as UTC to not make PostgreSQL unhappy
            var baseVersionAcceptedTime = context.EditVersion
                .Where(ev =>
                    ev.StatusId == "A"
                    && ev.SubmittedTime < rejectedTime)
                .Max(ev => ev.SubmittedTime);

            return context.Version
                .Single(v => v.EditId == context.EditVersion
                    .Where(ev => ev.SubmittedTime == DateTime.SpecifyKind((DateTime)baseVersionAcceptedTime, DateTimeKind.Utc))
                    .Max(ev => ev.EditId))
                .Id;
        }

        public int Id { get; set; }
        public int EditId { get; set; }
        public int EditVersionNumber { get; set; }
        /// <summary>
        /// "null" is the base version of the initial commit.
        /// </summary>
        public int? ReferenceVersion { get; set; }

        public string IdAndVersionNumber => $"{EditId}.{EditVersionNumber}";
        public override string ToString() => $"[{Id}] {IdAndVersionNumber}";

        public static EditVersionIdentifier FromEditVersionString(string editVersionString, SiteGuideContext context)
        {
            var parts = editVersionString.Split('.');
            if (parts.Length != 2)
                throw new ArgumentException();

            if (!int.TryParse(parts[0], out int editId))
                throw new ArgumentException();
            if (!int.TryParse(parts[1], out int editVersionNumber))
                throw new ArgumentException();
            var editVersion = context.EditVersion
                            .SingleOrDefault(ev =>
                                ev.EditId == editId
                                && ev.EditVersionNumber == editVersionNumber);

            if (editVersion == null)
                return null;

            return new EditVersionIdentifier(editVersion, context);
        }

        public static EditVersionIdentifier FromId(int id, SiteGuideContext context)
        {
            var editVersion = context.EditVersion
                .SingleOrDefault(ev => ev.Id == id);

            if (editVersion == null)
                return null;

            return new EditVersionIdentifier(editVersion, context);
        }
    }
}
