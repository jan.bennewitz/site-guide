﻿using Mailjet.Client;
using Mailjet.Client.Resources;
using Mailjet.Client.TransactionalEmails;
using Microsoft.AspNetCore.Connections;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.Service
{
    public interface IEmailService
    {
        //Task SendEmailAsync(string recipient, string subject, string htmlBody);
        Task SendEmailAsync(Email email);
    }

    public class Email
    {
        public NameAndEmail From { get; set; }
        public NameAndEmail To { get; set; }
        public NameAndEmail Bcc { get; set; }
        public string Subject { get; set; }
        public string HtmlBody { get; set; }
    }

    public class NameAndEmail
    {
        public NameAndEmail(string email, string name)
        {
            EmailAddress = email;
            Name = name;
        }

        public string Name { get; set; }
        public string EmailAddress { get; set; }

        public override string ToString() => $"{Name} <{EmailAddress}>";
    }

    public class MailJetSender : IEmailService
    {
        private readonly string _apiKeyPublic;
        private readonly string _apiKeyPrivate;
        private readonly ILogger<MailJetSender> _logger;

        public MailJetSender(IOptionsMonitor<MailJetOptions> optionsAccessor, ILogger<MailJetSender> logger)
        {
            var options = optionsAccessor.CurrentValue;
            _apiKeyPublic = options.ApiKeyPublic;
            _apiKeyPrivate = options.ApiKeyPrivate;
            _logger = logger;
        }

        //public Task SendEmailAsync(string recipient, string subject, string htmlBody) 
        //    => SendEmailAsync(new Email { Recipient = recipient, Subject = subject, HtmlBody = htmlBody });

        public async Task SendEmailAsync(Email email) // From https://github.com/mailjet/mailjet-apiv3-dotnet
        {
            _logger.LogInformation("Email send to {to} ({bcc}): \"{subject}\"", email.To, email.Bcc, email.Subject);

            var transactionalEmailBuilder = new TransactionalEmailBuilder()
                .WithFrom(new SendContact(email.From.EmailAddress, email.From.Name))
                .WithTo(email.To == null
                    ? null
                    : new SendContact(email.To.EmailAddress, email.To.Name)
                );
            if (email.Bcc != null)
                transactionalEmailBuilder = transactionalEmailBuilder
                    .WithBcc(email.Bcc == null
                        ? null
                        : new SendContact(email.Bcc.EmailAddress, email.Bcc.Name)
                    );
            var transactionalEmail = transactionalEmailBuilder
                .WithSubject(email.Subject)
                .WithHtmlPart(email.HtmlBody)
                .Build();

            var client = new MailjetClient(_apiKeyPublic, _apiKeyPrivate);
            var response = await client.SendTransactionalEmailAsync(transactionalEmail);
            if (response.Messages.Any(m => m.Errors != null && m.Errors.Any()))
            {
                var errorMessage = new StringBuilder();
                foreach (var message in response.Messages)
                {
                    errorMessage.AppendLine($"Message status: {message.Status}");
                    foreach (var error in message.Errors)
                        errorMessage.AppendLine($"Error [{error.ErrorCode}] \"{error.ErrorMessage}\"");
                }
                _logger.LogError("Email send failed. {Errors}", errorMessage.ToString());
            }
            else
                _logger.LogInformation("Email send succeeded.");
        }
    }
}
