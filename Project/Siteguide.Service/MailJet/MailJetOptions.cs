﻿namespace SiteGuide.Service
{
    public class MailJetOptions
    {
        public string ApiKeyPublic { get; set; }
        public string ApiKeyPrivate { get; set; }
    }
}