﻿using System;
using System.Windows.Forms;
using System.Configuration;
using VHPASiteGuide.Properties;

namespace VHPASiteGuide
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void BtnBrowseBaseDir_Click(object sender, EventArgs e)
        {
            BrowseDirForTextBox(baseDir, "Base Directory");
        }

        private void BtnBrowseSourceDir_Click(object sender, EventArgs e)
        {
            BrowseDirForTextBoxWithBaseDir(sourceDir, "Source Directory");
        }

        private void BtnBrowseTargetDir_Click(object sender, EventArgs e)
        {
            BrowseDirForTextBoxWithBaseDir(targetDir, "Target Directory");
        }

        private void BtnBrowseIncludeDir_Click(object sender, EventArgs e)
        {
            BrowseDirForTextBoxWithBaseDir(includeDir, "Copy files from ...");
        }

        private static void BrowseDirForTextBox(TextBox textBox, string description)
        {
            using FolderBrowserDialog dirBrowser = new FolderBrowserDialog { SelectedPath = textBox.Text, Description = description };
            if (dirBrowser.ShowDialog() == DialogResult.OK)
                textBox.Text = dirBrowser.SelectedPath;
        }

        private void BrowseDirForTextBoxWithBaseDir(TextBox textBox, string description)
        {
            using FolderBrowserDialog dirBrowser = new FolderBrowserDialog { SelectedPath = GetFullDir(textBox.Text), Description = description };
            if (dirBrowser.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    textBox.Text = GetSubDir(dirBrowser.SelectedPath);
                }
                catch (ArgumentException e)
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void BtnGenerate_Click(object sender, EventArgs e)
        {
            Enabled = false;
            Generator generator = new Generator(GetFullDir(sourceDir.Text), GetFullDir(targetDir.Text));
            generator.ProgressEvent += ProgressEvent;
            generator.GenerateAll();
            generator.CopyIncludeDir(GetFullDir(includeDir.Text));
            Status("Done.");
            StatusNewLine();
            Enabled = true;
        }

        private void ProgressEvent(object sender, ProgressEventArgs progressEventArgs)
        {
            Status(progressEventArgs.Text);
        }

        private void StatusNewLine()
        {
            progressReport.AppendText(Environment.NewLine);
        }

        private void Status(string text)
        {
            progressReport.AppendText(DateTime.Now.ToLongTimeString() + "." + DateTime.Now.ToString("fff") + ": " + text + Environment.NewLine);
        }

        private string GetFullDir(string subDir)
        {
            return System.IO.Path.Combine(baseDir.Text, subDir);
        }

        private string GetSubDir(string fullDir)
        {
            if (fullDir == baseDir.Text)
                return "";
            if (fullDir.StartsWith(baseDir.Text + "\\"))
                return fullDir.Substring(baseDir.Text.Length + 1);
            else
                throw new ArgumentException("Not a subdirectory of the base directory.");
        }

        private void SaveSettings_Click(object sender, EventArgs e)
        {
            Settings.Default.Save();
        }

        private void BtnExtract_Click(object sender, EventArgs e)
        {
            Enabled = false;
            Generator generator = new Generator(GetFullDir(sourceDir.Text), GetFullDir(targetDir.Text));
            generator.ProgressEvent += ProgressEvent;
            generator.ExtractData();
            Status("Done.");
            StatusNewLine();
            Enabled = true;
        }

        private void BtnUpdateCAR166Data_Click(object sender, EventArgs e)
        {
            Enabled = false;

            using (var fileBrowser = new OpenFileDialog { Title = "Select CAR166 OpenAir airspace file" })
                if (fileBrowser.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        int count = new Generator(GetFullDir(sourceDir.Text), "").UpdateCAR166DataFromFile(fileBrowser.FileName);
                        Status(count + " CAR166 aerodromes found and written to VHFAerodromes.json.");
                    }
                    catch (ArgumentException ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Status("Error: " + ex.Message);
                    }

                    Status("Done.");
                    StatusNewLine();
                }

            Enabled = true;
        }

        private void BtnCanungraAreas_Click(object sender, EventArgs e)
        {
            Enabled = false;
            int count;

            using (var fileBrowserSource = new OpenFileDialog
            {
                Title = "Select Canungra areas kml",
                Filter = "kml files (*.kml)|*.kml|All files (*.*)|*.*",
                FileName = "https://www.google.com/maps/d/u/0/kml?mid=1h1Gv6VQcD7eqYd8xFSakgibMfWE&forcekml=1"
            })// Get source from http://www.google.com/maps/d/u/0/kml?mid=1h1Gv6VQcD7eqYd8xFSakgibMfWE
                if (fileBrowserSource.ShowDialog() == DialogResult.OK)
                    using (var fileBrowserTarget = new SaveFileDialog
                    {
                        Title = "Select target file",
                        DefaultExt = "xml"
                    })
                        if (fileBrowserTarget.ShowDialog() == DialogResult.OK)
                            try
                            {
                                count = (new Generator("", "")).ReadCanungraAreas(fileBrowserSource.FileName, fileBrowserTarget.FileName);
                                Status(count + " areas imported and written to target file.");
                            }
                            catch (ArgumentException ex)
                            {
                                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                Status("Error: " + ex.Message);
                            }

            Status("Done.");
            StatusNewLine();
            Enabled = true;
        }
    }
}
