﻿using LibGit2Sharp;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using VHPASiteGuide.Schema;
using Html2Markdown;
using Ganss.Xss;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SiteGuide.Service.Extensions;
using Version = SiteGuide.DAL.Models.Version;
using System.Numerics;
using System.Linq.Expressions;
using System.Drawing;
using SiteGuide.DAL;
using SiteGuide.Service;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

namespace DataMigrator
{
    public static class Importer
    {
        public static readonly IList<string> SiteDataTables = new[] {
            "SiteContact",
            "Video",
            "SeeAlso",
            "Launch",
            "SiteMapshape",
            "Site",
            "MapShape",
            "Area",
            "MapShapeCategory",
            "InsetMapPosition",
            "Contact",
            "VideoType",
        };

        public static async Task ImportFromGitAsync(string connectionString, string repoPath, string dataFileRelativePath, string branch)
        {
            Console.WriteLine($"Import {dataFileRelativePath} from {branch} branch of {repoPath}:");

            using var repo = new Repository(repoPath);
            var versions = repo.Commits.QueryBy(dataFileRelativePath, new CommitFilter { IncludeReachableFrom = branch });

            versions = versions.OrderBy(v => v.Commit.Committer.When);

            int count = versions.Count();
            Console.WriteLine($"{count} versions found.");

            var i = 0;
            foreach (var version in versions)
            {
                i++;
                var c = version.Commit;
                Console.WriteLine($"{DateTime.Now:HH:mm:ss} {i}/{count}: {c.Committer.When} \"{c.Message.TrimEnd('\r', '\n')}\"");

                string authorName = $"{c.Committer.Name} <{c.Committer.Email}>";
                DateTime commitTime = c.Committer.When.UtcDateTime;

                using (var context = new SiteGuideContext(connectionString))
                {
                    //if (context.EditVersion.AsNoTracking().Any(ev => ev.InitialisedTime == commitTime && ev.AuthorName == authorName))
                    // Temporary fix for previously wrongly imported time - revert to above line after next import
                    if (context.EditVersion
                        .Select(ev => new { ev.InitialisedTime, ev.AuthorName })
                        .ToList()
                        .Any(ev => (ev.InitialisedTime == commitTime
                            || DateTime.SpecifyKind(ev.InitialisedTime, DateTimeKind.Local).ToUniversalTime() == commitTime) 
                            && ev.AuthorName == authorName))
                    {
                        Console.WriteLine($"Skip: Already imported.");
                        continue;
                    }
                }

                var treeEntry = c[version.Path];

                Debug.Assert(treeEntry.TargetType == TreeEntryTargetType.Blob);
                var blob = (Blob)treeEntry.Target;

                var contentStream = blob.GetContentStream();

                ImportRaw(contentStream, connectionString, commitTime, authorName, c.Message, out int editVersionId);

                await ApplyAsync(connectionString, editVersionId, c.Message);

                //correct EditVersion.SubmittedTime, which was automatically set to the time of applying the edit.
                using (var context = new SiteGuideContext(connectionString))
                {
                    var editVersion = context.EditVersion.Single(e => e.Id == editVersionId);
                    editVersion.SubmittedTime = DateTime.SpecifyKind(editVersion.InitialisedTime, DateTimeKind.Utc);
                    context.SaveChanges();
                }
            }
        }

        //public static void ImportFromXml(string connectionString, Stream sourceStream, DateTime time, string author, string comment)
        //{
        //    ResetDatabase(connectionString);
        //    ImportRaw(sourceStream, connectionString, time, author, comment, out _);

        //    HtmlToMarkdown(connectionString);

        //    // this isn't complete, if needed, model off ImportFromGitAsync
        //}

        private static void ImportRaw(Stream sourceStream, string connectionString, DateTime time, string author, string comment, out int editVersionId)
        {
            new ImportJob(sourceStream, connectionString, time, author, comment)
                .Run(out editVersionId);
        }

        public static void ResetDatabase(string connectionString)
        {
            using var context = new SiteGuideContext(connectionString);

            //Clear out all data
            Console.WriteLine("Resetting DB tables.");
            ResetTables(context.Database, SiteDataTables, true);
            ResetTables(context.Database, new[] { "Version", "EditVersion", "EditVersionStatus" }, true);
            PopulateEditVersionStatus(context);
            ImportEnum<videoTypeEnumType, VideoType>(context.VideoType);
            ImportEnum<insetMapPositionEnumType, InsetMapPosition>(context.InsetMapPosition);
            ImportEnum<mapShapeCategoryEnumType, MapShapeCategory>(context.MapShapeCategory);
            context.SaveChanges();
        }

        internal static async Task RemoveUnappliedEditsAsync(string connectionString, bool onlySinceLastApplied = true)
        {
            using var context = new SiteGuideContext(connectionString);

            // Find last applied edit:
            var lastAppliedEditId = context.Version
                .Max<Version, int?>(v => v.EditId);

            // Find unapplied edits:
            var unappliedEditVersions = context.EditVersion
                .Where(ev =>
                    !context.Version.Any(v => v.EditId == ev.EditId)
                    && (!onlySinceLastApplied || ev.EditId > lastAppliedEditId))
                .ToList();

            // Delete them:
            foreach (var unappliedEditVersion in unappliedEditVersions) // This assumes import data only ever has a single edit version.
            {
                Console.WriteLine($"Removing unapplied edit {unappliedEditVersion.EditId}:\n{unappliedEditVersion.Comment}");
                await new VersionService(context).RemoveEditAsync(unappliedEditVersion.EditId);
            }
        }

        private static void ResetTables(DatabaseFacade database, IEnumerable<string> tables, bool resetAutoIncrementId = false)
        {
            foreach (var table in tables)
                ResetTable(database, table, resetAutoIncrementId);
        }

        private static void ResetTable(DatabaseFacade database, string table, bool resetAutoIncrementId = false)
        {
            //database.ExecuteSqlRaw($"DELETE [{table}]"); // Sql Server
            //            if (resetAutoIncrementId)
            //                database.ExecuteSqlRaw($@"
            //if exists (
            //    select * 
            //    from sys.objects o 
            //    inner join sys.columns c 
            //        on o.object_id = c.object_id 
            //    where c.is_identity = 1 
            //    and o.name = '{table}'
            //) DBCC CHECKIDENT ([{table}], RESEED, 0)");
            database.ExecuteSqlRaw($"DELETE FROM \"{table}\""); // Postgres
        }

        private static void PopulateEditVersionStatus(SiteGuideContext context)
        {
            context.EditVersionStatus.AddRange(EditVersionStatus.All);
            context.SaveChanges();
        }
        private static void ImportEnum<O, N>(DbSet<N> collection) where O : Enum where N : class, new()
        {
            var newItems = new List<N>();
            // This will currently not necessarily map the old ids to the new enum members. If this is required, change the enum id field to not be auto-increment and set manually.
            foreach (O item in Enum.GetValues(typeof(O)))
            {
                var newItem = new N();
                PropertyInfo nameProp = newItem.GetType().GetProperty("Name", BindingFlags.Public | BindingFlags.Instance);
                nameProp.SetValue(newItem, GetSourceEnumMemberFullName(item), null);
                newItems.Add(newItem);
            }

            switch (newItems)
            {
                case List<VideoType> videoTypes:
                    foreach (var videoType in videoTypes)
                    {
                        switch (videoType.Name)
                        {
                            case "YouTube":
                                videoType.EmbedUrl = "https://www.youtube.com/embed/%id";
                                videoType.LinkUrl = "https://www.youtube.com/watch?v=%id";
                                videoType.ThumbnailUrl = "https://img.youtube.com/vi/%id/mqdefault.jpg";
                                videoType.SortOrder = 1;
                                break;
                            case "Vimeo":
                                videoType.EmbedUrl = "https://player.vimeo.com/video/%id";
                                videoType.LinkUrl = "https://vimeo.com/%id";
                                videoType.SortOrder = 2;
                                break;
                            case "DailyMotion":
                                videoType.EmbedUrl = "https://www.dailymotion.com/embed/video/%id";
                                videoType.LinkUrl = "https://www.dailymotion.com/video/%id";
                                videoType.ThumbnailUrl = "https://www.dailymotion.com/thumbnail/video/%id";
                                videoType.SortOrder = 3;
                                break;

                            default: throw new ArgumentException("Unhandled Video Type.");
                        }
                    }
                    break;
                case List<MapShapeCategory> mapShapeCategories:
                    var props = new Dictionary<string, object> {
                        { "Landing", new { Dimensions= 2, Color= "#0000ff" } },
                        { "No Landing", new  { Dimensions= 2, Color= "#ff0000" } },
                        { "No Launching", new  { Dimensions= 2, Color= "#ff7f00" } },
                        { "Emergency Landing", new { Dimensions= 2, Color= "#ff7f00" }},
                        { "No Fly Zone", new { Dimensions= 2, Color= "#ff0000" }},
                        { "Hazard", new { Dimensions= 1, Color= "#ff0000" }},
                        { "Hazard Area", new { Dimensions= 2, Color= "#ff0000" }},
                        { "Feature", new { Dimensions= 2, Color= "#ff7f00" }},
                        { "Access", new { Dimensions= 1, Color= "#0000ff" }},
                        { "Powerline", new { Dimensions= 1, Color= "#ff0000" } },
                    };
                    foreach (var mapShapeCategory in mapShapeCategories)
                    {
                        if (!props.TryGetValue(mapShapeCategory.Name, out dynamic p))
                            throw new ArgumentException("Unhandled mapShape category.");

                        mapShapeCategory.Dimensions = (byte)p.Dimensions;
                        mapShapeCategory.Color = p.Color;
                    }
                    break;

                default:
                    break;
            }
            collection.AddRange(newItems);
        }

        private static List<Type> GetDataTypes(SiteGuideContext context, IEnumerable<string> tableNames)
        {
            const string _namespace = "SiteGuide.DAL.Models";

            return Assembly.GetAssembly(context.GetType()).GetTypes()
                            .Where(t =>
                                t.IsClass
                                && t.Namespace == _namespace
                                && t.ReflectedType?.Name != "SiteGuideContext"
                                && tableNames.Contains(t.Name)
                            ).ToList();
        }

        public static string GetSourceEnumMemberFullName<T>(T value) where T : Enum
        {
            var memInfo = typeof(T).GetMember(value.ToString()).Single();
            var fullNameAttribute = (System.Xml.Serialization.XmlEnumAttribute)memInfo.GetCustomAttributes(typeof(System.Xml.Serialization.XmlEnumAttribute), false).SingleOrDefault();
            return fullNameAttribute != null ? fullNameAttribute.Name : Enum.GetName(typeof(T), value);
        }

        //private static async Task ApplyAllAsync(string connectionString)
        //{
        //    List<EditVersion> editVersions;
        //    using (var context = new SiteGuideContext(connectionString))
        //    {
        //        editVersions = context.EditVersion
        //            .OrderBy(i => i.EditId)
        //            .ToList();
        //    }
        //    foreach (var editVersion in editVersions)
        //    {
        //        await ApplyAsync(connectionString, editVersion);
        //    }
        //}

        private static async Task ApplyAsync(string connectionString, int editVersionId, string comment)
        {
            EditVersion editVersion;
            using (var context = new SiteGuideContext(connectionString))
            {
                editVersion = context.EditVersion.Single(e => e.Id == editVersionId);
            }
            await ApplyAsync(connectionString, editVersion, comment);
        }

        private static async Task ApplyAsync(string connectionString, EditVersion editVersion, string comment)
        {
            AddDeletes(connectionString, editVersion.Id);

            using var context = new SiteGuideContext(connectionString);

            CheckKeysUnique(context.MapShape);

            Console.WriteLine($"Apply edit {editVersion.EditId}");
            var wereAnyChangesMade = await new VersionService(context).ApplyEditAsync(editVersion.EditId, comment);
            if (!wereAnyChangesMade)
                Console.WriteLine($"No records were changed by edit {editVersion.EditId} and no corresponding version was therefore created.");
        }

        /// <summary>
        /// Get any records that have the same ID whose validity periods overlap. There must be none. If there are, throw exeption.
        /// </summary>
        private static void CheckKeysUnique<T>(DbSet<T> dbSet) where T : class, IRecord, IId
        {
            var existingViolations = dbSet
                .Where(m => m.ValidFromVersion != null)
                .Join(
                    dbSet.Where(m => m.ValidFromVersion != null),
                    m => m.Id, m => m.Id, (m1, m2) => new { m1, m2 })
                .Where(j =>
                    j.m1.RecordId != j.m2.RecordId // Not the same record
                    && (j.m1.ValidFromVersion <= j.m2.ValidToVersion || j.m2.ValidToVersion == null)
                    && (j.m2.ValidFromVersion <= j.m1.ValidToVersion || j.m1.ValidToVersion == null));
            var newViolations = dbSet
                .Where(m => m.ValidFromVersion == null && m.IsDelete != true)
                .Join(
                    dbSet.Where(m => m.ValidFromVersion == null && m.IsDelete != true),
                    m => m.Id, m => m.Id, (m1, m2) => new { m1, m2 })
                .Where(j =>
                    j.m1.RecordId != j.m2.RecordId); // Not the same record

            if (existingViolations.Any())
                throw new Exception($"Multiple {typeof(T).Name}s with the same id found in existing data.");
            if (newViolations.Any())
                throw new Exception($"Multiple {typeof(T).Name}s with the same id found in new data.");
        }

        private static void AddDeletes(string connectionString, int editVersionId)
        {
            using var context = new SiteGuideContext(connectionString);
            // Add deletes: Add a delete record for each record where a current record does not have a matching record in this set.
            AddDeletes(editVersionId, context.Site);
            AddDeletes(editVersionId, context.Launch);
            AddDeletes(editVersionId, context.SeeAlso);
            AddDeletes(editVersionId, context.SiteContact);
            AddDeletes(editVersionId, context.SiteMapshape);
            AddDeletes(editVersionId, context.MapShape);
            AddDeletes(editVersionId, context.Contact);
            AddDeletes(editVersionId, context.Video);

            context.SaveChanges();
        }

        private static void AddDeletes<T>(int editVersionId, DbSet<T> dbSet) where T : class, IRecord, new()
        {
            var changes = dbSet
                .AsNoTracking()
                .Where(i => i.EditVersionId == editVersionId)
                .ToList();
            var deletes = dbSet.ValidAtCurrentVersion()
                .AsNoTracking()
                .ToList()
                .Where(x => !changes.Any(i => i.Key.Equals(x.Key)))
                .ToList()
                //.Select(d =>
                //{
                //    // As we're using .AsNoTracking(), we can alter the existing record and add it as a new deletion record.
                //    // If we were to instead create a fresh record with just the below properties set, we'll run into issues 
                //    // as the NOT NULL fields will be populated with default values, which may not conform to foreign key constraints.
                //    // E.g. Mapshape.AreaId would default to 0, and the save would fail as there is no area with id 0.
                //    d.RecordId = null;
                //    d.EditVersionId = editVersion.Id;
                //    d.IsDelete = true;
                //    d.Key = d.Key;
                //    d.ValidFromVersion = null;
                //    if (d.ValidToVersion != null)
                //        throw new ApplicationException("Unexpected state.");

                //    return d;
                //});
                .Select(d => new T
                {
                    EditVersionId = editVersionId,
                    IsDelete = true,
                    Key = d.Key,
                }
                    .CopyRecordContentFrom(d)
                );

            dbSet.AddRange(deletes);
        }
    }
}