﻿using VHPASiteGuide.Schema;
using Microsoft.EntityFrameworkCore;
using SiteGuide.DAL;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Text.RegularExpressions;
using SiteGuide.Service.Extensions;
using System.Reflection;
using Ganss.Xss;
using Html2Markdown;
using Clipper2Lib;
using Humanizer;

namespace DataMigrator
{
    public class ImportJob
    {
        private SiteGuideContext _context;
        private readonly DateTime _time;
        private readonly string _author;
        private readonly string _comment;
        private readonly Stream _sourceStream;
        private readonly string _connectionString;
        private readonly List<(mapShapeRefType mapShapeRef, Site targetSite)> _mapshapeRefs = new();
        private int _nextAreaId;
        private int _nextMapShapeId;
        private int _nextLaunchId;
        private int _nextVideoId;
        private int _nextContactId;
        private int _nextSeeAlsoId;
        private contactType[] _sourceContacts;
        //private StreamWriter _sanitizerLog;
        private readonly HtmlSanitizer _sanitizer = new();
        private readonly Converter _converter = new();
        private readonly HashSet<int> _mappedMapShapeIds = new();

        private Dictionary<string, int> _insetMapPositionDictionary;

        public int EditVersionId { get; private set; }

        public ImportJob(Stream sourceStream, string connectionString, DateTime time, string author, string comment)
        {
            _sourceStream = sourceStream;
            _connectionString = connectionString;
            _time = time;
            _author = author;
            _comment = comment;

            _sanitizer.AllowedSchemes.Add("mailto");
            _sanitizer.RemovingTag += Sanitizer_RemovingTag;
            _sanitizer.RemovingAttribute += Sanitizer_RemovingAttribute;
        }

        public void Run(out int editVersionId)
        {
            var ser = new System.Xml.Serialization.XmlSerializer(typeof(siteGuideDataType));
            //_sanitizerLog = new StreamWriter(@"C:\temp\SiteGuideSanitizer.log", true);

            var sourceData = (siteGuideDataType)ser.Deserialize(new StreamReader(_sourceStream));

            _context = new SiteGuideContext(new DbContextOptionsBuilder<SiteGuideContext>()
                .UseNpgsql(_connectionString)
                .EnableSensitiveDataLogging()
                .Options);

            _nextAreaId = (_context.Area.Max<Area, int?>(s => s.Id) ?? 0) + 1;
            _nextMapShapeId = (_context.MapShape.Max<MapShape, int?>(s => s.Id) ?? 0) + 1;
            _nextLaunchId = (_context.Launch.Max<Launch, int?>(s => s.Id) ?? 0) + 1;
            _nextVideoId = (_context.Video.Max<Video, int?>(s => s.Id) ?? 0) + 1;
            _nextContactId = (_context.Contact.Max<Contact, int?>(s => s.Id) ?? 0) + 1;
            _nextSeeAlsoId = (_context.SeeAlso.Max<SeeAlso, int?>(s => s.Id) ?? 0) + 1;

            Import(sourceData);

            _context.Dispose();
            _context = null;

            editVersionId = EditVersionId;
            //Console.WriteLine("Import complete.");
            //Console.WriteLine();
            //_sanitizerLog = null;
        }

        private void Import(siteGuideDataType sourceData)
        {
            //Console.WriteLine("Creating change record.");

            var lastEditId = _context.EditVersion
                .Max<EditVersion, int?>(i => i.EditId);

            var editVersion = new EditVersion()
            {
                EditId = (lastEditId ?? 0) + 1,
                EditVersionNumber = 1,
                InitialisedTime = _time,
                SubmittedTime = _time,
                AuthorName = _author,
                AuthorRole = "Editor", // --All imported edits made by Jan, just hard code this.-- Not true anymore - may want to change.
                Comment = _comment,
                StatusId = "A",
            };
            _context.EditVersion.Add(editVersion);
            _context.SaveChanges();

            EditVersionId = editVersion.Id;
            //var lastVersionId = _context.Version
            //    .Max<SiteGuide.DAL.Models.Version, int?>(v => v.Id);

            //var importToVersion = new SiteGuide.DAL.Models.Version()
            //{
            //    Id = (lastVersionId ?? 0) + 1,
            //    EditId = editVersion.EditId,
            //};
            //_context.Version.Add(importToVersion);
            //_context.SaveChanges();
            //ImportToVersionId = importToVersion.Id;

            _insetMapPositionDictionary = _context.InsetMapPosition.ToDictionary(i => i.Name.Dehumanize(), i => i.Id);

            _sourceContacts = sourceData.contact;

            _context.Contact.AddRange(
                sourceData.contact
                .Select(c =>
                {
                    var existingContact = _context.Contact
                        .Where(i => i.ValidFromVersion != null)
                        .ToList()
                        .FirstOrDefault(t => IsMatchingContact(c, t)); // TODO: May want to .Select(i=>i.Id).Distinct.SingleOrDefault() to ensure uniqueness
                    var contact = new Contact()
                    {
                        EditVersionId = EditVersionId,
                        IsDelete = false,
                        Id = existingContact != null ? existingContact.Id : _nextContactId++,
                        Name = c.name,
                        FullName = c.fullname,
                        Link = c.link,
                        Phone = c.phone == null ? null : string.Join(",", c.phone.Where(p => p != null)),
                        Address = c.address,
                        SafaClubId = c.safaClubId,
                    };
                    HtmlToMarkdown(contact);

                    return contact;
                }));
            _context.SaveChanges();

            //Console.WriteLine("Importing data.");

            var targetAreaId = CreateArea(sourceData, null);
            AddTree(sourceData, targetAreaId);
            Console.WriteLine();

            //This doesn't handle root mapshapes (but none exist)
            //TODO: Unique constraints

            var mapshapeRefsFromSites = new List<SiteMapshape>();
            foreach (var (mapShape, targetSite) in _mapshapeRefs.Where(r => r.mapShapeRef.site != null).ToList())
            {
                var whatIsThisSite = _context.Site.Where(o => o.EditVersionId == EditVersionId).Single(s => s.Name == mapShape.site); // Not always the same as target site - TODO: name meaningfully

                mapshapeRefsFromSites.AddRange(
                    _context.SiteMapshape
                        .Where(o => o.EditVersionId == EditVersionId)
                        .Where(sm => sm.SiteId == whatIsThisSite.Id)
                    .Select(m => new SiteMapshape()
                    {
                        EditVersionId = EditVersionId,
                        IsDelete = false,
                        MapshapeId = m.MapshapeId,
                        SiteId = _context.Site
                            .Where(o => o.EditVersionId == EditVersionId).Single(s => s.Name == targetSite.Name)
                            .Id,
                    })
               );
            }
            _context.SiteMapshape.AddRange(mapshapeRefsFromSites);

            foreach (var (mapShape, targetSite) in _mapshapeRefs.Where(r => r.mapShapeRef.mapShape != null))
            {
                _context.SiteMapshape.Add(new SiteMapshape()
                {
                    EditVersionId = EditVersionId,
                    IsDelete = false,

                    SiteId = targetSite.Id,
                    MapshapeId = _context.MapShape
                        .Where(o => o.EditVersionId == EditVersionId)
                        .Single(s => s.Name == mapShape.mapShape)
                        .Id,
                });
            }

            _context.SaveChanges();
        }

        private static bool IsMatchingContact(contactType newContact, Contact existingContact)
            => existingContact.Name == newContact.name
            || (existingContact.FullName != null && existingContact.FullName == newContact.fullname)
            || (existingContact.SafaClubId != null && existingContact.SafaClubId == newContact.safaClubId);

        private int CreateArea(areaType sourceArea, int? targetParentAreaId)
        {
            int? adminId = string.IsNullOrEmpty(sourceArea.admin)
                ? (int?)null
                : _context.Contact
                    .Where(c => c.EditVersionId == EditVersionId)
                    .ToList()
                    .Single(c => IsMatchingContact(_sourceContacts.Single(a => a.name == sourceArea.admin), c))
                    .Id;

            string sourceAreaName = sourceArea.name ?? "Australia";
            Debug.Assert(!_context.Area.Any(a => a.EditVersionId == EditVersionId && a.Name == sourceAreaName));

            var existingArea = _context.Area
                .ValidAtAnyVersion()
                .FirstOrDefault(a => a.Name == sourceAreaName);

            var targetArea = new Area
            {
                EditVersionId = EditVersionId,
                IsDelete = false,

                Id = existingArea?.Id ?? _nextAreaId++,
                Name = sourceAreaName,
                FullName = sourceArea.fullName,
                ParentAreaId = targetParentAreaId,
                AdminId = adminId,
            };
            _context.Area.Add(targetArea);

            _context.SaveChanges();

            return targetArea.Id;
        }

        //private static int NextAutoIncrementId<T>(DbSet<T> entity) where T : class
        //{
        //    PropertyInfo idProp = typeof(T).GetProperty("Id", BindingFlags.Public | BindingFlags.Instance);
        //    return entity.Any() ? entity
        //        .AsEnumerable()
        //        .Max(c => (int)idProp.GetValue(c)) + 1 : 1;
        //}

        private void AddTree(areaType sourceParentArea, int targetParentAreaId)
        {
            Console.Write(".");
            if (sourceParentArea.mapShape != null)
            {
                // add area mapshapes
                _context.MapShape.AddRange(sourceParentArea.mapShape.Select(s => CreateMapshape(s, targetParentAreaId)));
                _context.SaveChanges();
            }

            if (sourceParentArea.siteFreeFlight != null)
                foreach (var sourceSite in sourceParentArea.siteFreeFlight)
                {
                    var importedContacts = _context.Contact
                        .Where(o => o.EditVersionId == EditVersionId)
                        .ToList();
                    var contacts = (sourceSite.contactName ?? new string[0])
                        .Select(c => importedContacts.Single(e => IsMatchingContact(_sourceContacts.Single(a => a.name == c), e)))
                        .ToList();
                    var responsibles = (sourceSite.responsibleName ?? new string[0])
                        .Select(c => importedContacts.Single(e => IsMatchingContact(_sourceContacts.Single(a => a.name == c), e)))
                        .ToList();

                    var targetSite = new Site()
                    {
                        EditVersionId = EditVersionId,
                        IsDelete = false,
                        Id = Lookups.SiteIds[sourceSite.name],
                        AreaId = targetParentAreaId,
                        Name = sourceSite.name,
                        Description = sourceSite.description,
                        Closed = sourceSite.closed,
                        Conditions = sourceSite.conditions,
                        Type = sourceSite.type,
                        Height = sourceSite.height,
                        Rating = sourceSite.rating,
                        Landowners = sourceSite.landowners,
                        ShortLocation = sourceSite.shortlocation,
                        ExtendedLocation = sourceSite.extendedlocation,
                        Restrictions = sourceSite.restrictions,
                        Takeoff = sourceSite.takeoff,
                        Landing = sourceSite.landing,
                        FlightComments = sourceSite.flightcomments,
                        HazardsComments = sourceSite.hazardscomments,
                        InsetMapPositionId = _insetMapPositionDictionary[sourceSite.insetMapPosition.ToString()],
                        SmallPicture = sourceSite.smallpicture,
                        SmallPictureHref = sourceSite.smallpicturehref,
                        SmallPictureTitle = sourceSite.smallpicturetitle,
                    };
                    HtmlToMarkdown(targetSite);
                    _context.Site.Add(targetSite);
                    _context.SaveChanges();

                    if (sourceSite.seeAlso != null)
                    {
                        _context.SeeAlso.AddRange(
                            sourceSite.seeAlso
                            .Select(s =>
                            {
                                var existingSeeAlso = _context.SeeAlso
                                    .Where(i => i.ValidFromVersion != null)
                                    .FirstOrDefault(x =>
                                        x.SiteId == targetSite.Id
                                        && (x.Name == s.name
                                            || x.Link == s.link)
                                    );

                                return new SeeAlso()
                                {
                                    EditVersionId = EditVersionId,
                                    IsDelete = false,
                                    Id = existingSeeAlso != null ? existingSeeAlso.Id : _nextSeeAlsoId++,
                                    SiteId = targetSite.Id,
                                    Name = s.name,
                                    Link = s.link,
                                    IsEntryStubForThis = s.entryIsStubForThis
                                };
                            })
                            .ToList());
                    }

                    _context.Launch.AddRange(
                        sourceSite.launch
                        ?.Select(l =>
                        {
                            Launch existingLaunch;

                            // If there is one existing and one new lauch for this site, assume they are the same launch.
                            var siteExistingLaunches = _context.Launch
                                .ValidAtCurrentVersion()
                                .Where(l => l.SiteId == targetSite.Id);

                            if (sourceSite.launch.Length == 1 && siteExistingLaunches.Count() == 1)
                                existingLaunch = siteExistingLaunches.Single();
                            else
                                existingLaunch = _context.Launch
                                    .Where(i => i.ValidFromVersion != null)
                                    .Where(x =>
                                        x.SiteId == targetSite.Id
                                        && (
                                                (x.Lat == (decimal)l.lat && x.Lon == (decimal)l.lon)
                                            || (!string.IsNullOrWhiteSpace(x.Name) && x.Name == l.name)
                                        )
                                    )
                                    .OrderByDescending(x => x.Lat == (decimal)l.lat && x.Lon == (decimal)l.lon) // Prioritise coord matches over name matches. This is here because the names of the two Gundowring launches were swapped in edit "Gundowring: Amended as per Roger and Mark's emails.".
                                    .FirstOrDefault();

                            var launch = new Launch()
                            {
                                EditVersionId = EditVersionId,
                                IsDelete = false,
                                Id = existingLaunch != null ? existingLaunch.Id : _nextLaunchId++,
                                SiteId = targetSite.Id,
                                Name = l.name,
                                Description = l.description,
                                Closed = l.closed,
                                Conditions = l.conditions,
                                EstaEmergencyMarker = l.estaEmergencyMarker,
                                Lat = (decimal)l.lat,
                                Lon = (decimal)l.lon,
                            };
                            HtmlToMarkdown(launch);
                            return launch;
                        })
                        ?.ToList()
                     );
                    if (sourceSite.video != null)
                    {
                        _context.Video.AddRange(sourceSite.video
                            .Select(v =>
                            {
                                int videoTypeId = _context.VideoType
                                    .Single(t => t.Name == Enum.GetName(typeof(videoTypeEnumType), v.type))
                                    .Id;
                                var existingVideo = _context.Video
                                    .Where(i => i.ValidFromVersion != null)
                                    .FirstOrDefault(x =>
                                        x.SiteId == targetSite.Id
                                        && x.TypeId == videoTypeId
                                        && x.Identifier == v.id);

                                return new Video()
                                {
                                    EditVersionId = EditVersionId,
                                    IsDelete = false,
                                    Id = existingVideo != null ? existingVideo.Id : _nextVideoId++,
                                    SiteId = targetSite.Id,
                                    TypeId = videoTypeId,
                                    Identifier = v.id
                                };
                            })
                            .ToList()
                        );
                    }
                    using (var transaction = _context.Database.BeginTransaction())
                    {
                        // Sql Server:
                        //_context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT [dbo].[Site] ON");
                        //_context.SaveChanges();
                        //_context.Database.ExecuteSqlRaw("SET IDENTITY_INSERT [dbo].[Site] OFF");

                        // Postgres:
                        _context.SaveChanges();
                        _context.Database.ExecuteSqlRaw("select setval(pg_get_serial_sequence('\"Site\"', 'RecordId'), (select max(\"RecordId\") from \"Site\"));");

                        transaction.Commit();
                    }

                    var siteContacts = contacts
                        .Select(c => new SiteContact()
                        {
                            EditVersionId = EditVersionId,
                            IsDelete = false,
                            ContactId = c.Id,
                            SiteId = targetSite.Id,
                            IsContact = true,
                            IsResponsible = responsibles.Any(r => r.Name == c.Name),
                        });

                    var responsiblesThatArentContacts = responsibles
                          .Where(r => !contacts.Any(c => c.Name == r.Name))
                          .Select(r => new SiteContact()
                          {
                              EditVersionId = EditVersionId,
                              IsDelete = false,
                              ContactId = r.Id,
                              SiteId = targetSite.Id,
                              IsContact = false,
                              IsResponsible = true,
                          });

                    if (responsiblesThatArentContacts != null)
                        siteContacts = siteContacts == null ? responsiblesThatArentContacts : siteContacts.Concat(responsiblesThatArentContacts);

                    _context.SiteContact.AddRange(
                        siteContacts?.ToList()
                    );

                    _context.SaveChanges();

                    // Add site mapshapes
                    if (sourceSite.mapShape != null)
                        foreach (var sourceMapshape in sourceSite.mapShape)
                        {
                            MapShape mapshape = CreateMapshape(sourceMapshape, targetParentAreaId);
                            _context.MapShape.Add(mapshape);
                            _context.SaveChanges();

                            _context.SiteMapshape.Add(new SiteMapshape()
                            {
                                EditVersionId = EditVersionId,
                                IsDelete = false,
                                MapshapeId = mapshape.Id,
                                SiteId = targetSite.Id
                            });
                            _context.SaveChanges();
                        }

                    if (sourceSite.mapShapeRef != null)
                    {
                        var shapeRefs = (sourceSite.mapShapeRef.Select(r => (r, targetSite)));
                        _mapshapeRefs.AddRange(shapeRefs);
                    }
                }

            if (sourceParentArea.area != null)
                foreach (var sourceArea in sourceParentArea.area)
                {
                    var targetAreaId = CreateArea(sourceArea, targetParentAreaId);
                    AddTree(sourceArea, targetAreaId);
                }
        }

        private MapShape CreateMapshape(mapShapeType sourceMapshape, int targetParentAreaId)
        {
            var existingMapshape = GetExistingMapshape(sourceMapshape, targetParentAreaId);
            if (existingMapshape != null && !_mappedMapShapeIds.Add(existingMapshape.Id))
                throw new Exception($"Mapshape {existingMapshape.Id} has already been mapped to.");

            var mapShape = new MapShape()
            {
                EditVersionId = EditVersionId,
                IsDelete = false,

                Id = existingMapshape != null ? existingMapshape.Id : _nextMapShapeId++,
                CategoryId = _context.MapShapeCategory
                    .Single(c => c.Name.Replace(" ", "") == sourceMapshape.category.ToString())
                    .Id,
                Name = sourceMapshape.name?.Trim(),
                Description = sourceMapshape.description?.Trim(),
                EstaEmergencyMarker = sourceMapshape.estaEmergencyMarker?.Trim(),
                Coordinates = GetSourceCoordsString(MapShape.GetCoordinates(sourceMapshape.coordinates)),
                AreaId = targetParentAreaId,
            };
            HtmlToMarkdown(mapShape);
            return mapShape;
        }

        private MapShape GetExistingMapshape(mapShapeType sourceMapshape, int targetParentAreaId)
        {
            //int[] landingCategories = { (int)mapShapeCategoryEnumType.Landing, (int)mapShapeCategoryEnumType.NoLanding, (int)mapShapeCategoryEnumType.EmergencyLanding };

            var sourceCoords = MapShape.GetCoordinates(sourceMapshape.coordinates);
            var sourceCoordsString = GetSourceCoordsString(sourceCoords);

            // Try to find an exact coordinates or ESTA match first and use that if it exists. This would cover most cases.
            var easyMatch = _context.MapShape
                .Where(i => i.ValidFromVersion != null)
                .Where(m =>
                    (sourceCoordsString != null && m.Coordinates == sourceCoordsString)
                    || (m.EstaEmergencyMarker != null && m.EstaEmergencyMarker == sourceMapshape.estaEmergencyMarker))
                .OrderByDescending(m => m.Category.Name.Replace(" ", "") == sourceMapshape.category.ToString())
                .FirstOrDefault();

            // Special case - "Sites: Ben Nevis: Marked southern part of bombout as closed - it's been sold." - landing split into landing half and no landing half but both retain ESTA marker. Reject match of new no landing half to old landing.
            if (sourceMapshape.estaEmergencyMarker == "BNS200")
                easyMatch = null;

            if (easyMatch != null)
                return easyMatch;

            // No exact coordinates or ESTA match, try to match on other criteria:
            var areaCompatibleMapshapes = _context.MapShape
                .Include(s => s.Category)
                .Where(x => x.ValidFromVersion != null // Valid at some time
                    && x.AreaId == targetParentAreaId // Same area
                    && (x.Category.Name.Replace(" ", "") == sourceMapshape.category.ToString()
                        || (x.Category.Name.Contains("Landing") && sourceMapshape.category.ToString().Contains("Landing")) // Compatible category

                        //|| (landingCategories.Contains(x.CategoryId) && landingCategories.Contains((int)sourceMapshape.category))
                        )
                    )
                .ToList();

            // Match on name (exclude some non-unique cases)
            var nameMatch = areaCompatibleMapshapes
                .OrderByDescending(m => m.Category.Name.Replace(" ", "") == sourceMapshape.category.ToString()) // Prefer matched category
                .FirstOrDefault(m => m.Name != null && m.Name != "Paragliders" && m.Name != "Hang gliders" && m.Name != "Bombout" && m.Name == sourceMapshape.name);
            if (nameMatch != null)
                return nameMatch;

            // Special cases. These were split from another mapshape and should be created new if not matched above.
            // We could make this smarter by matching both directions and favour the new mapshape with the same category that shares the greater area. 
            if (new[] { "Tomasoni", "Donahue's", "David Dart", "Merret", "Pollock", "King’s Paddock", "Temporary Skahill's LZ alternative", "Pat Mac's LZ" }.Contains(sourceMapshape.name)
                || sourceCoordsString == "149.374146,-35.100864 149.373539,-35.100652 149.373624,-35.100473 149.373833,-35.100337 149.374107,-35.100289 149.375045,-35.100396 149.37535,-35.100322 149.375827,-35.100054 149.376138,-35.099938 149.376395,-35.099979 149.376608,-35.100104 149.37733,-35.099804 149.377516,-35.099803 149.377876,-35.099999 149.378362,-35.100058 149.378783,-35.100193 149.37937,-35.100273" // Lake George access road split. This is the loosing path.
                || sourceCoordsString == "146.636324,-36.425219 146.633921,-36.434887 146.63847,-36.435647 146.639586,-36.434956 146.645164,-36.435509 146.647825,-36.436544 146.652632,-36.435371 146.65658,-36.436475 146.659927,-36.435232 146.661043,-36.42957 146.636324,-36.425219" // Eagle Rise new no landing area, overlaps the edge of the main no landing area but is a new mapshape.
                || sourceCoordsString == "146.660603,-36.418252 146.65921,-36.421735 146.662724,-36.422311 146.661164,-36.429613 146.667838,-36.430792 146.670351,-36.42951 146.675654,-36.429655 146.677991,-36.420999 146.660603,-36.418252" // Eagle Rise new no landing area, overlaps the edge of the main no landing area but is a new mapshape.
                || sourceCoordsString == "145.404264,-37.144049 145.40688,-37.145689 145.407456,-37.145034 145.404936,-37.143444 145.404264,-37.144049" // Broughton new HG bombout - within a larger no landing area.
                || sourceCoordsString == "145.410197,-37.140274 145.411557,-37.142595 145.411505,-37.145032 145.413754,-37.144911 145.417443,-37.137346 145.412297,-37.137088 145.410197,-37.140274" // Broughton no landing - was split into two separate mapshapes (for no good reason)
                || sourceMapshape.description == "2017-01-14 to 22: Temporary landing area for the 2017 Bright Open"
                || sourceMapshape.description == "2017-01-14 to 22: Temporary no landing area for the 2017 Bright Open"
                )
                return null;

            if (sourceMapshape.name == "Sarabah Valley power line") // Special case
                return _context.MapShape
                    .Single(m => m.Description == "A powerline runs east-west high across the Sarabah valley, from ridge to ridge. It is to the left of launch, about 1.5km NE. The marked location is only very approximate.");

            // Do mapshapes for this area with compatible categories intersect? If zero, no match. If one, match. If multiple, match single that has same category. Should never have multiple.
            if (sourceCoords == null)
                return null;

            var intersectingMapshapes = areaCompatibleMapshapes
                .Select(m => new { MapShape = m, measureIntersection = MeasureIntersection(m, sourceCoords) })
                .Where(r => r.measureIntersection >= 0)
                .ToList();

            if (intersectingMapshapes.Count == 0)
                return null;

            // Out of these, find the most recent set
            var mostRecentSet = intersectingMapshapes
                .GroupBy(m => m.MapShape.ValidToVersion)
                .OrderBy(g => g.Key == null ? int.MaxValue : g.Key) // prefer more current - currently valid is best.
                .First()
                .ToList();

            if (mostRecentSet.Count == 1)
                return mostRecentSet.Single().MapShape;

            var matchesByCategory = mostRecentSet
                .Where(m => m.MapShape.Category.Name.Replace(" ", "") == sourceMapshape.category.ToString())
                .ToList();

            if (matchesByCategory.Count == 1)
                return matchesByCategory.Single().MapShape;

            var bestByNameThenArea = matchesByCategory
                .OrderByDescending(r => r.MapShape.Name == sourceMapshape.name)
                .ThenByDescending(r => r.measureIntersection)
                .Select(r => r.MapShape)
                .ToList();

            return bestByNameThenArea.FirstOrDefault(); // Normally just SingleOrDefault, but need to just pick one for mapshape id 92, 93, edit 93
        }

        /// <summary>
        /// Returns a measure of the size of the intersecting area. 0 for touching areas. -1 for no intersection.
        /// </summary>
        /// <returns></returns>
        private double MeasureIntersection(MapShape m, List<List<Tuple<decimal, decimal>>> sourcePolys)
        {
            if (m.CoordinatesList == null)
                return -1;

            var sourcePaths = GetClipperPaths(sourcePolys);
            var mapshapePaths = GetClipperPaths(m.GetCoordinates());
            var intersection = Clipper.Intersect(sourcePaths, mapshapePaths, FillRule.NonZero, 7);

            if (intersection.Count == 0)
                return -1;

            return intersection.Sum(p => Math.Abs(Clipper.Area(p)));
        }

        private static PathsD GetClipperPaths(List<List<Tuple<decimal, decimal>>> sourcePolys)
        {
            var sourcePaths = new PathsD();
            foreach (var sourcePoly in sourcePolys)
            {
                var sourcePath = new PathD();
                sourcePath.AddRange(sourcePoly.Select(p => new PointD((double)p.Item1, (double)p.Item2)));
                sourcePaths.Add(sourcePath);
            }

            return sourcePaths;
        }

        private static string GetSourceCoordsString(List<List<Tuple<decimal, decimal>>> sourceCoords)
        {
            return sourceCoords == null || sourceCoords.Count == 0
                                ? null
                                : string.Join('|', sourceCoords.Select(p => string.Join(" ", p.Select(c => $"{c.Item2.Normalize()},{c.Item1.Normalize()}"))));
        }

        //public static void CopyProperties<TParent, TChild>(TParent parent, TChild child)
        //    where TParent : class
        //    where TChild : class
        //{
        //    var parentProperties = parent.GetType().GetProperties().Where(p =>
        //        p.PropertyType.Namespace != _namespace
        //        && p.PropertyType.Namespace != "System.Collections.Generic"
        //        && p.CanWrite
        //        && !(new[] { "RecordId", "EditVersionId", "IsDelete", "ValidFromVersion", "ValidToVersion" }.Contains(p.Name))
        //    );
        //    var childProperties = child.GetType().GetProperties().Where(p => p.PropertyType.Namespace != _namespace);

        //    foreach (var parentProperty in parentProperties)
        //    {
        //        bool propertyFound = false;
        //        foreach (var childProperty in childProperties)
        //        {
        //            if (parentProperty.Name == childProperty.Name && parentProperty.PropertyType == childProperty.PropertyType)
        //            {
        //                propertyFound = true;
        //                childProperty.SetValue(child, parentProperty.GetValue(parent));
        //                break;
        //            }
        //        }
        //        if (!propertyFound)
        //            throw new Exception($"Property \"{parentProperty.Name}\" not found.");
        //    }
        //}

        public void HtmlToMarkdown(object row)
        {
            var stringFields = row.GetType()
                .GetProperties()
                .Where(p => p.PropertyType == typeof(string));

            foreach (var field in stringFields)
            {
                PropertyInfo prop = row.GetType().GetProperty(field.Name);
                string curVal = (string)prop.GetValue(row, null);

                if (curVal == null)
                    continue;

                curVal = Regex.Replace(curVal, @"\s\s+", " ")  // Collapse repeated whitespace
                    .Trim();
                bool curValHasHtml = (curVal != null && curVal.Contains("<") && curVal.Contains(">")); // I know, I know. Good enough.

                if (IsHtmlField(field))
                {
                    if (!curValHasHtml)
                        curVal = $"<p>{curVal}</p>";

                    curVal = curVal.Replace("<a/>", "</a>"); // Fix for typo: "<a href="https://sunshinecoastsportsaviators.com.au/wp-content/uploads/Point-Cartwright-Exemption.pdf">HERE<a/>"
                    string sanitized = _sanitizer.Sanitize(curVal);
                    //if (sanitized != curVal)
                    //{
                    //    _sanitizerLog.WriteLine($"{row.GetType().Name},{field.Name},before,\"{curVal.Replace("\"", "\"\"")}\"");
                    //    _sanitizerLog.WriteLine($"{row.GetType().Name},{field.Name},after,\"{sanitized.Replace("\"", "\"\"")}\"");
                    //}
                    string markdown = _converter.Convert(sanitized);

                    prop.SetValue(row, markdown);
                    //if (curValHasHtml)
                    //    isHtmlFoundInHtmlField[field.Name] = true;
                }
                else
                {
                    prop.SetValue(row, curVal);

                    //Console.Write($"Checking no markup in {entity.GetType().Name}.{field}: ");

                    if (curValHasHtml)
                        Console.WriteLine($"Checking no markup in {row.GetType().Name}.{field.Name}: Found html: {curVal}");
                }
            }
        }

        private static bool IsHtmlField(PropertyInfo fieldInfo)
            => !fieldInfo.CustomAttributes.Any(a => a.AttributeType.Name == "StringLengthAttribute") && fieldInfo.Name != "Coordinates";

        //private static void HtmlToMarkdown(string connectionString)
        //{
        //    Console.WriteLine("Converting HTML to Markdown.");

        //    using var context = new SiteGuideContext(connectionString);

        //    foreach (var table in GetDataTypes(context, SiteDataTables))
        //    {
        //        PropertyInfo tableProp = context.GetType().GetProperty(table.Name);
        //        var rows = (IEnumerable<object>)tableProp.GetValue(context, null);
        //        //var stringFields = tableProp.PropertyType.GetGenericArguments()[0].GetProperties().Where(p => p.PropertyType == typeof(String));
        //        //var isHtmlFoundInHtmlField = new Dictionary<string, bool>(
        //        //    stringFields
        //        //        .Where(f => IsHtmlField(f))
        //        //        .Select(f => new KeyValuePair<string, bool>(f.Name, false))
        //        //);

        //        foreach (var row in rows)
        //            HtmlToMarkdown(row);

        //        //if (isHtmlFoundInHtmlField.Any(i => !i.Value))
        //        //    Console.WriteLine($"Found no html in {table.Name}: {string.Join(", ", isHtmlFoundInHtmlField.Where(i => !i.Value).Select(i => i.Key))}");
        //    }

        //    context.SaveChanges();
        //}

        private static void Sanitizer_RemovingAttribute(object sender, RemovingAttributeEventArgs e)
        {
            if (IsEmbeddedGmapOrYoutube(e.Tag) && new[] { "frameborder", "allowfullscreen" }.Contains(e.Attribute.Name))
                e.Cancel = true;
            else if (new[] { "hoodies", "inline-left", "inline-right", "alertIcon" }.Contains(e.Tag.ClassName) || e.Attribute.Name == "s%20point%20beach.html'")
                e.Cancel = false;
            else
            {
                // check
            }
        }

        private static void Sanitizer_RemovingTag(object sender, RemovingTagEventArgs e)
        {
            if (IsEmbeddedGmapOrYoutube(e.Tag))
                e.Cancel = true;
            else
            {
            }
        }
        private static bool IsEmbeddedGmapOrYoutube(AngleSharp.Dom.IElement e)
        {
            string src = e.GetAttribute("src");
            return e.NodeName.ToUpperInvariant() == "IFRAME" && (src.StartsWith("https://www.google.com/maps/embed?") || src.StartsWith("https://www.youtube.com/embed/"));
        }
    }
}
