﻿using CommandLine;
using LibGit2Sharp;
using Microsoft.EntityFrameworkCore;
using SiteGuide.DAL.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataMigrator
{
    class Program
    {
        // Repos:
        // "C:\Users\Jan\Coding Projects\Site guide";
        // "C:\Users\Jan\Coding Projects\vhpa-site-guide-archive-only";
        class Options
        {
            [Option('c', "connectionstring", Required = true)]
            public string ConnectionString { get; set; }

            [Option('r', "repopath", Required = true)]
            public string RepoPath { get; set; }

            [Option('b', "branch", Default = "master")]
            public string Branch { get; set; }

            [Option('d', "datafilerelativepath", Default = "Project/Generator/SiteGuideData/siteGuideData.xml")]
            public string DataFileRelativePath { get; set; }

            [Option('w', "wipe", Default = false, HelpText = "Wipe the database before import. If false, we will just remove any uncommitted edits before import.")]
            public bool Wipe { get; set; }

            [Option('s', "suppressconfirmation", Default = false, HelpText = "Will ask for confirmation only if false.")]
            public bool SuppressConfirmation { get; set; }
        }

        static async Task Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            Parser.Default.ParseArguments<Options>(args)
               .WithParsed<Options>(async options =>
                   {
                       if (!options.SuppressConfirmation)
                       {
                           Console.WriteLine($"Press 'g' key to re-import site guide data. Wipe all current data: {options.Wipe}. Press any other key to abort.");
                           var key = Console.ReadKey();
                           if (key.KeyChar != 'g')
                           {
                               Console.WriteLine("\nAborting.");
                               return;
                           }
                           Console.WriteLine();
                       }
                       if (options.Wipe)
                           Importer.ResetDatabase(options.ConnectionString);

                       await Importer.RemoveUnappliedEditsAsync(options.ConnectionString);

                       try
                       {
                           await Importer.ImportFromGitAsync(options.ConnectionString, options.RepoPath, options.DataFileRelativePath, options.Branch);
                       }
                       catch (Exception ex)
                       {
                           Console.WriteLine(ex.ToString());
                           throw;
                       }
                   });
        }

        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.ExceptionObject.ToString());
            Console.WriteLine("Press Enter to continue");
            Console.ReadLine();
            Environment.Exit(1);
        }
    }
}

