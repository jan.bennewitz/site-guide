﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SiteGuide.DAL.Models;
using Version = SiteGuide.DAL.Models.Version;

namespace SiteGuide.DAL;

public partial class SiteGuideContext : IdentityDbContext<IdentityUser>
{
    public SiteGuideContext(DbContextOptions<SiteGuideContext> options)
        : base(options)
    {
    }

         public SiteGuideContext(string connectionString) : base(new DbContextOptionsBuilder<SiteGuideContext>().UseNpgsql(connectionString).Options) { }

    public virtual DbSet<Area> Area { get; set; }

    public virtual DbSet<BackgroundTask> BackgroundTask { get; set; }

    public virtual DbSet<CacheUserClaimableEdits> CacheUserClaimableEdits { get; set; }

    public virtual DbSet<CacheUserClaimableEditsDetail> CacheUserClaimableEditsDetail { get; set; }

    public virtual DbSet<Contact> Contact { get; set; }

    public virtual DbSet<EditVersion> EditVersion { get; set; }

    public virtual DbSet<EditVersionStatus> EditVersionStatus { get; set; }

    public virtual DbSet<Editor> Editor { get; set; }

    public virtual DbSet<ExtCacheClub> ExtCacheClub { get; set; }

    public virtual DbSet<ExtCacheCtaf> ExtCacheCtaf { get; set; }

    public virtual DbSet<ExtCacheEditor> ExtCacheEditor { get; set; }

    public virtual DbSet<ExtCacheErsaEffectiveDate> ExtCacheErsaEffectiveDate { get; set; }

    public virtual DbSet<InsetMapPosition> InsetMapPosition { get; set; }

    public virtual DbSet<Launch> Launch { get; set; }

    public virtual DbSet<MapShape> MapShape { get; set; }

    public virtual DbSet<MapShapeCategory> MapShapeCategory { get; set; }

    public virtual DbSet<SeeAlso> SeeAlso { get; set; }

    public virtual DbSet<Site> Site { get; set; }

    public virtual DbSet<SiteContact> SiteContact { get; set; }

    public virtual DbSet<SiteMapshape> SiteMapshape { get; set; }

    public virtual DbSet<Version> Version { get; set; }

    public virtual DbSet<Video> Video { get; set; }

    public virtual DbSet<VideoType> VideoType { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<Area>(entity =>
        {
            entity.HasKey(e => e.RecordId).HasName("PK_Area_1");

            entity.HasOne(d => d.EditVersion).WithMany(p => p.Area).HasConstraintName("FK_Area_EditVersion");
        });

        modelBuilder.Entity<BackgroundTask>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("nextval('\"ExtCache_Id_seq\"'::regclass)");
        });

        modelBuilder.Entity<CacheUserClaimableEditsDetail>(entity =>
        {
            entity.HasKey(e => new { e.UserId, e.ClaimableEditId }).HasName("CacheUserClaimableEditsDetail_pkey");
        });

        modelBuilder.Entity<Contact>(entity =>
        {
            entity.HasOne(d => d.EditVersion).WithMany(p => p.Contact).HasConstraintName("FK_Contact_EditVersion");
        });

        modelBuilder.Entity<EditVersion>(entity =>
        {
            entity.Property(e => e.AuthorId).HasComment("This field is only relevant during the draft phase. We need to cover the case of the user deleting their account, so for logging purposes the user name is written to AuthorName. That is also the reason this isn't a FK.");
            entity.Property(e => e.AuthorName).HasComment("The author's name for logging purposes. AuthorId is not useable for this because the user may delete their account.");
            entity.Property(e => e.AuthorRole).HasComment("This is the role the author used during draft finalisation. For logging only.");

            entity.HasOne(d => d.Status).WithMany(p => p.EditVersion)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_EditVersion_EditVersionStatus");
        });

        modelBuilder.Entity<ExtCacheClub>(entity =>
        {
            entity.HasKey(e => e.SafaCode).HasName("PK_ExtCacheClub_1");

            entity.Property(e => e.Category).HasDefaultValueSql("'c'::character varying");
        });

        modelBuilder.Entity<ExtCacheCtaf>(entity =>
        {
            entity.Property(e => e.Code).IsFixedLength();
        });

        modelBuilder.Entity<ExtCacheErsaEffectiveDate>(entity =>
        {
            entity.HasKey(e => e.EffectiveDate).HasName("PK_ErsaEffectiveDate");
        });

        modelBuilder.Entity<Launch>(entity =>
        {
            entity.HasOne(d => d.EditVersion).WithMany(p => p.Launch).HasConstraintName("FK_Launch_EditVersion");
        });

        modelBuilder.Entity<MapShape>(entity =>
        {
            entity.HasOne(d => d.Category).WithMany(p => p.MapShape)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_MapShape_MapShapeCategory");

            entity.HasOne(d => d.EditVersion).WithMany(p => p.MapShape).HasConstraintName("FK_MapShape_EditVersion");
        });

        modelBuilder.Entity<SeeAlso>(entity =>
        {
            entity.HasOne(d => d.EditVersion).WithMany(p => p.SeeAlso).HasConstraintName("FK_SeeAlso_EditVersion");
        });

        modelBuilder.Entity<Site>(entity =>
        {
            entity.HasOne(d => d.EditVersion).WithMany(p => p.Site).HasConstraintName("FK_Site_EditVersion");

            entity.HasOne(d => d.InsetMapPosition).WithMany(p => p.Site)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Site_InsetMapPosition");
        });

        modelBuilder.Entity<SiteContact>(entity =>
        {
            entity.HasOne(d => d.EditVersion).WithMany(p => p.SiteContact).HasConstraintName("FK_SiteContact_EditVersion");
        });

        modelBuilder.Entity<SiteMapshape>(entity =>
        {
            entity.HasOne(d => d.EditVersion).WithMany(p => p.SiteMapshape).HasConstraintName("FK_SiteMapshape_EditVersion");
        });

        modelBuilder.Entity<Version>(entity =>
        {
            entity.Property(e => e.Id).ValueGeneratedNever();
        });

        modelBuilder.Entity<Video>(entity =>
        {
            entity.HasOne(d => d.EditVersion).WithMany(p => p.Video).HasConstraintName("FK_Video_EditVersion");

            entity.HasOne(d => d.Type).WithMany(p => p.Video)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Video_VideoType");
        });

        modelBuilder.Entity<VideoType>(entity =>
        {
            entity.Property(e => e.EmbedUrl).HasDefaultValueSql("''::character varying");
            entity.Property(e => e.LinkUrl).HasDefaultValueSql("''::character varying");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
