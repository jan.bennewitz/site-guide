﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace SiteGuide.DAL.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    PasswordHash = table.Column<string>(type: "text", nullable: true),
                    SecurityStamp = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BackgroundTask",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    LastUpdateUtc = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BackgroundTask", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CacheUserClaimableEdits",
                columns: table => new
                {
                    UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    ClaimableEditsCount = table.Column<int>(type: "integer", nullable: false),
                    DebugInfo = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CacheUserClaimableEdits", x => x.UserName);
                });

            migrationBuilder.CreateTable(
                name: "EditVersionStatus",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(1)", unicode: false, maxLength: 1, nullable: false),
                    Name = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EditVersionStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExtCacheClub",
                columns: table => new
                {
                    SafaCode = table.Column<string>(type: "character varying(400)", maxLength: 400, nullable: false),
                    Name = table.Column<string>(type: "character varying(400)", maxLength: 400, nullable: false),
                    Category = table.Column<string>(type: "character(1)", unicode: false, fixedLength: true, maxLength: 1, nullable: false, defaultValueSql: "('c')"),
                    Address = table.Column<string>(type: "character varying(4000)", maxLength: 4000, nullable: false),
                    Lat = table.Column<decimal>(type: "numeric(9,6)", nullable: false),
                    Lng = table.Column<decimal>(type: "numeric(9,6)", nullable: false),
                    Phone = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Email = table.Column<string>(type: "character varying(400)", maxLength: 400, nullable: true),
                    Url = table.Column<string>(type: "character varying(400)", maxLength: 400, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExtCacheClub_1", x => x.SafaCode);
                });

            migrationBuilder.CreateTable(
                name: "ExtCacheCtaf",
                columns: table => new
                {
                    Code = table.Column<string>(type: "character(4)", unicode: false, fixedLength: true, maxLength: 4, nullable: false),
                    Name = table.Column<string>(type: "character varying(250)", maxLength: 250, nullable: false),
                    Ctaf = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    Category = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    Lat = table.Column<double>(type: "double precision", nullable: false),
                    Lng = table.Column<double>(type: "double precision", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExtCacheCtaf", x => x.Code);
                });

            migrationBuilder.CreateTable(
                name: "ExtCacheEditor",
                columns: table => new
                {
                    Club = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    SafaId = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false),
                    Position = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Name = table.Column<string>(type: "character varying(400)", maxLength: 400, nullable: true),
                    Mobile = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Email = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    FirstName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Surname = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExtCacheEditor", x => new { x.Club, x.SafaId, x.Position });
                });

            migrationBuilder.CreateTable(
                name: "ExtCacheErsaEffectiveDate",
                columns: table => new
                {
                    EffectiveDate = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ErsaEffectiveDate", x => x.EffectiveDate);
                });

            migrationBuilder.CreateTable(
                name: "InsetMapPosition",
                columns: table => new
                {
                    Id = table.Column<byte>(type: "smallint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsetMapPosition", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MapShapeCategory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Dimensions = table.Column<byte>(type: "smallint", nullable: false),
                    Color = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MapShapeCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Version",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false),
                    EditId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Version", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VideoType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    LinkUrl = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false, defaultValueSql: "('')"),
                    EmbedUrl = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false, defaultValueSql: "('')"),
                    ThumbnailUrl = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    SortOrder = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VideoType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    ProviderKey = table.Column<string>(type: "text", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    RoleId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Value = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EditVersion",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EditId = table.Column<int>(type: "integer", nullable: false),
                    EditVersionNumber = table.Column<short>(type: "smallint", nullable: false),
                    InitialisedTime = table.Column<DateTime>(type: "timestamp(0) with time zone", precision: 0, nullable: false),
                    SubmittedTime = table.Column<DateTime>(type: "timestamp(0) with time zone", precision: 0, nullable: true),
                    AuthorName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    AuthorRole = table.Column<string>(type: "character varying(4000)", maxLength: 4000, nullable: true),
                    Comment = table.Column<string>(type: "text", nullable: true),
                    StatusId = table.Column<string>(type: "character varying(1)", unicode: false, maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EditVersion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EditVersion_EditVersionStatus",
                        column: x => x.StatusId,
                        principalTable: "EditVersionStatus",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Area",
                columns: table => new
                {
                    RecordId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EditVersionId = table.Column<int>(type: "integer", nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true),
                    ValidFromVersion = table.Column<int>(type: "integer", nullable: true),
                    ValidToVersion = table.Column<int>(type: "integer", nullable: true),
                    Id = table.Column<int>(type: "integer", nullable: false),
                    ParentAreaId = table.Column<int>(type: "integer", nullable: true),
                    Name = table.Column<string>(type: "character varying(255)", unicode: false, maxLength: 255, nullable: false),
                    FullName = table.Column<string>(type: "character varying(255)", unicode: false, maxLength: 255, nullable: true),
                    AdminId = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Area_1", x => x.RecordId);
                    table.ForeignKey(
                        name: "FK_Area_EditVersion",
                        column: x => x.EditVersionId,
                        principalTable: "EditVersion",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Contact",
                columns: table => new
                {
                    RecordId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EditVersionId = table.Column<int>(type: "integer", nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true),
                    ValidFromVersion = table.Column<int>(type: "integer", nullable: true),
                    ValidToVersion = table.Column<int>(type: "integer", nullable: true),
                    Id = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    FullName = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Link = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Address = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Phone = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    SafaClubId = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact", x => x.RecordId);
                    table.ForeignKey(
                        name: "FK_Contact_EditVersion",
                        column: x => x.EditVersionId,
                        principalTable: "EditVersion",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Launch",
                columns: table => new
                {
                    RecordId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EditVersionId = table.Column<int>(type: "integer", nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true),
                    ValidFromVersion = table.Column<int>(type: "integer", nullable: true),
                    ValidToVersion = table.Column<int>(type: "integer", nullable: true),
                    Id = table.Column<int>(type: "integer", nullable: false),
                    SiteId = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Closed = table.Column<string>(type: "text", nullable: true),
                    Conditions = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Lat = table.Column<decimal>(type: "numeric(18,9)", nullable: false),
                    Lon = table.Column<decimal>(type: "numeric(18,9)", nullable: false),
                    EstaEmergencyMarker = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Launch", x => x.RecordId);
                    table.ForeignKey(
                        name: "FK_Launch_EditVersion",
                        column: x => x.EditVersionId,
                        principalTable: "EditVersion",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MapShape",
                columns: table => new
                {
                    RecordId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EditVersionId = table.Column<int>(type: "integer", nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true),
                    ValidFromVersion = table.Column<int>(type: "integer", nullable: true),
                    ValidToVersion = table.Column<int>(type: "integer", nullable: true),
                    Id = table.Column<int>(type: "integer", nullable: false),
                    AreaId = table.Column<int>(type: "integer", nullable: false),
                    CategoryId = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Coordinates = table.Column<string>(type: "text", nullable: true),
                    EstaEmergencyMarker = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MapShape", x => x.RecordId);
                    table.ForeignKey(
                        name: "FK_MapShape_EditVersion",
                        column: x => x.EditVersionId,
                        principalTable: "EditVersion",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MapShape_MapShapeCategory",
                        column: x => x.CategoryId,
                        principalTable: "MapShapeCategory",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SeeAlso",
                columns: table => new
                {
                    RecordId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EditVersionId = table.Column<int>(type: "integer", nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true),
                    ValidFromVersion = table.Column<int>(type: "integer", nullable: true),
                    ValidToVersion = table.Column<int>(type: "integer", nullable: true),
                    Id = table.Column<int>(type: "integer", nullable: false),
                    SiteId = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Link = table.Column<string>(type: "character varying(4000)", maxLength: 4000, nullable: true),
                    IsEntryStubForThis = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeeAlso", x => x.RecordId);
                    table.ForeignKey(
                        name: "FK_SeeAlso_EditVersion",
                        column: x => x.EditVersionId,
                        principalTable: "EditVersion",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Site",
                columns: table => new
                {
                    RecordId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EditVersionId = table.Column<int>(type: "integer", nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true),
                    ValidFromVersion = table.Column<int>(type: "integer", nullable: true),
                    ValidToVersion = table.Column<int>(type: "integer", nullable: true),
                    Id = table.Column<int>(type: "integer", nullable: false),
                    AreaId = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    Closed = table.Column<string>(type: "text", nullable: true),
                    Rating = table.Column<string>(type: "text", nullable: true),
                    Height = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Type = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Restrictions = table.Column<string>(type: "text", nullable: true),
                    ShortLocation = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    ExtendedLocation = table.Column<string>(type: "text", nullable: true),
                    Conditions = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Takeoff = table.Column<string>(type: "text", nullable: true),
                    Landing = table.Column<string>(type: "text", nullable: true),
                    Landowners = table.Column<string>(type: "text", nullable: true),
                    FlightComments = table.Column<string>(type: "text", nullable: true),
                    HazardsComments = table.Column<string>(type: "text", nullable: true),
                    InsetMapPositionId = table.Column<byte>(type: "smallint", nullable: false),
                    SmallPicture = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    SmallPictureHref = table.Column<string>(type: "character varying(4000)", maxLength: 4000, nullable: true),
                    SmallPictureTitle = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Site", x => x.RecordId);
                    table.ForeignKey(
                        name: "FK_Site_EditVersion",
                        column: x => x.EditVersionId,
                        principalTable: "EditVersion",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Site_InsetMapPosition",
                        column: x => x.InsetMapPositionId,
                        principalTable: "InsetMapPosition",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SiteContact",
                columns: table => new
                {
                    RecordId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EditVersionId = table.Column<int>(type: "integer", nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true),
                    ValidFromVersion = table.Column<int>(type: "integer", nullable: true),
                    ValidToVersion = table.Column<int>(type: "integer", nullable: true),
                    ContactId = table.Column<int>(type: "integer", nullable: false),
                    SiteId = table.Column<int>(type: "integer", nullable: false),
                    IsContact = table.Column<bool>(type: "boolean", nullable: false),
                    IsResponsible = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteContact", x => x.RecordId);
                    table.ForeignKey(
                        name: "FK_SiteContact_EditVersion",
                        column: x => x.EditVersionId,
                        principalTable: "EditVersion",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SiteMapshape",
                columns: table => new
                {
                    RecordId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EditVersionId = table.Column<int>(type: "integer", nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true),
                    ValidFromVersion = table.Column<int>(type: "integer", nullable: true),
                    ValidToVersion = table.Column<int>(type: "integer", nullable: true),
                    MapshapeId = table.Column<int>(type: "integer", nullable: false),
                    SiteId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiteMapshape", x => x.RecordId);
                    table.ForeignKey(
                        name: "FK_SiteMapshape_EditVersion",
                        column: x => x.EditVersionId,
                        principalTable: "EditVersion",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Video",
                columns: table => new
                {
                    RecordId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EditVersionId = table.Column<int>(type: "integer", nullable: true),
                    IsDelete = table.Column<bool>(type: "boolean", nullable: true),
                    ValidFromVersion = table.Column<int>(type: "integer", nullable: true),
                    ValidToVersion = table.Column<int>(type: "integer", nullable: true),
                    Id = table.Column<int>(type: "integer", nullable: false),
                    SiteId = table.Column<int>(type: "integer", nullable: false),
                    TypeId = table.Column<int>(type: "integer", nullable: false),
                    Identifier = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    ThumbnailUrl = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Video", x => x.RecordId);
                    table.ForeignKey(
                        name: "FK_Video_EditVersion",
                        column: x => x.EditVersionId,
                        principalTable: "EditVersion",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Video_VideoType",
                        column: x => x.TypeId,
                        principalTable: "VideoType",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Area_EditVersionId",
                table: "Area",
                column: "EditVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_Area_ParentAreaId",
                table: "Area",
                column: "ParentAreaId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BackgroundTask",
                table: "BackgroundTask",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CacheUserClaimableEdits",
                table: "CacheUserClaimableEdits",
                column: "UserName");

            migrationBuilder.CreateIndex(
                name: "IX_Contact",
                table: "Contact",
                columns: new[] { "ValidFromVersion", "ValidToVersion", "Id" },
                unique: true,
                filter: "([ValidToVersion] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Contact_1",
                table: "Contact",
                columns: new[] { "EditVersionId", "Id" },
                unique: true,
                filter: "([EditVersionId] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Contact_EditVersionId",
                table: "Contact",
                column: "EditVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_EditVersion",
                table: "EditVersion",
                columns: new[] { "EditId", "EditVersionNumber" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_EditVersion_StatusId",
                table: "EditVersion",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_ExtCacheClub",
                table: "ExtCacheClub",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_ExtCacheClub_1",
                table: "ExtCacheClub",
                column: "SafaCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Launch",
                table: "Launch",
                columns: new[] { "ValidFromVersion", "ValidToVersion", "Id" },
                unique: true,
                filter: "([ValidToVersion] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Launch_1",
                table: "Launch",
                columns: new[] { "EditVersionId", "Id" },
                unique: true,
                filter: "([EditVersionId] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Launch_EditVersionId",
                table: "Launch",
                column: "EditVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_MapShape",
                table: "MapShape",
                columns: new[] { "ValidFromVersion", "ValidToVersion", "Id" },
                unique: true,
                filter: "([ValidToVersion] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_MapShape_1",
                table: "MapShape",
                columns: new[] { "EditVersionId", "Id" },
                unique: true,
                filter: "([EditVersionId] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_MapShape_AreaId",
                table: "MapShape",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_MapShape_CategoryId",
                table: "MapShape",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_MapShape_EditVersionId",
                table: "MapShape",
                column: "EditVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_SeeAlso",
                table: "SeeAlso",
                columns: new[] { "ValidFromVersion", "ValidToVersion", "Id" },
                unique: true,
                filter: "([ValidToVersion] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_SeeAlso_1",
                table: "SeeAlso",
                columns: new[] { "EditVersionId", "Id" },
                unique: true,
                filter: "([EditVersionId] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_SeeAlso_EditVersionId",
                table: "SeeAlso",
                column: "EditVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_Site",
                table: "Site",
                columns: new[] { "ValidFromVersion", "ValidToVersion", "Id" },
                unique: true,
                filter: "([ValidToVersion] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Site_1",
                table: "Site",
                columns: new[] { "EditVersionId", "Id" },
                unique: true,
                filter: "([EditVersionId] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Site_AreaId",
                table: "Site",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_Site_EditVersionId",
                table: "Site",
                column: "EditVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_Site_InsetMapPositionId",
                table: "Site",
                column: "InsetMapPositionId");

            migrationBuilder.CreateIndex(
                name: "IX_Site_ValidVersion",
                table: "Site",
                columns: new[] { "ValidToVersion", "ValidFromVersion" });

            migrationBuilder.CreateIndex(
                name: "IX_SiteContact",
                table: "SiteContact",
                columns: new[] { "ValidFromVersion", "ValidToVersion", "SiteId", "ContactId" },
                unique: true,
                filter: "([ValidToVersion] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_SiteContact_1",
                table: "SiteContact",
                columns: new[] { "EditVersionId", "SiteId", "ContactId" },
                unique: true,
                filter: "([EditVersionId] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_SiteContact_EditVersionId",
                table: "SiteContact",
                column: "EditVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_SiteMapshape",
                table: "SiteMapshape",
                columns: new[] { "RecordId", "ValidToVersion", "SiteId", "MapshapeId" },
                unique: true,
                filter: "([ValidToVersion] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_SiteMapshape_1",
                table: "SiteMapshape",
                columns: new[] { "SiteId", "MapshapeId" });

            migrationBuilder.CreateIndex(
                name: "IX_SiteMapshape_2",
                table: "SiteMapshape",
                columns: new[] { "MapshapeId", "SiteId" });

            migrationBuilder.CreateIndex(
                name: "IX_SiteMapshape_3",
                table: "SiteMapshape",
                columns: new[] { "EditVersionId", "SiteId", "MapshapeId" },
                unique: true,
                filter: "([EditVersionId] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_SiteMapshape_EditVersionId",
                table: "SiteMapshape",
                column: "EditVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_Video",
                table: "Video",
                columns: new[] { "ValidFromVersion", "ValidToVersion", "Id" },
                unique: true,
                filter: "([ValidToVersion] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Video_1",
                table: "Video",
                columns: new[] { "EditVersionId", "Id" },
                unique: true,
                filter: "([EditVersionId] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Video_EditVersionId",
                table: "Video",
                column: "EditVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_Video_TypeId",
                table: "Video",
                column: "TypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Area");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "BackgroundTask");

            migrationBuilder.DropTable(
                name: "CacheUserClaimableEdits");

            migrationBuilder.DropTable(
                name: "Contact");

            migrationBuilder.DropTable(
                name: "ExtCacheClub");

            migrationBuilder.DropTable(
                name: "ExtCacheCtaf");

            migrationBuilder.DropTable(
                name: "ExtCacheEditor");

            migrationBuilder.DropTable(
                name: "ExtCacheErsaEffectiveDate");

            migrationBuilder.DropTable(
                name: "Launch");

            migrationBuilder.DropTable(
                name: "MapShape");

            migrationBuilder.DropTable(
                name: "SeeAlso");

            migrationBuilder.DropTable(
                name: "Site");

            migrationBuilder.DropTable(
                name: "SiteContact");

            migrationBuilder.DropTable(
                name: "SiteMapshape");

            migrationBuilder.DropTable(
                name: "Version");

            migrationBuilder.DropTable(
                name: "Video");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "MapShapeCategory");

            migrationBuilder.DropTable(
                name: "InsetMapPosition");

            migrationBuilder.DropTable(
                name: "EditVersion");

            migrationBuilder.DropTable(
                name: "VideoType");

            migrationBuilder.DropTable(
                name: "EditVersionStatus");
        }
    }
}
