﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace SiteGuide.DAL.Models
{
    /// <summary>
    /// Record with tracked history.
    /// Fields are:
    /// RecordId - Primary Key
    /// EditVersionId, EditVersion and IsDelete: Header data if record is part of an EditVersion.
    /// ValidFromVersion and ValidToVersion: Header data if record is part of a Version.
    /// Key: Primary key of the record over time - not unique within the table but unique per Version or EditVersion. Can be composite for many-to-many tables.
    /// </summary>
    public interface IRecord
    {
        [JsonIgnore]
        [IgnoreDataMember]
        int RecordId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        /// <summary>
        /// Primary Key
        /// </summary>
        int? EditVersionId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        EditVersion EditVersion { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        bool? IsDelete { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        int? ValidFromVersion { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        int? ValidToVersion { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        object Key { get; set; }
    }
}
