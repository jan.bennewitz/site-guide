﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace SiteGuide.DAL.Models
{
    public interface ISiteManyToMany<T> : ISiteManyToMany where T : class, IRecord
    { }

    public interface ISiteManyToMany
    {
        public int SiteId { get; set; }
        public int ForeignKeyId { get; set; }
    }
}
