﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace SiteGuide.DAL.Models
{
    public interface IId // Could call this IPrimaryRecord ?
    {
        int Id { get; set; }
    }
}
