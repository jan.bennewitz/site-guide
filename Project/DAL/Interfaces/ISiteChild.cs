﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace SiteGuide.DAL.Models
{
    public interface ISiteChild
    {
        public int SiteId { get; set; }
    }
}
