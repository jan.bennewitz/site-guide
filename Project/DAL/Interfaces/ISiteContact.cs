﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace SiteGuide.DAL.Models
{
    public interface ISiteContact
    {
        public int SiteId { get; set; }
        public int ContactId { get; set; }
    }
}
