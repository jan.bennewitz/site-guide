﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class SiteContact
{
    [Key]
    public int RecordId { get; set; }

    public int? EditVersionId { get; set; }

    public bool? IsDelete { get; set; }

    public int? ValidFromVersion { get; set; }

    public int? ValidToVersion { get; set; }

    public int ContactId { get; set; }

    public int SiteId { get; set; }

    public bool IsContact { get; set; }

    public bool IsResponsible { get; set; }

    [ForeignKey("EditVersionId")]
    [InverseProperty("SiteContact")]
    public virtual EditVersion EditVersion { get; set; }
}
