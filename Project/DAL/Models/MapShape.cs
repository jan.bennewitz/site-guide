﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class MapShape
{
    [Key]
    public int RecordId { get; set; }

    public int? EditVersionId { get; set; }

    public bool? IsDelete { get; set; }

    public int? ValidFromVersion { get; set; }

    public int? ValidToVersion { get; set; }

    public int Id { get; set; }

    public int AreaId { get; set; }

    public int CategoryId { get; set; }

    [StringLength(255)]
    public string Name { get; set; }

    [Column(TypeName = "character varying")]
    public string Description { get; set; }

    [Column(TypeName = "character varying")]
    public string Coordinates { get; set; }

    [StringLength(255)]
    public string EstaEmergencyMarker { get; set; }

    [ForeignKey("CategoryId")]
    [InverseProperty("MapShape")]
    public virtual MapShapeCategory Category { get; set; }

    [ForeignKey("EditVersionId")]
    [InverseProperty("MapShape")]
    public virtual EditVersion EditVersion { get; set; }
}
