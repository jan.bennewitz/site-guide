﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class ExtCacheErsaEffectiveDate
{
    [Key]
    public DateOnly EffectiveDate { get; set; }
}
