﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class SeeAlso
{
    [Key]
    public int RecordId { get; set; }

    public int? EditVersionId { get; set; }

    public bool? IsDelete { get; set; }

    public int? ValidFromVersion { get; set; }

    public int? ValidToVersion { get; set; }

    public int Id { get; set; }

    public int SiteId { get; set; }

    [Required]
    [StringLength(255)]
    public string Name { get; set; }

    [StringLength(4000)]
    public string Link { get; set; }

    public bool IsEntryStubForThis { get; set; }

    [ForeignKey("EditVersionId")]
    [InverseProperty("SeeAlso")]
    public virtual EditVersion EditVersion { get; set; }
}
