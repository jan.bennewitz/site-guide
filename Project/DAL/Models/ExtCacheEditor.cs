﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

[PrimaryKey("Club", "SafaId", "Position")]
public partial class ExtCacheEditor
{
    [Key]
    [StringLength(50)]
    public string Club { get; set; }

    [Key]
    [StringLength(50)]
    public string SafaId { get; set; }

    [Key]
    [StringLength(255)]
    public string Position { get; set; }

    [StringLength(400)]
    public string Name { get; set; }

    [StringLength(255)]
    public string Mobile { get; set; }

    [Required]
    [StringLength(255)]
    public string Email { get; set; }

    [StringLength(255)]
    public string FirstName { get; set; }

    [StringLength(255)]
    public string Surname { get; set; }
}
