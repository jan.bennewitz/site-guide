﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class MapShapeCategory
{
    [Key]
    public int Id { get; set; }

    [Required]
    [StringLength(255)]
    public string Name { get; set; }

    public int Dimensions { get; set; }

    [Required]
    [StringLength(50)]
    public string Color { get; set; }

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("Category")]
    public virtual ICollection<MapShape> MapShape { get; set; } = new List<MapShape>();
}
