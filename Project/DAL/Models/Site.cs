﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class Site
{
    [Key]
    public int RecordId { get; set; }

    public int? EditVersionId { get; set; }

    public bool? IsDelete { get; set; }

    public int? ValidFromVersion { get; set; }

    public int? ValidToVersion { get; set; }

    public int Id { get; set; }

    public int AreaId { get; set; }

    [Required]
    [StringLength(255)]
    public string Name { get; set; }

    [Column(TypeName = "character varying")]
    public string Closed { get; set; }

    [Column(TypeName = "character varying")]
    public string Rating { get; set; }

    [StringLength(255)]
    public string Height { get; set; }

    [StringLength(255)]
    public string Type { get; set; }

    [Column(TypeName = "character varying")]
    public string Restrictions { get; set; }

    [StringLength(255)]
    public string ShortLocation { get; set; }

    [Column(TypeName = "character varying")]
    public string ExtendedLocation { get; set; }

    [StringLength(255)]
    public string Conditions { get; set; }

    [Column(TypeName = "character varying")]
    public string Description { get; set; }

    [Column(TypeName = "character varying")]
    public string Takeoff { get; set; }

    [Column(TypeName = "character varying")]
    public string Landing { get; set; }

    [Column(TypeName = "character varying")]
    public string Landowners { get; set; }

    [Column(TypeName = "character varying")]
    public string FlightComments { get; set; }

    [Column(TypeName = "character varying")]
    public string HazardsComments { get; set; }

    public int InsetMapPositionId { get; set; }

    [StringLength(255)]
    public string SmallPicture { get; set; }

    [StringLength(4000)]
    public string SmallPictureHref { get; set; }

    [StringLength(255)]
    public string SmallPictureTitle { get; set; }

    [ForeignKey("EditVersionId")]
    [InverseProperty("Site")]
    public virtual EditVersion EditVersion { get; set; }

    [ForeignKey("InsetMapPositionId")]
    [InverseProperty("Site")]
    public virtual InsetMapPosition InsetMapPosition { get; set; }
}
