﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class CacheUserClaimableEdits
{
    [Key]
    [StringLength(256)]
    public string UserId { get; set; }

    public int ClaimableEditsCount { get; set; }

    [Column(TypeName = "character varying")]
    public string DebugInfo { get; set; }
}
