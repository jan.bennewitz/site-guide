﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class InsetMapPosition
{
    [Key]
    public int Id { get; set; }

    [Required]
    [StringLength(255)]
    public string Name { get; set; }

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("InsetMapPosition")]
    public virtual ICollection<Site> Site { get; set; } = new List<Site>();
}
