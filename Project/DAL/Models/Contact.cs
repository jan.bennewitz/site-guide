﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class Contact
{
    [Key]
    public int RecordId { get; set; }

    public int? EditVersionId { get; set; }

    public bool? IsDelete { get; set; }

    public int? ValidFromVersion { get; set; }

    public int? ValidToVersion { get; set; }

    public int Id { get; set; }

    [Required]
    [StringLength(255)]
    public string Name { get; set; }

    [StringLength(255)]
    public string FullName { get; set; }

    [StringLength(255)]
    public string Link { get; set; }

    [StringLength(255)]
    public string Address { get; set; }

    [StringLength(255)]
    public string Phone { get; set; }

    [StringLength(50)]
    public string SafaClubId { get; set; }

    [StringLength(450)]
    public string UserId { get; set; }

    [ForeignKey("EditVersionId")]
    [InverseProperty("Contact")]
    public virtual EditVersion EditVersion { get; set; }
}
