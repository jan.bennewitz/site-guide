﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class ExtCacheCtaf
{
    [Key]
    [StringLength(4)]
    public string Code { get; set; }

    [Required]
    [StringLength(250)]
    public string Name { get; set; }

    [Required]
    [StringLength(50)]
    public string Ctaf { get; set; }

    [Required]
    [StringLength(50)]
    public string Category { get; set; }

    public double Lat { get; set; }

    public double Lng { get; set; }
}
