﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class Video
{
    [Key]
    public int RecordId { get; set; }

    public int? EditVersionId { get; set; }

    public bool? IsDelete { get; set; }

    public int? ValidFromVersion { get; set; }

    public int? ValidToVersion { get; set; }

    public int Id { get; set; }

    public int SiteId { get; set; }

    public int TypeId { get; set; }

    [Required]
    [StringLength(255)]
    public string Identifier { get; set; }

    [StringLength(255)]
    public string ThumbnailUrl { get; set; }

    [ForeignKey("EditVersionId")]
    [InverseProperty("Video")]
    public virtual EditVersion EditVersion { get; set; }

    [ForeignKey("TypeId")]
    [InverseProperty("Video")]
    public virtual VideoType Type { get; set; }
}
