﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class Version
{
    [Key]
    public int Id { get; set; }

    public int EditId { get; set; }

    [Column(TypeName = "character varying")]
    public string Comment { get; set; }
}
