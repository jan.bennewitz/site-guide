﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class VideoType
{
    [Key]
    public int Id { get; set; }

    [Required]
    [StringLength(255)]
    public string Name { get; set; }

    [Required]
    [StringLength(255)]
    public string LinkUrl { get; set; }

    [Required]
    [StringLength(255)]
    public string EmbedUrl { get; set; }

    [StringLength(255)]
    public string ThumbnailUrl { get; set; }

    public int SortOrder { get; set; }

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("Type")]
    public virtual ICollection<Video> Video { get; set; } = new List<Video>();
}
