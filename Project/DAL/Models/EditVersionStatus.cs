﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class EditVersionStatus
{
    [Key]
    [StringLength(1)]
    public string Id { get; set; }

    [Required]
    [StringLength(50)]
    public string Name { get; set; }

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("Status")]
    public virtual ICollection<EditVersion> EditVersion { get; set; } = new List<EditVersion>();
}
