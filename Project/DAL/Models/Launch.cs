﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class Launch
{
    [Key]
    public int RecordId { get; set; }

    public int? EditVersionId { get; set; }

    public bool? IsDelete { get; set; }

    public int? ValidFromVersion { get; set; }

    public int? ValidToVersion { get; set; }

    public int Id { get; set; }

    public int SiteId { get; set; }

    [StringLength(255)]
    public string Name { get; set; }

    [Column(TypeName = "character varying")]
    public string Closed { get; set; }

    [StringLength(255)]
    public string Conditions { get; set; }

    [Column(TypeName = "character varying")]
    public string Description { get; set; }

    [Precision(18, 9)]
    public decimal Lat { get; set; }

    [Precision(18, 9)]
    public decimal Lon { get; set; }

    [StringLength(255)]
    public string EstaEmergencyMarker { get; set; }

    [ForeignKey("EditVersionId")]
    [InverseProperty("Launch")]
    public virtual EditVersion EditVersion { get; set; }
}
