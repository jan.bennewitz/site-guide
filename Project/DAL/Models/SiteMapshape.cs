﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class SiteMapshape
{
    [Key]
    public int RecordId { get; set; }

    public int? EditVersionId { get; set; }

    public bool? IsDelete { get; set; }

    public int? ValidFromVersion { get; set; }

    public int? ValidToVersion { get; set; }

    public int MapshapeId { get; set; }

    public int SiteId { get; set; }

    [ForeignKey("EditVersionId")]
    [InverseProperty("SiteMapshape")]
    public virtual EditVersion EditVersion { get; set; }
}
