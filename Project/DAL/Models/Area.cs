﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace SiteGuide.DAL.Models;

public partial class Area
{
    [Key]
    public int RecordId { get; set; }

    public int? EditVersionId { get; set; }

    public bool? IsDelete { get; set; }

    public int? ValidFromVersion { get; set; }

    public int? ValidToVersion { get; set; }

    public int Id { get; set; }

    public int? ParentAreaId { get; set; }

    [Required]
    [StringLength(255)]
    public string Name { get; set; }

    [StringLength(255)]
    public string FullName { get; set; }

    public int? AdminId { get; set; }

    [JsonIgnore]
    [IgnoreDataMember]
    [ForeignKey("EditVersionId")]
    [InverseProperty("Area")]
    public virtual EditVersion EditVersion { get; set; }
}
