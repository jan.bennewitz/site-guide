﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

[PrimaryKey("UserId", "ClaimableEditId")]
public partial class CacheUserClaimableEditsDetail
{
    [Key]
    [StringLength(256)]
    public string UserId { get; set; }

    [Key]
    public int ClaimableEditId { get; set; }
}
