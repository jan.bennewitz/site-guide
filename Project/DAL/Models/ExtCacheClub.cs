﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

public partial class ExtCacheClub
{
    [Key]
    [StringLength(400)]
    public string SafaCode { get; set; }

    [Required]
    [StringLength(400)]
    public string Name { get; set; }

    [Required]
    [StringLength(4000)]
    public string Address { get; set; }

    [Precision(9, 6)]
    public decimal Lat { get; set; }

    [Precision(9, 6)]
    public decimal Lng { get; set; }

    [StringLength(255)]
    public string Phone { get; set; }

    [StringLength(400)]
    public string Email { get; set; }

    [StringLength(400)]
    public string Url { get; set; }

    [Required]
    [StringLength(1)]
    public string Category { get; set; }
}
