﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

[PrimaryKey("Club", "SafaId")]
public partial class Editor
{
    [Key]
    [StringLength(50)]
    public string Club { get; set; }

    [Key]
    [StringLength(50)]
    public string SafaId { get; set; }

    [StringLength(400)]
    public string Name { get; set; }

    [Required]
    [StringLength(255)]
    public string Email { get; set; }

    public string Notes { get; set; }
}
