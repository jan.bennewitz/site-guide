﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;

namespace SiteGuide.DAL.Models;

[Index("AuthorId", "EditVersionNumber", Name = "EditVersion_AuthorId_EditVersionNumber_idx")]
[Index("AuthorId", "StatusId", "EditVersionNumber", Name = "EditVersion_AuthorId_StatusId_EditVersionNumber_idx")]
[Index("EditId", "EditVersionNumber", Name = "EditVersion_EditId_EditVersionNumber_key", IsUnique = true)]
public partial class EditVersion
{
    [Key]
    public int Id { get; set; }

    public int EditId { get; set; }

    public int EditVersionNumber { get; set; }

    [Precision(0, 0)]
    public DateTime InitialisedTime { get; set; }

    [Precision(0, 0)]
    public DateTime? SubmittedTime { get; set; }

    /// <summary>
    /// The author&apos;s name for logging purposes. AuthorId is not useable for this because the user may delete their account.
    /// </summary>
    [Required]
    [StringLength(256)]
    public string AuthorName { get; set; }

    /// <summary>
    /// This is the role the author used during draft finalisation. For logging only.
    /// </summary>
    [StringLength(4000)]
    public string AuthorRole { get; set; }

    [Column(TypeName = "character varying")]
    public string Comment { get; set; }

    [Required]
    [StringLength(1)]
    public string StatusId { get; set; }

    /// <summary>
    /// This field is only relevant during the draft phase. We need to cover the case of the user deleting their account, so for logging purposes the user name is written to AuthorName. That is also the reason this isn&apos;t a FK.
    /// </summary>
    [StringLength(450)]
    public string AuthorId { get; set; }

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("EditVersion")]
    public virtual ICollection<Area> Area { get; set; } = new List<Area>();

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("EditVersion")]
    public virtual ICollection<Contact> Contact { get; set; } = new List<Contact>();

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("EditVersion")]
    public virtual ICollection<Launch> Launch { get; set; } = new List<Launch>();

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("EditVersion")]
    public virtual ICollection<MapShape> MapShape { get; set; } = new List<MapShape>();

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("EditVersion")]
    public virtual ICollection<SeeAlso> SeeAlso { get; set; } = new List<SeeAlso>();

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("EditVersion")]
    public virtual ICollection<Site> Site { get; set; } = new List<Site>();

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("EditVersion")]
    public virtual ICollection<SiteContact> SiteContact { get; set; } = new List<SiteContact>();

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("EditVersion")]
    public virtual ICollection<SiteMapshape> SiteMapshape { get; set; } = new List<SiteMapshape>();

    [ForeignKey("StatusId")]
    [InverseProperty("EditVersion")]
    public virtual EditVersionStatus Status { get; set; }

    [JsonIgnore]
    [IgnoreDataMember]
    [InverseProperty("EditVersion")]
    public virtual ICollection<Video> Video { get; set; } = new List<Video>();
}
