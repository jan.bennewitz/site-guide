﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteGuide.DAL.Models
{
    public partial class SiteContact : IRecord, ISiteManyToMany<Contact>, ISiteContact, ISiteChild
    {
        [NotMapped]
        public object Key
        {
            get => new Tuple<int, int>(SiteId, ContactId);
            set
            {
                SiteId = ((Tuple<int, int>)value).Item1;
                ContactId = ((Tuple<int, int>)value).Item2;
            }
        }

        [NotMapped]
        public int ForeignKeyId { get => ContactId; set => ContactId = value; }

        public Type GetSiteAssociatedType => typeof(Contact);
    }
}
