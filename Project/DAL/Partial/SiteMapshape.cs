﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteGuide.DAL.Models
{
    public partial class SiteMapshape : IRecord, ISiteManyToMany<MapShape>, ISiteMapshape, ISiteChild
    {
        [NotMapped]
        public object Key
        {
            get => new Tuple<int, int>(SiteId, MapshapeId);
            set
            {
                SiteId = ((Tuple<int, int>)value).Item1;
                MapshapeId = ((Tuple<int, int>)value).Item2;
            }
        }

        [NotMapped]
        public int ForeignKeyId { get => MapshapeId; set => MapshapeId = value; }

        public Type GetSiteAssociatedType => typeof(MapShape);
    }
}
