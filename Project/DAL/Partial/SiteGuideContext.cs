﻿using Microsoft.EntityFrameworkCore;
using SiteGuide.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.DAL {
    public partial class SiteGuideContext {
        public IQueryable<IRecord>[] RecordTables
            => new IQueryable<IRecord>[] {
                Site,
                Launch ,
                SeeAlso,
                SiteContact,
                SiteMapshape,
                Video,
                Area,
                Contact,
                MapShape,
            };
    }
}
