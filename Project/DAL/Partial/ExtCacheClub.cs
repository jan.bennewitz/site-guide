﻿using System;

namespace SiteGuide.DAL.Models
{
    public partial class ExtCacheClub
    {
        public override string ToString() => $"[{SafaCode}] {Name}";
    }
}
