﻿using System;

namespace SiteGuide.DAL.Models
{
    public partial class ExtCacheCtaf
    {
        public override string ToString() => $"[{Code}] {Name}";
    }
}
