﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace SiteGuide.DAL.Models
{
    public partial class EditVersionStatus
    {
        public static IList<EditVersionStatus> All => new List<EditVersionStatus> { Draft, Submitted, Accepted, Rejected };
        public static EditVersionStatus Draft => new EditVersionStatus { Id = "D", Name = "Draft" };
        public static EditVersionStatus Submitted => new EditVersionStatus { Id = "S", Name = "Submitted" };
        public static EditVersionStatus Accepted => new EditVersionStatus { Id = "A", Name = "Accepted" };
        public static EditVersionStatus Rejected => new EditVersionStatus { Id = "R", Name = "Rejected" };

        public override string ToString() => $"{Id} \"{Name}\"";
    }
}
