﻿using System;

namespace SiteGuide.DAL.Models
{
    public partial class ExtCacheErsaEffectiveDate
    {
        public override string ToString() => $"{EffectiveDate:yyyy-MM-dd}";
    }
}
