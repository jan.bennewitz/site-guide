﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace SiteGuide.DAL.Models
{
    public partial class MapShape : IRecord, IId
    {
        public string[] CoordinatesList => Coordinates?.Split('|') ?? Array.Empty<string>();

        [NotMapped]
        public object Key
        {
            get => Id;
            set => Id = (int)value;
        }

        public List<List<Tuple<decimal, decimal>>> GetCoordinates()
            => GetCoordinates(CoordinatesList);

        public static List<List<Tuple<decimal, decimal>>> GetCoordinates(string[] coordinates)
        {
            if (coordinates == null)
                return null;

            const int precision = 6; // 6 gives us sub-.1m precision, that should be enough
            var polys = new List<List<Tuple<decimal, decimal>>>();
            var regex = new Regex(@"(?<lon>[-+]?([1-8]+\d(\.\d+)?|90(\.0+)?)),\s*(?<lat>[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?))");
            foreach (var polySource in coordinates)
            {
                var coordMatches = regex.Matches(polySource);

                var polyCoords = new List<Tuple<decimal, decimal>>();
                foreach (Match coordMatch in coordMatches)
                {
                    decimal lat = decimal.Round(Convert.ToDecimal(coordMatch.Groups["lat"].Value), precision);
                    decimal lon = decimal.Round(Convert.ToDecimal(coordMatch.Groups["lon"].Value), precision);
                    polyCoords.Add(new Tuple<decimal, decimal>(lat, lon));
                }
                polys.Add(polyCoords);
            }

            return polys;
        }
    }
}
