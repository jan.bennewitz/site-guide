﻿using System;

namespace SiteGuide.DAL.Models
{
    public partial class ExtCacheEditor
    {
        public override string ToString() => $"{FirstName} {Surname} [{SafaId}] for {Club}";
    }
}
