﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteGuide.DAL.Models
{
    public partial class Site : IRecord, IId, IValidatableObject
    {
        public bool IsClosed => !string.IsNullOrEmpty(Closed);

        [NotMapped]
        public object Key
        {
            get => Id;
            set => Id = (int)value;
        }

        public override string ToString() => $"[{Id}] {Name}";

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(Name))
                yield return new ValidationResult("Name cannot be blank.", new[] { nameof(Name) });
        }
    }
}
