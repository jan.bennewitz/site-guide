﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteGuide.DAL.Models
{
    public partial class Launch : IRecord, IId, ISiteChild
    {
        public bool IsClosed { get { return !string.IsNullOrEmpty(Closed); } }
        //public bool IsLaunchOrSiteClosed { get { return IsClosed || Site.IsClosed; } }

        [NotMapped]
        public object Key
        {
            get => Id;
            set => Id = (int)value;
        }

        public override string ToString() => $"[{Id}] {Name ?? "(Unnamed)"}";
    }
}
