﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteGuide.DAL.Models
{
    public partial class Contact : IRecord, IId
    {
        [NotMapped]
        public object Key
        {
            get => Id;
            set => Id = (int)value;
        }

        public string FullNameOrName() => FullName ?? Name;

        public override string ToString() => $"[{Id}] {Name}";
    }
}
