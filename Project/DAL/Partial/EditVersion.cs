﻿using System;

namespace SiteGuide.DAL.Models
{
    public partial class EditVersion
    {
        public string EditIdAndVersionNumber => $"{EditId}.{EditVersionNumber}";
        public override string ToString() => $"[{Id}] {EditIdAndVersionNumber}";
    }
}
