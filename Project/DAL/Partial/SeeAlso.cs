﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteGuide.DAL.Models
{
    public partial class SeeAlso : IRecord, IId, ISiteChild
    {
        [NotMapped]
        public object Key
        {
            get => Id;
            set => Id = (int)value;
        }

        public override string ToString() => $"[{Id}] {Name}";
    }
}
