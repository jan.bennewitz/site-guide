﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SiteGuide.DAL.Extensions
{
    public static class PropertyInfoExtensions
    {
        /// <summary>
        /// Markdown fields have no max length. Plain text fields have a max length of 255, except for Coordinates.
        /// </summary>
        public static bool IsRecordFieldMarkdown(this PropertyInfo prop)
        {
            if (prop.PropertyType != typeof(string))
                //throw new ArgumentException("Field must be of type string.");
                return false;
            if (prop.Name == "Coordinates")
                return false;
            return !prop.GetCustomAttributes<StringLengthAttribute>().Any();
        }
    }
}
