﻿To generate models:
https://docs.microsoft.com/en-us/ef/core/miscellaneous/cli/powershell
Package Manager Console:
Scaffold-DbContext "Server=.;Database=SiteGuide_sqlserver;Trusted_Connection=True;TrustServerCertificate=True;" Microsoft.EntityFrameworkCore.SqlServer -force -DataAnnotations -OutputDir Models -ContextDir . -Context SiteGuideContext -Project DAL -Schemas "dbo" -NoPluralize -NoOnConfiguring
or
Scaffold-DbContext "***ConnStr***" Npgsql.EntityFrameworkCore.PostgreSQL -force -DataAnnotations -OutputDir Models -ContextDir . -Context SiteGuideContext -Project DAL -Schemas "public" -NoPluralize -NoOnConfiguring

Version:		Published version of the data. Each Version is numbered sequentially.
Edit:			A proposal for a new version, or an accepted new version. An Edit is based off a specific version. Several Edits can be based on the same version.
EditVersion:	An edit may go through several editversions. The last editversion may be accepted as a new version, or rejected, or it may still be pending.