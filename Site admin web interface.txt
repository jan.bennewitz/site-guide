Authenticate user
Get user sites (determined by matching 'responsible' field)
Display list of user sites
(user selects a site to edit) or (user selects add site)
Generate site edit page
(user saves changes)
Validate against xsd
Save a copy of old data in a backup/history directory
Update site data
Generate new site guide
Replace old guide with new
