USE [master]
GO
/****** Object:  Database [SiteGuide]    Script Date: 2019-06-23 10:16:54 ******/
CREATE DATABASE [SiteGuide]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SiteGuide', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\SiteGuide.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SiteGuide_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\SiteGuide_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [SiteGuide] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SiteGuide].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SiteGuide] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SiteGuide] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SiteGuide] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SiteGuide] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SiteGuide] SET ARITHABORT OFF 
GO
ALTER DATABASE [SiteGuide] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SiteGuide] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SiteGuide] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SiteGuide] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SiteGuide] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SiteGuide] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SiteGuide] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SiteGuide] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SiteGuide] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SiteGuide] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SiteGuide] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SiteGuide] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SiteGuide] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SiteGuide] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SiteGuide] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SiteGuide] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SiteGuide] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SiteGuide] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SiteGuide] SET  MULTI_USER 
GO
ALTER DATABASE [SiteGuide] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SiteGuide] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SiteGuide] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SiteGuide] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SiteGuide] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [SiteGuide] SET QUERY_STORE = OFF
GO
USE [SiteGuide]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Area]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Area](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentAreaId] [int] NULL,
	[Name] [varchar](255) NOT NULL,
	[FullName] [varchar](255) NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangeToContact]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeToContact](
	[ChangeId] [int] IDENTITY(1,1) NOT NULL,
	[EditVersionId] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[Id] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[FullName] [nvarchar](255) NULL,
	[Link] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
 CONSTRAINT [PK_ChangeToContact] PRIMARY KEY CLUSTERED 
(
	[ChangeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangeToLaunch]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeToLaunch](
	[ChangeId] [int] IDENTITY(1,1) NOT NULL,
	[EditVersionId] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[Id] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Closed] [nvarchar](max) NULL,
	[Conditions] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Lat] [decimal](18, 9) NOT NULL,
	[Lon] [decimal](18, 9) NOT NULL,
	[EstaEmergencyMarker] [nvarchar](255) NULL,
 CONSTRAINT [PK_ChangeToLaunch] PRIMARY KEY CLUSTERED 
(
	[ChangeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangeToMapShape]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeToMapShape](
	[ChangeId] [int] IDENTITY(1,1) NOT NULL,
	[EditVersionId] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[Id] [int] NOT NULL,
	[AreaId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Coordinates] [nvarchar](max) NULL,
	[EstaEmergencyMarker] [nvarchar](255) NULL,
 CONSTRAINT [PK_ChangeToMapShape] PRIMARY KEY CLUSTERED 
(
	[ChangeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangeToSeeAlso]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeToSeeAlso](
	[ChangeId] [int] IDENTITY(1,1) NOT NULL,
	[EditVersionId] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[Id] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Link] [nvarchar](4000) NULL,
	[IsEntryStubForThis] [bit] NOT NULL,
 CONSTRAINT [PK_ChangeToSeeAlso] PRIMARY KEY CLUSTERED 
(
	[ChangeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangeToSite]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeToSite](
	[ChangeId] [int] IDENTITY(1,1) NOT NULL,
	[EditVersionId] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[Id] [int] NOT NULL,
	[AreaId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Closed] [nvarchar](max) NULL,
	[Rating] [nvarchar](max) NULL,
	[Height] [nvarchar](255) NULL,
	[Type] [nvarchar](255) NULL,
	[Restrictions] [nvarchar](max) NULL,
	[ShortLocation] [nvarchar](255) NULL,
	[ExtendedLocation] [nvarchar](max) NULL,
	[Conditions] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Takeoff] [nvarchar](max) NULL,
	[Landing] [nvarchar](max) NULL,
	[Landowners] [nvarchar](max) NULL,
	[FlightComments] [nvarchar](max) NULL,
	[HazardsComments] [nvarchar](max) NULL,
	[InsetMapPositionId] [tinyint] NOT NULL,
	[SmallPicture] [nvarchar](255) NULL,
	[SmallPictureHref] [nvarchar](4000) NULL,
	[SmallPictureTitle] [nvarchar](255) NULL,
 CONSTRAINT [PK_ChangeToSite] PRIMARY KEY CLUSTERED 
(
	[ChangeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangeToSiteContact]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeToSiteContact](
	[ChangeId] [int] IDENTITY(1,1) NOT NULL,
	[EditVersionId] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[ContactId] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
	[IsContact] [bit] NOT NULL,
	[IsResponsible] [bit] NOT NULL,
 CONSTRAINT [PK_ChangeToSiteContact] PRIMARY KEY CLUSTERED 
(
	[ChangeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangeToSiteMapshape]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeToSiteMapshape](
	[ChangeId] [int] IDENTITY(1,1) NOT NULL,
	[EditVersionId] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[MapshapeId] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
 CONSTRAINT [PK_ChangeToSiteMapshape] PRIMARY KEY CLUSTERED 
(
	[ChangeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangeToVideo]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeToVideo](
	[ChangeId] [int] IDENTITY(1,1) NOT NULL,
	[EditVersionId] [int] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[Id] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[Identifier] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_ChangeToVideo] PRIMARY KEY CLUSTERED 
(
	[ChangeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[FullName] [nvarchar](255) NULL,
	[Link] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EditVersion]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EditVersion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EditId] [int] NOT NULL,
	[EditVersionNumber] [smallint] NOT NULL,
	[InitialisedTime] [datetime2](0) NOT NULL,
	[SubmittedTime] [datetime2](0) NULL,
	[AuthorUserName] [nvarchar](256) NOT NULL,
	[AuthorRole] [nvarchar](256) NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[StatusId] [char](1) NOT NULL,
 CONSTRAINT [PK_EditSite] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_EditVersion] UNIQUE NONCLUSTERED 
(
	[EditId] ASC,
	[EditVersionNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EditVersionStatus]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EditVersionStatus](
	[Id] [char](1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_EditVersionStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryOfContact]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryOfContact](
	[HistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ValidFromVersion] [int] NOT NULL,
	[ValidToVersion] [int] NULL,
	[Id] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[FullName] [nvarchar](255) NULL,
	[Link] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
 CONSTRAINT [PK_HistoryOfContact] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryOfLaunch]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryOfLaunch](
	[HistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ValidFromVersion] [int] NOT NULL,
	[ValidToVersion] [int] NULL,
	[Id] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Closed] [nvarchar](max) NULL,
	[Conditions] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Lat] [decimal](18, 9) NOT NULL,
	[Lon] [decimal](18, 9) NOT NULL,
	[EstaEmergencyMarker] [nvarchar](255) NULL,
 CONSTRAINT [PK_HistoryOfLaunch] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryOfMapShape]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryOfMapShape](
	[HistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ValidFromVersion] [int] NOT NULL,
	[ValidToVersion] [int] NULL,
	[Id] [int] NOT NULL,
	[AreaId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Coordinates] [nvarchar](max) NULL,
	[EstaEmergencyMarker] [nvarchar](255) NULL,
 CONSTRAINT [PK_HistoryOfMapShape] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryOfSeeAlso]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryOfSeeAlso](
	[HistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ValidFromVersion] [int] NOT NULL,
	[ValidToVersion] [int] NULL,
	[Id] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Link] [nvarchar](4000) NULL,
	[IsEntryStubForThis] [bit] NOT NULL,
 CONSTRAINT [PK_HistoryOfSeeAlso] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryOfSite]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryOfSite](
	[HistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ValidFromVersion] [int] NOT NULL,
	[ValidToVersion] [int] NULL,
	[Id] [int] NOT NULL,
	[AreaId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Closed] [nvarchar](max) NULL,
	[Rating] [nvarchar](max) NULL,
	[Height] [nvarchar](255) NULL,
	[Type] [nvarchar](255) NULL,
	[Restrictions] [nvarchar](max) NULL,
	[ShortLocation] [nvarchar](255) NULL,
	[ExtendedLocation] [nvarchar](max) NULL,
	[Conditions] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Takeoff] [nvarchar](max) NULL,
	[Landing] [nvarchar](max) NULL,
	[Landowners] [nvarchar](max) NULL,
	[FlightComments] [nvarchar](max) NULL,
	[HazardsComments] [nvarchar](max) NULL,
	[InsetMapPositionId] [tinyint] NOT NULL,
	[SmallPicture] [nvarchar](255) NULL,
	[SmallPictureHref] [nvarchar](4000) NULL,
	[SmallPictureTitle] [nvarchar](255) NULL,
 CONSTRAINT [PK_HistoryOfSite] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryOfSiteContact]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryOfSiteContact](
	[HistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ValidFromVersion] [int] NOT NULL,
	[ValidToVersion] [int] NULL,
	[ContactId] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
	[IsContact] [bit] NOT NULL,
	[IsResponsible] [bit] NOT NULL,
 CONSTRAINT [PK_HistoryOfSiteContact] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryOfSiteMapshape]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryOfSiteMapshape](
	[HistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ValidFromVersion] [int] NOT NULL,
	[ValidToVersion] [int] NULL,
	[MapshapeId] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
 CONSTRAINT [PK_HistoryOfSiteMapshape] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryOfVideo]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryOfVideo](
	[HistoryId] [int] IDENTITY(1,1) NOT NULL,
	[ValidFromVersion] [int] NOT NULL,
	[ValidToVersion] [int] NULL,
	[Id] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[Identifier] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_HistoryOfVideo] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InsetMapPosition]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InsetMapPosition](
	[Id] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_InsetMapPositionEnum] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Launch]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Launch](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SiteId] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Closed] [nvarchar](max) NULL,
	[Conditions] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Lat] [decimal](18, 9) NOT NULL,
	[Lon] [decimal](18, 9) NOT NULL,
	[EstaEmergencyMarker] [nvarchar](255) NULL,
 CONSTRAINT [PK_Launch] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MapShape]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapShape](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Coordinates] [nvarchar](max) NULL,
	[EstaEmergencyMarker] [nvarchar](255) NULL,
 CONSTRAINT [PK_MapShape] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MapShapeCategory]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapShapeCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_MapShapeCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SeeAlso]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SeeAlso](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SiteId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Link] [nvarchar](4000) NULL,
	[IsEntryStubForThis] [bit] NOT NULL,
 CONSTRAINT [PK_SeeAlso] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Site]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Site](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AreaId] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Closed] [nvarchar](max) NULL,
	[Rating] [nvarchar](max) NULL,
	[Height] [nvarchar](255) NULL,
	[Type] [nvarchar](255) NULL,
	[Restrictions] [nvarchar](max) NULL,
	[ShortLocation] [nvarchar](255) NULL,
	[ExtendedLocation] [nvarchar](max) NULL,
	[Conditions] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Takeoff] [nvarchar](max) NULL,
	[Landing] [nvarchar](max) NULL,
	[Landowners] [nvarchar](max) NULL,
	[FlightComments] [nvarchar](max) NULL,
	[HazardsComments] [nvarchar](max) NULL,
	[InsetMapPositionId] [tinyint] NOT NULL,
	[SmallPicture] [nvarchar](255) NULL,
	[SmallPictureHref] [nvarchar](4000) NULL,
	[SmallPictureTitle] [nvarchar](255) NULL,
 CONSTRAINT [PK_Site] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SiteContact]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SiteContact](
	[ContactId] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
	[IsContact] [bit] NOT NULL,
	[IsResponsible] [bit] NOT NULL,
 CONSTRAINT [PK_SiteContact] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC,
	[SiteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SiteMapshape]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SiteMapshape](
	[MapshapeId] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
 CONSTRAINT [PK_SiteMapshape] PRIMARY KEY CLUSTERED 
(
	[MapshapeId] ASC,
	[SiteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Version]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Version](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EditId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Video]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Video](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SiteId] [int] NOT NULL,
	[TypeId] [int] NOT NULL,
	[Identifier] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Video] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VideoType]    Script Date: 2019-06-23 10:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_VideoType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 2019-06-23 10:16:54 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 2019-06-23 10:16:54 ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 2019-06-23 10:16:54 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 2019-06-23 10:16:54 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 2019-06-23 10:16:54 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 2019-06-23 10:16:54 ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 2019-06-23 10:16:54 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SeeAlso] ADD  CONSTRAINT [DF_SeeAlso_IsEntryStubForThis]  DEFAULT ((0)) FOR [IsEntryStubForThis]
GO
ALTER TABLE [dbo].[Area]  WITH CHECK ADD  CONSTRAINT [FK_Area_Area] FOREIGN KEY([ParentAreaId])
REFERENCES [dbo].[Area] ([Id])
GO
ALTER TABLE [dbo].[Area] CHECK CONSTRAINT [FK_Area_Area]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[ChangeToContact]  WITH CHECK ADD  CONSTRAINT [FK_ChangeToContact_EditVersion] FOREIGN KEY([EditVersionId])
REFERENCES [dbo].[EditVersion] ([Id])
GO
ALTER TABLE [dbo].[ChangeToContact] CHECK CONSTRAINT [FK_ChangeToContact_EditVersion]
GO
ALTER TABLE [dbo].[ChangeToLaunch]  WITH CHECK ADD  CONSTRAINT [FK_ChangeToLaunch_EditVersion] FOREIGN KEY([EditVersionId])
REFERENCES [dbo].[EditVersion] ([Id])
GO
ALTER TABLE [dbo].[ChangeToLaunch] CHECK CONSTRAINT [FK_ChangeToLaunch_EditVersion]
GO
ALTER TABLE [dbo].[ChangeToMapShape]  WITH CHECK ADD  CONSTRAINT [FK_ChangeToMapShape_EditVersion] FOREIGN KEY([EditVersionId])
REFERENCES [dbo].[EditVersion] ([Id])
GO
ALTER TABLE [dbo].[ChangeToMapShape] CHECK CONSTRAINT [FK_ChangeToMapShape_EditVersion]
GO
ALTER TABLE [dbo].[ChangeToSeeAlso]  WITH CHECK ADD  CONSTRAINT [FK_ChangeToSeeAlso_EditVersion] FOREIGN KEY([EditVersionId])
REFERENCES [dbo].[EditVersion] ([Id])
GO
ALTER TABLE [dbo].[ChangeToSeeAlso] CHECK CONSTRAINT [FK_ChangeToSeeAlso_EditVersion]
GO
ALTER TABLE [dbo].[ChangeToSite]  WITH CHECK ADD  CONSTRAINT [FK_ChangeToSite_EditVersion] FOREIGN KEY([EditVersionId])
REFERENCES [dbo].[EditVersion] ([Id])
GO
ALTER TABLE [dbo].[ChangeToSite] CHECK CONSTRAINT [FK_ChangeToSite_EditVersion]
GO
ALTER TABLE [dbo].[ChangeToSiteContact]  WITH CHECK ADD  CONSTRAINT [FK_ChangeToSiteContact_EditVersion] FOREIGN KEY([EditVersionId])
REFERENCES [dbo].[EditVersion] ([Id])
GO
ALTER TABLE [dbo].[ChangeToSiteContact] CHECK CONSTRAINT [FK_ChangeToSiteContact_EditVersion]
GO
ALTER TABLE [dbo].[ChangeToSiteMapshape]  WITH CHECK ADD  CONSTRAINT [FK_ChangeToSiteMapshape_EditVersion] FOREIGN KEY([EditVersionId])
REFERENCES [dbo].[EditVersion] ([Id])
GO
ALTER TABLE [dbo].[ChangeToSiteMapshape] CHECK CONSTRAINT [FK_ChangeToSiteMapshape_EditVersion]
GO
ALTER TABLE [dbo].[ChangeToVideo]  WITH CHECK ADD  CONSTRAINT [FK_ChangeToVideo_EditVersion] FOREIGN KEY([EditVersionId])
REFERENCES [dbo].[EditVersion] ([Id])
GO
ALTER TABLE [dbo].[ChangeToVideo] CHECK CONSTRAINT [FK_ChangeToVideo_EditVersion]
GO
ALTER TABLE [dbo].[EditVersion]  WITH CHECK ADD  CONSTRAINT [FK_EditVersion_EditVersionStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[EditVersionStatus] ([Id])
GO
ALTER TABLE [dbo].[EditVersion] CHECK CONSTRAINT [FK_EditVersion_EditVersionStatus]
GO
ALTER TABLE [dbo].[Launch]  WITH CHECK ADD  CONSTRAINT [FK_Launch_Site] FOREIGN KEY([SiteId])
REFERENCES [dbo].[Site] ([Id])
GO
ALTER TABLE [dbo].[Launch] CHECK CONSTRAINT [FK_Launch_Site]
GO
ALTER TABLE [dbo].[MapShape]  WITH CHECK ADD  CONSTRAINT [FK_MapShape_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
GO
ALTER TABLE [dbo].[MapShape] CHECK CONSTRAINT [FK_MapShape_Area]
GO
ALTER TABLE [dbo].[MapShape]  WITH CHECK ADD  CONSTRAINT [FK_MapShape_MapShapeCategory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[MapShapeCategory] ([Id])
GO
ALTER TABLE [dbo].[MapShape] CHECK CONSTRAINT [FK_MapShape_MapShapeCategory]
GO
ALTER TABLE [dbo].[SeeAlso]  WITH CHECK ADD  CONSTRAINT [FK_SeeAlso_Site] FOREIGN KEY([SiteId])
REFERENCES [dbo].[Site] ([Id])
GO
ALTER TABLE [dbo].[SeeAlso] CHECK CONSTRAINT [FK_SeeAlso_Site]
GO
ALTER TABLE [dbo].[Site]  WITH CHECK ADD  CONSTRAINT [FK_Site_Area] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Area] ([Id])
GO
ALTER TABLE [dbo].[Site] CHECK CONSTRAINT [FK_Site_Area]
GO
ALTER TABLE [dbo].[Site]  WITH CHECK ADD  CONSTRAINT [FK_Site_InsetMapPositionEnum] FOREIGN KEY([InsetMapPositionId])
REFERENCES [dbo].[InsetMapPosition] ([Id])
GO
ALTER TABLE [dbo].[Site] CHECK CONSTRAINT [FK_Site_InsetMapPositionEnum]
GO
ALTER TABLE [dbo].[SiteContact]  WITH CHECK ADD  CONSTRAINT [FK_SiteContact_Contact] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contact] ([Id])
GO
ALTER TABLE [dbo].[SiteContact] CHECK CONSTRAINT [FK_SiteContact_Contact]
GO
ALTER TABLE [dbo].[SiteContact]  WITH CHECK ADD  CONSTRAINT [FK_SiteContact_Site] FOREIGN KEY([SiteId])
REFERENCES [dbo].[Site] ([Id])
GO
ALTER TABLE [dbo].[SiteContact] CHECK CONSTRAINT [FK_SiteContact_Site]
GO
ALTER TABLE [dbo].[SiteMapshape]  WITH CHECK ADD  CONSTRAINT [FK_SiteMapshape_MapShape] FOREIGN KEY([MapshapeId])
REFERENCES [dbo].[MapShape] ([Id])
GO
ALTER TABLE [dbo].[SiteMapshape] CHECK CONSTRAINT [FK_SiteMapshape_MapShape]
GO
ALTER TABLE [dbo].[SiteMapshape]  WITH CHECK ADD  CONSTRAINT [FK_SiteMapshape_Site] FOREIGN KEY([SiteId])
REFERENCES [dbo].[Site] ([Id])
GO
ALTER TABLE [dbo].[SiteMapshape] CHECK CONSTRAINT [FK_SiteMapshape_Site]
GO
ALTER TABLE [dbo].[Video]  WITH CHECK ADD  CONSTRAINT [FK_Video_Site] FOREIGN KEY([SiteId])
REFERENCES [dbo].[Site] ([Id])
GO
ALTER TABLE [dbo].[Video] CHECK CONSTRAINT [FK_Video_Site]
GO
ALTER TABLE [dbo].[Video]  WITH CHECK ADD  CONSTRAINT [FK_Video_VideoType] FOREIGN KEY([TypeId])
REFERENCES [dbo].[VideoType] ([Id])
GO
ALTER TABLE [dbo].[Video] CHECK CONSTRAINT [FK_Video_VideoType]
GO
USE [master]
GO
ALTER DATABASE [SiteGuide] SET  READ_WRITE 
GO
